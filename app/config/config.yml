imports:
    - { resource: parameters.yml }
    - { resource: security.yml }
    - { resource: elastica.yml }
    - { resource: push_notifications.yml }
    - { resource: eo.yml }

framework:
    #esi:             ~
    translator:      { fallback: en }
    secret:          "%secret%"
    router:
        resource: "%kernel.root_dir%/config/routing.yml"
        strict_requirements: ~
    form:            ~
    csrf_protection: ~
    validation:      { enabled: true, enable_annotations: false }
    templating:
        engines: ['twig']
    default_locale:  "%locale%"
    trusted_hosts:   ~
    trusted_proxies: ~
    session:
        handler_id:  ~
    fragments:       ~
    http_method_override: true

# Twig Configuration
twig:
    debug:            "%kernel.debug%"
    strict_variables: "%kernel.debug%"

# Assetic Configuration
assetic:
    debug:          "%kernel.debug%"
    use_controller: false
    bundles:        [ ]
    filters:
        cssrewrite: ~

# Doctrine Configuration
doctrine:
    dbal:
        driver:   "%database_driver%"
        host:     "%database_host%"
        port:     "%database_port%"
        dbname:   "%database_name%"
        user:     "%database_user%"
        password: "%database_password%"
        charset:  UTF8
        types:
            date_interval: EO\CoreBundle\Doctrine\DBAL\Type\DateIntervalType
        mapping_types:
            date_interval: date_interval
    orm:
        auto_generate_proxy_classes: "%kernel.debug%"
        auto_mapping: false
        mappings:
            FOSUserBundle: ~
            EOUserBundle:
                type: yml
                prefix: EO\UserBundle\Model
                dir: "Resources/config/doctrine"
            EOCompanyBundle:
                type: yml
                prefix: EO\CompanyBundle\Model
                dir: "Resources/config/doctrine"
            EONetworkBundle:
                type: yml
                prefix: EO\NetworkBundle\Model
                dir: "Resources/config/doctrine"
            EOTicketBundle:
                type: yml
                prefix: EO\TicketBundle\Model
                dir: "Resources/config/doctrine"
            EOTerminalBundle:
                type: yml
                prefix: EO\TerminalBundle\Model
                dir: "Resources/config/doctrine"
            EOInspectorBundle:
                type: yml
                prefix: EO\InspectorBundle\Model
                dir: "Resources/config/doctrine"
            EOFineBundle:
                type: yml
                prefix: EO\FineBundle\Model
                dir: "Resources/config/doctrine"
            EOComplaintBundle:
                type: yml
                prefix: EO\ComplaintBundle\Model
                dir: "Resources/config/doctrine"
            EOPaymentBundle:
                type: yml
                prefix: EO\PaymentBundle\Model
                dir: "Resources/config/doctrine"
            EOMiscBundle:
                type: yml
                prefix: EO\MiscBundle\Model
                dir: "Resources/config/doctrine"

# Swiftmailer Configuration
swiftmailer:
    transport: "%mailer_transport%"
    encryption: "%mailer_encryption%"
    auth_mode: "%mailer_auth_mode%"
    host: "%mailer_host%"
    username: "%mailer_user%"
    password: "%mailer_password%"
    spool: { type: memory }

# JmsSerializer Configuration
jms_serializer:
    metadata:
        auto_detection: true
        directories:
            FOSUser:
                namespace_prefix: "FOS\\UserBundle"
                path: "%kernel.root_dir%/serializer/FOSUserBundle"
            EOUser:
                namespace_prefix: "EO\\UserBundle"
                path: "@EOUserBundle/Resources/config/serializer"
            EOCompany:
                namespace_prefix: "EO\\CompanyBundle"
                path: "@EOCompanyBundle/Resources/config/serializer"
            EONetwork:
                namespace_prefix: "EO\\NetworkBundle"
                path: "@EONetworkBundle/Resources/config/serializer"
            EOTicket:
                namespace_prefix: "EO\\TicketBundle"
                path: "@EOTicketBundle/Resources/config/serializer"
            EOTerminal:
                namespace_prefix: "EO\\TerminalBundle"
                path: "@EOTerminalBundle/Resources/config/serializer"
            EOInspector:
                namespace_prefix: "EO\\InspectorBundle"
                path: "@EOInspectorBundle/Resources/config/serializer"
            EOFine:
                namespace_prefix: "EO\\FineBundle"
                path: "@EOFineBundle/Resources/config/serializer"
            EOComplaint:
                namespace_prefix: "EO\\ComplaintBundle"
                path: "@EOComplaintBundle/Resources/config/serializer"
            EOMisc:
                namespace_prefix: "EO\\MiscBundle"
                path: "@EOMiscBundle/Resources/config/serializer"

# FosRestBundle Configuration
fos_rest:
    param_fetcher_listener: true
    body_listener: true
    format_listener: true
    service:
        serializer: serializer
    view:
        serialize_null: true
        view_response_listener: force
        formats:
            json: true
            xml: true
            html: false
        templating_formats:
            html: false
        force_redirects:
            html: false
        failed_validation: HTTP_BAD_REQUEST
        default_engine: twig
    routing_loader:
        default_format: json

# FosUserBundle Configuration
fos_user:
    db_driver: "%database_type%"
    firewall_name: eos_secured
    user_class: EO\UserBundle\Model\User
    group:
        group_class: EO\UserBundle\Model\Group

# KnpPaginatorBundle Configuration
knp_paginator:
    default_options:
        page_name: page
        sort_field_name: sort
        sort_direction_name: direction

# NelmioCorsBundle Configuration
nelmio_cors:
    paths:
        '^/':
            allow_credentials: true
            allow_origin: ['*']
            allow_headers: ['Content-Type', 'Authorization', 'X-EO-PLATFORM', 'X-EO-TOKEN']
            allow_methods: ['POST', 'PUT', 'PATCH', 'GET', 'DELETE', 'OPTIONS']
            max_age: 3600

# NelmioApiDocBundle Configuration
nelmio_api_doc:
    name: EO API Documentation
    sandbox:
        enabled: false

# VichUploaderBundle Configuration
vich_uploader:
    db_driver: "%database_type%"
    mappings:
        logo:
            uri_prefix: /logos
            upload_destination: %kernel.root_dir%/../web/images/logos
            delete_on_remove: true
            delete_on_update: true