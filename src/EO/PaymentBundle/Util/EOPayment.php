<?php

namespace EO\PaymentBundle\Util;

class EOPayment
{
    const PRODUCT_NAME_TICKET = "EO-Ticket";
    const PRODUCT_TYPE_TICKET = "ticket";
}