<?php

namespace EO\PaymentBundle\Handler;

use EO\PaymentBundle\Handler\Endpoints\CustomerHandlerInterface;
use EO\PaymentBundle\Handler\Endpoints\OrderHandlerInterface;
use EO\PaymentBundle\Handler\Endpoints\ProductHandlerInterface;
use EO\PaymentBundle\Handler\Endpoints\SkuHandlerInterface;

interface PaymentHandlerInterface
{
    /**
     * @return ProductHandlerInterface
     */
    public function product();

    /**
     * @return SkuHandlerInterface
     */
    public function sku();

    /**
     * @return CustomerHandlerInterface
     */
    public function customer();

    /**
     * @return OrderHandlerInterface
     */
    public function order();
}