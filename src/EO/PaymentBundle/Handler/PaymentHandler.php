<?php

namespace EO\PaymentBundle\Handler;

use EO\PaymentBundle\Handler\Endpoints\CustomerHandler;
use EO\PaymentBundle\Handler\Endpoints\CustomerHandlerInterface;
use EO\PaymentBundle\Handler\Endpoints\OrderHandler;
use EO\PaymentBundle\Handler\Endpoints\OrderHandlerInterface;
use EO\PaymentBundle\Handler\Endpoints\ProductHandler;
use EO\PaymentBundle\Handler\Endpoints\ProductHandlerInterface;
use EO\PaymentBundle\Handler\Endpoints\SkuHandler;
use EO\PaymentBundle\Handler\Endpoints\SkuHandlerInterface;
use Stripe\Stripe;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PaymentHandler implements PaymentHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var ProductHandlerInterface
     */
    protected $product;

    /**
     * @var SkuHandlerInterface
     */
    protected $sku;

    /**
     * @var CustomerHandlerInterface
     */
    protected $customer;

    /**
     * @var OrderHandlerInterface
     */
    protected $order;

    public function __construct(ContainerInterface $container, $apiKey)
    {
        Stripe::setApiKey($apiKey);
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function product()
    {
        if ($this->product === null) {
            $this->product = new ProductHandler($this->container->get('eo_payment.manager.product'));
        }
        return $this->product;
    }

    /**
     * {@inheritdoc}
     */
    public function sku()
    {
        if ($this->sku === null) {
            $this->sku = new SkuHandler($this->container->get('eo_payment.manager.sku'), $this->product());
        }
        return $this->sku;
    }

    /**
     * {@inheritdoc}
     */
    public function customer()
    {
        if ($this->customer === null) {
            $this->customer = new CustomerHandler($this->container->get('eo_payment.manager.customer'));
        }
        return $this->customer;
    }

    /**
     * {@inheritdoc}
     */
    public function order()
    {
        if ($this->order === null) {
            $this->order = new OrderHandler($this->container->get('eo_payment.manager.order'), $this->sku(), $this->customer());
        }
        return $this->order;
    }
}