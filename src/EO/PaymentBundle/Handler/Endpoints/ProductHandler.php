<?php

namespace EO\PaymentBundle\Handler\Endpoints;

use EO\PaymentBundle\Manager\DBAL\ProductManagerInterface;
use EO\PaymentBundle\Util\EOPayment;
use Stripe\Product;

class ProductHandler implements ProductHandlerInterface
{
    /**
     * @var ProductManagerInterface
     */
    private $productManager;

    public function __construct(ProductManagerInterface $productManager)
    {
        $this->productManager = $productManager;
    }

    /**
     * {@inheritdoc}
     */
    public function createTicket()
    {
        $stripeProduct = $this->checkCurrentProducts();
        if (null === $stripeProduct) {
            try {
                $stripeProduct = Product::create(array(
                    "name" => EOPayment::PRODUCT_NAME_TICKET,
                    "attributes" => array(
                        "network",
                        "departure",
                        "arrival",
                        "validity",
                        "pricing"
                    ),
                    "active" => true,
                    "shippable" => false
                ));
            } catch (\Exception $e) {
                return null;
            }
        }
        /** @var \EO\PaymentBundle\Model\Product $product */
        $product = $this->productManager->create();
        $product->setType(EOPayment::PRODUCT_TYPE_TICKET);
        $product->setStripeId($stripeProduct->id);
        $this->productManager->update($product);
        return $stripeProduct;
    }

    /**
     * @return null|Product
     */
    private function checkCurrentProducts()
    {
        try {
            $products = Product::all();
            foreach ($products->jsonSerialize()["data"] as $product) {
                $product = Product::constructFrom($product, array());
                if ($product->name === EOPayment::PRODUCT_NAME_TICKET) {
                    return $product;
                }
            }
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTicket()
    {
        if (null !== ($product = $this->productManager->findOneByType(EOPayment::PRODUCT_TYPE_TICKET))) {
            try {
                return Product::retrieve($product->getStripeId());
            } catch (\Exception $e) {
                return null;
            }
        }
        return null;
    }
}