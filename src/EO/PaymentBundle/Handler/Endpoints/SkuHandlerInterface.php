<?php

namespace EO\PaymentBundle\Handler\Endpoints;

use EO\TicketBundle\Model\TicketTypeInterface;

interface SkuHandlerInterface
{
    /**
     * @param TicketTypeInterface $ticketType
     *
     * @return null|\Stripe\SKU
     */
    public function create(TicketTypeInterface $ticketType);

    /**
     * @param TicketTypeInterface $ticketType
     *
     * @return null|\Stripe\SKU
     */
    public function get(TicketTypeInterface $ticketType);
}