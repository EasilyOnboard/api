<?php

namespace EO\PaymentBundle\Handler\Endpoints;

use Stripe\Product;

interface ProductHandlerInterface
{
    /**
     * @return Product|null
     */
    public function createTicket();

    /**
     * @return Product|null
     */
    public function getTicket();
}