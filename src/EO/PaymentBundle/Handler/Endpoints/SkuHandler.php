<?php

namespace EO\PaymentBundle\Handler\Endpoints;

use EO\TicketBundle\Model\TicketTypeInterface;
use EO\PaymentBundle\Manager\DBAL\SkuManagerInterface;
use Stripe\SKU;

class SkuHandler implements SkuHandlerInterface
{
    /**
     * @var SkuManagerInterface
     */
    private $skuManager;

    /**
     * @var ProductHandlerInterface
     */
    private $productHandler;

    public function __construct(SkuManagerInterface $skuManager, ProductHandlerInterface $productHandler)
    {
        $this->skuManager = $skuManager;
        $this->productHandler = $productHandler;
    }

    /**
     * {@inheritdoc}
     */
    public function create(TicketTypeInterface $ticketType)
    {
        if (($stripeProduct = $this->productHandler->getTicket()) === null) {
            return null;
        }
        $parameters = array(
            "product" => $stripeProduct->id,
            "attributes" => array(
                "network" => $ticketType->getNetwork()->getName(),
                "validity" => $ticketType->getValidity()->getName(),
                "pricing" => ($ticketType->isReducedPrice()) ? "reduced" : "normal"
            ),
            "price" => $ticketType->getPrice() * 100, // TODO: check if yen
            "currency" => $ticketType->getCurrency(),
            "inventory" => array(
                "type" => "infinite"
            ),
            "active" => true
        );
        if (null !== $ticketType->getStationA()) {
            $parameters["attributes"]["departure"] = $ticketType->getStationA()->getName();
        } elseif (null !== $ticketType->getZoneA()) {
            $parameters["attributes"]["departure"] = $ticketType->getZoneA()->getName();
        } else {
            $parameters["attributes"]["departure"] = "none";
        }
        if (null !== $ticketType->getStationB()) {
            $parameters["attributes"]["arrival"] = $ticketType->getStationB()->getName();
        } else if (null !== $ticketType->getZoneB()) {
            $parameters["attributes"]["arrival"] = $ticketType->getZoneB()->getName();
        } else {
            $parameters["attributes"]["arrival"] = "none";
        }
        try {
            $stripeSku = SKU::create($parameters);
        } catch (\Exception $e) {
            return null;
        }
        /** @var \EO\PaymentBundle\Model\Sku $sku */
        $sku = $this->skuManager->create();
        $sku->setStripeId($stripeSku->id);
        $sku->setTicketType($ticketType);
        $this->skuManager->update($sku);
        return $stripeSku;
    }

    /**
     * {@inheritdoc}
     */
    public function get(TicketTypeInterface $ticketType)
    {
        if (($sku = $this->skuManager->findOneByTicketType($ticketType)) === null) {
            return null;
        }
        try {
            return SKU::retrieve($sku->getStripeId());
        } catch (\Exception $e) {
            return null;
        }
    }
}