<?php

namespace EO\PaymentBundle\Handler\Endpoints;

use EO\PaymentBundle\Model\Card;
use EO\PaymentBundle\Model\Order;

interface OrderHandlerInterface
{
    /**
     * @param Order $order
     *
     * @return null|\Stripe\Order
     */
    public function create(Order $order);

    /**
     * @param Order $order
     *
     * @return null|\Stripe\Order
     */
    public function get(Order $order);

    /**
     * @param Order $order
     *
     * @return boolean
     */
    public function pay(Order $order);

    /**
     * @param Order $order
     * @param Card $card
     *
     * @return boolean
     */
    public function payWithOtherCard(Order $order, Card $card);
}