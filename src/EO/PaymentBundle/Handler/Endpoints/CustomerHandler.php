<?php

namespace EO\PaymentBundle\Handler\Endpoints;

use EO\PaymentBundle\Manager\DBAL\CustomerManagerInterface;
use EO\PaymentBundle\Model\Card;
use EO\UserBundle\Model\UserInterface;
use Stripe\Customer;

class CustomerHandler implements CustomerHandlerInterface
{
    /**
     * @var CustomerManagerInterface
     */
    private $customerManager;

    /**
     * @param CustomerManagerInterface $customerManager
     */
    public function __construct(CustomerManagerInterface $customerManager)
    {
        $this->customerManager = $customerManager;
    }

    /**
     * {@inheritdoc}
     */
    public function create(UserInterface $user)
    {
        try {
            $stripeCustomer = Customer::create(array(
                "email" => $user->getEmail()
            ));
        } catch (\Exception $e) {
            return null;
        }
        /** @var \EO\PaymentBundle\Model\Customer $customer */
        $customer = $this->customerManager->create();
        $customer->setStripeId($stripeCustomer->id);
        $customer->setCustomer($user);
        $this->customerManager->update($customer);
        return $stripeCustomer;
    }

    /**
     * {@inheritdoc}
     */
    public function get(UserInterface $user)
    {
        if (null !== ($customer = $this->customerManager->findOneByCustomer($user))) {
            try {
                $customerStripe = Customer::retrieve($customer->getStripeId());
                if ($customerStripe->deleted !== true) {
                    return $customerStripe;
                }
            } catch (\Exception $e) {
            }
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(UserInterface $user)
    {
        if (null !== ($customer = $this->get($user))) {
            try {
                $customer->delete();
                return true;
            } catch (\Exception $e) {
                return false;
            }
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function addCard(UserInterface $user, Card $card)
    {
        if (null === ($customer = $this->get($user))) {
            return null;
        }
        try {
            /** @var \Stripe\Card $stripeCard */
            $stripeCard = $customer->sources->create(array(
                "source" => array(
                    "object" => "card",
                    "name" => $card->getName(),
                    "number" => $card->getNumber(),
                    "cvc" => $card->getCvc(),
                    "exp_month" => $card->getExpireMonth(),
                    "exp_year" => $card->getExpireYear(),
                    "address_line1" => $card->getFullAddress()->getAddress(),
                    "address_line2" => null,
                    "address_city" => $card->getFullAddress()->getCity(),
                    "address_zip" => $card->getFullAddress()->getPostcode(),
                    "address_state" => $card->getFullAddress()->getState(),
                    "address_country" => $card->getFullAddress()->getCountry()
                )
            ));
            return $stripeCard;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCards(UserInterface $user)
    {
        if (null === ($customer = $this->get($user))) {
            return null;
        }
        return $customer->sources->all();
    }

    /**
     * {@inheritdoc}
     */
    public function updateCard(UserInterface $user, Card $card, $id)
    {
        if (null === ($customer = $this->get($user))) {
            return null;
        }
        try {
            /** @var \Stripe\Card $cardStripe */
            $cardStripe = $customer->sources->retrieve($id);
            $cardStripe->name = $card->getName();
            $cardStripe->exp_month = $card->getExpireMonth();
            $cardStripe->exp_year = $card->getExpireYear();
            $cardStripe->address_line1 = $card->getFullAddress()->getAddress();
            $cardStripe->address_line2 = null;
            $cardStripe->address_city = $card->getFullAddress()->getCity();
            $cardStripe->address_zip = $card->getFullAddress()->getPostcode();
            $cardStripe->address_state = $card->getFullAddress()->getState();
            $cardStripe->address_country = $card->getFullAddress()->getCountry();
            $cardStripe->save();
            return $cardStripe;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function removeCard(UserInterface $user, $id)
    {
        if (null === ($customer = $this->get($user))) {
            return false;
        }
        try {
            /** @var \Stripe\StripeObject $result */
            $result = $customer->sources->retrieve($id)->delete();
            return $result->deleted;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultCard(UserInterface $user, $id)
    {
        if (null === ($customer = $this->get($user))) {
            return false;
        }
        try {
            /** @var \Stripe\Card $card */
            $card = $customer->sources->retrieve($id);
            $customer->default_source = $card->id;
            $customer->save();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
}