<?php

namespace EO\PaymentBundle\Handler\Endpoints;

use EO\PaymentBundle\Manager\DBAL\OrderManagerInterface;
use EO\PaymentBundle\Model\Card;
use EO\PaymentBundle\Model\Order;

class OrderHandler implements OrderHandlerInterface
{
    /**
     * @var OrderManagerInterface
     */
    private $orderManager;

    /**
     * @var SkuHandlerInterface
     */
    private $skuHandler;
    /**
     * @var CustomerHandlerInterface
     */
    private $customerHandler;

    /**
     * @param OrderManagerInterface $orderManager
     * @param SkuHandlerInterface $skuHandler
     * @param CustomerHandlerInterface $customerHandler
     */
    public function __construct(
        OrderManagerInterface $orderManager,
        SkuHandlerInterface $skuHandler,
        CustomerHandlerInterface $customerHandler)
    {
        $this->orderManager = $orderManager;
        $this->skuHandler = $skuHandler;
        $this->customerHandler = $customerHandler;
    }

    public function create(Order $order)
    {
        $sku = $this->skuHandler->get($order->getTicketType());
        $customer = $this->customerHandler->get($order->getCustomer());
        try {
            $stripeOrder = \Stripe\Order::create(array(
                "items" => array(
                    array(
                        "type" => "sku",
                        "quantity" => $order->getQuantity(),
                        "parent" => $sku->id
                    )
                ),
                "currency" => $order->getCurrency(),
                "email" => $order->getCustomer()->getEmail(),
                "customer" => $customer->id
            ));
        } catch (\Exception $e) {
            return null;
        }
        $order->setStripeId($stripeOrder->id);
        $this->orderManager->update($order);
        return $stripeOrder;
    }

    public function get(Order $order)
    {
        try {
            return \Stripe\Order::retrieve($order->getStripeId());
        } catch (\Exception $e) {
            return null;
        }
    }

    public function pay(Order $order, array $parameters = array())
    {
        if (null !== ($stripeOrder = $this->get($order))) {
            try {
                $result = $stripeOrder->pay($parameters);
                if ($result->status === "paid") {
                    $order->setPaid(true);
                    $this->orderManager->update($order);
                    return true;
                }
            } catch (\Exception $e) {}
        }
        return false;
    }

    public function payWithOtherCard(Order $order, Card $card)
    {
        return $this->pay($order, array(
            "source" => array(
                "object" => "card",
                "name" => $card->getName(),
                "number" => $card->getNumber(),
                "cvc" => $card->getCvc(),
                "exp_month" => $card->getExpireMonth(),
                "exp_year" => $card->getExpireYear(),
                "address_line1" => $card->getFullAddress()->getAddress(),
                "address_line2" => null,
                "address_city" => $card->getFullAddress()->getCity(),
                "address_zip" => $card->getFullAddress()->getPostcode(),
                "address_state" => $card->getFullAddress()->getState(),
                "address_country" => $card->getFullAddress()->getCountry()
            )
        ));
    }
}