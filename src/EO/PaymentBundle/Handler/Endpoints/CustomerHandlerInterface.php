<?php

namespace EO\PaymentBundle\Handler\Endpoints;

use EO\UserBundle\Model\UserInterface;
use EO\PaymentBundle\Model\Card;

interface CustomerHandlerInterface
{
    /**
     * @param UserInterface $user
     *
     * @return null|\Stripe\Customer
     */
    public function create(UserInterface $user);

    /**
     * @param UserInterface $user
     *
     * @return null|\Stripe\Customer
     */
    public function get(UserInterface $user);

    /**
     * @param UserInterface $user
     *
     * @return boolean
     */
    public function delete(UserInterface $user);

    /**
     * @param UserInterface $user
     * @param Card $card
     *
     * @return null|\Stripe\Card
     */
    public function addCard(UserInterface $user, Card $card);

    /**
     * @param UserInterface $user
     * @param Card          $card
     * @param string        $id
     *
     * @return null|\Stripe\Card
     */
    public function updateCard(UserInterface $user, Card $card, $id);

    /**
     * @param UserInterface $user
     *
     * @return null|\Stripe\Collection
     */
    public function getCards(UserInterface $user);

    /**
     * @param UserInterface $user
     * @param string        $id
     *
     * @return boolean
     */
    public function removeCard(UserInterface $user, $id);

    /**
     * @param UserInterface $user
     * @param string        $id
     *
     * @return boolean
     */
    public function setDefaultCard(UserInterface $user, $id);
}