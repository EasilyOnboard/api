<?php

namespace EO\PaymentBundle\Model;

class Base
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $stripeId;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $stripeId
     * @return Product
     */
    public function setStripeId($stripeId)
    {
        $this->stripeId = $stripeId;
        return $this;
    }

    /**
     * @param \DateTime $createdAt
     * @return Product
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }
}