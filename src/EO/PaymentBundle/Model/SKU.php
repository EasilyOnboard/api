<?php

namespace EO\PaymentBundle\Model;

use EO\TicketBundle\Model\TicketTypeInterface;

class Sku extends Base
{
    /**
     * @var TicketTypeInterface
     */
    protected $ticketType;

    /**
     * @return TicketTypeInterface
     */
    public function getTicketType()
    {
        return $this->ticketType;
    }

    /**
     * @param TicketTypeInterface $ticketType
     * @return SKU
     */
    public function setTicketType($ticketType)
    {
        $this->ticketType = $ticketType;
        return $this;
    }
}