<?php

namespace EO\PaymentBundle\Model;

use EO\MiscBundle\Model\AddressInterface;

class Card extends Base
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var
     */
    protected $number;

    /**
     * @var string
     */
    protected $cvc;

    /**
     * @var int
     */
    protected $expireMonth;

    /**
     * @var int
     */
    protected $expireYear;

    /**
     * @var AddressInterface
     */
    protected $fullAddress;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getCvc()
    {
        return $this->cvc;
    }

    /**
     * @return int
     */
    public function getExpireMonth()
    {
        return $this->expireMonth;
    }

    /**
     * @return int
     */
    public function getExpireYear()
    {
        return $this->expireYear;
    }

    /**
     * @return AddressInterface
     */
    public function getFullAddress()
    {
        return $this->fullAddress;
    }

    /**
     * @param string $name
     * @return Card
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param mixed $number
     * @return Card
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @param string $cvc
     * @return Card
     */
    public function setCvc($cvc)
    {
        $this->cvc = $cvc;
        return $this;
    }

    /**
     * @param int $expireMonth
     * @return Card
     */
    public function setExpireMonth($expireMonth)
    {
        $this->expireMonth = $expireMonth;
        return $this;
    }

    /**
     * @param int $expireYear
     * @return Card
     */
    public function setExpireYear($expireYear)
    {
        $this->expireYear = $expireYear;
        return $this;
    }

    /**
     * @param AddressInterface $fullAddress
     * @return Card
     */
    public function setFullAddress(AddressInterface $fullAddress)
    {
        $this->fullAddress = $fullAddress;
        return $this;
    }
}