<?php

namespace EO\PaymentBundle\Model;

use EO\TicketBundle\Model\TicketTypeInterface;
use EO\UserBundle\Model\UserInterface;

class Order extends Base
{
    /**
     * @var string
     */
    protected $currency;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * @var TicketTypeInterface
     */
    protected $ticketType;

    /**
     * @var UserInterface
     */
    protected $customer;

    /**
     * @var boolean
     */
    protected $paid;

    public function __construct()
    {
        parent::__construct();
        $this->quantity = 1;
        $this->paid = false;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return TicketTypeInterface
     */
    public function getTicketType()
    {
        return $this->ticketType;
    }

    /**
     * @return UserInterface
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return boolean
     */
    public function isPaid()
    {
        return $this->paid;
    }

    /**
     * @param string $currency
     * @return Order
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @param int $quantity
     * @return Order
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @param TicketTypeInterface $ticketType
     * @return Order
     */
    public function setTicketType(TicketTypeInterface $ticketType)
    {
        $this->ticketType = $ticketType;
        return $this;
    }

    /**
     * @param UserInterface $customer
     * @return Order
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @param boolean $paid
     * @return Order
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
        return $this;
    }
}