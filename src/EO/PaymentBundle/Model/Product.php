<?php

namespace EO\PaymentBundle\Model;

class Product extends Base
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Product
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}