<?php

namespace EO\PaymentBundle\Model;

use EO\UserBundle\Model\UserInterface;

class Customer extends Base
{
    /**
     * @var UserInterface
     */
    protected $customer;

    /**
     * @return UserInterface
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param UserInterface $customer
     * @return Customer
     */
    public function setCustomer(UserInterface $customer)
    {
        $this->customer = $customer;
        return $this;
    }
}