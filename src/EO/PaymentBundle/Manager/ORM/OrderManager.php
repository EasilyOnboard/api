<?php

namespace EO\PaymentBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\PaymentBundle\Manager\DBAL\OrderManagerInterface;

class OrderManager extends Manager implements OrderManagerInterface
{
}