<?php

namespace EO\PaymentBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\PaymentBundle\Manager\DBAL\ProductManagerInterface;

class ProductManager extends Manager implements ProductManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneByType($type)
    {
        return $this->repository->createQueryBuilder('p')
            ->where('p.type = :type')
            ->setParameter('type', $type)
            ->getQuery()
            ->getOneOrNullResult();
    }
}