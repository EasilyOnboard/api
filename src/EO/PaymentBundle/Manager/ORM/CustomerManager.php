<?php

namespace EO\PaymentBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\UserBundle\Model\UserInterface;
use EO\PaymentBundle\Manager\DBAL\CustomerManagerInterface;

class CustomerManager extends Manager implements CustomerManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneByCustomer(UserInterface $user)
    {
        return $this->repository->createQueryBuilder('c')
            ->where('c.customer = :customer')
            ->setParameter('customer', $user)
            ->getQuery()
            ->getOneOrNullResult();
    }
}