<?php

namespace EO\PaymentBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\TicketBundle\Model\TicketTypeInterface;
use EO\PaymentBundle\Manager\DBAL\SkuManagerInterface;

class SkuManager extends Manager implements SkuManagerInterface
{
    public function findOneByTicketType(TicketTypeInterface $ticketType)
    {
        return $this->repository->createQueryBuilder('s')
            ->where('s.ticketType = :ticketType')
            ->setParameter('ticketType', $ticketType)
            ->getQuery()
            ->getOneOrNullResult();
    }
}