<?php

namespace EO\PaymentBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\PaymentBundle\Model\Product;

interface ProductManagerInterface extends ManagerInterface
{
    /**
     * @param string $type
     * @return Product|null
     */
    public function findOneByType($type);
}