<?php

namespace EO\PaymentBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\UserBundle\Model\UserInterface;
use EO\PaymentBundle\Model\Customer;

interface CustomerManagerInterface extends ManagerInterface
{
    /**
     * @param UserInterface $user
     *
     * @return Customer|null
     */
    public function findOneByCustomer(UserInterface $user);
}