<?php

namespace EO\PaymentBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;

interface OrderManagerInterface extends ManagerInterface
{
}