<?php

namespace EO\PaymentBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\TicketBundle\Model\TicketTypeInterface;
use EO\PaymentBundle\Model\Sku;

interface SkuManagerInterface extends ManagerInterface
{
    /**
     * @param TicketTypeInterface $ticketType
     *
     * @return Sku|null
     */
    public function findOneByTicketType(TicketTypeInterface $ticketType);
}