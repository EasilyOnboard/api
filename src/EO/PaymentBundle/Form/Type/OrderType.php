<?php

namespace EO\PaymentBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class OrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ticket_type', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_ticket.model.ticket_type.class')
            ))
            ->add('quantity', 'number', array(
                'precision' => 0
            ));
    }

    public function getName()
    {
        return 'eo_payment_order';
    }
}