<?php

namespace EO\PaymentBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CardInfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('expire_month', 'number', array(
                'precision' => 0
            ))
            ->add('expire_year', 'number', array(
                'precision' => 0
            ))
            ->add('full_address', 'eo_address')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array('validation_groups' => array('info')));
    }

    public function getName()
    {
        return 'eo_payment_card_info';
    }
}