<?php

namespace EO\ShopBundle\Handler;

use EO\PaymentBundle\Handler\PaymentHandlerInterface;
use EO\PaymentBundle\Manager\DBAL\OrderManagerInterface;
use EO\PaymentBundle\Model\Card;
use EO\PaymentBundle\Model\Order;
use EO\TicketBundle\Factory\TicketFactoryInterface;
use EO\TicketBundle\Model\TicketInterface;

class OrderHandler implements OrderHandlerInterface
{
    /**
     * @var PaymentHandlerInterface
     */
    private $paymentHandler;

    /**
     * @var OrderManagerInterface
     */
    private $orderManager;

    /**
     * @var TicketFactoryInterface
     */
    private $ticketFactory;

    /**
     * @param PaymentHandlerInterface $paymentHandler
     * @param TicketFactoryInterface $ticketFactory
     * @param OrderManagerInterface $orderManager
     */
    public function __construct(PaymentHandlerInterface $paymentHandler, TicketFactoryInterface $ticketFactory, OrderManagerInterface $orderManager)
    {
        $this->paymentHandler = $paymentHandler;
        $this->orderManager = $orderManager;
        $this->ticketFactory = $ticketFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function newOrder(Order $order)
    {
        if (null === $this->paymentHandler->product()->getTicket()) {
            $this->paymentHandler->product()->createTicket();
        }
        $order->setCurrency($order->getTicketType()->getNetwork()->getCompany()->getCurrency());
        if (null === $this->paymentHandler->order()->create($order)) {
            return null;
        }
        return $order;
    }

    /**
     * {@inheritdoc}
     */
    public function payOrder(Order $order)
    {
        if ($this->paymentHandler->order()->pay($order)) {
            return $this->generateNewTickets($order);
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function payOrderWithNotRegisteredCard(Order $order, Card $card)
    {
        if ($this->paymentHandler->order()->payWithOtherCard($order, $card)) {
            return $this->generateNewTickets($order);
        }
        return null;
    }

    /**
     * @param Order $order
     *
     * @return TicketInterface[]
     */
    private function generateNewTickets(Order $order)
    {
        $tickets = array();
        for ($i = 0; $i < $order->getQuantity(); $i++) {
            $tickets[] = $this->ticketFactory->create($order->getCustomer(), $order->getTicketType());
        }
        return $tickets;
    }
}