<?php

namespace EO\ShopBundle\Handler;

use EO\PaymentBundle\Handler\PaymentHandlerInterface;
use EO\TicketBundle\Handler\TicketHandlerInterface;
use EO\TicketBundle\Manager\DBAL\TicketTypeManagerInterface;
use EO\TicketBundle\Model\TicketTypeInterface;

class ShopHandler implements ShopHandlerInterface
{
    /**
     * @var TicketHandlerInterface
     */
    private $ticketHandler;

    /**
     * @var TicketTypeManagerInterface
     */
    private $ticketTypeManager;

    /**
     * @var PaymentHandlerInterface
     */
    private $paymentHandler;

    public function __construct(TicketHandlerInterface $ticketHandler, TicketTypeManagerInterface $ticketTypeManager, PaymentHandlerInterface $paymentHandler)
    {
        $this->ticketHandler = $ticketHandler;
        $this->ticketTypeManager = $ticketTypeManager;
        $this->paymentHandler = $paymentHandler;
    }

    /**
     * {@inheritdoc}
     */
    public function newTicketType(TicketTypeInterface $ticketType)
    {
        $equivalent = $this->ticketTypeManager->findEquivalent($ticketType);
        if ($equivalent !== null) {
            $ticketType = $equivalent;
        } else {
            $this->ticketHandler->handlePrice($ticketType);
            if ($ticketType->getName() === null) {
                $ticketType->setName('Custom ticket'); // TODO: Generate name
            }
            $this->ticketTypeManager->update($ticketType);
        }
        if (null === $this->paymentHandler->sku()->get($ticketType)) {
            $this->paymentHandler->sku()->create($ticketType);
        }
        return $ticketType;
    }
}