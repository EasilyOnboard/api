<?php

namespace EO\ShopBundle\Handler;

use EO\TicketBundle\Model\TicketTypeInterface;

interface ShopHandlerInterface
{
    /**
     * @param TicketTypeInterface $ticketType
     *
     * @return TicketTypeInterface
     */
    public function newTicketType(TicketTypeInterface $ticketType);
}