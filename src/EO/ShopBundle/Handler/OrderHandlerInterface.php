<?php

namespace EO\ShopBundle\Handler;

use EO\PaymentBundle\Model\Card;
use EO\PaymentBundle\Model\Order;
use EO\TicketBundle\Model\TicketInterface;

interface OrderHandlerInterface
{
    /**
     * @param Order $order
     *
     * @return Order
     */
    public function newOrder(Order $order);

    /**
     * @param Order $order
     *
     * @return null|TicketInterface[]
     */
    public function payOrder(Order $order);

    /**
     * @param Order $order
     * @param Card $card
     *
     * @return null|TicketInterface[]
     */
    public function payOrderWithNotRegisteredCard(Order $order, Card $card);
}