<?php

namespace EO\ShopBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\PaymentBundle\Model\Card;
use EO\PaymentBundle\Model\Order;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class OrderController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create a new order.",
     *  input="eo_payment_order",
     *  statusCodes={
     *         Codes::HTTP_CREATED="Returned when successful",
     *         Codes::HTTP_NOT_ACCEPTABLE="Returned when an error has occurred",
     *         Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *     }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function newAction(Request $request)
    {
        $order = new Order();
        $order->setCustomer($this->getUser());
        $form = $this->createRestForm('eo_payment_order', $order, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            if (null !== ($this->getOrderHandler()->newOrder($order))) {
                return $this->createView($order, Codes::HTTP_CREATED, array('basic'));
            }
            return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Pay an order.",
     *  statusCodes={
     *         Codes::HTTP_CREATED="Returned when successful",
     *         Codes::HTTP_NOT_ACCEPTABLE="Returned when the payment was refused"
     *     }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $orderId
     *
     * @return View
     */
    public function payAction($orderId)
    {
        /** @var Order $order */
        $order = $this->getOrderManager()->find($orderId);
        if (!$order) {
            throw $this->createNotFoundException();
        }
        if (null !== ($tickets = $this->getOrderHandler()->payOrder($order))) {
            return $this->createView($tickets, Codes::HTTP_ACCEPTED, array('basic', 'full_base_ticket', 'full_ticket'));
        }
        return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Pay an order with a not registered card.",
     *  input="eo_payment_card",
     *  statusCodes={
     *         Codes::HTTP_CREATED="Returned when successful",
     *         Codes::HTTP_NOT_ACCEPTABLE="Returned when the payment was refused",
     *         Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *     }
     * )
     *
     * @Rest\Post("/{orderId}/pay")
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     * @param string $orderId
     *
     * @return View
     */
    public function payCardAction(Request $request, $orderId)
    {
        /** @var Order $order */
        $order = $this->getOrderManager()->find($orderId);
        if (!$order) {
            throw $this->createNotFoundException();
        }
        $card = new Card();
        $form = $this->createRestForm('eo_payment_card', $card);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            if (null !== ($tickets = $this->getOrderHandler()->payOrderWithNotRegisteredCard($order, $card))) {
                return $this->createView($tickets, Codes::HTTP_ACCEPTED, array('basic', 'full_base_ticket', 'full_ticket'));
            }
            return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }


    /**
     * @return \EO\PaymentBundle\Manager\DBAL\OrderManagerInterface
     */
    private function getOrderManager()
    {
        return $this->get('eo_payment.manager.order');
    }

    /**
     * @return \EO\ShopBundle\Handler\OrderHandlerInterface
     */
    private function getOrderHandler()
    {
        return $this->get('eo_shop.handler.order');
    }
}