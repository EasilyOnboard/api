<?php

namespace EO\CoreBundle;

use Doctrine\ODM\MongoDB\Types\Type;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EOCoreBundle extends Bundle
{
    public function __construct()
    {
        Type::registerType('date-interval', 'EO\CoreBundle\Doctrine\MongoDB\Type\DateIntervalType');
    }
}
