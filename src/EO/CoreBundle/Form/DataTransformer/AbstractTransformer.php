<?php

namespace EO\CoreBundle\Form\DataTransformer;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\DataTransformerInterface;

abstract class AbstractTransformer implements DataTransformerInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var string
     */
    protected $class;

    /**
     * @param ContainerInterface $container
     * @param string    $class
     */
    public function __construct(ContainerInterface $container, $class)
    {
        $this->container = $container;
        $this->class = $class;
    }
}
