<?php

namespace EO\CoreBundle\Form\Type;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType as BaseAbstractType;

abstract class AbstractType extends BaseAbstractType
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var string
     */
    protected $class;

    /**
     * @param ContainerInterface $container
     * @param string    $class
     */
    public function __construct(ContainerInterface $container, $class)
    {
        $this->container = $container;
        $this->class = $class;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
        ));
    }

    /**
     * @return string
     */
    public function getModelType()
    {
        return $this->container->getParameter('eo_core.form.type.model');
    }
}
