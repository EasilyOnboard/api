<?php

namespace EO\CoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension as BaseExtension;
use Symfony\Component\DependencyInjection\Loader;

class Extension extends BaseExtension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $reflector = new \ReflectionClass(get_class($this));
        $fn = $reflector->getFileName();
        $loader = new Loader\YamlFileLoader($container, new FileLocator(dirname($fn).'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('types.yml');
        $loader->load($container->getParameter('eo_core.db_driver').'.yml');
    }
}