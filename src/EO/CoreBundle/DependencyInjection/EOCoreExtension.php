<?php

namespace EO\CoreBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

class EOCoreExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
#        $loader->load('types.xml');
        $loader->load(sprintf('%s.yml', $config['db_driver']));
        $container->setParameter('eo_core.db_driver', $config['db_driver']);
        $container->setParameter('eo_core.model_manager_name', $config['model_manager_name']);
        $container->setParameter('eo_core.device.email', $config['device']['email']);
    }
}