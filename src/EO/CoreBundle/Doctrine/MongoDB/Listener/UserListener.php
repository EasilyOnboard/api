<?php

namespace EO\CoreBundle\Doctrine\MongoDB\Listener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Event\LifecycleEventArgs;
use Doctrine\ODM\MongoDB\Events;
use EO\CoreBundle\Model\CustomerInterface;
use EO\CoreBundle\Model\EmployeeInterface;
use EO\CoreBundle\Model\Manager\CustomerManagerInterface;
use EO\CoreBundle\Model\Manager\EmployeeManagerInterface;
use EO\CoreBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserListener implements EventSubscriber
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Synchronise double data between an User Document
     * to a Customer or a Employee Document.
     *
     * @param LifecycleEventArgs $eventArgs
     */
    public function preUpdate(LifecycleEventArgs $eventArgs)
    {
        $document = $eventArgs->getDocument();
        if ($document instanceof UserInterface) {
            /** @var CustomerManagerInterface $customerManager */
            $customerManager = $this->container->get('eo_core.customer_manager');
            /** @var EmployeeManagerInterface $employeeManager */
            $employeeManager = $this->container->get('eo_core.employee_manager');

            $customer = $customerManager->findOneByUser($document);
            if ($customer) {
                $this->syncUserToObject($document, $customer, $eventArgs->getDocumentManager());
            } else {
                $employee = $employeeManager->findOneByUser($document);
                if ($employee) {
                    $this->syncUserToObject($document, $employee, $eventArgs->getDocumentManager());
                }
            }
        }
    }

    /**
     * @param UserInterface $user
     * @param CustomerInterface|EmployeeInterface $object
     * @param DocumentManager $dm
     */
    private function syncUserToObject(UserInterface $user, $object, DocumentManager $dm)
    {
        if ($object instanceof CustomerInterface || $object instanceof EmployeeInterface) {
            $object->setFirstname($user->getFirstname());
            $object->setLastname($user->getLastname());
            $object->setEmail($user->getEmail());
            $dm->persist($object);
        }
    }

    /**
     * Returns an array of events this subscriber wants to listen to.
     *
     * @return array
     */
    public function getSubscribedEvents()
    {
        return array(
            Events::preUpdate,
        );
    }
}