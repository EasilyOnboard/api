<?php

namespace EO\CoreBundle\Doctrine\MongoDB\Type;

use Doctrine\DBAL\Types\ConversionException;
use Doctrine\ODM\MongoDB\Types\Type;

/**
 * Class DateIntervalType Based on Herrera\DateInterval\DateInterval class
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class DateIntervalType extends Type
{
    /**
     * The date properties.
     *
     * @var array
     */
    private static $date = array('y' => 'Y', 'm' => 'M', 'd' => 'D');

    /**
     * The time properties.
     *
     * @var array
     */
    private static $time = array('h' => 'H', 'i' => 'M', 's' => 'S');

    /**
     * @param \DateInterval $value
     * @throws ConversionException
     * @return string
     */
    public function convertToDatabaseValue($value)
    {
        if ($value === null || !($value instanceof \DateInterval)) {
            return null;
        }

        $string = "P";

        foreach (self::$date as $property => $suffix) {
            if ($value->{$property}) {
                $string .= $value->{$property} . $suffix;
            }
        }

        if ($value->h || $value->i || $value->s) {
            $string .= 'T';

            foreach (self::$time as $property => $suffix) {
                if ($value->{$property}) {
                    $string .= $value->{$property} . $suffix;
                }
            }
        }

        return $value->format($string);
    }

    /**
     * @param string $value
     * @return \DateInterval
     */
    public function convertToPHPValue($value)
    {
        return new \DateInterval($value);
    }
}