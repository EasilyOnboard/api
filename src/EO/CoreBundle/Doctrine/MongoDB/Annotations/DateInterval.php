<?php

namespace EO\CoreBundle\Doctrine\MongoDB\Annotations;

use Doctrine\ODM\MongoDB\Mapping\Annotations\AbstractField;

/** @Annotation */
class DateInterval extends AbstractField
{
    public $type = 'date-interval';
}