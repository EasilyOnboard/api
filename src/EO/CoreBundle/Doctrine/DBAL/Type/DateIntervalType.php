<?php

namespace EO\CoreBundle\Doctrine\DBAL\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

class DateIntervalType extends StringType
{
    const DATE_INTERVAL = "date_interval";

    /**
     * The date properties.
     *
     * @var array
     */
    private static $date = array('y' => 'Y', 'm' => 'M', 'd' => 'D');

    /**
     * The time properties.
     *
     * @var array
     */
    private static $time = array('h' => 'H', 'i' => 'M', 's' => 'S');

    /**
     * @{@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return new \DateInterval($value);
    }

    /**
     * @{@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value === null || !($value instanceof \DateInterval)) {
            return null;
        }

        $string = "P";

        foreach (self::$date as $property => $suffix) {
            if ($value->{$property}) {
                $string .= $value->{$property} . $suffix;
            }
        }

        if ($value->h || $value->i || $value->s) {
            $string .= 'T';

            foreach (self::$time as $property => $suffix) {
                if ($value->{$property}) {
                    $string .= $value->{$property} . $suffix;
                }
            }
        }

        return $value->format($string);
    }

    /**
     * @{@inheritdoc}
     */
    public function getName()
    {
        return self::DATE_INTERVAL;
    }
}