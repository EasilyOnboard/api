<?php

namespace EO\CoreBundle\Search\Transformer;

use Elastica\Document;
use FOS\ElasticaBundle\Transformer\ModelToElasticaTransformerInterface;

class NetworkElementToElasticaTransformer implements  ModelToElasticaTransformerInterface
{
    /**
     * @param object $object
     * @param array $fields
     * @return Document
     */
    public function transform($object, array $fields)
    {
        $document = new Document($object->getId(), array(
            'name' => $object->getName()
        ));
        $document->setParent($object->getNetwork()->getId());
        return $document;
    }
}