<?php

namespace EO\CoreBundle\Search\Repository;

use EO\CoreBundle\Search\Model\NetworkElementSearch;
use FOS\ElasticaBundle\Repository;

class NetworkRepository extends Repository
{
    public function search($arg)
    {
        $boolQuery = new \Elastica\Query\Bool();

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('name', $arg);
        $boolQuery->addShould($fieldQuery);

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('city', $arg);
        $boolQuery->addShould($fieldQuery);

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('status', true);
        $boolQuery->addMust($fieldQuery);

        return $this->find($boolQuery);
    }

    /**
     * @param NetworkElementSearch $element
     * @return array
     */
    public function searchElement(NetworkElementSearch $element)
    {
        $boolQuery = new \Elastica\Query\Bool();

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('name', $element->getArgument());
        $boolQuery->addMust($fieldQuery);

        if ($element->getNetwork() !== null) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('id', $element->getNetwork()->getId());
            $boolQuery->addMust(new \Elastica\Query\HasParent($fieldQuery, 'network'));
        }

        return $this->find($boolQuery);
    }
}