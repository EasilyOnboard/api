<?php

namespace EO\CoreBundle\Search\Model;

use EO\CoreBundle\Model\NetworkInterface;

class NetworkElementSearch
{
    /**
     * @var NetworkInterface
     */
    protected $network;

    /**
     * @var string
     */
    protected $argument;

    /**
     * @return NetworkInterface
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * @return string
     */
    public function getArgument()
    {
        return $this->argument;
    }

    /**
     * @param NetworkInterface $network
     * @return self
     */
    public function setNetwork(NetworkInterface $network)
    {
        $this->network = $network;

        return $this;
    }

    /**
     * @param string $argument
     * @return self
     */
    public function setArgument($argument)
    {
        $this->argument = $argument;

        return $this;
    }
}