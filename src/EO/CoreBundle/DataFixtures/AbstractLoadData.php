<?php

namespace EO\CoreBundle\DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Yaml\Yaml;
use Doctrine\Common\DataFixtures\AbstractFixture;

abstract class AbstractLoadData extends AbstractFixture implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * Return the fixtures for the current model.
     *
     * @return array
     */
    protected function getModelFixtures()
    {
        if ($this->getEnvironments() === null || empty($this->getEnvironments())) {
            throw new \InvalidArgumentException("Must have at least one environment set.");
        }
        if (!$this->validEnvironment()) {
            return null;
        }
        if ($this->getModelFile() === null) {
            throw new \InvalidArgumentException("Must define a model file.");
        }
        $reflector = new \ReflectionClass(get_class($this));
        $fn = $reflector->getFileName();
        $fixturesPath = realpath(dirname($fn).'/../fixtures/'.$this->getEnvironments()[0].'/'.$this->getModelFile().'.yml');
        if (!file_exists($fixturesPath)) {
            throw new FileNotFoundException(null, 0, null, $fixturesPath);
        }
        return Yaml::parse(file_get_contents($fixturesPath));
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        $fixtures = $this->getModelFixtures();
        foreach ($fixtures[$this->getModelFile()] as $reference => $data) {
            $this->loadData($reference, $data);
        }
    }

    /**
     * @param string $reference
     * @param array $data
     */
    abstract protected function loadData($reference, array $data);

    /**
     * @return bool
     */
    protected function validEnvironment()
    {
        return in_array($this->container->get('kernel')->getEnvironment(), $this->getEnvironments());
    }

    /**
     * Return the file name for the current model.
     */
    abstract protected function getModelFile();

    /**
     * Return environments which in the fixture can be loaded.
     */
    abstract protected function getEnvironments();

    /**
     * @return \EO\CoreBundle\Manager\DBAL\ManagerInterface
     */
    abstract protected function getManager();

    /**
     * @return \Doctrine\ORM\EntityManagerInterface
     */
    protected function getEntityManager()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}