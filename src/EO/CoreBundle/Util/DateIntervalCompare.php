<?php

namespace EO\CoreBundle\Util;

class DateIntervalCompare
{
    public static function compare(\DateInterval $a, \DateInterval $b)
    {
        $aTotalSeconds = self::getTotalSeconds($a);
        $bTotalSeconds = self::getTotalSeconds($b);

        if($aTotalSeconds < $bTotalSeconds)
            return -1;
        elseif($aTotalSeconds == $bTotalSeconds)
            return 0;
        return 1;
    }

    private static function getTotalSeconds(\DateInterval $dateInterval)
    {
        $iSeconds = $dateInterval->s + ($dateInterval->i * 60) + ($dateInterval->h * 3600);
        if($dateInterval->days > 0)
            $iSeconds += ($dateInterval->days * 86400);
        else
            $iSeconds += ($dateInterval->d * 86400) + ($dateInterval->m * 2592000) + ($dateInterval->y * 31536000);
        if($dateInterval->invert)
            $iSeconds *= -1;
        return $iSeconds;
    }
}