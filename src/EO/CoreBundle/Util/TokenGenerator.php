<?php

namespace EO\CoreBundle\Util;

class TokenGenerator
{
    /**
     * @return string
     */
    public static function newUniqueToken()
    {
        return bin2hex(openssl_random_pseudo_bytes(32));
    }
}