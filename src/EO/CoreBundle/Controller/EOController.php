<?php

namespace EO\CoreBundle\Controller;

use EO\UserBundle\Model\UserInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormTypeInterface;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Translation\LoggingTranslator;

class EOController extends FOSRestController
{
    /**
     * @param object|array $data
     * @param integer      $code
     * @param array        $groups
     *
     * @return View
     */
    protected function createView($data, $code, array $groups = array())
    {
        if ($data === null) {
            $data = new \stdClass();
        }
        $serializationContext = SerializationContext::create();
        if (!empty($groups)) {
            $serializationContext->setGroups($groups);
        }
        $serializationContext->enableMaxDepthChecks();
        return View::create($data, $code)->setSerializationContext($serializationContext);
    }

    /**
     * @param string|FormTypeInterface $type
     * @param mixed                    $data
     * @param string                   $method
     * @param array                    $options
     *
     * @return Form
     */
    protected function createRestForm($type, $data = null, $method = 'PUT', array $options = array())
    {
        $options = array_merge($options, array('method' => $method, 'csrf_protection' => false));
        $form = parent::createForm($type, $data, $options);

        return $form;
    }

    /**
     * @return UserInterface|null
     */
    public function getUser()
    {
        return parent::getUser();
    }

    /**
     * @return LoggingTranslator
     */
    protected function getTranslator()
    {
        $translator = $this->get('translator');
        if ($this->getUser() != null && $this->getUser()->getLanguage() != null) {
            $translator->setLocale($this->getUser()->getLanguage());
        }
        return $translator;
    }
}
