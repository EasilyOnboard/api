<?php

namespace EO\CoreBundle\Doctrine\Manager\MongoDB;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\DocumentRepository;
use EO\CoreBundle\Doctrine\Manager\AbstractManager;

class Manager extends AbstractManager
{
    /**
     * @var DocumentManager
     */
    protected $dm;

    /**
     * @var DocumentRepository
     */
    protected $repository;

    /**
     * @param DocumentManager $dm
     * @param string          $class
     */
    public function __construct(DocumentManager $dm, $class)
    {
        parent::__construct($class);
        $this->dm = $dm;
        $this->repository = $this->dm->getRepository($this->class);
    }

    /**
     * @return DocumentRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param $id
     *
     * @return object
     *
     * @throws \Doctrine\ODM\MongoDB\LockException
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $criteria
     *
     * @return object
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findAllBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * @param object $object
     */
    public function update($object)
    {
        $this->dm->persist($object);
        $this->dm->flush();
    }

    /**
     * @param object $object
     */
    public function reload($object)
    {
        $this->dm->refresh($object);
    }

    /**
     * @param object $object
     */
    public function delete($object)
    {
        $this->dm->remove($object);
        $this->dm->flush();
    }
}
