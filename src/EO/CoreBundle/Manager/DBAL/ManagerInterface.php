<?php

namespace EO\CoreBundle\Manager\DBAL;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\Query;

interface ManagerInterface
{
    /**
     * @return ObjectRepository
     */
    public function getRepository();

    /**
     * @return string
     */
    public function getClass();

    /**
     * @return object
     */
    public function create();

    /**
     * @param string $id
     *
     * @return object
     */
    public function find($id);

    /**
     * @param array $criteria
     *
     * @return object
     */
    public function findOneBy(array $criteria);

    /**
     * @return array
     */
    public function findAll();

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findAllBy(array $criteria);

    /**
     * @param object $object
     */
    public function update($object);

    /**
     * @param object $object
     */
    public function reload($object);

    /**
     * @param object $object
     */
    public function delete($object);

    /**
     * @param Query $query
     *
     * @return array|null
     */
    public function paginate(Query $query);
}
