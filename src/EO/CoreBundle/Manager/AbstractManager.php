<?php

namespace EO\CoreBundle\Manager;

use Doctrine\ORM\Query;
use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use Knp\Component\Pager\Paginator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractManager implements ManagerInterface
{
    /**
     * @var string
     */
    protected $class;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     * @param string $class
     */
    public function __construct(ContainerInterface $container, $class)
    {
        $this->class = $class;
        $this->container = $container;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return object
     */
    public function create()
    {
        return new $this->class();
    }

    /**
     * @param Query $query
     *
     * @return array
     */
    public function paginate(Query $query)
    {
        if ($this->getRequest()->query->get('page') || $this->getRequest()->query->get('limit')) {
            return $this->getPaginator()->paginate(
                $query,
                $this->getRequest()->query->get('page', 1),
                $this->getRequest()->query->get('limit', 20)
            )->getItems();
        }
        return $query->getResult();
    }

    /**
     * @return Request
     */
    protected function getRequest()
    {
        return $this->container->get('request');
    }

    /**
     * @return Paginator
     */
    protected function getPaginator()
    {
        return $this->container->get('knp_paginator');
    }
}