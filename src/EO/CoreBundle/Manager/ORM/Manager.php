<?php

namespace EO\CoreBundle\Manager\ORM;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use EO\CoreBundle\Manager\AbstractManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Manager extends AbstractManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @param ContainerInterface $container
     * @param EntityManager $em
     * @param string $class
     */
    public function __construct(ContainerInterface $container, EntityManager $em, $class)
    {
        parent::__construct($container, $class);
        $this->em = $em;
        $this->repository = $this->em->getRepository($this->class);
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param string $id
     *
     * @return object|null
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $criteria
     *
     * @return object|null
     */
    public function findOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }

    /**
     * @param array $criteria
     *
     * @return array
     */
    public function findAllBy(array $criteria)
    {
        return $this->repository->findBy($criteria);
    }

    /**
     * @param object $object
     */
    public function update($object)
    {
        $this->em->persist($object);
        $this->em->flush();
    }

    /**
     * @param object $object
     */
    public function reload($object)
    {
        $this->em->refresh($object);
    }

    /**
     * @param object $object
     */
    public function delete($object)
    {
        $this->em->remove($object);
        $this->em->flush();
    }
}
