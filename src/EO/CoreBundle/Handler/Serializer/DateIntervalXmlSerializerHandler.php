<?php

namespace EO\CoreBundle\Handler\Serializer;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;

class DateIntervalXmlSerializerHandler implements SubscribingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribingMethods()
    {
        return array(
            array(
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format' => 'xml',
                'type' => 'DateInterval',
                'method' => 'serializeDateIntervalToXml',
            ),
        );
    }

    public function serializeDateIntervalToXml(
        JsonSerializationVisitor $visitor,
        \DateInterval $dateInterval,
        array $type,
        Context $context)
    {
        $seconds = $dateInterval->days * 86400 + $dateInterval->h * 3600 + $dateInterval->i * 60 + $dateInterval->s;

        return $seconds;
    }
}
