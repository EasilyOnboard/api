<?php

namespace EO\NetworkBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class NetworkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('city', 'text')
            ->add('postcode', 'text')
            ->add('status', 'radio', array(
                'value' => false,
            ))
        ;
    }

    public function getName()
    {
        return 'eo_network';
    }
}
