<?php

namespace EO\NetworkBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('color', 'text')
            ->add('line_type', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.line_type.class'),
            ))
        ;
    }

    public function getName()
    {
        return 'eo_line';
    }
}
