<?php

namespace EO\NetworkBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LineTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('icon', 'text')
        ;
    }

    public function getName()
    {
        return 'eo_line_type';
    }
}
