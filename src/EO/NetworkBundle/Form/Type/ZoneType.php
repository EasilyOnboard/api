<?php

namespace EO\NetworkBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\NetworkBundle\Model\ZoneInterface;
use Symfony\Component\Form\FormBuilderInterface;

class ZoneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ZoneInterface $zone */
        $zone = $builder->getData();

        $builder
            ->add('name', 'text')
            ->add('color', 'text')
            ->add('number', 'number', array(
                'precision' => 0
            ))
            ->add('price', 'money', array(
                'currency' => ($zone) ? $zone->getNetwork()->getCompany() : 'EUR',
            ))
        ;
    }

    public function getName()
    {
        return 'eo_zone';
    }
}
