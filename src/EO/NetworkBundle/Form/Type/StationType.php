<?php

namespace EO\NetworkBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class StationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('longitude', 'number')
            ->add('latitude', 'number')
            ->add('zone', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.zone.class'),
            ))
        ;
    }

    public function getName()
    {
        return 'eo_station';
    }
}
