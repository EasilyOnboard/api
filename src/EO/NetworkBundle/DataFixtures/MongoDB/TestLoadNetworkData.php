<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\Company;
use EO\CoreBundle\Document\Network;

class TestLoadNetworkData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $networks = $this->getModelFixtures();

        /** @var ManagerInterface $networkManager */
        $networkManager = $this->container->get('eo_core.network_manager');

        foreach ($networks['Networks'] as $reference => $data) {
            /** @var Network $network */
            $network = $networkManager->create();

            $network->setName($data['name']);
            $network->setStatus($data['status']);
            $network->setCity($data['city']);
            $network->setPostcode($data['postcode']);
            /** @var Company $company */
            $company = $this->getReference('Company_'.$data['company']);
            $network->setCompany($company);

            $this->addReference('Network_'.$reference, $network);

            $networkManager->update($network);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getModelFile()
    {
        return 'networks';
    }

    /**
     * {@inheritdoc}
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}
