<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\Line;
use EO\CoreBundle\Document\Station;

class TestLoadLineStationOrderData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $lineStationOrders = $this->getModelFixtures();

        /** @var ManagerInterface $lineManager */
        $lineManager = $this->container->get('eo_core.line_manager');

        foreach ($lineStationOrders['Line_station_orders'] as $reference => $data) {
            /** @var Line $line */
            $line = $this->getReference('Line_'.$data['line']);

            $orders = array();
            if (!empty($data['order'])) {
                foreach ($data['order'] as $key => $value) {
                    /** @var Station $station */
                    $station = $this->getReference('Station_'.$key);
                    $orders[$station->getId()] = $value;
                }
            }
            $line->setOrders($orders);

            $lineManager->update($line);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'line_station_orders';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}
