<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\Network;
use EO\CoreBundle\Document\Line;
use EO\CoreBundle\Document\Station;
use EO\CoreBundle\Document\Zone;

class TestLoadStationData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $stations = $this->getModelFixtures();

        /** @var ManagerInterface $stationManager */
        $stationManager = $this->container->get('eo_core.station_manager');

        foreach ($stations['Stations'] as $reference => $data) {
            /** @var Station $station */
            $station = $stationManager->create();

            $station->setName($data['name']);
            $station->setLongitude($data['longitude']);
            $station->setLatitude($data['latitude']);
            /** @var Zone $zone */
            $zone = $this->getReference('Zone_'.$data['zone']);
            /** @var Network $network */
            $network = $this->getReference('Network_'.$data['network']);
            $station->setZone($zone);
            $station->setNetwork($network);
            $lines = array();
            if (!empty($data['lines'])) {
                foreach ($data['lines'] as $value) {
                    /** @var Line $line */
                    $line = $this->getReference('Line_'.$value);
                    $station->addLine($line);
                    $line->addStation($station);
                    $lines[] = $line;
                }
            }
            $stationManager->update($station);
            foreach ($lines as $line) {
                $stationManager->update($line);
            }
            $this->setReference('Station_'.$reference, $station);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'stations';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 4;
    }
}
