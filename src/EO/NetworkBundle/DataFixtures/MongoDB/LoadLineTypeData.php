<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\LineType;

class LoadLineTypeData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $lineTypes = $this->getModelFixtures();

        /** @var ManagerInterface $lineTypeManager */
        $lineTypeManager = $this->container->get('eo_core.line_type_manager');

        foreach ($lineTypes['Line_types'] as $reference => $data) {
            /** @var LineType $lineType */
            $lineType = $lineTypeManager->create();

            $lineType->setName($data['name']);
            $lineType->setIcon($data['icon']);

            $this->setReference('LineType_'.$reference, $lineType);

            $lineTypeManager->update($lineType);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getModelFile()
    {
        return 'line_types';
    }

    /**
     * {@inheritdoc}
     */
    public function getEnvFolder()
    {
        return 'prod';
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
