<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\LineType;
use EO\CoreBundle\Document\Network;
use EO\CoreBundle\Document\Line;

class TestLoadLineData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $lines = $this->getModelFixtures();

        /** @var ManagerInterface $lineManager */
        $lineManager = $this->container->get('eo_core.line_manager');

        foreach ($lines['Lines'] as $reference => $data) {
            /** @var Line $line */
            $line = $lineManager->create();

            $line->setName($data['name']);
            $line->setColor($data['color']);
            /** @var LineType $lineType */
            $lineType = $this->getReference('LineType_'.$data['lineType']);
            $line->setLineType($lineType);
            /** @var Network $network */
            $network = $this->getReference('Network_'.$data['network']);
            $line->setNetwork($network);

            $this->setReference('Line_'.$reference, $line);

            $lineManager->update($line);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'lines';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}
