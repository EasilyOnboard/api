<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\Network;
use EO\CoreBundle\Document\Zone;

class TestLoadZoneData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $zones = $this->getModelFixtures();

        /** @var ManagerInterface $zoneManager */
        $zoneManager = $this->container->get('eo_core.zone_manager');

        foreach ($zones['Zones'] as $reference => $data) {
            /** @var Zone $zone */
            $zone = $zoneManager->create();

            $zone->setName($data['name']);
            $zone->setColor($data['color']);
            $zone->setPrice($data['price']);
            /** @var Network $network */
            $network = $this->getReference('Network_'.$data['network']);
            $zone->setNetwork($network);

            $this->setReference('Zone_'.$reference, $zone);

            $zoneManager->update($zone);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'zones';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}
