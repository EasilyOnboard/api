<?php

namespace EO\NetworkBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\LineTypeInterface;
use EO\NetworkBundle\Model\NetworkInterface;

class TestLoadLineData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var LineInterface $line */
        $line = $this->getManager()->create();
        $line->setName($data['name']);
        $line->setColor($data['color']);
        /** @var LineTypeInterface $lineType */
        $lineType = $this->getReference('LineType_'.$data['lineType']);
        $line->setLineType($lineType);
        /** @var NetworkInterface $network */
        $network = $this->getReference('Network_'.$data['network']);
        $line->setNetwork($network);
        $this->setReference('Line_'.$reference, $line);
        $this->getManager()->update($line);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_network.manager.line');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'lines';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}
