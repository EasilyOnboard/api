<?php

namespace EO\NetworkBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\NetworkBundle\Model\NetworkInterface;

class TestLoadNetworkData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var NetworkInterface $network */
        $network = $this->getManager()->create();
        $network->setName($data['name']);
        $network->setStatus($data['status']);
        $network->setCity($data['city']);
        $network->setPostcode($data['postcode']);
        /** @var CompanyInterface $company */
        $company = $this->getReference('Company_'.$data['company']);
        $network->setCompany($company);
        $this->addReference('Network_'.$reference, $network);
        $this->getManager()->update($network);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_network.manager.network');
    }

    /**
     * {@inheritdoc}
     */
    public function getModelFile()
    {
        return 'networks';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}
