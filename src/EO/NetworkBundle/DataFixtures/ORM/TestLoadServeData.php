<?php

namespace EO\NetworkBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\ServeInterface;
use EO\NetworkBundle\Model\StationInterface;

class TestLoadServeData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var LineInterface $line */
        $line = $this->getReference('Line_'.$data['line']);
        $order = 0;
        foreach ($data['stations'] as $s) {
            /** @var StationInterface $station */
            $station = $this->getReference('Station_'.$s['station']);
            /** @var ServeInterface $serve */
            $serve = $this->getManager()->create();
            $serve->setLine($line);
            $serve->setStation($station);
            $serve->setOrder($order);
            if (isset($s['oneWay'])) {
                $serve->setOneWay($s['oneWay']);
            }
            if (isset($s['forward'])) {
                $serve->setOneWay($s['forward']);
            }
            $order++;
            $this->getManager()->update($serve);
        }
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_network.manager.serve');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'serves';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}
