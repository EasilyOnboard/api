<?php

namespace EO\NetworkBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\NetworkBundle\Model\ZoneInterface;

class TestLoadStationData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var StationInterface $station */
        $station = $this->getManager()->create();
        $station->setName($data['name']);
        $station->setLongitude($data['longitude']);
        $station->setLatitude($data['latitude']);
        /** @var ZoneInterface $zone */
        $zone = $this->getReference('Zone_'.$data['zone']);
        /** @var NetworkInterface $network */
        $network = $this->getReference('Network_'.$data['network']);
        $station->setZone($zone);
        $station->setNetwork($network);
        $this->setReference('Station_'.$reference, $station);
        $this->getManager()->update($station);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_network.manager.station');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'stations';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 4;
    }
}
