<?php

namespace EO\NetworkBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\NetworkBundle\Model\LineTypeInterface;

class LoadLineTypeData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var LineTypeInterface $lineType */
        $lineType = $this->getManager()->create();
        $lineType->setName($data['name']);
        $lineType->setIcon($data['icon']);
        $this->setReference('LineType_'.$reference, $lineType);
        $this->getManager()->update($lineType);
    }

    protected function getEnvironments()
    {
        return array('prod', 'dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_network.manager.line_type');
    }

    /**
     * {@inheritdoc}
     */
    public function getModelFile()
    {
        return 'line_types';
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
