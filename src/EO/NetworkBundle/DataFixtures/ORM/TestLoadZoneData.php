<?php

namespace EO\NetworkBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\ZoneInterface;

class TestLoadZoneData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var ZoneInterface $zone */
        $zone = $this->getManager()->create();
        $zone->setName($data['name']);
        $zone->setNumber($data['number']);
        $zone->setColor($data['color']);
        $zone->setPrice($data['price']);
        /** @var NetworkInterface $network */
        $network = $this->getReference('Network_'.$data['network']);
        $zone->setNetwork($network);
        $this->setReference('Zone_'.$reference, $zone);
        $this->getManager()->update($zone);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_network.manager.zone');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'zones';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}
