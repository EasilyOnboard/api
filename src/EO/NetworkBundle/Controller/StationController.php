<?php

namespace EO\NetworkBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\StationInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class StationController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All stations.",
     *  output={
     *      "class"="array<EO\NetworkBundle\Model\Station>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param string $networkId
     *
     * @return View
     */
    public function allAction($networkId)
    {
        if ($this->isGranted('ROLE_CUSTOMER')) {
            $network = $this->getNetworkManager()->findOneEnable($networkId);
        } else if ($this->isGranted('ROLE_STAFF') && $this->isGranted('ROLE_NETWORK_LIST')) {
            $network = $this->getNetworkManager()->find($networkId);
        } else {
            throw $this->createAccessDeniedException();
        }
        if (!$network) {
            throw $this->createNotFoundException();
        }
        $stations = $this->getStationManager()->findAllByNetwork($network);
        return $this->createView(array('stations' => $stations), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All stations by line.",
     *  output={
     *      "class"="array<EO\NetworkBundle\Model\Station>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get("/{networkId}/{lineId}/all")
     *
     * @Security("has_role('ROLE_NETWORK_LIST')")
     *
     * @param string $networkId
     * @param string $lineId
     *
     * @return View
     */
    public function allByLineAction($networkId, $lineId)
    {
        $line = $this->getLineManager()->findOneByNetworkAndId($networkId, $lineId);
        if (!$line) {
            throw $this->createNotFoundException();
        }
        $stations = $this->getStationManager()->findAllByLine($line);
        return $this->createView(array('stations' => $stations), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create station.",
     *  input="eo_station",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Station",
     *      "groups"={"basic", "full_station", "line"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_NETWORK_CREATE')")
     *
     * @param Request $request
     * @param string  $networkId
     *
     * @return View
     */
    public function postAction(Request $request, $networkId)
    {
        /** @var NetworkInterface $network */
        $network = $this->getNetworkManager()->find($networkId);
        if (!$network) {
            throw $this->createNotFoundException();
        }
        /** @var StationInterface $station */
        $station = $this->getStationManager()->create();
        $station->setNetwork($network);
        $form = $this->createRestForm('eo_station', $station, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getStationManager()->update($station);
            return $this->createView($station, Codes::HTTP_CREATED, array("basic", 'full_station', 'line'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get station.",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Station",
     *      "groups"={"basic", "full_station", "line"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get("/{networkId}/{stationId}")
     *
     * @Security("has_role('ROLE_NETWORK_GET')")
     *
     * @param string $networkId
     * @param string $stationId
     *
     * @return View
     */
    public function getAction($networkId, $stationId)
    {
        $station = $this->getStationManager()->findOneByNetworkAndId($networkId, $stationId);
        if (!$station) {
            throw $this->createNotFoundException();
        }
        return $this->createView($station, Codes::HTTP_OK, array("basic", 'full_station', 'line'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update station.",
     *  input="eo_station",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Station",
     *      "groups"={"basic", "full_station", "line"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put("/{networkId}/{stationId}")
     *
     * @Security("has_role('ROLE_NETWORK_UPDATE')")
     *
     * @param Request $request
     * @param string  $networkId
     * @param string  $stationId
     *
     * @return View
     */
    public function putAction(Request $request, $networkId, $stationId)
    {
        $station = $this->getStationManager()->findOneByNetworkAndId($networkId, $stationId);
        if (!$station) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_station', $station);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getStationManager()->update($station);
            return $this->createView($station, Codes::HTTP_ACCEPTED, array("basic", 'full_station', 'line'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete station.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when the Station is successfully deleted"
     *  }
     * )
     *
     * @Rest\Delete("/{networkId}/{stationId}")
     *
     * @Security("has_role('ROLE_NETWORK_DELETE')")
     *
     * @param string $networkId
     * @param string $stationId
     *
     * @return View
     */
    public function deleteAction($networkId, $stationId)
    {
        $station = $this->getStationManager()->findOneByNetworkAndId($networkId, $stationId);
        if (!$station) {
            throw $this->createNotFoundException();
        }
        $this->getStationManager()->delete($station);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @return \EO\NetworkBundle\Manager\DBAL\StationManagerInterface
     */
    private function getStationManager()
    {
        return $this->get('eo_network.manager.station');
    }

    /**
     * @return \EO\NetworkBundle\Manager\DBAL\LineManagerInterface
     */
    private function getLineManager()
    {
        return $this->get('eo_network.manager.line');
    }

    /**
     * @return \EO\NetworkBundle\Manager\DBAL\NetworkManagerInterface
     */
    private function getNetworkManager()
    {
        return $this->get('eo_network.manager.network');
    }
}
