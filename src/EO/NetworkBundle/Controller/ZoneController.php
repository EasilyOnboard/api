<?php

namespace EO\NetworkBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\ZoneInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class ZoneController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All zones.",
     *  output={
     *      "class"="array<EO\NetworkBundle\Model\Zone>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param string $networkId
     *
     * @return View
     */
    public function allAction($networkId)
    {
        if ($this->isGranted('ROLE_CUSTOMER')) {
            $network = $this->getNetworkManager()->findOneEnable($networkId);
        } else if ($this->isGranted('ROLE_STAFF') && $this->isGranted('ROLE_NETWORK_LIST')) {
            $network = $this->getNetworkManager()->find($networkId);
        } else {
            throw $this->createAccessDeniedException();
        }
        if (!$network) {
            throw $this->createNotFoundException();
        }
        $zones = $this->getZoneManager()->findAllByNetwork($network);
        return $this->createView(array('zones' => $zones), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create zone.",
     *  input="eo_zone",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Zone",
     *      "groups"={"basic", "full_zone"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_NETWORK_CREATE')")
     *
     * @param Request $request
     * @param string  $networkId
     *
     * @return View
     */
    public function postAction(Request $request, $networkId)
    {
        /** @var NetworkInterface $network */
        $network = $this->getNetworkManager()->find($networkId);
        if (!$network) {
            throw $this->createAccessDeniedException();
        }
        /** @var ZoneInterface $zone */
        $zone = $this->getZoneManager()->create();
        $zone->setNetwork($network);
        $form = $this->createRestForm('eo_zone', $zone, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getZoneManager()->update($zone);
            return $this->createView($zone, Codes::HTTP_CREATED, array("basic", 'full_zone'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get zone.",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Zone",
     *      "groups"={"basic", "full_zone"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get("/{networkId}/{zoneId}")
     *
     * @Security("has_role('ROLE_NETWORK_GET')")
     *
     * @param string $networkId
     * @param string $zoneId
     *
     * @return View
     */
    public function getAction($networkId, $zoneId)
    {
        $zone = $this->getZoneManager()->findOneByNetworkAndId($networkId, $zoneId);
        if (!$zone) {
            throw $this->createNotFoundException();
        }
        return $this->createView($zone, Codes::HTTP_OK, array("basic", 'full_zone'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update zone.",
     *  input="eo_zone",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Zone",
     *      "groups"={"basic", "full_zone"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put("/{networkId}/{zoneId}")
     *
     * @Security("has_role('ROLE_NETWORK_UPDATE')")
     *
     * @param Request $request
     * @param string  $networkId
     * @param string  $zoneId
     *
     * @return View
     */
    public function putAction(Request $request, $networkId, $zoneId)
    {
        $zone = $this->getZoneManager()->findOneByNetworkAndId($networkId, $zoneId);
        if (!$zone) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_zone', $zone);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getZoneManager()->update($zone);
            return $this->createView($zone, Codes::HTTP_ACCEPTED, array("basic", 'full_zone'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete zone.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when the Zone is successfully deleted"
     *  }
     * )
     *
     * @Rest\Delete("/{networkId}/{zoneId}")
     *
     * @Security("has_role('ROLE_NETWORK_DELETE')")
     *
     * @param string $networkId
     * @param string $zoneId
     *
     * @return View
     */
    public function deleteAction($networkId, $zoneId)
    {
        $zone = $this->getZoneManager()->findOneByNetworkAndId($networkId, $zoneId);
        if (!$zone) {
            throw $this->createNotFoundException();
        }
        $this->getZoneManager()->delete($zone);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @return \EO\NetworkBundle\Manager\DBAL\ZoneManagerInterface
     */
    private function getZoneManager()
    {
        return $this->get('eo_network.manager.zone');
    }

    /**
     * @return \EO\NetworkBundle\Manager\DBAL\NetworkManagerInterface
     */
    private function getNetworkManager()
    {
        return $this->get('eo_network.manager.network');
    }
}
