<?php

namespace EO\NetworkBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\NetworkBundle\Model\LineTypeInterface;
use EO\NetworkBundle\Model\NetworkInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class LineTypeController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All line types.",
     *  output={
     *      "class"="array<EO\NetworkBundle\Model\LineType>"
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_NETWORK_LIST')")
     *
     * @return View
     */
    public function allAction()
    {
        $lineTypes = $this->getLineTypeManager()->findAll();
        return $this->createView(array('line_types' => $lineTypes), Codes::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All line types by network.",
     *  output={
     *      "class"="array<EO\NetworkBundle\Model\LineType>"
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get("/{networkId}/all")
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $networkId
     *
     * @return View
     */
    public function allByNetworkAction($networkId)
    {
        /** @var NetworkInterface $network */
        $network = $this->getNetworkManager()->find($networkId);
        if ($network) {
            throw $this->createNotFoundException();
        }
        $lineTypes = $this->getLineTypeManager()->findAllByNetwork($network);
        return $this->createView(array('line_types' => $lineTypes), Codes::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create line type.",
     *  input="eo_line_type",
     *  output={
     *      "class"="EO\NetworkBundle\Model\LineType"
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        /** @var LineTypeInterface $lineType */
        $lineType = $this->getLineTypeManager()->create();
        $form = $this->createRestForm('eo_line_type', $lineType);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getLineTypeManager()->update($lineType);
            return $this->createView($lineType, Codes::HTTP_CREATED);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get line type.",
     *  output={
     *      "class"="EO\NetworkBundle\Model\LineType"
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     *
     * @param string $lineTypeId
     *
     * @return View
     */
    public function getAction($lineTypeId)
    {
        /** @var LineTypeInterface $lineType */
        $lineType = $this->getLineTypeManager()->find($lineTypeId);
        if (!$lineType) {
            throw $this->createNotFoundException();
        }
        return $this->createView($lineType, Codes::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update line type.",
     *  input="eo_line_type",
     *  output={
     *      "class"="EO\NetworkBundle\Model\LineType"
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     *
     * @param Request $request
     * @param string  $lineTypeId
     *
     * @return View
     */
    public function putAction(Request $request, $lineTypeId)
    {
        /** @var LineTypeInterface $lineType */
        $lineType = $this->getLineTypeManager()->find($lineTypeId);
        if (!$lineType) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_line_type', $lineType);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getLineTypeManager()->update($lineType);
            return $this->createView($lineType, Codes::HTTP_ACCEPTED);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete a Line Type by the Id.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when is successfully deleted"
     *  }
     * )
     *
     * @Rest\Delete
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     *
     * @param string $lineTypeId
     *
     * @return View
     */
    public function deleteAction($lineTypeId)
    {
        $lineType = $this->getLineTypeManager()->find($lineTypeId);
        if (!$lineType) {
            throw $this->createNotFoundException('Unable to find the Line Type');
        }
        $this->getLineTypeManager()->delete($lineType);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @return \EO\NetworkBundle\Manager\ORM\LineTypeManager
     */
    private function getLineTypeManager()
    {
        return $this->get('eo_network.manager.line_type');
    }

    /**
     * @return \EO\NetworkBundle\Manager\ORM\NetworkManager
     */
    private function getNetworkManager()
    {
        return $this->get('eo_network.manager.network');
    }
}
