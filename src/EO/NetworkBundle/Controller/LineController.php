<?php

namespace EO\NetworkBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\NetworkInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class LineController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All lines.",
     *  output={
     *      "class"="array<EO\NetworkBundle\Model\Line>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param string $networkId
     *
     * @return View
     */
    public function allAction($networkId)
    {
        if ($this->isGranted('ROLE_CUSTOMER')) {
            $network = $this->getNetworkManager()->findOneEnable($networkId);
        } else if ($this->isGranted('ROLE_STAFF') && $this->isGranted('ROLE_NETWORK_LIST')) {
            $network = $this->getNetworkManager()->find($networkId);
        } else {
            throw $this->createAccessDeniedException();
        }
        if (!$network) {
            throw $this->createNotFoundException();
        }
        $lines = $this->getLineManager()->findAllByNetwork($network);
        return $this->createView(array('lines' => $lines), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create line.",
     *  input="eo_line",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Line",
     *      "groups"={"basic", "full_line", "station"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_NETWORK_CREATE')")
     *
     * @param Request $request
     * @param string  $networkId
     *
     * @return View
     */
    public function postAction(Request $request, $networkId)
    {
        /** @var NetworkInterface $network */
        $network = $this->getNetworkManager()->find($networkId);
        if (!$network) {
            throw $this->createNotFoundException();
        }
        /** @var LineInterface $line */
        $line = $this->getLineManager()->create();
        $line->setNetwork($network);
        $form = $this->createRestForm('eo_line', $line, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getLineManager()->update($line);
            return $this->createView($line, Codes::HTTP_CREATED, array('basic', 'full_line', 'station'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get line.",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Line",
     *      "groups"={"basic", "full_line", "station"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get("/{networkId}/{lineId}")
     *
     * @Security("has_role('ROLE_NETWORK_GET')")
     *
     * @param string $networkId
     * @param string $lineId
     *
     * @return View
     */
    public function getAction($networkId, $lineId)
    {
        $line = $this->getLineManager()->findOneByNetworkAndId($networkId, $lineId);
        if (!$line) {
            throw $this->createNotFoundException();
        }
        return $this->createView($line, Codes::HTTP_OK, array('basic', 'full_line', 'station'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update line.",
     *  input="eo_line",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Line",
     *      "groups"={"basic", "full_line", "station"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put("/{networkId}/{lineId}")
     *
     * @Security("has_role('ROLE_NETWORK_UPDATE')")
     *
     * @param Request $request
     * @param string  $networkId
     * @param string  $lineId
     *
     * @return View
     */
    public function putAction(Request $request, $networkId, $lineId)
    {
        $line = $this->getLineManager()->findOneByNetworkAndId($networkId, $lineId);
        if (!$line) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_line', $line);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getLineManager()->update($line);
            return $this->createView($line, Codes::HTTP_ACCEPTED, array('basic', 'full_line', 'station'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete line.",
     *  statusCodes={
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid",
     *      Codes::HTTP_ACCEPTED="Returned when the Line is successfully deleted"
     *  }
     * )
     *
     * @Rest\Delete("/{networkId}/{lineId}")
     *
     * @Security("has_role('ROLE_NETWORK_DELETE')")
     *
     * @param string $networkId
     * @param string $lineId
     *
     * @return View
     */
    public function deleteAction($networkId, $lineId)
    {
        $line = $this->getLineManager()->findOneByNetworkAndId($networkId, $lineId);
        if (!$line) {
            throw $this->createNotFoundException();
        }
        $this->getLineManager()->delete($line);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @return \EO\NetworkBundle\Manager\DBAL\LineManagerInterface
     */
    private function getLineManager()
    {
        return $this->get('eo_network.manager.line');
    }

    /**
     * @return \EO\NetworkBundle\Manager\DBAL\NetworkManagerInterface
     */
    private function getNetworkManager()
    {
        return $this->get('eo_network.manager.network');
    }
}
