<?php

namespace EO\NetworkBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\NetworkBundle\Model\NetworkInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class NetworkController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All Networks.",
     *  output={
     *      "class"="array<EO\NetworkBundle\Model\Network>",
     *      "groups"={"basic", "company"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return View
     */
    public function allAction()
    {
        if ($this->isGranted('ROLE_STAFF')) {
            if ($this->isGranted('ROLE_NETWORK_LIST')) {
                $networks = $this->getNetworkManager()->findAllByCompany($this->getUser()->getCompany());
                return $this->createView(array('networks' => $networks), Codes::HTTP_OK, array('basic', 'status'));
            }
            throw $this->createAccessDeniedException();
        }
        $networks = $this->getNetworkManager()->findAllEnable();
        return $this->createView(array('networks' => $networks), Codes::HTTP_OK, array('basic', 'company'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create network.",
     *  input="eo_network",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Network",
     *      "groups"={"basic", "full_network"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_NETWORK_CREATE')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        /** @var NetworkInterface $network */
        $network = $this->getNetworkManager()->create();
        $network->setCompany($this->getUser()->getCompany());
        $form = $this->createRestForm('eo_network', $network, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getNetworkManager()->update($network);
            return $this->createView($network, Codes::HTTP_CREATED, array("basic", 'full_network'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get network.",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Network",
     *      "groups"={"basic", "full_network"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_NETWORK_GET')")
     *
     * @param string $networkId
     *
     * @return View
     */
    public function getAction($networkId)
    {
        $network = $this->getNetworkManager()->find($networkId);
        if (!$network) {
            throw $this->createNotFoundException();
        }
        return $this->createView($network, Codes::HTTP_OK, array("basic", 'full_network'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update a Network in the Company by the Id.",
     *  input="eo_network",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Network",
     *      "groups"={"basic", "full_network"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put
     *
     * @Security("has_role('ROLE_NETWORK_UPDATE')")
     *
     * @param Request $request
     * @param string  $networkId
     *
     * @return View
     */
    public function putAction(Request $request, $networkId)
    {
        $network = $this->getNetworkManager()->find($networkId);
        if (!$network) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_network', $network);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getNetworkManager()->update($network);
            return $this->createView($network, Codes::HTTP_ACCEPTED, array("basic", 'full_network'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Change the status of a Network in the Company by the Id.",
     *  input="eo_network_status",
     *  output={
     *      "class"="EO\NetworkBundle\Model\Network",
     *      "groups"={"basic", "full_network"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_NETWORK_UPDATE')")
     *
     * @param Request $request
     * @param string  $networkId
     *
     * @return View
     */
    public function patchAction(Request $request, $networkId)
    {
        $network = $this->getNetworkManager()->find($networkId);
        if (!$network) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_network_status', $network);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getNetworkManager()->update($network);
            return $this->createView($network, Codes::HTTP_ACCEPTED, array("basic", 'full_network'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete a Network in the Company by the Id.",
     *  statusCodes={
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid",
     *      Codes::HTTP_ACCEPTED="Returned when the Network is successfully deleted"
     *  }
     * )
     *
     * @Rest\Delete
     *
     * @Security("has_role('ROLE_NETWORK_DELETE')")
     *
     * @param string $networkId
     *
     * @return View
     */
    public function deleteAction($networkId)
    {
        $network = $this->getNetworkManager()->find($networkId);
        if (!$network) {
            throw $this->createNotFoundException();
        }
        $this->getNetworkManager()->delete($network);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @return \EO\NetworkBundle\Manager\DBAL\NetworkManagerInterface
     */
    private function getNetworkManager()
    {
        return $this->get('eo_network.manager.network');
    }
}
