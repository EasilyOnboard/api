<?php

namespace EO\NetworkBundle\PathFinding;

use Doctrine\Common\Collections\ArrayCollection;
use EO\NetworkBundle\Manager\DBAL\LineManagerInterface;
use EO\NetworkBundle\Manager\DBAL\StationManagerInterface;
use EO\NetworkBundle\Manager\Model\NetworkManagerInterface;
use EO\NetworkBundle\Model\ServeInterface;
use EO\NetworkBundle\Model\StationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PathFinding implements PathFindingInterface
{
    /**
     * @var NetworkManagerInterface
     */
    private $networkManager;

    /**
     * @var LineManagerInterface
     */
    private $lineManager;

    /**
     * @var StationManagerInterface
     */
    private $stationManager;

    /**
     * @var ArrayCollection
     */
    private $closedSet;

    public function __construct(ContainerInterface $container)
    {
        $this->networkManager = $container->get('eo_network.manager.network');
        $this->lineManager = $container->get('eo_network.manager.line');
        $this->stationManager = $container->get('eo_network.manager.station');
        $this->closedSet = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function find(StationInterface $stationA, StationInterface $stationB)
    {
        $result = array();

        if ($stationA === null || $stationB === null) {
            return $result;
        }
        $start = new Node();
        $start->setCurrent($stationA);
        $this->closedSet->add($stationA);

        $end = new Node();
        $end->setCurrent($stationB);

        for ($depth = 0; $depth < 50; $depth++) {
            $node = $this->depthSearch($start, $end, $depth);
            if ($node !== null) {
                while ($node !== null) {
                    $result[] = $node->getCurrent();
                    $node = $node->getParent();
                }
                $result = array_reverse($result);
                break;
            }
        }
        return $result;
    }

    /**
     * @param Node $node
     * @param Node $end
     * @param integer $depth
     * @return Node
     */
    private function depthSearch(Node $node, Node $end, $depth)
    {
        if ($depth > 0) {
            $configs = $this->nextConfig($node);
            /** @var Node $config */
            foreach ($configs as $config) {
                $node->addChild($config);
                $this->closedSet->add($config->getCurrent());
                $result = $this->depthSearch($config, $end, $depth - 1);
                if ($result !== null) {
                    return $result;
                }
                $node->removeChild($config);
                $this->closedSet->removeElement($config->getCurrent());
            }
            return null;
        } else {
            return ($node->equal($end)) ? $node : null;
        }
    }

    /**
     * @param Node $node
     * @return array
     */
    private function nextConfig(Node $node)
    {
        $nodes = array();
        $serves = $node->getCurrent()->getServes();
        /** @var ServeInterface $serve */
        foreach ($serves as $serve) {
            $previous = null;
            $next = null;
            if (!$serve->isOneWay() || ($serve->isOneWay() && $serve->isForward())) {
                $next = $this->stationManager->findOneByOrderOnLine($serve->getLine(), $serve->getOrder() + 1);
            }
            if (!$serve->isOneWay() || ($serve->isOneWay() && !$serve->isForward())) {
                $previous = $this->stationManager->findOneByOrderOnLine($serve->getLine(), $serve->getOrder() - 1);
            }
            if ($previous !== null && $previous !== $node->getCurrent() && !$this->closedSet->contains($previous)) {
                $newNode = new Node();
                $newNode->setCurrent($previous);
                $nodes[] = $newNode;
            }
            if ($next !== null && $next !== $node->getCurrent() && !$this->closedSet->contains($next)) {
                $newNode = new Node();
                $newNode->setCurrent($next);
                $nodes[] = $newNode;
            }
        }
        return $nodes;
    }
}