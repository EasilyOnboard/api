<?php

namespace EO\NetworkBundle\PathFinding;

use Doctrine\Common\Collections\ArrayCollection;
use EO\NetworkBundle\Model\StationInterface;

class Node
{
    /**
     * @var StationInterface
     */
    private $current;

    /**
     * @var Node
     */
    private $parent;

    /**
     * @var ArrayCollection
     */
    private $children;

    /**
     * @var integer
     */
    private $depth;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return StationInterface
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * @return StationInterface
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @return integer
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param StationInterface $station
     *
     * @return Node
     */
    public function setCurrent(StationInterface $station)
    {
        $this->current = $station;

        return $this;
    }

    /**
     * @param Node $node
     *
     * @return Node
     */
    public function setParent(Node $node = null)
    {
        $this->parent = $node;

        return $this;
    }

    /**
     * @param integer $depth
     *
     * @return Node
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * @param Node $node
     *
     * @return Node
     */
    public function addChild(Node $node)
    {
        if (!$this->children->contains($node)) {
            $this->children->add($node);
            $node->setParent($this);
        }

        return $this;
    }

    /**
     * @param Node $node
     *
     * @return Node
     */
    public function removeChild(Node $node)
    {
        if ($this->children->contains($node)) {
            $this->children->removeElement($node);
            $node->setParent(null);
        }

        return $this;
    }

    /**
     * @param Node $other
     *
     * @return bool
     */
    public function equal(Node $other)
    {
        return ($this->current->getId() === $other->current->getId());
    }
}