<?php

namespace EO\NetworkBundle\PathFinding;

use EO\NetworkBundle\Model\StationInterface;

interface PathFindingInterface
{
    /**
     * Find the path between two stations.
     *
     * @param StationInterface $stationA
     * @param StationInterface $stationB
     * @return array
     */
    public function find(StationInterface $stationA, StationInterface $stationB);
}