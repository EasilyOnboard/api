<?php

namespace EO\NetworkBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\NetworkBundle\Model\NetworkInterface;

interface LineTypeManagerInterface extends ManagerInterface
{
    /**
     * @param NetworkInterface $network
     *
     * @return array
     */
    public function findAllByNetwork(NetworkInterface $network);
}