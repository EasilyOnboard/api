<?php

namespace EO\NetworkBundle\Manager\DBAL;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\StationInterface;

interface StationManagerInterface extends ManagerInterface
{
    /**
     * @param CompanyInterface $company
     *
     * @return array
     */
    public function findAllByCompany(CompanyInterface $company);

    /**
     * @param NetworkInterface $network
     *
     * @return array
     */
    public function findAllByNetwork(NetworkInterface $network);

    /**
     * @param LineInterface $line
     *
     * @return array
     */
    public function findAllByLine(LineInterface $line);

    /**
     * @param LineInterface $line
     * @param int $order
     *
     * @return StationInterface
     */
    public function findOneByOrderOnLine(LineInterface $line, $order);

    /**
     * @param string $networkId
     * @param string $lineId
     *
     * @return LineInterface|null
     */
    public function findOneByNetworkAndId($networkId, $lineId);
}
