<?php

namespace EO\NetworkBundle\Manager\DBAL;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\NetworkBundle\Model\NetworkInterface;

interface NetworkManagerInterface extends ManagerInterface
{
    /**
     * @param CompanyInterface $company
     *
     * @return array
     */
    public function findAllByCompany(CompanyInterface $company);

    /**
     * @return array
     */
    public function findAllEnable();

    /**
     * @param string $id
     * @param string $cid
     * @return NetworkInterface
     */
    public function findOneInCompany($id, $cid);

    /**
     * @param string $id
     * @return NetworkInterface
     */
    public function findOneEnable($id);

    /**
     * @param string $id
     * @param string $cid
     * @return NetworkInterface
     */
    public function findOneEnableInCompany($id, $cid);
}
