<?php

namespace EO\NetworkBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\ZoneInterface;

interface ZoneManagerInterface extends ManagerInterface
{
    /**
     * @param NetworkInterface $network
     *
     * @return array
     */
    public function findAllByNetwork(NetworkInterface $network);

    /**
     * @param string $networkId
     * @param string $zoneId
     *
     * @return ZoneInterface|null
     */
    public function findOneByNetworkAndId($networkId, $zoneId);
}