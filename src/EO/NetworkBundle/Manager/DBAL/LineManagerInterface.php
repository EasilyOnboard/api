<?php

namespace EO\NetworkBundle\Manager\DBAL;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\NetworkInterface;

interface LineManagerInterface extends ManagerInterface
{
    /**
     * @param CompanyInterface $company
     *
     * @return array
     */
    public function findAllByCompany(CompanyInterface $company);

    /**
     * @param NetworkInterface $network
     *
     * @return array
     */
    public function findAllByNetwork(NetworkInterface $network);

    /**
     * @param string $networkId
     * @param string $lineId
     *
     * @return LineInterface|null
     */
    public function findOneByNetworkAndId($networkId, $lineId);
}
