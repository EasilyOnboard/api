<?php

namespace EO\NetworkBundle\Manager\ORM;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\ORM\Manager;
use EO\NetworkBundle\Manager\DBAL\LineManagerInterface;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\NetworkInterface;

class LineManager extends Manager implements LineManagerInterface
{
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder('l')
            ->leftJoin('l.network', 'n')
            ->where('n.company = :company')
            ->setParameter('company', $company)
            ->getQuery()
            ->getResult();
    }

    public function findAllByNetwork(NetworkInterface $network)
    {
        $query = $this->repository->createQueryBuilder('l')
            ->where('l.network = :network')
            ->setParameter('network', $network)
            ->getQuery();
        return $this->paginate($query);
    }

    public function findOneByNetworkAndId($networkId, $lineId)
    {
        return $this->repository->createQueryBuilder('l')
            ->join('l.network', 'n')
            ->where('l.id = :id')
            ->andWhere('n.id = :nid')
            ->setParameters(array(
                'id' => $lineId,
                'nid' => $networkId
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function delete($line)
    {
        if ($line instanceof LineInterface) {
            $terminalManager = $this->container->get('eo_terminal.manager.terminal');
            $terminals = $terminalManager->findAllByLine($line);
            foreach ($terminals as $terminal) {
                $terminalManager->delete($terminal);
            }
            parent::delete($line);
        }
    }
}
