<?php

namespace EO\NetworkBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\NetworkBundle\Manager\DBAL\ZoneManagerInterface;
use EO\NetworkBundle\Model\NetworkInterface;

class ZoneManager extends Manager implements ZoneManagerInterface
{
    public function findAllByNetwork(NetworkInterface $network)
    {
        return $this->repository->findBy(array('network' => $network));
    }

    public function findOneByNetworkAndId($networkId, $zoneId)
    {
        return $this->repository->createQueryBuilder('z')
            ->leftJoin('z.network', 'n')
            ->where('z.id = :id')
            ->andWhere('n.id = :nid')
            ->setParameters(array(
                'id' => $zoneId,
                'nid' => $networkId
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }
}
