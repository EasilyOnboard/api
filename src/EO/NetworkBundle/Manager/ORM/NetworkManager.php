<?php

namespace EO\NetworkBundle\Manager\ORM;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\ORM\Manager;
use EO\NetworkBundle\Manager\DBAL\NetworkManagerInterface;

class NetworkManager extends Manager implements NetworkManagerInterface
{
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->findByCompany($company);
    }

    public function findAllEnable()
    {
        return $this->repository->findBy(array('status' => true));
    }

    public function findOneEnable($id)
    {
        return $this->repository->findOneBy(array(
            'id' => $id,
            'status' => true
        ));
    }

    public function findOneInCompany($id, $cid)
    {
        return $this->repository->createQueryBuilder('n')
            ->join('n.company', 'c')
            ->where('n.id = :id')
            ->andWhere('c.id = :cid')
            ->setParameters(array(
                'id' => $id,
                'cid' => $cid
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneEnableInCompany($id, $cid)
    {
        return $this->repository->createQueryBuilder('n')
            ->join('n.company', 'c')
            ->where('n.id = :id')
            ->andWhere('c.id = :cid')
            ->andWhere('n.status = :status')
            ->setParameters(array(
                'id' => $id,
                'cid' => $cid,
                'status' => true
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }
}