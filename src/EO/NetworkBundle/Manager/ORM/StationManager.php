<?php

namespace EO\NetworkBundle\Manager\ORM;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\ORM\Manager;
use EO\NetworkBundle\Manager\DBAL\StationManagerInterface;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\NetworkInterface;

class StationManager extends Manager implements StationManagerInterface
{
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder('s')
            ->leftJoin('s.network', 'n')
            ->where('n.company = :company')
            ->setParameter('company', $company)
            ->getQuery()
            ->getResult();
    }

    public function findAllByNetwork(NetworkInterface $network)
    {
        $query = $this->repository->createQueryBuilder('s')
            ->where('s.network = :network')
            ->setParameter('network', $network)
            ->getQuery();
        return $this->paginate($query);
    }

    public function findAllByLine(LineInterface $line)
    {
        return $this->repository->createQueryBuilder('s')
            ->leftJoin('s.serves', 'serve')
            ->innerJoin('serve.line', 'l')
            ->where('l = :line')
            ->setParameter('line', $line)
            ->getQuery()
            ->getResult();
    }

    public function findOneByOrderOnLine(LineInterface $line, $order)
    {
        return $this->repository->createQueryBuilder('station')
            ->join('station.serves', 'serve')
            ->join('serve.line', 'line')
            ->where('line = :line')
            ->andWhere('serve.order = :order')
            ->setParameters(array(
                'line' => $line,
                'order' => $order
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findOneByNetworkAndId($networkId, $lineId)
    {
        return $this->repository->createQueryBuilder('s')
            ->join('s.network', 'n')
            ->where('s.id = :id')
            ->andWhere('n.id = :nid')
            ->setParameters(array(
                'id' => $lineId,
                'nid' => $networkId,
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }
}
