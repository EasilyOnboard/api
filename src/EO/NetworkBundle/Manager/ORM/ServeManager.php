<?php

namespace EO\NetworkBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\NetworkBundle\Manager\DBAL\ServeManagerInterface;

class ServeManager extends Manager implements ServeManagerInterface
{
}