<?php

namespace EO\NetworkBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\NetworkBundle\Manager\DBAL\LineTypeManagerInterface;
use EO\NetworkBundle\Model\NetworkInterface;

class LineTypeManager extends Manager implements LineTypeManagerInterface
{
    public function findAllByNetwork(NetworkInterface $network)
    {
        $qb = $this->repository->createQueryBuilder('lt');
        $qb2 = $this->em->createQueryBuilder();
        $query = $qb
            ->distinct(true)
            ->where($qb->expr()
                ->in(
                    'lt.id',
                    $qb2
                        ->select('_lt.id')
                        ->from('EO\CoreBundle\Model\Line', 'l')
                        ->leftJoin('l.lineType', '_lt')
                        ->leftjoin('l.network', 'n')
                        ->where('n.id = :nid')
                        ->getDQL()
                ))
            ->setParameter('nid', $network->getId())
            ->getQuery();
        return $query->getResult();
    }
}
