<?php

namespace EO\NetworkBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class Zone implements ZoneInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $number;

    /**
     * @var float
     */
    protected $price;

    /**
     * @var string
     */
    protected $color;

    /**
     * @var ArrayCollection
     */
    protected $stations;

    /**
     * @var NetworkInterface
     */
    protected $network;

    public function __construct()
    {
        $this->stations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * {@inheritdoc}
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * {@inheritdoc}
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * {@inheritdoc}
     */
    public function getStations()
    {
        return $this->stations;
    }

    /**
     * {@inheritdoc}
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function AddStation(StationInterface $station)
    {
        if (!$this->stations->contains($station)) {
            $this->stations->add($station);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function RemoveStation(StationInterface $station)
    {
        if ($this->stations->contains($station)) {
            $this->stations->removeElement($station);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setNetwork(NetworkInterface $network)
    {
        $this->network = $network;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
