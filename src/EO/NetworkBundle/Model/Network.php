<?php

namespace EO\NetworkBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\CompanyBundle\Model\CompanyInterface;

class Network implements NetworkInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var bool
     */
    protected $status;

    /**
     * @var string
     */
    protected $city;

    /**
     * @var string
     */
    protected $postcode;

    /**
     * @var ArrayCollection
     */
    protected $zones;

    /**
     * @var ArrayCollection
     */
    protected $lines;

    /**
     * @var ArrayCollection
     */
    protected $stations;

    /**
     * @var CompanyInterface
     */
    protected $company;

    public function __construct()
    {
        $this->zones = new ArrayCollection();
        $this->lines = new ArrayCollection();
        $this->stations = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * {@inheritdoc}
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * {@inheritdoc}
     */
    public function getZones()
    {
        return $this->zones;
    }

    /**
     * {@inheritdoc}
     */
    public function getLines()
    {
        return $this->lines;
    }

    /**
     * {@inheritdoc}
     */
    public function getStations()
    {
        return $this->stations;
    }

    /**
     * {@inheritdoc}
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addZone(ZoneInterface $zone)
    {
        if (!$this->zones->contains($zone)) {
            $this->zones->add($zone);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeZone(ZoneInterface $zone)
    {
        if ($this->zones->contains($zone)) {
            $this->zones->removeElement($zone);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addLine(LineInterface $line)
    {
        if (!$this->lines->contains($line)) {
            $this->lines->add($line);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeLine(LineInterface $line)
    {
        if ($this->lines->contains($line)) {
            $this->lines->removeElement($line);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addStation(StationInterface $station)
    {
        if (!$this->stations->contains($station)) {
            $this->stations->add($station);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeStation(StationInterface $station)
    {
        if ($this->stations->contains($station)) {
            $this->stations->removeElement($station);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCompany(CompanyInterface $company)
    {
        $this->company = $company;

        return $this;
    }
}
