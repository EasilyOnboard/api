<?php

namespace EO\NetworkBundle\Model;

interface ServeInterface
{
    /**
     * Get Station.
     *
     * @return StationInterface
     */
    public function getStation();

    /**
     * Get Line.
     *
     * @return LineInterface
     */
    public function getLine();

    /**
     * Get Order.
     *
     * @return Integer
     */
    public function getOrder();

    /**
     * Is One Way.
     *
     * @return boolean
     */
    public function isOneWay();

    /**
     * Is Forward.
     *
     * @return boolean
     */
    public function isForward();

    /**
     * Set Station.
     *
     * @param StationInterface $station
     *
     * @return self
     */
    public function setStation(StationInterface $station);

    /**
     * Set Line.
     *
     * @param LineInterface $line
     *
     * @return self
     */
    public function setLine(LineInterface $line);

    /**
     * Set Order.
     *
     * @param int $order
     *
     * @return self
     */
    public function setOrder($order);

    /**
     * Set One Way.
     *
     * @param boolean $oneWay
     *
     * @return self
     */
    public function setOneWay($oneWay);

    /**
     * Set Forward.
     *
     * @param boolean $forward
     *
     * @return self
     */
    public function setForward($forward);
}