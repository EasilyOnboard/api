<?php

namespace EO\NetworkBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

interface StationInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Name.
     *
     * @return String
     */
    public function getName();

    /**
     * Get Longitude.
     *
     * @return float
     */
    public function getLongitude();

    /**
     * Get Latitude.
     *
     * @return float
     */
    public function getLatitude();

    /**
     * Get Zone.
     *
     * @return ZoneInterface
     */
    public function getZone();

    /**
     * Get Serves.
     *
     * @return ArrayCollection
     */
    public function getServes();

    /**
     * Get Network.
     *
     * @return NetworkInterface
     */
    public function getNetwork();

    /**
     * Set Name.
     *
     * @param String $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * Set Longitude.
     *
     * @param float $longitude
     *
     * @return self
     */
    public function setLongitude($longitude);

    /**
     * Set Latitude.
     *
     * @param float $latitude
     *
     * @return self
     */
    public function setLatitude($latitude);

    /**
     * Set Zone.
     *
     * @param ZoneInterface $zone
     *
     * @return self
     */
    public function setZone(ZoneInterface $zone);

    /**
     * Add Serve if not exist.
     *
     * @param ServeInterface $serve
     *
     * @return self
     */
    public function addServe(ServeInterface $serve);

    /**
     * Remove Serve if exist.
     *
     * @param ServeInterface $serve
     *
     * @return self
     */
    public function removeServe(ServeInterface $serve);

    /**
     * Set Network.
     *
     * @param NetworkInterface $network
     *
     * @return self
     */
    public function setNetwork(NetworkInterface $network);
}
