<?php

namespace EO\NetworkBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class Station implements StationInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var float
     */
    protected $longitude;

    /**
     * @var float
     */
    protected $latitude;

    /**
     * @var ZoneInterface
     */
    protected $zone;

    /**
     * @var ArrayCollection<ServeInterface>
     */
    protected $serves;

    /**
     * @var NetworkInterface
     */
    protected $network;

    public function __construct()
    {
        $this->serves = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getZone()
    {
        return $this->zone;
    }

    public function getServes()
    {
        return $this->serves;
    }

    public function getNetwork()
    {
        return $this->network;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function setZone(ZoneInterface $zone)
    {
        $this->zone = $zone;

        return $this;
    }

    public function addServe(ServeInterface $serve)
    {
        if (!$this->serves->contains($serve)) {
            $this->serves->add($serve);
        }

        return $this;
    }

    public function removeServe(ServeInterface $serve)
    {
        if ($this->serves->contains($serve)) {
            $this->serves->removeElement($serve);
        }

        return $this;
    }

    public function setNetwork(NetworkInterface $network)
    {
        $this->network = $network;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}
