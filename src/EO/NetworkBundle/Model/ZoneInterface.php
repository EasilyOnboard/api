<?php

namespace EO\NetworkBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

interface ZoneInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get Number.
     *
     * @return integer
     */
    public function getNumber();

    /**
     * Get Price.
     *
     * @return float
     */
    public function getPrice();

    /**
     * Get Color.
     *
     * @return string
     */
    public function getColor();

    /**
     * Get Stations.
     *
     * @return ArrayCollection
     */
    public function getStations();

    /**
     * Get Network.
     *
     * @return NetworkInterface
     */
    public function getNetwork();

    /**
     * Set Name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * Set Number.
     *
     * @param integer $number
     *
     * @return self
     */
    public function setNumber($number);

    /**
     * Set Price.
     *
     * @param float $price
     *
     * @return self
     */
    public function setPrice($price);

    /**
     * Set Color.
     *
     * @param string $color
     *
     * @return self
     */
    public function setColor($color);

    /**
     * Add Station.
     *
     * @param StationInterface $station
     *
     * @return self
     */
    public function AddStation(StationInterface $station);

    /**
     * Remove Station.
     *
     * @param StationInterface $station
     *
     * @return self
     */
    public function RemoveStation(StationInterface $station);

    /**
     * Set Network.
     *
     * @param NetworkInterface $network
     *
     * @return self
     */
    public function setNetwork(NetworkInterface $network);
}
