<?php

namespace EO\NetworkBundle\Model;

interface LineTypeInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get Icon.
     *
     * @return string
     */
    public function getIcon();

    /**
     * Set Name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * Set Icon.
     *
     * @param string $icon
     *
     * @return self
     */
    public function setIcon($icon);
}
