<?php

namespace EO\NetworkBundle\Model;

class Serve implements ServeInterface
{
    /**
     * @var StationInterface
     */
    protected $station;

    /**
     * @var LineInterface
     */
    protected $line;

    /**
     * @var int
     */
    protected $order;

    /**
     * @var bool
     */
    protected $oneWay;

    /**
     * @var bool|null
     */
    protected $forward;

    public function __construct()
    {
        $this->order = 0;
        $this->oneWay = false;
        $this->forward = null;
    }

    public function getStation()
    {
        return $this->station;
    }

    public function getLine()
    {
        return $this->line;
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function isOneWay()
    {
        return $this->oneWay;
    }

    public function isForward()
    {
        return $this->forward;
    }

    public function setStation(StationInterface $station)
    {
        $this->station = $station;

        return $this;
    }

    public function setLine(LineInterface $line)
    {
        $this->line = $line;

        return $this;
    }

    public function setOrder($order)
    {
        $this->order = $order;

        return $this;
    }

    public function setOneWay($oneWay)
    {
        $this->oneWay = $oneWay;

        return $this;
    }

    public function setForward($forward)
    {
        $this->forward = $forward;

        return $this;
    }

}