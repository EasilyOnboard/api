<?php

namespace EO\NetworkBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class Line implements LineInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var String
     */
    protected $name;

    /**
     * @var string
     */
    protected $color;

    /**
     * @var LineTypeInterface
     */
    protected $lineType;

    /**
     * @var ArrayCollection<ServeInterface>
     */
    protected $serves;

    /**
     * @var LineInterface
     */
    protected $attached;

    /**
     * @var ArrayCollection<LineInterface>
     */
    protected $attachedLines;

    /**
     * @var NetworkInterface
     */
    protected $network;

    public function __construct()
    {
        $this->serves = new ArrayCollection();
        $this->attachedLines = new ArrayCollection();
        $this->orders = array();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function getLineType()
    {
        return $this->lineType;
    }

    public function getServes()
    {
        return $this->serves;
    }

    public function isAttached()
    {
        return null !== $this->attached;
    }

    public function getAttached()
    {
        return $this->attached;
    }

    public function getAttachedLines()
    {
        return $this->attachedLines;
    }

    public function getNetwork()
    {
        return $this->network;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    public function setLineType(LineTypeInterface $lineType)
    {
        $this->lineType = $lineType;

        return $this;
    }

    public function addServe(ServeInterface $serve)
    {
        if (!$this->serves->contains($serve)) {
            $this->serves->add($serve);
        }

        return $this;
    }

    public function removeServe(ServeInterface $serve)
    {
        if ($this->serves->contains($serve)) {
            $this->serves->removeElement($serve);
        }

        return $this;
    }

    public function setAttached(LineInterface $line)
    {
        $this->attached = $line;

        return $this;
    }

    public function addAttachedLine(LineInterface $line)
    {
        if (!$this->attachedLines->contains($line)) {
            $this->attachedLines->add($line);
        }

        return $this;
    }

    public function removeAttachedLine(LineInterface $line)
    {
        if ($this->attachedLines->contains($line)) {
            $this->attachedLines->removeElement($line);
        }

        return $this;
    }

    public function setNetwork(NetworkInterface $network)
    {
        $this->network = $network;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
