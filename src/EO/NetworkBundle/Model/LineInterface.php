<?php

namespace EO\NetworkBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

interface LineInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Name.
     *
     * @return String
     */
    public function getName();

    /**
     * Get Color.
     *
     * @return string
     */
    public function getColor();

    /**
     * Get Line Type.
     *
     * @return LineTypeInterface
     */
    public function getLineType();

    /**
     * Get Serves.
     *
     * @return ArrayCollection<ServeInterface>
     */
    public function getServes();

    /**
     * Is Attached.
     *
     * @return boolean
     */
    public function isAttached();

    /**
     * Get Attached.
     * 
     * @return LineInterface
     */
    public function getAttached();

    /**
     * Get Attached Lines.
     * 
     * @return ArrayCollection<LineInterface>
     */
    public function getAttachedLines();

    /**
     * Get Network.
     *
     * @return NetworkInterface
     */
    public function getNetwork();

    /**
     * Set Name.
     *
     * @param $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * Set Color.
     *
     * @param string $color
     *
     * @return self
     */
    public function setColor($color);

    /**
     * Set Line Type.
     *
     * @param LineTypeInterface $lineType
     *
     * @return self
     */
    public function setLineType(LineTypeInterface $lineType);

    /**
     * Add Serve.
     *
     * @param ServeInterface $serve
     *
     * @return self
     */
    public function addServe(ServeInterface $serve);

    /**
     * Remove Serve.
     *
     * @param ServeInterface $serve
     *
     * @return self
     */
    public function removeServe(ServeInterface $serve);

    /**
     * Set Attached.
     * 
     * @param LineInterface $line
     * 
     * @return self
     */
    public function setAttached(LineInterface $line);

    /**
     * Add AttachedLine.
     *
     * @param LineInterface $Line
     *
     * @return self
     */
    public function addAttachedLine(LineInterface $Line);

    /**
     * Remove AttachedLine.
     *
     * @param LineInterface $Line
     *
     * @return self
     */
    public function removeAttachedLine(LineInterface $Line);
    
    /**
     * Set Network.
     *
     * @param NetworkInterface $network
     *
     * @return self
     */
    public function setNetwork(NetworkInterface $network);
}
