<?php

namespace EO\NetworkBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\CompanyBundle\Model\CompanyInterface;

interface NetworkInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get city.
     *
     * @return string $city
     */
    public function getCity();

    /**
     * Get postcode.
     *
     * @return string $postcode
     */
    public function getPostcode();

    /**
     * Get Status.
     *
     * @return bool
     */
    public function getStatus();

    /**
     * Get Zones.
     *
     * @return ArrayCollection
     */
    public function getZones();

    /**
     * Get Lines.
     *
     * @return ArrayCollection
     */
    public function getLines();

    /**
     * Get Stations.
     *
     * @return ArrayCollection
     */
    public function getStations();

    /**
     * Get Company.
     *
     * @return CompanyInterface
     */
    public function getCompany();

    /**
     * Set Name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return self
     */
    public function setCity($city);

    /**
     * Set postcode.
     *
     * @param string $postcode
     *
     * @return self
     */
    public function setPostcode($postcode);

    /**
     * Set Status.
     *
     * @param bool $status
     *
     * @return self
     */
    public function setStatus($status);

    /**
     * Add Zone.
     *
     * @param ZoneInterface $zone
     *
     * @return self
     */
    public function addZone(ZoneInterface $zone);

    /**
     * Remove Zone if exists.
     *
     * @param ZoneInterface $zone
     *
     * @return self
     */
    public function removeZone(ZoneInterface $zone);

    /**
     * Add Line.
     *
     * @param LineInterface $line
     *
     * @return self
     */
    public function addLine(LineInterface $line);

    /**
     * Remove Line if exists.
     *
     * @param LineInterface $line
     *
     * @return self
     */
    public function removeLine(LineInterface $line);

    /**
     * Add Station.
     *
     * @param StationInterface $station
     *
     * @return self
     */
    public function addStation(StationInterface $station);

    /**
     * Remove Station if exists.
     *
     * @param StationInterface $station
     *
     * @return self
     */
    public function removeStation(StationInterface $station);

    /**
     * Set Company.
     *
     * @param CompanyInterface $company
     *
     * @return self
     */
    public function setCompany(CompanyInterface $company);
}
