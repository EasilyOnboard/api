<?php

namespace EO\ComplaintBundle\Manager\DBAL;

use EO\ComplaintBundle\Model\ComplaintTypeInterface;
use EO\CoreBundle\Manager\DBAL\ManagerInterface;

interface ComplaintTypeManagerInterface extends ManagerInterface
{
    /**
     * @return ComplaintTypeInterface[]
     */
    public function findAllPrimary();
}