<?php

namespace EO\ComplaintBundle\Manager\DBAL;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\UserBundle\Model\UserInterface;

interface ComplaintManagerInterface extends ManagerInterface
{
    /**
     * @param CompanyInterface $company
     *
     * @return array
     */
    public function findAllByCompany(CompanyInterface $company);

    /**
     * @param UserInterface $customer
     *
     * @return array
     */
    public function findAllByCustomer(UserInterface $customer);

    /**
     * @return array
     */
    public function findAllUnassigned();
}
