<?php

namespace EO\ComplaintBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;

interface ComplaintMessageManagerInterface extends ManagerInterface
{
}
