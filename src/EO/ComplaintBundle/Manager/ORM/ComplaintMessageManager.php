<?php

namespace EO\ComplaintBundle\Manager\ORM;

use EO\ComplaintBundle\Manager\DBAL\ComplaintMessageManagerInterface;
use EO\CoreBundle\Manager\ORM\Manager;

class ComplaintMessageManager extends Manager implements ComplaintMessageManagerInterface
{
}
