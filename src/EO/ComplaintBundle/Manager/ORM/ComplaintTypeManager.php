<?php

namespace EO\ComplaintBundle\Manager\ORM;

use EO\ComplaintBundle\Manager\DBAL\ComplaintTypeManagerInterface;
use EO\CoreBundle\Manager\ORM\Manager;

class ComplaintTypeManager extends Manager implements ComplaintTypeManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllPrimary()
    {
        return $this->repository->createQueryBuilder('ct')
            ->where('ct.type IS NULL')
            ->getQuery()
            ->getResult();
    }
}