<?php

namespace EO\ComplaintBundle\Manager\ORM;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\ComplaintBundle\Manager\DBAL\ComplaintManagerInterface;
use EO\CoreBundle\Manager\ORM\Manager;
use EO\UserBundle\Model\UserInterface;

class ComplaintManager extends Manager implements ComplaintManagerInterface
{
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->findByCompany($company);
    }

    public function findAllByCustomer(UserInterface $customer)
    {
        return $this->repository->findByCustomer($customer);
    }

    public function findAllUnassigned()
    {
        return $this->repository->createQueryBuilder('c')
            ->where('c.staff = :staff')
            ->setParameter('staff', null)
            ->getQuery()
            ->getResult();
    }
}
