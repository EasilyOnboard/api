<?php

namespace EO\ComplaintBundle\Enum;

final class ComplaintStatus
{
    const OPEN = 'open';
    const CLOSED = 'closed';
    const IN_PROGRESS = 'in_progress';
}