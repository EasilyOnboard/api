<?php

namespace EO\ComplaintBundle\Controller;

use EO\ComplaintBundle\Model\ComplaintInterface;
use EO\ComplaintBundle\Model\ComplaintMessageInterface;
use EO\CoreBundle\Controller\EOController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class ComplaintController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All complaints.",
     *  output={
     *      "class"="array<EO\ComplaintBundle\Model\Complaint>",
     *      "groups"={"basic", "staff"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return View
     */
    public function allAction()
    {
        $groups = array('basic');
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $complaints = $this->getComplaintManager()->findAllUnassigned();
            $groups[] = 'staff';
        } elseif ($this->isGranted('ROLE_STAFF') && $this->isGranted('ROLE_COMPLAINT_LIST')) {
            $complaints = $this->getComplaintManager()->findAllByCompany($this->getUser()->getCompany());
            $groups[] = 'staff';
        } elseif ($this->isGranted('ROLE_CUSTOMER')) {
            $complaints = $this->getComplaintManager()->findAllByCustomer($this->getUser());
        } else {
            throw $this->createAccessDeniedException();
        }
        return $this->createView(array('complaints' => $complaints), Codes::HTTP_OK, $groups);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get complaint.",
     *  output={
     *      "class"="EO\ComplaintBundle\Model\Complaint",
     *      "groups"={"basic", "staff", "messages"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param string $complaintId
     *
     * @return View
     */
    public function getAction($complaintId)
    {
        if ($this->isGranted('ROLE_STAFF') && !$this->isGranted('ROLE_COMPLAINT_GET')) {
            throw $this->createAccessDeniedException();
        }
        $complaint = $this->getComplaintById($complaintId);
        $groups = array('basic', 'messages');
        if (!$this->isGranted('ROLE_CUSTOMER')) {
            $groups[] = 'staff';
        }
        return $this->createView($complaint, Codes::HTTP_OK, $groups);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create complaint.",
     *  input="eo_complaint_customer",
     *  output={
     *      "class"="EO\ComplaintBundle\Model\Complaint",
     *      "groups"={"basic", "messages"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        /** @var ComplaintInterface $complaint */
        $complaint = $this->getComplaintManager()->create();
        $complaint->setCustomer($this->getUser());
        $form = $this->createRestForm('eo_complaint_customer', $complaint);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getComplaintManager()->update($complaint);
            return $this->createView($complaint, Codes::HTTP_CREATED, array('basic', 'messages'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create complaint message.",
     *  input="eo_complaint_message",
     *  output={
     *      "class"="EO\ComplaintBundle\Model\ComplaintMessage",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post("/{complaintId}")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @param string $complaintId
     *
     * @return View
     */
    public function postMessageAction(Request $request, $complaintId)
    {
        if ($this->isGranted('ROLE_STAFF') && !$this->isGranted('ROLE_COMPLAINT_MESSAGE')) {
            throw $this->createAccessDeniedException();
        }
        $complaint = $this->getComplaintById($complaintId);
        /** @var ComplaintMessageInterface $message */
        $message = $this->getComplaintMessageManager()->create();
        $message->setComplaint($complaint);
        $message->setUser($this->getUser());
        $form = $this->createRestForm('eo_complaint_message', $message);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getComplaintManager()->update($message);
            return $this->createView($message, Codes::HTTP_CREATED, array('basic'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update complaint.",
     *  input="eo_complaint_full",
     *  output={
     *      "class"="EO\ComplaintBundle\Model\Complaint",
     *      "groups"={"basic", "staff", "messages"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put
     *
     * @Security("has_role('ROLE_COMPLAINT_UPDATE')")
     *
     * @param Request $request
     * @param string  $complaintId
     *
     * @return View
     */
    public function putAction(Request $request, $complaintId)
    {
        $complaint = $this->getComplaintById($complaintId);
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $form = $this->createRestForm('eo_complaint_full', $complaint);
        } else {
            $form = $this->createRestForm('eo_complaint', $complaint);
        }
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getComplaintManager()->update($complaint);
            return $this->createView($complaint, Codes::HTTP_ACCEPTED, array('basic', 'staff', 'messages'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Change complaint status.",
     *  input="eo_complaint_status",
     *  output={
     *      "class"="EO\ComplaintBundle\Model\Complaint",
     *      "groups"={"basic", "messages"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_COMPLAINT_PATCH')")
     *
     * @param Request $request
     * @param string  $complaintId
     *
     * @return View
     */
    public function patchAction(Request $request, $complaintId)
    {
        $complaint = $this->getComplaintById($complaintId);
        $form = $this->createRestForm('eo_complaint_status', $complaint, 'PATCH');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getComplaintManager()->update($complaint);
            return $this->createView($complaint, Codes::HTTP_ACCEPTED, array('basic', 'messages'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Set complaint message saw.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Patch("/{complaintId}/{messageId}")
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param string $complaintId
     * @param string $messageId
     *
     * @return View
     */
    public function patchMessageAction($complaintId, $messageId)
    {
        $complaint = $this->getComplaintById($complaintId);
        /** @var ComplaintMessageInterface $message */
        $message = $this->getComplaintMessageManager()->find($messageId);
        if ($message->getComplaint() !== $complaint) {
            throw $this->createAccessDeniedException();
        }
        $message->setSaw(true);
        $this->getComplaintMessageManager()->update($message);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @param string $complaintId
     *
     * @return ComplaintInterface
     */
    private function getComplaintById($complaintId)
    {
        /** @var ComplaintInterface $complaint */
        $complaint = $this->getComplaintManager()->find($complaintId);
        if (!$complaint) {
            throw $this->createNotFoundException();
        }
        if (($this->isGranted('ROLE_STAFF') && $complaint->getCompany() === $this->getUser()->getCompany()) ||
            ($this->isGranted('ROLE_CUSTOMER') && $complaint->getCustomer() === $this->getUser()) ||
            $this->isGranted('ROLE_SUPER_ADMIN')) {
            return $complaint;
        }
        throw $this->createAccessDeniedException();
    }

    /**
     * @return \EO\ComplaintBundle\Manager\DBAL\ComplaintManagerInterface
     */
    private function getComplaintManager()
    {
        return $this->container->get('eo_complaint.manager.complaint');
    }

    /**
     * @return \EO\ComplaintBundle\Manager\DBAL\ComplaintMessageManagerInterface
     */
    private function getComplaintMessageManager()
    {
        return $this->container->get('eo_complaint.manager.complaint_message');
    }
}
