<?php

namespace EO\ComplaintBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ComplaintTypeController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All complaint types.",
     *  output={
     *      "class"="array<EO\ComplaintBundle\Model\ComplaintType>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return View
     */
    public function allAction()
    {
        $complaintTypes = $this->getComplaintTypeManager()->findAllPrimary();
        return $this->createView(array('complaint_types' => $complaintTypes), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @return \EO\ComplaintBundle\Manager\DBAL\ComplaintTypeManagerInterface
     */
    private function getComplaintTypeManager()
    {
        return $this->container->get('eo_complaint.manager.complaint_type');
    }
}