<?php

namespace EO\ComplaintBundle\Model;

use EO\UserBundle\Model\UserInterface;

interface ComplaintMessageInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Message.
     *
     * @return string
     */
    public function getMessage();

    /**
     * Get Date.
     *
     * @return \DateTime
     */
    public function getDate();

    /**
     * Get User.
     *
     * @return UserInterface
     */
    public function getUser();

    /**
     * Get Saw.
     *
     * @return bool
     */
    public function getSaw();

    /**
     * Get Complaint.
     *
     * @return ComplaintInterface
     */
    public function getComplaint();

    /**
     * Set Message.
     *
     * @param string $message
     *
     * @return self
     */
    public function setMessage($message);

    /**
     * Set User.
     *
     * @param UserInterface $user
     *
     * @return self
     */
    public function setUser(UserInterface $user);

    /**
     * Set Saw.
     *
     * @param bool $saw
     *
     * @return self
     */
    public function setSaw($saw);

    /**
     * Set Complain.
     *
     * @param ComplaintInterface $complain
     *
     * @return self
     */
    public function setComplaint(ComplaintInterface $complain);
}
