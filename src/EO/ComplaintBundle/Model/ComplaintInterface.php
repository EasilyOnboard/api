<?php

namespace EO\ComplaintBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\CompanyBundle\Model\CompanyInterface;
use EO\UserBundle\Model\UserInterface;

interface ComplaintInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Body.
     *
     * @return string
     */
    public function getBody();

    /**
     * Get Comment.
     *
     * @return string
     */
    public function getComment();

    /**
     * Get Created.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Get Updated.
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Get Closed.
     *
     * @return \DateTime
     */
    public function getClosedAt();

    /**
     * Get Body.
     *
     * @return string
     */
    public function getStatus();

    /**
     * Get Type.
     *
     * @return ComplaintTypeInterface
     */
    public function getType();

    /**
     * Get Customer.
     *
     * @return UserInterface
     */
    public function getCustomer();

    /**
     * Get Staff.
     *
     * @return UserInterface
     */
    public function getStaff();

    /**
     * Get Company.
     *
     * @return CompanyInterface
     */
    public function getCompany();

    /**
     * Get Messages.
     *
     * @return ArrayCollection
     */
    public function getMessages();

    /**
     * Set Comment.
     *
     * @param string $comment
     *
     * @return self
     */
    public function setComment($comment);

    /**
     * Set updated at.
     *
     * @param \DateTime $updatedAt
     *
     * @return ComplaintInterface
     */
    public function setUpdatedAt(\DateTime $updatedAt);

    /**
     * Set closed at.
     *
     * @param \DateTime $closedAt
     *
     * @return ComplaintInterface
     */
    public function setClosedAt(\DateTime $closedAt);

    /**
     * Set Status.
     *
     * @param string $status
     *
     * @return self
     */
    public function setStatus($status);

    /**
     * Set Type.
     *
     * @param ComplaintTypeInterface $type
     *
     * @return self
     */
    public function setType(ComplaintTypeInterface $type);

    /**
     * Set Customer.
     *
     * @param UserInterface $customer
     *
     * @return self
     */
    public function setCustomer(UserInterface $customer);

    /**
     * Set Staff.
     *
     * @param UserInterface $staff
     *
     * @return self
     */
    public function setStaff(UserInterface $staff);

    /**
     * Set Company.
     *
     * @param CompanyInterface $company
     *
     * @return self
     */
    public function setCompany(CompanyInterface $company);

    /**
     * Add Message.
     *
     * @param ComplaintMessageInterface $message
     *
     * @return self
     */
    public function addMessage(ComplaintMessageInterface $message);

    /**
     * Remove Message.
     *
     * @param ComplaintMessageInterface $message
     *
     * @return self
     */
    public function removeMessage(ComplaintMessageInterface $message);
}
