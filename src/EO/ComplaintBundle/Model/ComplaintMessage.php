<?php

namespace EO\ComplaintBundle\Model;

use EO\UserBundle\Model\UserInterface;

class ComplaintMessage implements ComplaintMessageInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @var bool
     */
    protected $saw;

    /**
     * @var ComplaintInterface
     */
    protected $complaint;

    public function __construct()
    {
        $this->date = new \DateTime('now');
        $this->saw = false;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function getSaw()
    {
        return $this->saw;
    }

    /**
     * {@inheritdoc}
     */
    public function getComplaint()
    {
        return $this->complaint;
    }

    /**
     * {@inheritdoc}
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setUser(UserInterface $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setSaw($saw)
    {
        $this->saw = $saw;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setComplaint(ComplaintInterface $complaint)
    {
        $this->complaint = $complaint;

        return $this;
    }
}
