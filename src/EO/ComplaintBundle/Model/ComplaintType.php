<?php

namespace EO\ComplaintBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class ComplaintType implements ComplaintTypeInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var ComplaintTypeInterface
     */
    protected $type;

    /**
     * @var ArrayCollection
     */
    protected $subtypes;

    public function __construct()
    {
        $this->subtypes = new ArrayCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * {@inheritdoc}
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubtypes()
    {
        return $this->subtypes;
    }

    /**
     * {@inheritdoc}
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setType(ComplaintTypeInterface $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addSubtype(ComplaintTypeInterface $type)
    {
        if (!$this->subtypes->contains($type)) {
            $this->subtypes->add($type);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeSubtype(ComplaintTypeInterface $type)
    {
        if ($this->subtypes->contains($type)) {
            $this->subtypes->removeElement($type);
        }

        return $this;
    }
}
