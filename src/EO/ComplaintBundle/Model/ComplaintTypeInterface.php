<?php

namespace EO\ComplaintBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

interface ComplaintTypeInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Label.
     *
     * @return string
     */
    public function getLabel();

    /**
     * Get Type.
     *
     * @return ComplaintTypeInterface
     */
    public function getType();

    /**
     * Get Subtypes.
     *
     * @return ArrayCollection
     */
    public function getSubtypes();

    /**
     * Set Label.
     *
     * @param string $label
     *
     * @return self
     */
    public function setLabel($label);

    /**
     * Set Type.
     *
     * @param ComplaintTypeInterface $type
     *
     * @return self
     */
    public function setType(ComplaintTypeInterface $type);

    /**
     * Add Subtype.
     *
     * @param ComplaintTypeInterface $type
     *
     * @return self
     */
    public function addSubtype(ComplaintTypeInterface $type);

    /**
     * Remove Subtype.
     *
     * @param ComplaintTypeInterface $type
     *
     * @return self
     */
    public function removeSubtype(ComplaintTypeInterface $type);
}
