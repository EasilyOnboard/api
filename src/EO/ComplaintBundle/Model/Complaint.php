<?php

namespace EO\ComplaintBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\CompanyBundle\Model\CompanyInterface;
use EO\UserBundle\Model\UserInterface;

class Complaint implements ComplaintInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $body;

    /**
     * @var string
     */
    protected $comment;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @var \DateTime
     */
    protected $closedAt;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var ComplaintTypeInterface
     */
    protected $type;

    /**
     * @var UserInterface
     */
    protected $customer;

    /**
     * @var UserInterface
     */
    protected $staff;

    /**
     * @var CompanyInterface
     */
    protected $company;

    /**
     * @var ArrayCollection
     */
    protected $messages;

    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->created = new \DateTime('now');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * {@inheritdoc}
     */
    public function getComment()
    {
        return $this->comment;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getClosedAt()
    {
        return $this->closedAt;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function getStaff()
    {
        return $this->staff;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function setClosedAt(\DateTime $closedAt)
    {
        $this->closedAt = $closedAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setType(ComplaintTypeInterface $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomer(UserInterface $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setStaff(UserInterface $staff)
    {
        $this->staff = $staff;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCompany(CompanyInterface $company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addMessage(ComplaintMessageInterface $message)
    {
        if (!$this->messages->contains($message)) {
            $this->messages->add($message);
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeMessage(ComplaintMessageInterface $message)
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
        }

        return $this;
    }
}
