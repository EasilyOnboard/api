<?php

namespace EO\ComplaintBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

class ComplaintType extends ComplaintStatusType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('comment', 'text')
            ->add('employee', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_user.model.user.class'),
            ))
        ;
    }

    public function getName()
    {
        return 'eo_complaint';
    }
}