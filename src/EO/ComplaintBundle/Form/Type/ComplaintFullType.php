<?php

namespace EO\ComplaintBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

class ComplaintFullType extends ComplaintType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('company', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_company.model.company.class')
            ));
    }

    public function getName()
    {
        return 'eo_complaint_full';
    }
}
