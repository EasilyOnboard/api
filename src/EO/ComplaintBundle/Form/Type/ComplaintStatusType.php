<?php

namespace EO\ComplaintBundle\Form\Type;

use EO\ComplaintBundle\Enum\ComplaintStatus;
use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ComplaintStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', 'choice', array(
                'choices' => array(
                    ComplaintStatus::OPEN,
                    ComplaintStatus::CLOSED,
                    ComplaintStatus::IN_PROGRESS,
                ),
                'data' => 'o',
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_complaint_status';
    }
}
