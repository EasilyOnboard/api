<?php

namespace EO\ComplaintBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CustomerComplaintType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('body', 'text')
            ->add('type', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_complaint.model.complaint_type.class')
            ));
    }

    public function getName()
    {
        return 'eo_complaint_customer';
    }
}