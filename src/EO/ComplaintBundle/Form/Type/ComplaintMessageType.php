<?php

namespace EO\ComplaintBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ComplaintMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('message', 'text');
    }

    public function getName()
    {
        return 'eo_complaint_message';
    }
}
