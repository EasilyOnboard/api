<?php

namespace EO\InspectorBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\FineBundle\Model\FineInterface;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\TicketBundle\Model\TicketInterface;
use EO\UserBundle\Model\UserInterface;

class Control implements ControlInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var UserInterface
     */
    protected $customer;

    /**
     * @var ArrayCollection
     */
    protected $tickets;

    /**
     * @var LineInterface
     */
    protected $line;

    /**
     * @var StationInterface
     */
    protected $station;

    /**
     * @var FineInterface|null
     */
    protected $fine;

    /**
     * @var UserInterface
     */
    protected $inspector;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    public function __construct()
    {
        $this->tickets = new ArrayCollection();
        $this->createdAt = new \DateTime('now');
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @inheritDoc
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * {@inheritdoc}
     */
    public function getTickets()
    {
        return $this->tickets;
    }

    /**
     * {@inheritdoc}
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * {@inheritdoc}
     */
    public function getStation()
    {
        return $this->station;
    }

    /**
     * {@inheritdoc}
     */
    public function getFine()
    {
        return $this->fine;
    }

    /**
     * {@inheritdoc}
     */
    public function getInspector()
    {
        return $this->inspector;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setCustomer(UserInterface $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addTicket(TicketInterface $ticket)
    {
        if (!$this->tickets->contains($ticket)) {
            $this->tickets->add($ticket);
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeTicket(TicketInterface $ticket)
    {
        if ($this->tickets->contains($ticket)) {
            $this->tickets->removeElement($ticket);
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setLine(LineInterface $line)
    {
        $this->line = $line;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setStation(StationInterface $station)
    {
        $this->station = $station;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setFine(FineInterface $fine = null)
    {
        $this->fine = $fine;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setInspector(UserInterface $inspector)
    {
        $this->inspector = $inspector;

        return $this;
    }
}