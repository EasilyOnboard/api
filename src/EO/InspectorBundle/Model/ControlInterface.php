<?php

namespace EO\InspectorBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\FineBundle\Model\FineInterface;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\TicketBundle\Model\TicketInterface;
use EO\UserBundle\Model\UserInterface;

interface ControlInterface
{
    /**
     * @return string
     */
    public function getId();

    /**
     * @return integer
     */
    public function getStatus();

    /**
     * @return UserInterface
     */
    public function getCustomer();

    /**
     * @return ArrayCollection
     */
    public function getTickets();

    /**
     * @return LineInterface
     */
    public function getLine();

    /**
     * @return StationInterface
     */
    public function getStation();

    /**
     * @return FineInterface|null
     */
    public function getFine();

    /**
     * @return UserInterface
     */
    public function getInspector();

    /**
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * @param integer $status
     *
     * @return ControlInterface
     */
    public function setStatus($status);

    /**
     * @param UserInterface $customer
     *
     * @return ControlInterface
     */
    public function setCustomer(UserInterface $customer);

    /**
     * @param TicketInterface $ticket
     *
     * @return ControlInterface
     */
    public function addTicket(TicketInterface $ticket);

    /**
     * @param TicketInterface $ticket
     *
     * @return ControlInterface
     */
    public function removeTicket(TicketInterface $ticket);

    /**
     * @param LineInterface $line
     *
     * @return ControlInterface
     */
    public function setLine(LineInterface $line);

    /**
     * @param StationInterface $station
     *
     * @return ControlInterface
     */
    public function setStation(StationInterface $station);

    /**
     * @param FineInterface|null $fine
     *
     * @return ControlInterface
     */
    public function setFine(FineInterface $fine = null);

    /**
     * @param UserInterface $inspector
     *
     * @return ControlInterface
     */
    public function setInspector(UserInterface $inspector);
}