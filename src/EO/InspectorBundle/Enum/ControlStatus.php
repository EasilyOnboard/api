<?php

namespace EO\InspectorBundle\Enum;

class ControlStatus
{
    const TICKET_VALID = 0;
    const TICKET_EXPIRED = 1;
    const NO_TICKET = 2;
}