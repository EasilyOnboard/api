<?php

namespace EO\InspectorBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\InspectorBundle\Model\Control;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class ControlController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Control user.",
     *  input="eo_control",
     *  output={
     *      "class"="EO\InspectorBundle\Model\Control",
     *      "groups"={"basic", "full_base_ticket", "full_ticket"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when is valid",
     *      Codes::HTTP_NOT_ACCEPTABLE="Returned when is not valid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_INSPECTOR')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        $control = new Control();
        $control->setInspector($this->getUser());
        $form = $this->createRestForm('eo_control', $control, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $control = $this->container->get('eo_ticket.handler.ticket')->handleControl($control);
            $this->getControlManager()->update($control);
            return $this->createView($control, Codes::HTTP_ACCEPTED, array('basic', 'full_base_ticket', 'full_ticket'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All controls.",
     *  output={
     *      "class"="array<EO\InspectorBundle\Model\Control>",
     *      "groups"={"basic", "full_base_ticket", "full_ticket"}
     *  },
     *  parameters={
     *      {"name"="page", "dataType"="integer", "required"=false, "description"="Page number"},
     *      {"name"="limit", "dataType"="integer", "required"=false, "description"="Offset per page, 20 by default"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when is valid",
     *      Codes::HTTP_NOT_ACCEPTABLE="Returned when is not valid"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_INSPECTOR')")
     *
     * @return View
     */
    public function allAction()
    {
        $controls = $this->container->get('eo_inspector.manager.control')->findAllByInspector($this->getUser());
        return $this->createView(array('controls' => $controls), Codes::HTTP_OK, array('basic', 'full_base_ticket', 'full_ticket'));
    }

    /**
     * @return \EO\InspectorBundle\Manager\DBAL\ControlManagerInterface
     */
    private function getControlManager()
    {
        return $this->container->get('eo_inspector.manager.control');
    }
}