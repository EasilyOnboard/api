<?php

namespace EO\InspectorBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\InspectorBundle\Model\Control;
use Symfony\Component\Form\FormBuilderInterface;

class ControlType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $customers = $this->container->get('eo_user.manager.user')->findAllCustomers();
        /** @var Control $control */
        $control = $builder->getData();
        $stations = array();
        $lines = array();
        if ($control !== null) {
            $company = $control->getInspector()->getCompany();
            $stations = $this->container->get('eo_network.manager.station')->findAllByCompany($company);
            $lines = $this->container->get('eo_network.manager.line')->findAllByCompany($company);
        }
        $builder
            ->add('customer', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_user.model.user.class'),
                'choices' => $customers
            ))
            ->add('line', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.line.class'),
                'choices' => $lines
            ))
            ->add('station', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.station.class'),
                'choices' => $stations
            ))
        ;
    }

    public function getName()
    {
        return 'eo_control';
    }
}