<?php

namespace EO\InspectorBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\InspectorBundle\Model\ControlInterface;
use EO\UserBundle\Model\UserInterface;

interface ControlManagerInterface extends ManagerInterface
{
    /**
     * @param UserInterface $inspector
     *
     * @return ControlInterface[]
     */
    public function findAllByInspector(UserInterface $inspector);

    /**
     * @param UserInterface $inspector
     *
     * @return ControlInterface|null
     */
    public function findLast(UserInterface $inspector);
}