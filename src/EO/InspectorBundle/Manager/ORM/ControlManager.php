<?php

namespace EO\InspectorBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\InspectorBundle\Manager\DBAL\ControlManagerInterface;
use EO\UserBundle\Model\UserInterface;

class ControlManager extends Manager implements ControlManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByInspector(UserInterface $inspector)
    {
        return $this->paginate($this->repository->createQueryBuilder('c')
            ->where('c.inspector = :inspector')
            ->orderBy('c.createdAt', 'DESC')
            ->getQuery());
    }

    /**
     * {@inheritdoc}
     */
    public function findLast(UserInterface $inspector)
    {
        return $this->repository->createQueryBuilder('c')
            ->where('c.inspector = :inspector')
            ->orderBy('c.createdAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}