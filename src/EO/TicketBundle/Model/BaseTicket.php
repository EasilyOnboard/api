<?php

namespace EO\TicketBundle\Model;

use EO\NetworkBundle\Model\LineTypeInterface;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\NetworkBundle\Model\ZoneInterface;
use EO\TicketBundle\Enum\TicketAvailableDays;
use EO\TicketBundle\Enum\TicketAvailablePeople;

class BaseTicket implements BaseTicketInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var String
     */
    protected $name;

    /**
     * @var boolean
     */
    protected $reducedPrice;

    /**
     * @var integer
     */
    protected $availableDays;

    /**
     * @var integer
     */
    protected $availablePeople;

    /**
     * @var array
     */
    protected $path;

    /**
     * @var float
     */
    protected $price;

    /**
     * @var String
     */
    protected $currency;

    /**
     * @var ValidityInterface
     */
    protected $validity;

    /**
     * @var ZoneInterface
     */
    protected $zoneA;

    /**
     * @var ZoneInterface
     */
    protected $zoneB;

    /**
     * @var StationInterface
     */
    protected $stationA;

    /**
     * @var StationInterface
     */
    protected $stationB;

    /**
     * @var LineTypeInterface
     */
    protected $lineType;

    /**
     * @var NetworkInterface
     */
    protected $network;

    public function __construct()
    {
        $this->reducedPrice = false;
        $this->availableDays = TicketAvailableDays::ALL;
        $this->availablePeople = TicketAvailablePeople::ALL;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function isReducedPrice()
    {
        return $this->reducedPrice;
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailableDays()
    {
        return $this->availableDays;
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailablePeople()
    {
        return $this->availablePeople;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * {@inheritdoc}
     */
    public function getValidity()
    {
        return $this->validity;
    }

    /**
     * {@inheritdoc}
     */
    public function getZoneA()
    {
        return $this->zoneA;
    }

    /**
     * {@inheritdoc}
     */
    public function getZoneB()
    {
        return $this->zoneB;
    }

    /**
     * {@inheritdoc}
     */
    public function getStationA()
    {
        return $this->stationA;
    }

    public function getStationB()
    {
        return $this->stationB;
    }

    /**
     * {@inheritdoc}
     */
    public function getLineType()
    {
        return $this->lineType;
    }

    /**
     * {@inheritdoc}
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setReducedPrice($reducedPrice)
    {
        $this->reducedPrice = $reducedPrice;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setAvailableDays($availableDays)
    {
        $this->availableDays = $availableDays;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setAvailablePeople($availablePeople)
    {
        $this->availablePeople = $availablePeople;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPath(array $path = null)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setValidity(ValidityInterface $validity)
    {
        $this->validity = $validity;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setZoneA(ZoneInterface $zoneA = null)
    {
        $this->zoneA = $zoneA;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setZoneB(ZoneInterface $zoneB = null)
    {
        $this->zoneB = $zoneB;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setStationA(StationInterface $stationA = null)
    {
        $this->stationA = $stationA;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setStationB(StationInterface $stationB = null)
    {
        $this->stationB = $stationB;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setLineType(LineTypeInterface $lineType = null)
    {
        $this->lineType = $lineType;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setNetwork(NetworkInterface $network)
    {
        $this->network = $network;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}