<?php

namespace EO\TicketBundle\Model;

use EO\NetworkBundle\Model\StationInterface;
use EO\TicketBundle\Enum\TicketStatus;
use EO\UserBundle\Model\UserInterface;

class Ticket extends BaseTicket implements TicketInterface
{
    /**
     * @var string
     */
    protected $status;

    /**
     * @var bool
     */
    protected $hidden;

    /**
     * @var \DateTime
     */
    protected $buyAt;

    /**
     * @var UserInterface
     */
    protected $buyBy;

    /**
     * @var UserInterface
     */
    protected $ownBy;

    /**
     * @var \DateTime
     */
    protected $transferredAt;

    /**
     * @var \DateTime
     */
    protected $compostedAt;

    /**
     * @var StationInterface
     */
    protected $compostedIn;

    public function __construct()
    {
        parent::__construct();
        $this->hidden = false;
        $this->buyAt = new \DateTime('now');
        $this->status = TicketStatus::VALID;
    }

    /**
     * {@inheritdoc}
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * {@inheritdoc}
     */
    public function isHidden()
    {
        return $this->hidden;
    }

    /**
     * {@inheritdoc}
     */
    public function getBuyAt()
    {
        return $this->buyAt;
    }

    /**
     * {@inheritdoc}
     */
    public function getBuyBy()
    {
        return $this->buyBy;
    }

    /**
     * {@inheritdoc}
     */
    public function getOwnBy()
    {
        return $this->ownBy;
    }

    /**
     * {@inheritdoc}
     */
    public function getTransferredAt()
    {
        return $this->transferredAt;
    }

    /**
     * {@inheritdoc}
     */
    public function getCompostedAt()
    {
        return $this->compostedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function getCompostedIn()
    {
        return $this->compostedIn;
    }

    /**
     * {@inheritdoc}
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setBuyAt(\Datetime $buyAt)
    {
        $this->buyAt = $buyAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setBuyBy(UserInterface $buyBy)
    {
        $this->buyBy = $buyBy;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOwnBy(UserInterface $ownBy)
    {
        $this->ownBy = $ownBy;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTransferredAt(\DateTime $transferredAt)
    {
        $this->transferredAt = $transferredAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCompostedAt(\DateTime $compostedAt)
    {
        $this->compostedAt = $compostedAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCompostedIn(StationInterface $compostedIn)
    {
        $this->compostedIn = $compostedIn;

        return $this;
    }
}
