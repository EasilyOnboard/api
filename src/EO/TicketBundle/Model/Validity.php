<?php

namespace EO\TicketBundle\Model;

use EO\TicketBundle\Enum\ValidityTransit;

class Validity implements ValidityInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var \DateInterval
     */
    protected $term;

    /**
     * @var string
     */
    protected $transitType;

    /**
     * @var float
     */
    protected $multiplier;

    public function __construct()
    {
        $this->transitType = ValidityTransit::SINGLE;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * {@inheritdoc}
     */
    public function getTransitType()
    {
        return $this->transitType;
    }

    /**
     * {@inheritdoc}
     */
    public function getMultiplier()
    {
        return $this->multiplier;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTerm(\DateInterval $term)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTransitType($transitType)
    {
        $this->transitType = $transitType;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setMultiplier($multiplier)
    {
        $this->multiplier = $multiplier;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }
}