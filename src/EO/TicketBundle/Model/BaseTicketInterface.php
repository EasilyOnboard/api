<?php

namespace EO\TicketBundle\Model;

use EO\NetworkBundle\Model\LineTypeInterface;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\NetworkBundle\Model\ZoneInterface;

interface BaseTicketInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get if is reduced price.
     *
     * @return boolean
     */
    public function isReducedPrice();

    /**
     * Get Available People.
     *
     * @return integer
     */
    public function getAvailablePeople();

    /**
     * Get Available Days.
     *
     * @return integer
     */
    public function getAvailableDays();

    /**
     * Get Path.
     *
     * @return array
     */
    public function getPath();

    /**
     * Get Price.
     *
     * @return float
     */
    public function getPrice();

    /**
     * Get Currency.
     *
     * @return String
     */
    public function getCurrency();

    /**
     * Get Validity.
     *
     * @return ValidityInterface
     */
    public function getValidity();

    /**
     * Get Zone A.
     *
     * @return ZoneInterface
     */
    public function getZoneA();

    /**
     * Get Zone B.
     *
     * @return ZoneInterface
     */
    public function getZoneB();

    /**
     * Get Station A.
     *
     * @return StationInterface
     */
    public function getStationA();

    /**
     * Get Station B.
     *
     * @return StationInterface
     */
    public function getStationB();

    /**
     * Get Line Type.
     *
     * @return LineTypeInterface
     */
    public function getLineType();

    /**
     * Get Network.
     *
     * @return NetworkInterface
     */
    public function getNetwork();

    /**
     * Set Name.
     *
     * @param string $name
     *
     * @return BaseTicketInterface
     */
    public function setName($name);

    /**
     * Set Reduced Price.
     *
     * @param boolean $reducedPrice
     *
     * @return BaseTicketInterface
     */
    public function setReducedPrice($reducedPrice);

    /**
     * Set Available Days.
     *
     * @param integer $availableDays
     * @return BaseTicketInterface
     */
    public function setAvailableDays($availableDays);

    /**
     * Set Available People.
     *
     * @param integer $availablePeople
     * @return BaseTicketInterface
     */
    public function setAvailablePeople($availablePeople);

    /**
     * Set Path.
     *
     * @param array $path
     * @return BaseTicketInterface
     */
    public function setPath(array $path);

    /**
     * Set Price.
     *
     * @param float $price
     *
     * @return BaseTicketInterface
     */
    public function setPrice($price);

    /**
     * Set Currency.
     *
     * @param string $currency
     *
     * @return BaseTicketInterface
     */
    public function setCurrency($currency);

    /**
     * Set Validity.
     *
     * @param ValidityInterface $validity
     *
     * @return BaseTicketInterface
     */
    public function setValidity(ValidityInterface $validity);

    /**
     * Set Zone A.
     *
     * @param ZoneInterface $zone
     *
     * @return BaseTicketInterface
     */
    public function setZoneA(ZoneInterface $zone);

    /**
     * Set Zone B.
     *
     * @param ZoneInterface $zone
     *
     * @return BaseTicketInterface
     */
    public function setZoneB(ZoneInterface $zone);

    /**
     * Set Station A.
     *
     * @param StationInterface $stationA
     *
     * @return BaseTicketInterface
     */
    public function setStationA(StationInterface $stationA);

    /**
     * Set Station B.
     *
     * @param StationInterface $stationB
     *
     * @return BaseTicketInterface
     */
    public function setStationB(StationInterface $stationB);

    /**
     * Set Line Type.
     *
     * @param LineTypeInterface $lineType
     *
     * @return BaseTicketInterface
     */
    public function setLineType(LineTypeInterface $lineType);

    /**
     * Set Network.
     *
     * @param NetworkInterface $network
     *
     * @return BaseTicketInterface
     */
    public function setNetwork(NetworkInterface $network);
}