<?php

namespace EO\TicketBundle\Model;

use EO\NetworkBundle\Model\StationInterface;
use EO\UserBundle\Model\UserInterface;

interface TicketInterface extends BaseTicketInterface
{
    /**
     * Get Status.
     *
     * @return string
     */
    public function getStatus();

    /**
     * Is Hidden.
     *
     * @return bool
     */
    public function isHidden();

    /**
     * Get Buy At.
     *
     * @return \DateTime
     */
    public function getBuyAt();

    /**
     * Get Buy By.
     *
     * @return UserInterface
     */
    public function getBuyBy();

    /**
     * Get Own By.
     *
     * @return UserInterface
     */
    public function getOwnBy();

    /**
     * Get Transferred At.
     *
     * @return \DateTime
     */
    public function getTransferredAt();

    /**
     * Get Composted At.
     *
     * @return \DateTime
     */
    public function getCompostedAt();

    /**
     * Get Composted In.
     *
     * @return StationInterface
     */
    public function getCompostedIn();

    /**
     * Set Status.
     *
     * @param String $status
     *
     * @return TicketInterface
     */
    public function setStatus($status);

    /**
     * Set Hidden.
     *
     * @param bool $hidden
     *
     * @return TicketInterface
     */
    public function setHidden($hidden);

    /**
     * Set Buy At.
     *
     * @param \DateTime $buyAt
     *
     * @return TicketInterface
     */
    public function setBuyAt(\Datetime $buyAt);

    /**
     * Set Buy By.
     *
     * @param UserInterface $buyBy
     *
     * @return TicketInterface
     */
    public function setBuyBy(UserInterface $buyBy);

    /**
     * Set Own By.
     *
     * @param UserInterface $ownBy
     *
     * @return TicketInterface
     */
    public function setOwnBy(UserInterface $ownBy);

    /**
     * Set Transferred At
     *
     * @param \DateTime $transferredAt
     *
     * @return TicketInterface
     */
    public function setTransferredAt(\DateTime $transferredAt);

    /**
     * Set Composted At.
     *
     * @param \DateTime $compostedAt
     *
     * @return TicketInterface
     */
    public function setCompostedAt(\DateTime $compostedAt);

    /**
     * Set Composted In.
     *
     * @param StationInterface $compostedIn
     *
     * @return TicketInterface
     */
    public function setCompostedIn(StationInterface $compostedIn);
}
