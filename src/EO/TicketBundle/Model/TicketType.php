<?php

namespace EO\TicketBundle\Model;

use Symfony\Component\Validator\Context\ExecutionContextInterface;

class TicketType extends BaseTicket implements TicketTypeInterface
{
    /**
     * @var bool
     */
    protected $cached;

    public function __construct()
    {
        parent::__construct();
        $this->cached = false;
    }

    public function isCached()
    {
        return $this->cached;
    }

    public function setCache($cache)
    {
        $this->cached = $cache;

        return $this;
    }

    public function validation(ExecutionContextInterface $context)
    {
        if ($this->getLineType() !== null &&
            ($this->getZoneA() !== null || $this->getZoneB() !== null ||
                $this->getStationA() !== null || $this->getStationB() !== null)) {
            $context->addViolation("Don't have to select station or zone if you select a line type.");
        }
        if (($this->getStationA() !== null || $this->getStationB() !== null) &&
            ($this->getZoneA() !== null || $this->getZoneB() !== null)) {
            $context->addViolation("Can't select station(s) and zone at the same time.");
        }
    }
}
