<?php

namespace EO\TicketBundle\Model;

interface TicketTypeInterface extends BaseTicketInterface
{
    /**
     * Get if is cached.
     *
     * @return bool
     */
    public function isCached();

    /**
     * Set cache.
     *
     * @param bool $cache
     *
     * @return TicketTypeInterface
     */
    public function setCache($cache);
}