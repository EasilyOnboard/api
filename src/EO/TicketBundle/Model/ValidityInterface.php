<?php

namespace EO\TicketBundle\Model;

interface ValidityInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get Term.
     *
     * @return \DateInterval
     */
    public function getTerm();

    /**
     * Get Transit Type.
     *
     * @return string
     */
    public function getTransitType();

    /**
     * Get Multiplier.
     *
     * @return float
     */
    public function getMultiplier();

    /**
     * Set Name.
     *
     * @param string $name
     *
     * @return ValidityInterface
     */
    public function setName($name);

    /**
     * Set Term.
     *
     * @param \DateInterval $term
     *
     * @return ValidityInterface
     */
    public function setTerm(\DateInterval $term);

    /**
     * @param string $transitType
     *
     * @return ValidityInterface
     */
    public function setTransitType($transitType);

    /**
     * Set Multiplier.
     *
     * @param float $multiplier
     *
     * @return ValidityInterface
     */
    public function setMultiplier($multiplier);
}