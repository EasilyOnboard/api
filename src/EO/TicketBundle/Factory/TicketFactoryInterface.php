<?php

namespace EO\TicketBundle\Factory;

use EO\TicketBundle\Model\TicketInterface;
use EO\TicketBundle\Model\TicketTypeInterface;
use EO\UserBundle\Model\UserInterface;

interface TicketFactoryInterface
{
    /**
     * Create a new Ticket form a TicketType.
     *
     * @param UserInterface         $customer
     * @param TicketTypeInterface   $ticketType
     *
     * @return TicketInterface
     */
    public function create(UserInterface $customer, TicketTypeInterface $ticketType);
}