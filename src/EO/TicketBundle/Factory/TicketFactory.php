<?php

namespace EO\TicketBundle\Factory;

use EO\TicketBundle\Manager\DBAL\TicketManagerInterface;
use EO\TicketBundle\Model\TicketInterface;
use EO\TicketBundle\Model\TicketTypeInterface;
use EO\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TicketFactory implements TicketFactoryInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var TicketManagerInterface
     */
    private $manager;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->manager = $this->container->get('eo_ticket.manager.ticket');
    }

    /**
     * {@inheritdoc}
     */
    public function create(UserInterface $customer, TicketTypeInterface $ticketType)
    {
        /** @var TicketInterface $ticket */
        $ticket = $this->manager->create();
        $ticket
            ->setBuyBy($customer)
            ->setOwnBy($customer)
            ->setName($ticketType->getName())
            ->setValidity($ticketType->getValidity())
            ->setZoneA($ticketType->getZoneA())
            ->setZoneB($ticketType->getZoneB())
            ->setStationA($ticketType->getStationA())
            ->setStationB($ticketType->getStationB())
            ->setLineType($ticketType->getLineType())
            ->setNetwork($ticketType->getNetwork())
            ->setCurrency($ticketType->getCurrency())
            ->setPrice($ticketType->getPrice())
            ->setReducedPrice($ticketType->isReducedPrice())
            ->setAvailableDays($ticketType->getAvailableDays())
            ->setAvailablePeople($ticketType->getAvailablePeople())
            ->setPath($ticketType->getPath())
        ;
        $this->manager->update($ticket);
        return $ticket;
    }
}
