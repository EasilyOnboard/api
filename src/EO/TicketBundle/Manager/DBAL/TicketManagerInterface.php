<?php

namespace EO\TicketBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\TicketBundle\Model\TicketInterface;
use EO\UserBundle\Model\UserInterface;

interface TicketManagerInterface extends ManagerInterface
{
    /**
     * @param UserInterface $customer
     *
     * @return array
     */
    public function findAllByCustomer(UserInterface $customer);

    /**
     * @param string $id
     * @param string $customerId
     *
     * @return TicketInterface
     */
    public function findOneByCustomerId($id, $customerId);

    /**
     * @param UserInterface $customer
     *
     * @return array
     */
    public function findAllValidByCustomer(UserInterface $customer);

    /**
     * @param UserInterface $customer
     *
     * @return array
     */
    public function findAllCompostedByCustomer(UserInterface $customer);

    /**
     * @param UserInterface $customer
     *
     * @return array
     */
    public function findAllExpiredByCustomer(UserInterface $customer);
}
