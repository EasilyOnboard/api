<?php

namespace EO\TicketBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\TicketBundle\Model\TicketTypeInterface;

interface TicketTypeManagerInterface extends ManagerInterface
{
    /**
     * @param NetworkInterface $network
     *
     * @return array
     */
    public function findAllByNetwork(NetworkInterface $network);

    /**
     * @param TicketTypeInterface $ticketType
     *
     * @return TicketTypeInterface
     */
    public function findEquivalent(TicketTypeInterface $ticketType);
}
