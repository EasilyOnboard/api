<?php

namespace EO\TicketBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\TicketBundle\Enum\TicketStatus;
use EO\TicketBundle\Manager\DBAL\TicketManagerInterface;
use EO\UserBundle\Model\UserInterface;

class TicketManager extends Manager implements TicketManagerInterface
{
    public function findAllByCustomer(UserInterface $customer)
    {
        return $this->repository->findBy(array(
            'ownBy' => $customer,
            'hidden' => false
        ));
    }

    public function findOneByCustomerId($id, $customerId)
    {
        return $this->repository->createQueryBuilder('t')
            ->join('t.ownBy', 'cu')
            ->where('t.id = :id')
            ->andWhere('cu.id = :customerId')
            ->setParameters(array(
                'id' => $id,
                'customerId' => $customerId
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findAllValidByCustomer(UserInterface $customer)
    {
        return $this->findAllByStatusAndCustomer(TicketStatus::VALID, $customer);
    }

    public function findAllCompostedByCustomer(UserInterface $customer)
    {
        return $this->findAllByStatusAndCustomer(TicketStatus::COMPOSTED, $customer);
    }

    public function findAllExpiredByCustomer(UserInterface $customer)
    {
        return $this->findAllByStatusAndCustomer(TicketStatus::EXPIRED, $customer);
    }

    /**
     * @param string $status
     * @param UserInterface $customer
     * @return array
     */
    private function findAllByStatusAndCustomer($status, UserInterface $customer)
    {
        return $this->repository->createQueryBuilder('t')
            ->where('t.ownBy = :customer')
            ->andWhere('t.status = :status')
            ->andWhere('t.hidden = :hidden')
            ->setParameters(array(
                'customer' => $customer,
                'status' => $status,
                'hidden' => false
            ))
            ->getQuery()
            ->getResult();
    }
}
