<?php

namespace EO\TicketBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\TicketBundle\Manager\DBAL\TicketTypeManagerInterface;
use EO\TicketBundle\Model\TicketTypeInterface;

class TicketTypeManager extends Manager implements TicketTypeManagerInterface
{
    public function findAllByNetwork(NetworkInterface $network)
    {
        return $this->repository->findBy(array(
            'network' => $network,
            'cached' => false
        ));
    }

    public function findEquivalent(TicketTypeInterface $ticketType)
    {
        return $this->repository->findOneBy(array(
            'network' => $ticketType->getNetwork(),
            'zoneA' => $ticketType->getZoneA(),
            'zoneB' => $ticketType->getZoneB(),
            'stationA' => $ticketType->getStationA(),
            'stationB' => $ticketType->getStationB(),
            'lineType' => $ticketType->getLineType(),
            'validity' => $ticketType->getValidity(),
        ));
    }
}
