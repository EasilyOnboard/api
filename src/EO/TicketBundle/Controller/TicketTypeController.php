<?php

namespace EO\TicketBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\TicketBundle\Model\TicketTypeInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class TicketTypeController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All Ticket types.",
     *  output={
     *      "class"="array<EO\TicketBundle\Model\TicketType>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param string $networkId
     *
     * @return View
     */
    public function allAction($networkId)
    {
        $groups = array('basic');
        if ($this->isGranted('ROLE_CUSTOMER')) {
            /** @var NetworkInterface $network */
            $network = $this->getNetworkManager()->findOneEnable($networkId);
            $groups[] = 'full_base_ticket';
        } elseif ($this->isGranted('ROLE_STAFF') && $this->isGranted('ROLE_TICKET_TYPE_LIST')) {
            /** @var NetworkInterface $network */
            $network = $this->getNetworkManager()->find($networkId);
        } else {
            throw $this->createAccessDeniedException();
        }
        if (!$network) {
            throw $this->createNotFoundException();
        }
        $ticketTypes = $this->getTicketTypeManager()->findAllByNetwork($network);
        return $this->createView(array('ticket_types' => $ticketTypes), Codes::HTTP_OK, $groups);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create ticket type.",
     *  input="eo_ticket_type",
     *  output={
     *      "class"="EO\TicketBundle\Model\TicketType",
     *      "groups"={"basic", "full_base_ticket"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     * @param string  $networkId
     *
     * @return View
     */
    public function postAction(Request $request, $networkId)
    {
        if ($this->isGranted('ROLE_CUSTOMER')) {
            /** @var NetworkInterface $network */
            $network = $this->getNetworkManager()->findOneEnable($networkId);
        } elseif ($this->isGranted('ROLE_STAFF') && $this->isGranted('ROLE_TICKET_TYPE_CREATE')) {
            /** @var NetworkInterface $network */
            $network = $this->getNetworkManager()->find($networkId);
        } else {
            throw $this->createAccessDeniedException();
        }
        /** @var TicketTypeInterface $ticketType */
        $ticketType = $this->getTicketTypeManager()->create();
        $ticketType->setNetwork($network);
        $ticketType->setCurrency($network->getCompany()->getCurrency());
        if ($this->isGranted('ROLE_CUSTOMER')) {
            $ticketType->setCache(true);
            $form = $this->createRestForm('eo_ticket_type', $ticketType, 'POST');
        } else {
            $form = $this->createRestForm('eo_ticket_type_extend', $ticketType, 'POST', array(
                'validation_groups' => array('intranet')
            ));
        }
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $ticketType = $this->getShopHandler()->newTicketType($ticketType);
            return $this->createView($ticketType, Codes::HTTP_CREATED, array('basic', 'full_base_ticket'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get ticket type.",
     *  output={
     *      "class"="EO\TicketBundle\Model\TicketType",
     *      "groups"={"basic", "full_base_ticket"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_TICKET_TYPE_GET')")
     *
     * @param string $ticketTypeId
     *
     * @return View
     */
    public function getAction($ticketTypeId)
    {
        $ticketType = $this->getTicketTypeManager()->find($ticketTypeId);
        if (!$ticketType) {
            throw $this->createNotFoundException();
        }
        return $this->createView($ticketType, Codes::HTTP_OK, array('basic', 'full_base_ticket'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update ticket type.",
     *  input="eo_ticket_type",
     *  output={
     *      "class"="EO\TicketBundle\Model\TicketType",
     *      "groups"={"basic", "full_base_ticket"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put
     *
     * @Security("has_role('ROLE_TICKET_TYPE_UPDATE')")
     *
     * @param Request $request
     * @param string  $ticketTypeId
     *
     * @return View
     */
    public function putAction(Request $request, $ticketTypeId)
    {
        $ticketType = $this->getTicketTypeManager()->find($ticketTypeId);
        if (!$ticketType) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_ticket_type', $ticketType);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getTicketTypeManager()->update($ticketType);
            return $this->createView($ticketType, Codes::HTTP_ACCEPTED, array('basic', 'full_base_ticket'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete ticket type.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when the Ticket Type is successfully deleted"
     *  }
     * )
     *
     * @Rest\Delete
     *
     * @Security("has_role('ROLE_TICKET_TYPE_DELETE')")
     *
     * @param string $ticketTypeId
     *
     * @return View
     */
    public function deleteAction($ticketTypeId)
    {
        $ticketType = $this->getTicketTypeManager()->find($ticketTypeId);
        if (!$ticketType) {
            throw $this->createNotFoundException();
        }
        $this->getTicketTypeManager()->delete($ticketType);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @return \EO\TicketBundle\Manager\DBAL\TicketTypeManagerInterface
     */
    private function getTicketTypeManager()
    {
        return $this->get('eo_ticket.manager.ticket_type');
    }

    /**
     * @return \EO\NetworkBundle\Manager\DBAL\NetworkManagerInterface
     */
    private function getNetworkManager()
    {
        return $this->get('eo_network.manager.network');
    }

    /**
     * @return \EO\ShopBundle\Handler\ShopHandlerInterface
     */
    private function getShopHandler()
    {
        return $this->get('eo_shop.handler.shop');
    }
}
