<?php

namespace EO\TicketBundle\Controller;

use EO\MiscBundle\Model\NotificationInterface;
use EO\TicketBundle\Enum\TicketStatus;
use EO\TicketBundle\Model\TicketInterface;
use EO\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use EO\CoreBundle\Controller\EOController;

class TicketController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All tickets.",
     *  output={
     *      "class"="array<EO\TicketBundle\Model\Ticket>",
     *      "groups"={"basic", "full_base_ticket", "full_ticket"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful"
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return View
     */
    public function allAction()
    {
        $groups = array('basic');
        if ($this->isGranted('ROLE_CUSTOMER')) {
            $tickets = $this->getTicketManager()->findAllByCustomer($this->getUser());
            /** @var TicketInterface $ticket */
            foreach ($tickets as $ticket) {
                $this->getTicketHandler()->handleValidity($ticket);
            }
            $groups = array_merge($groups, array('full_base_ticket', 'full_ticket'));
        } elseif ($this->isGranted('ROLE_STAFF') && $this->isGranted('ROLE_TICKET_LIST')) {
            $tickets = $this->getTicketManager()->findAll();
        } else {
            throw $this->createAccessDeniedException();
        }
        return $this->createView(array('tickets' => $tickets), Codes::HTTP_OK, $groups);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All tickets by customer.",
     *  output={
     *      "class"="array<EO\TicketBundle\Model\Ticket>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get("/{customerId}/all")
     *
     * @Security("has_role('ROLE_TICKET_LIST')")
     *
     * @param string $customerId
     *
     * @return View
     */
    public function allByCustomerAction($customerId)
    {
        /** @var UserInterface $customer */
        $customer = $this->getUserManager()->find($customerId);
        if (!$customer) {
            throw $this->createNotFoundException();
        }
        $tickets = $this->getTicketManager()->findAllByCustomer($customer);
        return $this->createView(array('tickets' => $tickets), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get ticket.",
     *  output={
     *      "class"="EO\TicketBundle\Model\Ticket",
     *      "groups"={"basic", "full_base_ticket", "full_ticket"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_TICKET_GET')")
     *
     * @param string $ticketId
     *
     * @return View
     */
    public function getAction($ticketId)
    {
        $ticket = $this->getTicketManager()->find($ticketId);
        if (!$ticket) {
            throw $this->createNotFoundException();
        }
        return $this->createView($ticket, Codes::HTTP_OK, array('basic', 'full_base_ticket', 'full_ticket'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Share ticket.",
     *  input="eo_ticket_share",
     *  statusCodes={
     *         Codes::HTTP_ACCEPTED="Returned when successful",
     *         Codes::HTTP_NOT_ACCEPTABLE="Returned when ticket is already composted",
     *         Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *     }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     * @param string  $ticketId
     *
     * @return View
     */
    public function shareAction(Request $request, $ticketId)
    {
        /** @var TicketInterface $ticket */
        $ticket = $this->getTicketManager()->find($ticketId);
        if (!$ticket) {
            throw $this->createNotFoundException();
        }
        if ($ticket->getOwnBy() !== $this->getUser()) {
            return $this->createAccessDeniedException();
        }
        if ($ticket->getStatus() !== TicketStatus::VALID) {
            return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
        }
        $form = $this->createRestForm('eo_ticket_share', $ticket, 'PATCH');
        $form->submit($request->request->all());
        if ($form->isValid())
        {
            $ticket->setTransferredAt(new \DateTime("now"));
            $this->getTicketManager()->update($ticket);
            /** @var NotificationInterface $notification */
            $notification = $this->getNotificationManager()->create();
            $notification->setUser($ticket->getOwnBy());
            $notification->setType('eo_ticket_share');
            $message = $this->getTranslator()->trans('ticket.share', array(
                '%name%' => $this->getUser()->__toString()), 'notification');
            $notification->setMessage($message);
            $notification->setData(array('id' => $ticketId));
            $this->getNotificationManager()->update($notification);
            $this->getNotificationHandler()->handlePushNewNotification($notification);
            return $this->createView(null, Codes::HTTP_ACCEPTED);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Hide ticket.",
     *  statusCodes={
     *         Codes::HTTP_ACCEPTED="Returned when successful",
     *         Codes::HTTP_NOT_ACCEPTABLE="Returned when ticket is not expired",
     *     }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string  $ticketId
     *
     * @return View
     */
    public function patchAction($ticketId)
    {
        /** @var TicketInterface $ticket */
        $ticket = $this->getTicketManager()->find($ticketId);
        if (!$ticket) {
            throw $this->createNotFoundException();
        }
        if ($ticket->getOwnBy() !== $this->getUser()) {
            return $this->createAccessDeniedException();
        }
        if ($ticket->getStatus() !== TicketStatus::EXPIRED) {
            return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
        }
        $ticket->setHidden(true);
        $this->getTicketManager()->update($ticket);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /*
    public function newCustomerCompanyNetworkTicket_typeTicketAction($cuid, $cid, $nid, $ttid)
    {
        $network = $this->getNetworkManager()->findOneInCompany($nid, $cid);
        if (!$network) {
            throw $this->createNotFoundException();
        }
        $customer = $this->getUserManager()->find($cuid);
        if (!$customer) {
            throw $this->createNotFoundException();
        }
        $ticketType = $this->getTicketTypeManager()->findOneByNetwork($ttid, $network);
        if (!$ticketType) {
            throw $this->createNotFoundException();
        }
        $ticket = $this->getTicketFactory()->create($customer, $ticketType);
        $this->getTicketManager()->update($ticket);
        return $this->createView($ticket, Codes::HTTP_CREATED, array('basic', 'full_base_ticket'));
    }*/

    /**
     * @return \EO\TicketBundle\Manager\DBAL\TicketManagerInterface
     */
    private function getTicketManager()
    {
        return $this->get('eo_ticket.manager.ticket');
    }

    /**
     * @return \EO\TicketBundle\Handler\TicketHandlerInterface
     */
    private function getTicketHandler()
    {
        return $this->get('eo_ticket.handler.ticket');
    }

    /**
     * @return \EO\MiscBundle\Manager\DBAL\NotificationManagerInterface
     */
    private function getNotificationManager()
    {
        return $this->get('eo_misc.manager.notification');
    }

    /**
     * @return \EO\MiscBundle\Handler\NotificationHandlerInterface
     */
    private function getNotificationHandler()
    {
        return $this->get('eo_misc.handler.notification');
    }

    /**
     * @return \EO\UserBundle\Manager\DBAL\UserManagerInterface
     */
    private function getUserManager()
    {
        return $this->get('eo_user.manager.user');
    }
}