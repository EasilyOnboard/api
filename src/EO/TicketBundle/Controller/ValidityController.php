<?php

namespace EO\TicketBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\TicketBundle\Manager\DBAL\ValidityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ValidityController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All validity's",
     *  output="array",
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful"
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return View
     */
    public function allAction()
    {
        $validities = $this->getValidityManager()->findAll();
        return $this->createView(array('validities' => $validities), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @return ValidityManagerInterface
     */
    private function getValidityManager()
    {
        return $this->get('eo_ticket.manager.validity');
    }
}