<?php

namespace EO\TicketBundle\Enum;

final class TicketAvailablePeople
{
    const ALL = 1;
    const MINOR = 2;
    const STUDENT = 4;
    const SENIOR = 8;
    const SPECIAL = 16;
}