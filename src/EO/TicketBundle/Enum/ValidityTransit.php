<?php

namespace EO\TicketBundle\Enum;

final class ValidityTransit
{
    const SINGLE = 'single';
    const MULTI = 'multi';
}