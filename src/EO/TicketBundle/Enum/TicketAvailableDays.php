<?php

namespace EO\TicketBundle\Enum;

final class TicketAvailableDays
{
    const ALL = 1;
    const BANK_HOLIDAYS = 2;
    const WEEK_END = 4;
}