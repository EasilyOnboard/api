<?php

namespace EO\TicketBundle\Enum;

final class TicketStatus
{
    const VALID = 'v';
    const COMPOSTED = 'c';
    const EXPIRED = 'e';
}