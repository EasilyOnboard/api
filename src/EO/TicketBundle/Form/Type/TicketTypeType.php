<?php

namespace EO\TicketBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\NetworkBundle\Manager\DBAL\StationManagerInterface;
use EO\NetworkBundle\Manager\DBAL\ZoneManagerInterface;
use EO\TicketBundle\Model\TicketTypeInterface;
use Symfony\Component\Form\FormBuilderInterface;

class TicketTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var TicketTypeInterface $ticketType */
        $ticketType = $builder->getData();

        $stations = array();
        $zones = array();

        if ($ticketType) {
            /** @var StationManagerInterface $stationManager */
            $stationManager = $this->container->get('eo_network.manager.station');
            /** @var ZoneManagerInterface $zoneManager */
            $zoneManager = $this->container->get('eo_network.manager.zone');
            $stations = $stationManager->findAllByNetwork($ticketType->getNetwork());
            $zones = $zoneManager->findAllByNetwork($ticketType->getNetwork());
        }

        $builder
            ->add('station_a', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.station.class'),
                'choices' => $stations
            ))
            ->add('station_b', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.station.class'),
                'choices' => $stations
            ))
            ->add('zone_a', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.zone.class'),
                'choices' => $zones
            ))
            ->add('zone_b', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.zone.class'),
                'choices' => $zones
            ))
            ->add('line_type', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.line_type.class')
            ))
            ->add('validity', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_ticket.model.validity.class')
            ))
        ;
    }

    public function getName()
    {
        return 'eo_ticket_type';
    }
}