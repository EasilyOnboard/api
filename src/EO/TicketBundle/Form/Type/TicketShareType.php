<?php

namespace EO\TicketBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\TicketBundle\Model\TicketInterface;
use EO\UserBundle\Manager\DBAL\UserManagerInterface;
use Symfony\Component\Form\FormBuilderInterface;

class TicketShareType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $members = array();
        /** @var TicketInterface $ticket */
        if (($ticket = $builder->getData()) != null) {
            /** @var UserManagerInterface $userManager */
            $userManager = $this->container->get('eo_user.manager.user');
            $customer = $ticket->getOwnBy();
            $userManager->reload($customer);
            $members = $customer->getFamily()->getMembers()->toArray();
        }
        $builder
            ->add('own_by', $this->container->getParameter('eo_core.form.type.model'), array(
                'class' => $this->container->getParameter('eo_user.model.user.class'),
                'choices' => $members,
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_ticket_share';
    }
}
