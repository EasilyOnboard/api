<?php

namespace EO\TicketBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\TicketBundle\Enum\TicketStatus;
use Symfony\Component\Form\FormBuilderInterface;

class TicketStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', 'choice', array(
                'choices' => array(
                    TicketStatus::VALID,
                    TicketStatus::COMPOSTED,
                    TicketStatus::EXPIRED),
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_ticket_status';
    }
}
