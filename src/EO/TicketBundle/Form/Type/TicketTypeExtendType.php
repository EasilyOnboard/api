<?php

namespace EO\TicketBundle\Form\Type;

use EO\TicketBundle\Model\TicketTypeInterface;
use Symfony\Component\Form\FormBuilderInterface;

class TicketTypeExtendType extends TicketTypeType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        /** @var TicketTypeInterface $ticketType */
        $ticketType = $builder->getData();
        $builder
            ->add('name', 'text')
            ->add('reduced_price', 'checkbox')
            ->add('available_days', 'number', array(
                'precision' => 0
            ))
            ->add('available_people', 'number', array(
                'precision' => 0
            ))
            ->add('price', 'money', array(
                'currency' => ($ticketType) ? $ticketType->getCurrency() : 'EUR'
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_ticket_type_extend';
    }
}