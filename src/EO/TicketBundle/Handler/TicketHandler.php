<?php

namespace EO\TicketBundle\Handler;

use Doctrine\Common\Collections\ArrayCollection;
use EO\CoreBundle\Util\DateIntervalCompare;
use EO\InspectorBundle\Enum\ControlStatus;
use EO\InspectorBundle\Model\Control;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\NetworkBundle\Model\ZoneInterface;
use EO\NetworkBundle\PathFinding\PathFindingInterface;
use EO\TerminalBundle\Enum\TerminalType;
use EO\TicketBundle\Enum\TicketStatus;
use EO\TicketBundle\Manager\DBAL\TicketManagerInterface;
use EO\TicketBundle\Model\TicketInterface;
use EO\TicketBundle\Model\TicketTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TicketHandler implements TicketHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function handleValidity(TicketInterface $ticket)
    {
        if ($ticket->getStatus() === TicketStatus::COMPOSTED) {
            $diff = $ticket->getCompostedAt()->diff(new \DateTime("now"));
            if ($diff->invert == 1 || DateIntervalCompare::compare($diff, $ticket->getValidity()->getTerm()) != -1) {
                $ticket->setStatus(TicketStatus::EXPIRED);
                $this->getTicketManager()->update($ticket);
            }
        }
    }

    public function handlePositionUsage(
        TicketInterface $ticket,
        StationInterface $station,
        LineInterface $line,
        $type = TerminalType::INTERMEDIATE)
    {
        if ($ticket->getNetwork() === $station->getNetwork()) {
            if (null === $ticket->getLineType() &&
                (null !== $ticket->getZoneA() || null !== $ticket->getZoneB() ||
                    null !== $ticket->getStationA() || null !== $ticket->getStationB())) {
                if (null !== $ticket->getZoneA()) {
                    if (null === $ticket->getZoneB() && $ticket->getZoneA() === $station->getZone()) {
                        return true;
                    } else {
                        $zoneANumber = $ticket->getZoneA()->getNumber();
                        $zoneBNumber = $ticket->getZoneB()->getNumber();
                        $currentZone = $station->getZone()->getNumber();
                        if ($zoneANumber <= $currentZone && $zoneBNumber >= $currentZone) {
                            return true;
                        }
                    }
                } elseif (null !== $ticket->getStationA()) {
                    if (null !== $ticket->getStationB()) {
                        if ($type === TerminalType::ENTRY &&
                            ($ticket->getStationA() === $station || $ticket->getStationB() === $station)) {
                            return true;
                        } else {
                            // TODO: Check for intermediate stations transit
                        }
                    } elseif (null !== $ticket->getZoneB()) {
                        if ($type === TerminalType::ENTRY &&
                            ($ticket->getStationA() === $station || $ticket->getZoneB() === $station->getZone())) {
                            return true;
                        } else {
                            // TODO: Check for intermediate stations transit
                        }
                    }
                }
            } elseif ($ticket->getLineType() === $line->getLineType()) {
                return true;
            }
        }
        return false;
    }

    public function handleControl(Control $control)
    {
        $tickets = $this->getTicketManager()->findAllCompostedByCustomer($control->getCustomer());
        /** @var TicketInterface $ticket */
        foreach ($tickets as $ticket) {
            if ($ticket->getNetwork() === $control->getStation()->getNetwork()) {
                $control->addTicket($ticket);
                if ($this->handlePositionUsage($ticket, $control->getStation(), $control->getLine())) {
                    $this->handleValidity($ticket);
                    if ($ticket->getStatus() === TicketStatus::COMPOSTED) {
                        $control->setStatus(ControlStatus::TICKET_VALID);
                    }
                }
            }
        }
        if ($control->getTickets()->isEmpty()) {
            $control->setStatus(ControlStatus::NO_TICKET);
        } elseif (!$control->getTickets()->isEmpty() && !$control->getStatus() !== ControlStatus::TICKET_VALID) {
            $control->setStatus(ControlStatus::TICKET_EXPIRED);
        }
        return $control;
    }

    public function handlePrice(TicketTypeInterface $ticketType)
    {
        $zones = new ArrayCollection();
        if ($ticketType->getStationA() && $ticketType->getStationB()) {
            /** @var PathFindingInterface $pathFinding */
            $pathFinding = $this->container->get('eo_network.path_finding');
            $path = $pathFinding->find($ticketType->getStationA(), $ticketType->getStationB());
            $ids = array();
            /** @var StationInterface $station */
            foreach ($path as $station) {
                $ids[] = $station->getId();
                if (!$zones->contains($station->getZone())) {
                    $zones->add($station->getZone());
                }
            }
            $ticketType->setPath($ids);
        } elseif ($ticketType->getZoneA() && $ticketType->getZoneB()) {
            $zones->add($ticketType->getZoneA());
            $zones->add($ticketType->getZoneB());
            $allZones = $ticketType->getNetwork()->getZones();
            /** @var ZoneInterface $zone */
            foreach ($allZones as $zone) {
                if ($zone->getNumber() > $ticketType->getZoneA()->getNumber() &&
                    $zone->getNumber() < $ticketType->getZoneB()->getNumber()) {
                    $zones->add($zone);
                }
            }
        } elseif ($ticketType->getZoneA()) {
            $zones->add($ticketType->getZoneA());
        }
        $price = 0.;
        /** @var ZoneInterface $zone */
        foreach ($zones as $zone) {
            $price += $zone->getPrice();
        }
        $price = $price * $ticketType->getValidity()->getMultiplier();
        if ($ticketType->isReducedPrice()) {
            $price = $price * 0.6;
        }
        $ticketType->setPrice($price);
    }

    /**
     * @return TicketManagerInterface
     */
    public function getTicketManager()
    {
        return $this->container->get('eo_ticket.manager.ticket');
    }
}