<?php

namespace EO\TicketBundle\Handler;

use EO\InspectorBundle\Model\Control;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\TerminalBundle\Enum\TerminalType;
use EO\TicketBundle\Model\TicketInterface;
use EO\TicketBundle\Model\TicketTypeInterface;

interface TicketHandlerInterface
{
    /**
     * @param TicketInterface $ticket
     */
    public function handleValidity(TicketInterface $ticket);

    /**
     * @param TicketInterface $ticket
     * @param StationInterface $station
     * @param LineInterface $line
     * @param string $type
     *
     * @return bool
     */
    public function handlePositionUsage(
        TicketInterface $ticket,
        StationInterface $station,
        LineInterface $line,
        $type = TerminalType::INTERMEDIATE);

    /**
     * @param Control $control
     *
     * @return Control
     */
    public function handleControl(Control $control);

    /**
     * @param TicketTypeInterface $ticketType
     */
    public function handlePrice(TicketTypeInterface $ticketType);
}