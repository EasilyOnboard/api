<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\LineType;
use EO\CoreBundle\Document\Network;
use EO\CoreBundle\Document\Station;
use EO\CoreBundle\Document\TicketType;
use EO\CoreBundle\Document\Zone;

class TestLoadTicketTypeData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $ticketTypes = $this->getModelFixtures();

        /** @var ManagerInterface $ticketTypeManager */
        $ticketTypeManager = $this->container->get('eo_core.ticket_type_manager');

        foreach ($ticketTypes['Ticket_types'] as $reference => $data) {
            /** @var TicketType $ticketType */
            $ticketType = $ticketTypeManager->create();

            $ticketType->setName($data['name']);
            $ticketType->setPrice($data['price']);
            $ticketType->setCurrency($data['currency']);
            $ticketType->setTerm(new \DateInterval('PT'.$data['term'].'H0M0S'));
            if (!empty($data['zoneA'])) {
                /** @var Zone $zone */
                $zone = $this->getReference('Zone_'.$data['zoneA']);
                $ticketType->setZoneA($zone);
            }
            if (!empty($data['zoneB'])) {
                /** @var Zone $zone */
                $zone = $this->getReference('Zone_'.$data['zoneB']);
                $ticketType->setZoneB($zone);
            }
            if (!empty($data['stationA'])) {
                /** @var Station $station */
                $station = $this->getReference('Station_'.$data['stationA']);
                $ticketType->setStationA($station);
            }
            if (!empty($data['stationB'])) {
                /** @var Station $station */
                $station = $this->getReference('Station_'.$data['stationB']);
                $ticketType->setStationB($station);
            }
            if (!empty($data['lineType'])) {
                /** @var LineType $lineType */
                $lineType = $this->getReference('LineType_'.$data['lineType']);
                $ticketType->setLineType($lineType);
            }
            if (!empty($data['network'])) {
                /** @var Network $network */
                $network = $this->getReference('Network_'.$data['network']);
                $ticketType->setNetwork($network);
            }

            $ticketTypeManager->update($ticketType);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'ticket_types';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}
