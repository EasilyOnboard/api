<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\Customer;
use EO\CoreBundle\Document\LineType;
use EO\CoreBundle\Document\Network;
use EO\CoreBundle\Document\Station;
use EO\CoreBundle\Document\Ticket;
use EO\CoreBundle\Document\Zone;

class TestLoadTicketData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $tickets = $this->getModelFixtures();

        /* @var ManagerInterface $ticketTypeManager */
        $ticketManager = $this->container->get('eo_core.ticket_manager');

        foreach ($tickets['Tickets'] as $reference => $data) {
            /** @var Ticket $ticket */
            $ticket = $ticketManager->create();

            $ticket->setPrice($data['price']);
            $ticket->setCurrency($data['currency']);
            $ticket->setTerm(new \DateInterval('PT'.$data['term'].'H0M0S'));
            /** @var Customer $customer */
            $customer = $this->getReference('Customer_'.$data['buyBy']);
            $ticket->setBuyBy($customer);
            $ticket->setBuyAt(new \DateTime($data['buyAt']));
            $ticket->setData('truc');
            $customer = $this->getReference('Customer_'.$data['ownBy']);
            $ticket->setOwnBy($customer);
            if (!empty($data['transferredAt'])) {
                $ticket->setTransferredAt(new \DateTime($data['transferredAt']));
            }
            if (!empty($data['compostedAt'])) {
                $ticket->setCompostedAt(new \DateTime($data['compostedAt']));
            }
            if (!empty($data['compostedBy'])) {
                /** @var Customer $customer */
                $customer = $this->getReference('Customer_'.$data['compostedBy']);
                $ticket->setCompostedBy($customer);
            }
            if (!empty($data['compostedIn'])) {
                /** @var Station $station */
                $station = $this->getReference('Station_'.$data['compostedIn']);
                $ticket->setCompostedIn($station);
            }
            if (!empty($data['zoneA'])) {
                /** @var Zone $zone */
                $zone = $this->getReference('Zone_'.$data['zoneA']);
                $ticket->setZoneA($zone);
            }
            if (!empty($data['zoneB'])) {
                /** @var Zone $zone */
                $zone = $this->getReference('Zone_'.$data['zoneB']);
                $ticket->setZoneB($zone);
            }
            if (!empty($data['stationA'])) {
                /** @var Station $station */
                $station = $this->getReference('Station_'.$data['stationA']);
                $ticket->setStationA($station);
            }
            if (!empty($data['stationB'])) {
                /** @var Station $station */
                $station = $this->getReference('Station_'.$data['stationB']);
                $ticket->setStationB($station);
            }
            if (!empty($data['lineType'])) {
                /** @var LineType $lineType */
                $lineType = $this->getReference('LineType_'.$data['lineType']);
                $ticket->setLineType($lineType);
            }
            if (!empty($data['network'])) {
                /** @var Network $network */
                $network = $this->getReference('Network_'.$data['network']);
                $ticket->setNetwork($network);
            }

            $ticketManager->update($ticket);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'tickets';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}
