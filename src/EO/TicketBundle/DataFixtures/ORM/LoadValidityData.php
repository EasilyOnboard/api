<?php

namespace EO\TicketBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\TicketBundle\Model\ValidityInterface;

class LoadValidityData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var ValidityInterface $validity */
        $validity = $this->getManager()->create();
        $validity->setName($data['name']);
        $validity->setTerm(new \DateInterval($data['term']));
        $validity->setTransitType($data['transitType']);
        $validity->setMultiplier($data['multiplier']);
        $this->addReference('Validity_'.$reference, $validity);
        $this->getManager()->update($validity);
    }

    protected function getEnvironments()
    {
        return array('prod', 'dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_ticket.manager.validity');
    }

    public function getModelFile()
    {
        return 'validities';
    }

    public function getOrder()
    {
        return 1;
    }
}