<?php

namespace EO\TicketBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\NetworkBundle\Model\LineTypeInterface;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\NetworkBundle\Model\ZoneInterface;
use EO\TicketBundle\Model\TicketTypeInterface;
use EO\TicketBundle\Model\ValidityInterface;

class TestLoadTicketTypeData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var TicketTypeInterface $ticketType */
        $ticketType = $this->getManager()->create();
        $validity = $this->getReference('Validity_'.$data['validity']);
        if (!$validity || !$validity instanceof ValidityInterface) {
            return;
        }
        $ticketType->setName($data['name']);
        $ticketType->setPrice($data['price']);
        $ticketType->setCurrency($data['currency']);
        $ticketType->setValidity($validity);
        if (!empty($data['zoneA'])) {
            /** @var ZoneInterface $zone */
            $zone = $this->getReference('Zone_'.$data['zoneA']);
            $ticketType->setZoneA($zone);
        }
        if (!empty($data['zoneB'])) {
            /** @var ZoneInterface $zone */
            $zone = $this->getReference('Zone_'.$data['zoneB']);
            $ticketType->setZoneB($zone);
        }
        if (!empty($data['stationA'])) {
            /** @var StationInterface $station */
            $station = $this->getReference('Station_'.$data['stationA']);
            $ticketType->setStationA($station);
        }
        if (!empty($data['stationB'])) {
            /** @var StationInterface $station */
            $station = $this->getReference('Station_'.$data['stationB']);
            $ticketType->setStationB($station);
        }
        if (!empty($data['lineType'])) {
            /** @var LineTypeInterface $lineType */
            $lineType = $this->getReference('LineType_'.$data['lineType']);
            $ticketType->setLineType($lineType);
        }
        if (!empty($data['network'])) {
            /** @var NetworkInterface $network */
            $network = $this->getReference('Network_'.$data['network']);
            $ticketType->setNetwork($network);
        }
        $this->getManager()->update($ticketType);
        $this->getPaymentHandler()->sku()->create($ticketType);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_ticket.manager.ticket_type');
    }

    protected function getPaymentHandler()
    {
        return $this->container->get('eo_payment.handler');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'ticket_types';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}
