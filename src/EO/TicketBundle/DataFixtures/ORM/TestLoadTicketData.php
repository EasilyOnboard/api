<?php

namespace EO\TicketBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\NetworkBundle\Model\LineTypeInterface;
use EO\NetworkBundle\Model\NetworkInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\NetworkBundle\Model\ZoneInterface;
use EO\TicketBundle\Model\TicketInterface;
use EO\TicketBundle\Model\ValidityInterface;
use EO\UserBundle\Model\UserInterface;

class TestLoadTicketData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var TicketInterface $ticket */
        $ticket = $this->getManager()->create();
        $validity = $this->getReference('Validity_'.$data['validity']);
        if (!$validity || !$validity instanceof ValidityInterface) {
            return;
        }
        $ticket->setName($data['name']);
        $ticket->setPrice($data['price']);
        $ticket->setCurrency($data['currency']);
        $ticket->setValidity($validity);
        /** @var UserInterface $user */
        $user = $this->getReference('User_'.$data['buyBy']);
        $ticket->setBuyBy($user);
        $ticket->setBuyAt(new \DateTime($data['buyAt']));
        $ticket->setStatus($data['status']);
        $user = $this->getReference('User_'.$data['ownBy']);
        $ticket->setOwnBy($user);
        if (!empty($data['transferredAt'])) {
            $ticket->setTransferredAt(new \DateTime($data['transferredAt']));
        }
        if (!empty($data['compostedAt'])) {
            $ticket->setCompostedAt(new \DateTime($data['compostedAt']));
        }
        if (!empty($data['compostedIn'])) {
            /** @var StationInterface $station */
            $station = $this->getReference('Station_'.$data['compostedIn']);
            $ticket->setCompostedIn($station);
        }
        if (!empty($data['zoneA'])) {
            /** @var ZoneInterface $zone */
            $zone = $this->getReference('Zone_'.$data['zoneA']);
            $ticket->setZoneA($zone);
        }
        if (!empty($data['zoneB'])) {
            /** @var ZoneInterface $zone */
            $zone = $this->getReference('Zone_'.$data['zoneB']);
            $ticket->setZoneB($zone);
        }
        if (!empty($data['stationA'])) {
            /** @var StationInterface $station */
            $station = $this->getReference('Station_'.$data['stationA']);
            $ticket->setStationA($station);
        }
        if (!empty($data['stationB'])) {
            /** @var StationInterface $station */
            $station = $this->getReference('Station_'.$data['stationB']);
            $ticket->setStationB($station);
        }
        if (!empty($data['lineType'])) {
            /** @var LineTypeInterface $lineType */
            $lineType = $this->getReference('LineType_'.$data['lineType']);
            $ticket->setLineType($lineType);
        }
        if (!empty($data['network'])) {
            /** @var NetworkInterface $network */
            $network = $this->getReference('Network_'.$data['network']);
            $ticket->setNetwork($network);
        }

        $this->getManager()->update($ticket);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_ticket.manager.ticket');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'tickets';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}
