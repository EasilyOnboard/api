<?php

namespace EO\FineBundle\Model;

use EO\CompanyBundle\Model\CompanyInterface;

class FineType implements FineTypeInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var int
     */
    protected $status;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var float
     */
    protected $amount;

    /**
     * @var string
     */
    protected $currency;

    /**
     * @var CompanyInterface
     */
    protected $company;

    public function getId()
    {
        return $this->id;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    public function setCompany(CompanyInterface $company)
    {
        $this->company = $company;

        return $this;
    }

    public function __toString()
    {
        return $this->message;
    }
}