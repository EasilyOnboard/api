<?php

namespace EO\FineBundle\Model;

use EO\FineBundle\Enum\FineStatus;
use EO\MiscBundle\Model\Address;
use EO\MiscBundle\Model\AddressInterface;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\UserBundle\Model\UserInterface;

class Fine implements FineInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var FineTypeInterface
     */
    protected $type;

    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $sex;

    /**
     * @var \Datetime
     */
    protected $birthday;

    /**
     * @var AddressInterface
     */
    protected $fullAddress;

    /**
     * @var StationInterface
     */
    protected $station;

    /**
     * @var LineInterface
     */
    protected $line;

    /**
     * @var UserInterface
     */
    protected $customer;

    /**
     * @var UserInterface
     */
    protected $inspector;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $paidAt;

    public function __construct()
    {
        $this->status = FineStatus::UNPAID;
        $this->fullAddress = new Address();
        $this->createdAt = new \DateTime("now");
    }

    public function getId()
    {
        return $this->id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getSex()
    {
        return $this->sex;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getFullAddress()
    {
        return $this->fullAddress;
    }

    public function getAddress()
    {
        return $this->fullAddress->getAddress();
    }

    public function getCity()
    {
        return $this->fullAddress->getCity();
    }

    public function getPostcode()
    {
        return $this->fullAddress->getPostcode();
    }

    public function getState()
    {
        return $this->fullAddress->getState();
    }

    public function getCountry()
    {
        return $this->fullAddress->getCountry();
    }

    public function getPhone()
    {
        return $this->fullAddress->getPhone();
    }

    public function getStation()
    {
        return $this->station;
    }

    public function getLine()
    {
        return $this->line;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function getInspector()
    {
        return $this->inspector;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getPaidAt()
    {
        return $this->paidAt;
    }

    public function setType(FineTypeInterface $fineType)
    {
        $this->type = $fineType;

        return $this;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        if ($this->status === FineStatus::PAID) {
            $this->paidAt = new \DateTime('now');
        }
        return $this;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function setFullAddress(AddressInterface $address)
    {
        $this->fullAddress = $address;

        return $this;
    }

    public function setAddress($address)
    {
        $this->fullAddress->setAddress($address);

        return $this;
    }

    public function setCity($city)
    {
        $this->fullAddress->setCity($city);

        return $this;
    }

    public function setPostcode($postcode)
    {
        $this->fullAddress->setPostcode($postcode);

        return $this;
    }

    public function setState($state)
    {
        $this->fullAddress->setState($state);

        return $this;
    }

    public function setCountry($country)
    {
        $this->fullAddress->setCountry($country);

        return $this;
    }

    public function setPhone($phone)
    {
        $this->fullAddress->setPhone($phone);

        return $this;
    }

    public function setStation(StationInterface $station)
    {
        $this->station = $station;

        return $this;
    }

    public function setLine(LineInterface $line)
    {
        $this->line = $line;

        return $this;
    }

    public function setCustomer(UserInterface $customer)
    {
        $this->customer = $customer;

        return $this;
    }

    public function setInspector(UserInterface $inspector)
    {
        $this->inspector = $inspector;

        return $this;
    }
}