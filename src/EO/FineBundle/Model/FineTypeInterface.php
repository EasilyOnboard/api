<?php

namespace EO\FineBundle\Model;

use EO\CompanyBundle\Model\CompanyInterface;

interface FineTypeInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Status.
     *
     * @return int
     */
    public function getStatus();

    /**
     * Get Message.
     *
     * @return string
     */
    public function getMessage();

    /**
     * Get Amount.
     *
     * @return float
     */
    public function getAmount();

    /**
     * Get Currency.
     *
     * @return string
     */
    public function getCurrency();

    /**
     * Get Company.
     *
     * @return CompanyInterface
     */
    public function getCompany();

    /**
     * Set Status.
     *
     * @param int $status
     *
     * @return self
     */
    public function setStatus($status);

    /**
     * Set Message.
     *
     * @param string $message
     *
     * @return self
     */
    public function setMessage($message);

    /**
     * Set Amount.
     *
     * @param float $amount
     *
     * @return self
     */
    public function setAmount($amount);

    /**
     * Set Currency.
     *
     * @param string $currency
     *
     * @return self
     */
    public function setCurrency($currency);

    /**
     * Set Company.
     *
     * @param CompanyInterface $company
     *
     * @return self
     */
    public function setCompany(CompanyInterface $company);
}
