<?php

namespace EO\FineBundle\Model;

use EO\MiscBundle\Model\AddressInterface;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\UserBundle\Model\UserInterface;

interface FineInterface extends AddressInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Type.
     *
     * @return FineTypeInterface
     */
    public function getType();

    /**
     * Get Status.
     *
     * @return string
     */
    public function getStatus();

    /**
     * Get First name.
     *
     * @return string
     */
    public function getFirstName();

    /**
     * Get Last name.
     *
     * @return string
     */
    public function getLastName();

    /**
     * Get Email.
     *
     * @return string
     */
    public function getEmail();

    /**
     * Get Sex.
     *
     * @return string
     */
    public function getSex();

    /**
     * Get Birthday.
     *
     * @return \Datetime
     */
    public function getBirthday();

    /**
     * Get Full Address.
     *
     * @return AddressInterface
     */
    public function getFullAddress();

    /**
     * Get Station.
     *
     * @return StationInterface
     */
    public function getStation();

    /**
     * Get Line.
     *
     * @return LineInterface
     */
    public function getLine();

    /**
     * Get Customer.
     *
     * @return UserInterface
     */
    public function getCustomer();

    /**
     * Get Inspector.
     *
     * @return UserInterface
     */
    public function getInspector();

    /**
     * Get Created At.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Get Paid At.
     *
     * @return \DateTime
     */
    public function getPaidAt();

    /**
     * Set Type.
     *
     * @param FineTypeInterface $fineType
     *
     * @return self
     */
    public function setType(FineTypeInterface $fineType);

    /**
     * Set Status.
     *
     * @param string $status
     *
     * @return self
     */
    public function setStatus($status);

    /**
     * Set First name.
     *
     * @param string $firstName
     *
     * @return self
     */
    public function setFirstName($firstName);

    /**
     * Set Last name.
     *
     * @param string $lastName
     *
     * @return self
     */
    public function setLastName($lastName);

    /**
     * Set Email.
     *
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email);

    /**
     * Set Sex.
     *
     * @param string $sex
     *
     * @return self
     */
    public function setSex($sex);

    /**
     * Set Birthday.
     *
     * @param \Datetime $birthday
     *
     * @return self
     */
    public function setBirthday($birthday);

    /**
     * Set Full Address.
     *
     * @param AddressInterface $address
     *
     * @return self
     */
    public function setFullAddress(AddressInterface $address);

    /**
     * Set Station.
     *
     * @param StationInterface $station
     *
     * @return self
     */
    public function setStation(StationInterface $station);

    /**
     * Set Line.
     *
     * @param LineInterface $line
     *
     * @return self
     */
    public function setLine(LineInterface $line);

    /**
     * Set Customer.
     *
     * @param UserInterface $customer
     *
     * @return self
     */
    public function setCustomer(UserInterface $customer);

    /**
     * Set Inspector.
     *
     * @param UserInterface $inspector
     *
     * @return self
     */
    public function setInspector(UserInterface $inspector);
}
