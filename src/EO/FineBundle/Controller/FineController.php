<?php

namespace EO\FineBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\FineBundle\Model\FineInterface;
use EO\InspectorBundle\Enum\ControlStatus;
use EO\InspectorBundle\Model\ControlInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class FineController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All fines.",
     *  output={
     *      "class"="array<EO\FineBundle\Model\Fine>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_FINE_LIST')")
     *
     * @return View
     */
    public function allAction()
    {
        $fines = $this->getFineManager()->findAllByCompany($this->getUser()->getCompany());
        return $this->createView(array('fines' => $fines), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create fine.",
     *  input="eo_fine",
     *  output={
     *      "class"="EO\FineBundle\Model\Fine",
     *      "groups"={"basic", "full_fine"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_FINE_CREATE')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        $staff = $this->getUser();
        /** @var FineInterface $fine */
        $fine = $this->getFineManager()->create();
        $fine->setInspector($staff);
        $form = $this->createRestForm('eo_fine', $fine, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getFineManager()->update($fine);
            return $this->createView($fine, Codes::HTTP_CREATED, array('basic', 'full_fine'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create fine with a control.",
     *  input="eo_fine_basic",
     *  output={
     *      "class"="EO\FineBundle\Model\Fine",
     *      "groups"={"basic", "full_fine"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post("/{controlId}")
     *
     * @Security("has_role('ROLE_FINE_CREATE')")
     *
     * @param Request $request
     * @param string $controlId
     *
     * @return View
     */
    public function postByControlAction(Request $request, $controlId)
    {
        $staff = $this->getUser();
        /** @var ControlInterface $control */
        $control = $this->container->get('eo_inspector.manager.control')->find($controlId);
        if ($control === null) {
            throw $this->createNotFoundException();
        }
        if ($control->getStatus() === ControlStatus::TICKET_VALID) {
            return $this->createView((object)array("message" => "The control is valid."), Codes::HTTP_NOT_ACCEPTABLE);
        }
        /** @var FineInterface $fine */
        $fine = $this->getFineManager()->create();
        $fine->setInspector($staff);
        $fine->setCustomer($control->getCustomer());
        $fine->setStation($control->getStation());
        $fine->setLine($control->getLine());
        $form = $this->createRestForm('eo_fine_basic', $fine, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getFineManager()->update($fine);
            $control->setFine($fine);
            $this->container->get('eo_inspector.manager.control')->update($control);
            return $this->createView($fine, Codes::HTTP_CREATED, array('basic', 'full_fine'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get fine.",
     *  output={
     *      "class"="EO\FineBundle\Model\Fine",
     *      "groups"={"basic", "full_fine"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_FINE_GET')")
     *
     * @param string $fineId
     *
     * @return View
     */
    public function getAction($fineId)
    {
        $fine = $this->getFineManager()->find($fineId);
        if (!$fine) {
            throw $this->createNotFoundException();
        }
        return $this->createView($fine, Codes::HTTP_OK, array('basic', 'full_fine'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update fine status.",
     *  input="eo_fine_status",
     *  output={
     *      "class"="EO\FineBundle\Model\Fine",
     *      "groups"={"basic", "full_fine"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_FINE_PATCH')")
     *
     * @param Request $request
     * @param string  $fineId
     *
     * @return View
     */
    public function patchAction(Request $request, $fineId)
    {
        /** @var FineInterface $fine */
        $fine = $this->getFineManager()->find($fineId);
        if (!$fine) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_fine_status', $fine, 'PATCH');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getFineManager()->update($fine);
            return $this->createView($fine, Codes::HTTP_ACCEPTED, array('basic', 'full_fine'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @return \EO\FineBundle\Manager\DBAL\FineManagerInterface
     */
    public function getFineManager()
    {
        return $this->container->get('eo_fine.manager.fine');
    }
}
