<?php

namespace EO\FineBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\FineBundle\Model\FineTypeInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class FineTypeController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All fine types.",
     *  output={
     *      "class"="array<EO\FineBundle\Model\FineType>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_FINE_TYPE_LIST')")
     *
     * @return View
     */
    public function allAction()
    {
        $fineTypes = $this->getFineTypeManager()->findAllByCompany($this->getUser()->getCompany());
        return $this->createView(array('fine_types' => $fineTypes), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get fine type.",
     *  output={
     *      "class"="EO\FineBundle\Model\FineType",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful",
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_FINE_TYPE_LIST')")
     *
     * @param string $fineTypeId
     *
     * @return View
     */
    public function getAction($fineTypeId)
    {
        /** @var FineTypeInterface $fineType */
        $fineType = $this->getFineTypeManager()->find($fineTypeId);
        if (!$fineType || $fineType->getCompany() !== $this->getUser()->getCompany()) {
            throw $this->createAccessDeniedException();
        }
        return $this->createView($fineType, Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create fine type.",
     *  input="eo_fine_type",
     *  output={
     *      "class"="EO\FineBundle\Model\FineType",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_FINE_TYPE_CREATE')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        /** @var FineTypeInterface $fineType */
        $fineType = $this->getFineTypeManager()->create();
        $fineType->setCompany($this->getUser()->getCompany());
        $fineType->setCurrency($fineType->getCompany()->getCurrency());
        $form = $this->createRestForm('eo_fine_type', $fineType, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getFineTypeManager()->update($fineType);
            return $this->createView($fineType, Codes::HTTP_CREATED, array('basic'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update fine type.",
     *  input="eo_fine_type",
     *  output={
     *      "class"="EO\FineBundle\Model\FineType",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put
     *
     * @Security("has_role('ROLE_FINE_TYPE_UPDATE')")
     *
     * @param Request $request
     * @param string $fineTypeId
     *
     * @return View
     */
    public function putAction(Request $request, $fineTypeId)
    {
        /** @var FineTypeInterface $fineType */
        $fineType = $this->getFineTypeManager()->find($fineTypeId);
        if (!$fineType || $fineType->getCompany() !== $this->getUser()->getCompany()) {
            throw $this->createAccessDeniedException();
        }
        $form = $this->createRestForm('eo_fine_type', $fineType);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getFineTypeManager()->update($fineType);
            return $this->createView($fineType, Codes::HTTP_ACCEPTED, array('basic'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete fine type.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful"
     *  }
     * )
     *
     * @Rest\Delete
     *
     * @Security("has_role('ROLE_FINE_TYPE_DELETE')")
     *
     * @param string $fineTypeId
     *
     * @return View
     */
    public function deleteAction($fineTypeId)
    {
        /** @var FineTypeInterface $fineType */
        $fineType = $this->getFineTypeManager()->find($fineTypeId);
        if (!$fineType || $fineType->getCompany() !== $this->getUser()->getCompany()) {
            throw $this->createAccessDeniedException();
        }
        $this->getFineTypeManager()->delete($fineType);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @return \EO\FineBundle\Manager\DBAL\FineTypeManagerInterface
     */
    public function getFineTypeManager()
    {
        return $this->container->get('eo_fine.manager.fine_type');
    }
}