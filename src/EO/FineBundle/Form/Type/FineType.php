<?php

namespace EO\FineBundle\Form\Type;

use EO\FineBundle\Model\FineInterface;
use Symfony\Component\Form\FormBuilderInterface;

class FineType extends BasicFineType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var FineInterface $fine */
        $fine = $builder->getData();
        $stations = array();
        $lines = array();
        if ($fine) {
            $stations = $this->container->get('eo_network.manager.station')
                ->findAllByCompany($fine->getInspector()->getCompany());
            $lines = $this->container->get('eo_network.manager.line')
                ->findAllByCompany($fine->getInspector()->getCompany());
        }
        $customers = $this->container->get('eo_user.manager.user')->findAllCustomers();

        parent::buildForm($builder, $options);
        $builder
            ->add('customer', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_user.model.user.class'),
                'choices' => $customers
            ))
            ->add('station', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.station.class'),
                'choices' => $stations
            ))
            ->add('line', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.line.class'),
                'choices' => $lines
            ))
        ;
    }

    public function getName()
    {
        return 'eo_fine';
    }
}
