<?php

namespace EO\FineBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\FineBundle\Enum\FineStatus;
use Symfony\Component\Form\FormBuilderInterface;

class FineStatusType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', 'choice', array(
                'choices' => array(
                    FineStatus::PAID => FineStatus::PAID,
                    FineStatus::UNPAID => FineStatus::UNPAID,
                    FineStatus::CANCELED => FineStatus::CANCELED
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_fine_status';
    }
}
