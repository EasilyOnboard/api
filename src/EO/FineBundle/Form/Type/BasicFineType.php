<?php

namespace EO\FineBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\FineBundle\Model\FineInterface;
use EO\UserBundle\Enum\Sex;
use Symfony\Component\Form\FormBuilderInterface;

class BasicFineType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var FineInterface $fine */
        $fine = $builder->getData();
        $fineTypes = array();
        if ($fine) {
            $fineTypes = $this->container->get('eo_fine.manager.fine_type')
                ->findAllByCompany($fine->getInspector()->getCompany());
        }

        $builder
            ->add('email', 'email')
            ->add('first_name', 'text')
            ->add('last_name', 'text')
            ->add('sex', 'choice', array(
                'choices' => array(
                    Sex::MALE => "male",
                    Sex::FEMALE => "female",
                    Sex::OTHER => "other"
                )
            ))
            ->add('birthday', 'date', array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'yyyy-MM-dd'
            ))
            ->add('full_address', 'eo_address')
            ->add('type', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_fine.model.fine_type.class'),
                'choices' => $fineTypes
            ));
        ;
    }

    public function getName()
    {
        return 'eo_fine_basic';
    }
}