<?php

namespace EO\FineBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\FineBundle\Model\FineTypeInterface;
use EO\InspectorBundle\Enum\ControlStatus;
use Symfony\Component\Form\FormBuilderInterface;

class FineTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var FineTypeInterface $fineType */
        $fineType = $builder->getData();
        $builder
            ->add('message', 'text')
            ->add('status', 'choice', array(
                'choices' => array(
                    ControlStatus::TICKET_EXPIRED => ControlStatus::TICKET_EXPIRED,
                    ControlStatus::NO_TICKET => ControlStatus::NO_TICKET,
                )
            ))
            ->add('amount', 'money', array(
                'currency' => ($fineType) ? $fineType->getCurrency() : 'EUR'
            ))
        ;
    }

    public function getName()
    {
        return 'eo_fine_type';
    }
}