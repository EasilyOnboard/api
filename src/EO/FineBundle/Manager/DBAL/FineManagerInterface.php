<?php

namespace EO\FineBundle\Manager\DBAL;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\FineBundle\Model\FineInterface;
use EO\FineBundle\Model\FineTypeInterface;
use EO\UserBundle\Model\UserInterface;

interface FineManagerInterface extends ManagerInterface
{
    /**
     * @param CompanyInterface $company
     *
     * @return array
     */
    public function findAllByCompany(CompanyInterface $company);

    /**
     * @param UserInterface $customer
     *
     * @return array
     */
    public function findAllByCustomer(UserInterface $customer);

    /**
     * @param UserInterface $inspector
     *
     * @return array
     */
    public function findAllByInspector(UserInterface $inspector);

    /**
     * @param FineTypeInterface $fineType
     *
     * @return array
     */
    public function findAllByFineType(FineTypeInterface $fineType);

    /**
     * @param string $id
     * @param string $companyId
     *
     * @return FineInterface
     */
    public function findOneByIdAndCompanyId($id, $companyId);
}
