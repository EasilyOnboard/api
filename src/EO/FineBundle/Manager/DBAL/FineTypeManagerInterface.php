<?php

namespace EO\FineBundle\Manager\DBAL;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\DBAL\ManagerInterface;

interface FineTypeManagerInterface extends ManagerInterface
{
    /**
     * @param CompanyInterface $company
     *
     * @return array
     */
    public function findAllByCompany(CompanyInterface $company);
}