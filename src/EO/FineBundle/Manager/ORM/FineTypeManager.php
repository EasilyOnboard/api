<?php

namespace EO\FineBundle\Manager\ORM;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\ORM\Manager;
use EO\FineBundle\Manager\DBAL\FineTypeManagerInterface;
use EO\FineBundle\Model\FineTypeInterface;

class FineTypeManager extends Manager implements FineTypeManagerInterface
{
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->findByCompany($company);
    }

    public function delete($object)
    {
        if (!$object instanceof FineTypeInterface) {
            return;
        }
        $fines = $this->container->get('eo_fine.manager.fine')->findAllByFineType($object);
        foreach ($fines as $fine) {
            $this->em->remove($fine);
        }
        parent::delete($object);
    }
}