<?php

namespace EO\FineBundle\Manager\ORM;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\ORM\Manager;
use EO\FineBundle\Manager\DBAL\FineManagerInterface;
use EO\FineBundle\Model\FineTypeInterface;
use EO\UserBundle\Model\UserInterface;

class FineManager extends Manager implements FineManagerInterface
{
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder('f')
            ->leftJoin('f.type', 't')
            ->where('t.company = :company')
            ->setParameter('company', $company)
            ->getQuery()
            ->getResult();
    }

    public function findAllByCustomer(UserInterface $customer)
    {
        return $this->repository->findByCustomer($customer);
    }

    public function findAllByInspector(UserInterface $inspector)
    {
        return $this->repository->findByInspector($inspector);
    }

    public function findAllByFineType(FineTypeInterface $fineType)
    {
        return $this->repository->findByType($fineType);
    }

    public function findOneByIdAndCompanyId($id, $companyId)
    {
        return $this->repository->createQueryBuilder('f')
            ->leftJoin('f.company', 'c')
            ->where('f.id = :id')
            ->andWhere('c.id = :companyId')
            ->setParameters(array(
                'id' => $id,
                'companyId' => $companyId
            ))
            ->getQuery()
            ->getResult();
    }
}
