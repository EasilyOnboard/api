<?php

namespace EO\FineBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\FineBundle\Model\FineTypeInterface;

class TestLoadFineTypesData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var FineTypeInterface $fineType */
        $fineType = $this->getManager()->create();
        $fineType->setMessage($data['message']);
        $fineType->setAmount($data['amount']);
        $fineType->setCurrency($data['currency']);
        $fineType->setStatus($data['status']);
        /** @var CompanyInterface $company */
        $company = $this->getReference('Company_'.$data['company']);
        $fineType->setCompany($company);
        $this->getManager()->update($fineType);
        $this->setReference('FineType_' . $reference, $fineType);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_fine.manager.fine_type');
    }

    public function getModelFile()
    {
        return 'fine_types';
    }

    public function getOrder()
    {
        return 2;
    }
}