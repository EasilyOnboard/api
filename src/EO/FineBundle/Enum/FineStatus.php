<?php

namespace EO\FineBundle\Enum;

final class FineStatus
{
    const UNPAID = 'unpaid';
    const PAID = 'paid';
    const CANCELED = 'canceled';
}