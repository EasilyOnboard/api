<?php

namespace EO\TerminalBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\NetworkBundle\Model\StationInterface;
use EO\TerminalBundle\Form\Model\Compost;
use EO\TerminalBundle\Form\Model\Generate;
use EO\TerminalBundle\Handler\TerminalHandlerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class TerminalController extends EOController
{

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Me.",
     *  output={
     *      "class"="EO\TerminalBundle\Model\Terminal",
     *      "groups"={"basic", "terminal"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful"
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_TERMINAL')")
     *
     * @return View
     */
    public function meAction()
    {
        $terminal = $this->getUser()->getTerminal();
        return $this->createView($terminal, Codes::HTTP_OK, array('basic', 'terminal'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All terminals.",
     *  output={
     *      "class"="array<EO\TerminalBundle\Model\Terminal>",
     *      "groups"={"basic", "terminal"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful"
     *     }
     * )
     * @Rest\Get
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param string $stationId
     *
     * @return View
     */
    public function allAction($stationId)
    {
        /** @var StationInterface $station */
        $station = $this->container->get('eo_network.manager.station')->find($stationId);
        if (null === $station) {
            throw $this->createAccessDeniedException();
        }
        $terminals = $this->container->get('eo_terminal.manager.terminal')->findAllByStation($station);
        return $this->createView(array('terminals' => $terminals), Codes::HTTP_OK, array('basic', 'terminal'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Compost ticket.",
     *  input="eo_terminal_compost",
     *  output={
     *      "class"="EO\TerminalBundle\Form\Model\Compost"
     *  },
     *  statusCodes={
     *         Codes::HTTP_ACCEPTED="Composting is successful",
     *         Codes::HTTP_NOT_ACCEPTABLE="Ticket not valid",
     *     }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_TERMINAL')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function compostAction(Request $request)
    {
        $terminal = $this->getUser()->getTerminal();
        if (null === $terminal) {
            throw $this->createNotFoundException();
        }
        $compost = new Compost();
        $form = $this->createRestForm('eo_terminal_compost', $compost, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            /** @var TerminalHandlerInterface $handler */
            $handler = $this->get('eo_terminal.handler');
            $handler->handleCompost($compost, $terminal);
            $compost->setTicket(null);
            return $this->createView($compost, $compost->isValid() ? Codes::HTTP_ACCEPTED : Codes::HTTP_NOT_ACCEPTABLE);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Generate terminals.",
     *  input="eo_terminal_generate",
     *  output={
     *      "class"="array<EO\TerminalBundle\Model\Terminal>",
     *      "groups"={"basic", "terminal"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_CREATED="Returned when successful",
     *         Codes::HTTP_BAD_REQUEST="Returned when form is not valid",
     *     }
     * )
     * @Rest\Post
     *
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function generateAction(Request $request)
    {
        $generate = new Generate();
        $form = $this->createRestForm('eo_terminal_generate', $generate, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $factory = $this->container->get('eo_terminal.factory');
            return $this->createView(
                array('terminals' => $factory->generateTerminals($generate)),
                Codes::HTTP_CREATED,
                array('basic', 'terminal')
            );
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }
}