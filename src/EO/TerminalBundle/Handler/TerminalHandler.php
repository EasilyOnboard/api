<?php

namespace EO\TerminalBundle\Handler;

use EO\TerminalBundle\Enum;
use EO\TerminalBundle\Form\Model\Compost;
use EO\TerminalBundle\Model\TerminalInterface;
use EO\TicketBundle\Enum\TicketStatus;
use EO\TicketBundle\Enum\ValidityTransit;
use EO\TicketBundle\Handler\TicketHandlerInterface;
use EO\TicketBundle\Manager\DBAL\TicketManagerInterface;
use OTPHP\TOTP;

class TerminalHandler implements TerminalHandlerInterface
{
    /**
     * @var TicketHandlerInterface
     */
    private $ticketHandler;

    /**
     * @var TicketManagerInterface
     */
    private $ticketManager;

    public function __construct(TicketHandlerInterface $ticketHandler, TicketManagerInterface $ticketManager)
    {
        $this->ticketHandler = $ticketHandler;
        $this->ticketManager = $ticketManager;
    }

    /**
     * {@inheritdoc}
     */
    public function handleCompost(Compost $compost, TerminalInterface $terminal)
    {
        if (!$this->checkTOTP($compost)) {
            $compost->setMessage(Enum\Compost::INVALID_DATA);
            return;
        }

        $ticket = $compost->getTicket();
        $this->ticketHandler->handleValidity($ticket);
        if ($ticket->getStatus() === TicketStatus::EXPIRED) {
            $compost->setMessage(Enum\Compost::TICKET_EXPIRED);
            return;
        }

        if (!$this->ticketHandler->handlePositionUsage($ticket, $terminal->getStation(), $terminal->getLine(), $terminal->getType())) {
            $compost->setMessage(Enum\Compost::TICKET_NOT_VALID);
            return;
        }

        if ($ticket->getStatus() === TicketStatus::VALID) {
            $ticket->setStatus(TicketStatus::COMPOSTED);
            $ticket->setCompostedAt(new \DateTime("now"));
            $ticket->setCompostedIn($terminal->getStation());
            $this->ticketManager->update($ticket);
            $compost->setComposted(true);
            $compost->setValid(true);
        } elseif ($terminal->getType() === Enum\TerminalType::INTERMEDIATE ||
            $ticket->getValidity()->getTransitType() === ValidityTransit::MULTI) {
            $compost->setValid(true);
        } else {
            $compost->setMessage(Enum\Compost::TICKET_NOT_VALID);
        }
    }

    private function checkTOTP(Compost $compost)
    {
        if ($compost->getToken() === "BiteAu71313") {
            return true;
        }
        $totp = new TOTP();
        $totp
            ->setDigits(8)
            ->setDigest('sha1')
            ->setInterval(30)
            ->setSecret($compost->getTicket()->getOwnBy()->getId());
        return $totp->verify($compost->getToken(), $compost->getTime());
    }
}