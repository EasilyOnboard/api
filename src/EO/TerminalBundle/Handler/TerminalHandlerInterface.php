<?php

namespace EO\TerminalBundle\Handler;

use EO\TerminalBundle\Form\Model\Compost;
use EO\TerminalBundle\Model\TerminalInterface;

interface TerminalHandlerInterface
{
    /**
     * @param Compost $compost
     * @param TerminalInterface $terminal
     */
    public function handleCompost(Compost $compost, TerminalInterface $terminal);
}