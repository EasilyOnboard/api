<?php

namespace EO\TerminalBundle\Factory;

use EO\TerminalBundle\Form\Model\Generate;
use EO\TerminalBundle\Manager\DBAL\TerminalManagerInterface;
use EO\TerminalBundle\Model\TerminalInterface;
use EO\UserBundle\Manager\DBAL\UserManagerInterface;
use EO\UserBundle\Model\UserInterface;

class TerminalFactory implements TerminalFactoryInterface
{
    const TERMINAL_NAME = "term";
    const USER_NAME = "user";
    const SEPARATOR = "-";
    const SUFFIX = "@terminal.eo";

    /**
     * @var TerminalManagerInterface
     */
    private $terminalManager;

    /**
     * @var UserManagerInterface
     */
    private $userManager;

    public function __construct(TerminalManagerInterface $terminalManager, UserManagerInterface $userManager)
    {
        $this->terminalManager = $terminalManager;
        $this->userManager = $userManager;
    }

    /**
     * {@inheritdoc}
     */
    public function generateTerminals(Generate $generate)
    {
        $terminals = array();

        for ($i = 0; $i < $generate->getCount(); $i++) {
            $id = substr(base64_encode(time() + $i), 0, 14);
            /** @var UserInterface $user */
            $user = $this->userManager->create();
            $user->setEnabled(true);
            $user->setUsername(self::USER_NAME.self::SEPARATOR.$id.self::SUFFIX);
            $password = $this->generateNewPassword();
            $user->setPlainPassword($password);
            $user->addRole('ROLE_TERMINAL');
            $this->userManager->update($user);
            /** @var TerminalInterface $terminal */
            $terminal = $this->terminalManager->create();
            $terminal->setName(self::TERMINAL_NAME.self::SEPARATOR.$id);
            $terminal->setStation($generate->getStation());
            $terminal->setLine($generate->getLine());
            $terminal->setUser($user);
            $this->terminalManager->update($terminal);
            $terminals[] = $terminal;
        }
        return $terminals;
    }

    /**
     * @return string
     */
    private function generateNewPassword()
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 16);
    }
}