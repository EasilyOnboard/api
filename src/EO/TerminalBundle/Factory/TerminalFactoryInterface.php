<?php

namespace EO\TerminalBundle\Factory;

use EO\TerminalBundle\Form\Model\Generate;
use EO\TerminalBundle\Model\TerminalInterface;

interface TerminalFactoryInterface
{
    /**
     * Generate Terminals with associated basic users.
     *
     * @param Generate $generate
     *
     * @return TerminalInterface[]
     */
    public function generateTerminals(Generate $generate);
}