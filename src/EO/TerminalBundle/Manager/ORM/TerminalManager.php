<?php

namespace EO\TerminalBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\TerminalBundle\Manager\DBAL\TerminalManagerInterface;

class TerminalManager extends Manager implements TerminalManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByStation(StationInterface $station)
    {
        return $this->repository->findBy(array('station' => $station));
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByLine(LineInterface $line)
    {
        return $this->repository->findBy(array('line' => $line));
    }
}