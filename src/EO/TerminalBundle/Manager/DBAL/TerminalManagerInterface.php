<?php

namespace EO\TerminalBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;

interface TerminalManagerInterface extends ManagerInterface
{
    /**
     * @param StationInterface $station
     * @return array
     */
    public function findAllByStation(StationInterface $station);

    /**
     * @param LineInterface $line
     * @return array
     */
    public function findAllByLine(LineInterface $line);
}