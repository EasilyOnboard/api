<?php

namespace EO\TerminalBundle\Model;

use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\UserBundle\Model\UserInterface;

interface TerminalInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get Type.
     *
     * @return string
     */
    public function getType();

    /**
     * Get User.
     *
     * @return UserInterface
     */
    public function getUser();

    /**
     * Get Station.
     *
     * @return StationInterface
     */
    public function getStation();

    /**
     * Get Line.
     *
     * @return LineInterface
     */
    public function getLine();

    /**
     * Set Name.
     *
     * @param string $name
     * @return self
     */
    public function setName($name);

    /**
     * Set Type.
     *
     * @param string $type
     * @return self
     */
    public function setType($type);

    /**
     * Set User.
     *
     * @param UserInterface $user
     * @return self
     */
    public function setUser(UserInterface $user);

    /**
     * Set Station.
     *
     * @param StationInterface $station
     * @return self
     */
    public function setStation(StationInterface $station);
    
    /**
     * Set Line.
     *
     * @param LineInterface $line
     * @return self
     */
    public function setLine(LineInterface $line);
}