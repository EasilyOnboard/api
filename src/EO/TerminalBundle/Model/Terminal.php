<?php

namespace EO\TerminalBundle\Model;

use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\UserBundle\Model\UserInterface;

class Terminal implements TerminalInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var UserInterface
     */
    protected $user;

    /**
     * @var StationInterface
     */
    protected $station;

    /**
     * @var LineInterface
     */
    protected $line;

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getStation()
    {
        return $this->station;
    }

    public function getLine()
    {
        return $this->line;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function setUser(UserInterface $user)
    {
        $this->user = $user;

        return $this;
    }

    public function setStation(StationInterface $station)
    {
        $this->station = $station;

        return $this;
    }

    public function setLine(LineInterface $line)
    {
        $this->line = $line;

        return $this;
    }
}