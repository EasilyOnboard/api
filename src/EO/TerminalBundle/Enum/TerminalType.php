<?php

namespace EO\TerminalBundle\Enum;

final class TerminalType
{
    const ENTRY = 'entry';
    const INTERMEDIATE = 'intermediate';
}