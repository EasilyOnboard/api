<?php

namespace EO\TerminalBundle\Enum;

class Compost
{
    const INVALID_DATA = 'invalid_data';
    const TICKET_NOT_VALID = 'ticket_not_valid';
    const TICKET_EXPIRED = 'ticket_expired';
}