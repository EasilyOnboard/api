<?php

namespace EO\TerminalBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GenerateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('station', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.station.class')
            ))
            ->add('line', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_network.model.line.class')
            ))
            ->add('count', 'number', array(
                'precision' => 0
            ));
    }

    public function getName()
    {
        return 'eo_terminal_generate';
    }
}