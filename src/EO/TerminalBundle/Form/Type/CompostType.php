<?php

namespace EO\TerminalBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CompostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ticket', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_ticket.model.ticket.class')
            ))
            ->add('token', 'text')
            /*->add('time', 'datetime', array(
                'date_widget' => 'single_text'
            ))*/;
    }

    public function getName()
    {
        return 'eo_terminal_compost';
    }
}