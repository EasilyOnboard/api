<?php

namespace EO\TerminalBundle\Form\Model;

use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;

class Generate
{
    /**
     * @var StationInterface
     */
    private $station;

    /**
     * @var LineInterface
     */
    private $line;

    /**
     * @var int
     */
    private $count;

    /**
     * @return StationInterface
     */
    public function getStation()
    {
        return $this->station;
    }

    /**
     * @return LineInterface
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param StationInterface $station
     *
     * @return Generate
     */
    public function setStation(StationInterface $station)
    {
        $this->station = $station;

        return $this;
    }

    /**
     * @param LineInterface $line
     *
     * @return Generate
     */
    public function setLine(LineInterface $line)
    {
        $this->line = $line;

        return $this;
    }

    /**
     * @param int $count
     *
     * @return Generate
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }
}