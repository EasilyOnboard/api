<?php

namespace EO\TerminalBundle\Form\Model;

use EO\TicketBundle\Model\TicketInterface;

class Compost
{
    /**
     * @var TicketInterface|null
     */
    protected $ticket;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var \DateTime
     */
    protected $time;

    /**
     * @var bool
     */
    protected $valid;

    /**
     * @var bool
     */
    protected $composted;

    /**
     * @var string
     */
    protected $message;

    public function __construct()
    {
        $this->valid = false;
        $this->composted = false;
    }

    /**
     * @return TicketInterface
     */
    public function getTicket()
    {
        return $this->ticket;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return boolean
     */
    public function isValid()
    {
        return $this->valid;
    }

    /**
     * @return boolean
     */
    public function isComposted()
    {
        return $this->composted;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param TicketInterface|null $ticket
     *
     * @return Compost
     */
    public function setTicket(TicketInterface $ticket = null)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * @param string $token
     *
     * @return Compost
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @param \DateTime $time
     *
     * @return Compost
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * @param boolean $valid
     *
     * @return Compost
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * @param boolean $composted
     *
     * @return Compost
     */
    public function setComposted($composted)
    {
        $this->composted = $composted;

        return $this;
    }

    /**
     * @param string $message
     *
     * @return Compost
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }
}