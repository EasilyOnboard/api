<?php

namespace EO\TerminalBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\NetworkBundle\Model\LineInterface;
use EO\NetworkBundle\Model\StationInterface;
use EO\TerminalBundle\Model\TerminalInterface;
use EO\UserBundle\Model\UserInterface;

class TestLoadTerminalData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var TerminalInterface $terminal */
        $terminal = $this->getManager()->create();
        /** @var StationInterface $station */
        $station = $this->getReference('Station_'.$data['station']);
        /** @var LineInterface $line */
        $line = $this->getReference('Line_'.$data['line']);
        /** @var UserInterface $user */
        $user = $this->getReference('User_'.$data['user']);
        $terminal->setName($data['name']);
        $terminal->setType($data['type']);
        $terminal->setStation($station);
        $terminal->setLine($line);
        $terminal->setUser($user);
        $this->setReference('Terminal_'.$reference, $terminal);
        $this->getManager()->update($terminal);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_terminal.manager.terminal');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'terminals';
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}