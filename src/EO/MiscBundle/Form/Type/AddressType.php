<?php

namespace EO\MiscBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address', 'textarea')
            ->add('city', 'text')
            ->add('postcode', 'text')
            ->add('state', 'text')
            ->add('country', 'country')
            ->add('phone', 'text')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array('validation_groups' => array('Default')));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_address';
    }
}
