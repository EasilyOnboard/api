<?php

namespace EO\MiscBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ImageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'vich_image');
    }

    /**
     * @{@inheritdoc}
     */
    public function getName()
    {
        return 'eo_image';
    }
}
