<?php

namespace EO\MiscBundle\Manager\ORM;

use EO\MiscBundle\Manager\DBAL\NotificationManagerInterface;
use EO\CoreBundle\Manager\ORM\Manager;
use EO\UserBundle\Model\UserInterface;

class NotificationManager extends Manager implements NotificationManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByUser(UserInterface $user)
    {
        return $this->paginate($this->repository->createQueryBuilder('n')
            ->where('n.user = :user')
            ->orderBy('n.createdAt', 'DESC')
            ->setParameter('user', $user)
            ->getQuery());
    }
}