<?php

namespace EO\MiscBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\UserBundle\Model\UserInterface;

interface NotificationManagerInterface extends ManagerInterface
{
    /**
     * @param UserInterface $user
     *
     * @return array
     */
    public function findAllByUser(UserInterface $user);
}