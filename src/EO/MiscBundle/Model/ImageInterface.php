<?php

namespace EO\MiscBundle\Model;

use Symfony\Component\HttpFoundation\File\File;

interface ImageInterface
{
    /**
     * Get Image File.
     *
     * @return File
     */
    public function getFile();

    /**
     * Get Image Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get Created At.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Get Updated At.
     *
     * @return \DateTime
     */
    public function getUpdatedAt();

    /**
     * Set Image File.
     *
     * @param File $file
     *
     * @return self
     */
    public function setFile(File $file);

    /**
     * Set Image name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name);
}
