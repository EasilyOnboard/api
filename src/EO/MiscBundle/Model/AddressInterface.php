<?php

namespace EO\MiscBundle\Model;

interface AddressInterface
{
    /**
     * Get address.
     *
     * @return string $address
     */
    public function getAddress();

    /**
     * Get city.
     *
     * @return string $city
     */
    public function getCity();

    /**
     * Get postcode.
     *
     * @return string $postcode
     */
    public function getPostcode();

    /**
     * Get state.
     *
     * @return string $state
     */
    public function getState();

    /**
     * Get country.
     *
     * @return string $country
     */
    public function getCountry();

    /**
     * Get phone.
     *
     * @return string $phone
     */
    public function getPhone();

    /**
     * Set address.
     *
     * @param string $address
     *
     * @return self
     */
    public function setAddress($address);

    /**
     * Set city.
     *
     * @param string $city
     *
     * @return self
     */
    public function setCity($city);

    /**
     * Set postcode.
     *
     * @param string $postcode
     *
     * @return self
     */
    public function setPostcode($postcode);

    /**
     * Set state.
     *
     * @param string $state
     *
     * @return self
     */
    public function setState($state);

    /**
     * Set country.
     *
     * @param string $country
     *
     * @return self
     */
    public function setCountry($country);

    /**
     * Set phone.
     *
     * @param string $phone
     *
     * @return self
     */
    public function setPhone($phone);
}
