<?php

namespace EO\MiscBundle\Model;

use EO\UserBundle\Model\UserInterface;

interface NotificationInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Message.
     *
     * @return string
     */
    public function getMessage();

    /**
     * Get Type.
     *
     * @return string
     */
    public function getType();

    /**
     * Get Data.
     *
     * @return array
     */
    public function getData();

    /**
     * Get Created At.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Get Seen.
     *
     * @return boolean
     */
    public function getSeen();

    /**
     * Get Seen At.
     *
     * @return \DateTime
     */
    public function getSeenAt();

    /**
     * Get User.
     *
     * @return UserInterface
     */
    public function getUser();

    /**
     * Set Message.
     *
     * @param string $message
     *
     * @return self
     */
    public function setMessage($message);

    /**
     * Set Type.
     *
     * @param string $type
     *
     * @return self
     */
    public function setType($type);

    /**
     * Set Data.
     *
     * @param array $data
     *
     * @return self
     */
    public function setData(array $data);

    /**
     * Set Seen.
     *
     * @param boolean $seen
     *
     * @return mixed
     */
    public function setSeen($seen);

    /**
     * Set Seen At.
     *
     * @param \DateTime $seenAt
     *
     * @return self
     */
    public function setSeenAt(\DateTime $seenAt);

    /**
     * Set User.
     *
     * @param UserInterface $user
     *
     * @return self
     */
    public function setUser(UserInterface $user);
}