<?php

namespace EO\MiscBundle\Model;

use Symfony\Component\HttpFoundation\File\File;

class Image implements ImageInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var File
     */
    protected $file;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->updatedAt = $this->createdAt;
    }

    /**
     * @{@inheritdoc}
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @{@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @{@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @{@inheritdoc}
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @{@inheritdoc}
     */
    public function setFile(File $file)
    {
        $this->file = $file;
        if ($file) {
            $this->updatedAt = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @{@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $name;
    }
}
