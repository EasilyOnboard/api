<?php

namespace EO\MiscBundle\Datafixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\MiscBundle\Model\NotificationInterface;
use EO\UserBundle\Model\UserInterface;

class LoadTestNotificationData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var NotificationInterface $notification */
        $notification = $this->getManager()->create();

        $notification->setMessage($data['message']);
        $notification->setType($data['type']);
        if (isset($data['data'])) {
            $notification->setData($data['data']);
        }
        if (isset($data['seen'])) {
            $notification->setSeen($data['seen']);
        }
        if (isset($data['seenAt'])) {
            $notification->setMessage($data['seenAt']);
        }
        /** @var UserInterface $user */
        $user = $this->getReference('User_'.$data['user']);
        $notification->setUser($user);

        $this->getManager()->update($notification);
    }

    /**
     * {@inheritdoc}
     */
    protected function getModelFile()
    {
        return 'notifications';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    /**
     * @return \EO\MiscBundle\Manager\DBAL\NotificationManagerInterface
     */
    protected function getManager()
    {
        return $this->container->get('eo_misc.manager.notification');
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 4;
    }
}