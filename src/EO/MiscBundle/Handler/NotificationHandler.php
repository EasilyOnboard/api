<?php

namespace EO\MiscBundle\Handler;

use EO\MiscBundle\Model\NotificationInterface;
use EO\UserBundle\Enum\DeviceOS;
use EO\UserBundle\Model\DeviceInterface;
use RMS\PushNotificationsBundle\Message\AndroidMessage;
use RMS\PushNotificationsBundle\Message\iOSMessage;
use RMS\PushNotificationsBundle\Message\WindowsphoneMessage;
use RMS\PushNotificationsBundle\Service\Notifications;

class NotificationHandler implements NotificationHandlerInterface
{
    /**
     * @var Notifications
     */
    private $notificationService;

    /**
     * @param Notifications $notificationService
     */
    public function __construct(Notifications $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * {@inheritdoc}
     */
    public function handlePushNewNotification(NotificationInterface $notification)
    {
        $user = $notification->getUser();
        /** @var DeviceInterface $device */
        foreach ($user->getDevices() as $device) {
            $message = null;
            if ($device->getOS() == DeviceOS::IOS) {
                $message = new iOSMessage();
            } elseif ($device->getOS() == DeviceOS::ANDROID) {
                $message = new AndroidMessage();
            } elseif ($device->getOS() == DeviceOS::WP) {
                $message = new WindowsphoneMessage();
            }
            if ($message !== null) {
                $message->setMessage($notification->getMessage());
                $message->setData(array_merge(array('type' => $notification->getType()), $notification->getData()));
                $message->setDeviceIdentifier($device->getAppId());
                $this->notificationService->send($message);
            }
        }
    }
}