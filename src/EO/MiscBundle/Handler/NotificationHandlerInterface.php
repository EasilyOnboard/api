<?php

namespace EO\MiscBundle\Handler;

use EO\MiscBundle\Model\NotificationInterface;

interface NotificationHandlerInterface
{
    /**
     * @param NotificationInterface $notification
     */
    public function handlePushNewNotification(NotificationInterface $notification);
}