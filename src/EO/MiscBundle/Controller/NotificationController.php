<?php

namespace EO\MiscBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\MiscBundle\Model\NotificationInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class NotificationController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All Notifications.",
     *  output={
     *      "class"="array<EO\MiscBundle\Model\Notification>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful",
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return View
     */
    public function allAction()
    {
        $notifications = $this->getNotificationManager()->findAllByUser($this->getUser());
        return $this->createView(array('notifications' => $notifications), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get notification.",
     *  output={
     *      "class"="EO\MiscBundle\Model\Notification",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful",
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $notificationId
     *
     * @return View
     */
    public function getAction($notificationId)
    {
        /** @var NotificationInterface $notification */
        $notification = $this->getNotificationManager()->find($notificationId);
        if (!$notification) {
            throw $this->createNotFoundException();
        }
        if ($notification->getUser()->getId() != $this->getUser()->getId()) {
            throw $this->createAccessDeniedException();
        }
        return $this->createView($notification, Codes::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update notification",
     *  output={
     *      "class"="EO\MiscBundle\Model\Notification",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful"
     *     }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $notificationId
     *
     * @return View
     */
    public function patchAction($notificationId)
    {
        /** @var NotificationInterface $notification */
        $notification = $this->getNotificationManager()->find($notificationId);
        if (!$notification) {
            throw $this->createNotFoundException();
        }
        if ($notification->getUser()->getId() != $this->getUser()->getId()) {
            throw $this->createAccessDeniedException();
        }
        $notification->setSeen(true);
        $notification->setSeenAt(new \DateTime("now"));
        $this->getNotificationManager()->update($notification);
        return $this->createView($notification, Codes::HTTP_OK, array('basic'));
    }

    /**
     * @return \EO\MiscBundle\Manager\DBAL\NotificationManagerInterface
     */
    public function getNotificationManager()
    {
        return $this->container->get('eo_misc.manager.notification');
    }
}