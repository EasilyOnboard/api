<?php

namespace EO\CompanyBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text')
            ->add('email', 'email')
            ->add('full_address', 'eo_address')
            ->add('currency', 'currency')
//            ->add('logo', 'eo_image')
//            ->add('ticketLogo', 'eo_image')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_company';
    }
}
