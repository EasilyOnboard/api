<?php

namespace EO\CompanyBundle\Model;

use EO\MiscBundle\Model\Address;
use EO\MiscBundle\Model\AddressInterface;
use EO\MiscBundle\Model\Image;
use EO\MiscBundle\Model\ImageInterface;

class Company implements CompanyInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var AddressInterface
     */
    protected $fullAddress;

    /**
     * @var string
     */
    protected $currency;

    /**
     * @var ImageInterface
     */
    protected $logo;

    /**
     * @var ImageInterface
     */
    protected $ticketLogo;

    public function __construct()
    {
        $this->fullAddress = new Address();
        $this->logo = new Image();
        $this->ticketLogo = new Image();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function getFullAddress()
    {
        return $this->fullAddress;
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * {@inheritdoc}
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * {@inheritdoc}
     */
    public function getTicketLogo()
    {
        return $this->ticketLogo;
    }

    /**
     * {@inheritdoc}
     */
    public function setAddress($address)
    {
        $this->fullAddress->setAddress($address);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAddress()
    {
        return $this->fullAddress->getAddress();
    }

    /**
     * {@inheritdoc}
     */
    public function setCity($city)
    {
        $this->fullAddress->setCity($city);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCity()
    {
        return $this->fullAddress->getCity();
    }

    /**
     * {@inheritdoc}
     */
    public function setPostcode($postcode)
    {
        $this->fullAddress->setPostcode($postcode);

        return $this;
    }

    public function getPostcode()
    {
        return $this->fullAddress->getPostcode();
    }

    /**
     * {@inheritdoc}
     */
    public function setState($state)
    {
        $this->fullAddress->setState($state);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getState()
    {
        return $this->fullAddress->getState();
    }

    /**
     * {@inheritdoc}
     */
    public function setCountry($country)
    {
        $this->fullAddress->setCountry($country);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCountry()
    {
        return $this->fullAddress->getCountry();
    }

    /**
     * {@inheritdoc}
     */
    public function setPhone($phone)
    {
        $this->fullAddress->setPhone($phone);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPhone()
    {
        return $this->fullAddress->getPhone();
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setFullAddress(AddressInterface $fullAddress)
    {
        $this->fullAddress = $fullAddress;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setLogo(ImageInterface $logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTicketLogo(ImageInterface $ticketLogo)
    {
        $this->ticketLogo = $ticketLogo;

        return $this;
    }

    /**
     * Company to string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}
