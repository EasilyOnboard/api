<?php

namespace EO\CompanyBundle\Model;

use EO\MiscBundle\Model\AddressInterface;
use EO\MiscBundle\Model\ImageInterface;

interface CompanyInterface extends AddressInterface
{
    /**
     * Get id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail();

    /**
     * Get full address.
     *
     * @return AddressInterface
     */
    public function getFullAddress();

    /**
     * Get Currency.
     *
     * @return string
     */
    public function getCurrency();

    /**
     * Get Logo.
     *
     * @return ImageInterface
     */
    public function getLogo();

    /**
     * Get TicketLogo.
     *
     * @return ImageInterface
     */
    public function getTicketLogo();

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return self
     */
    public function setEmail($email);

    /**
     * Set full address.
     *
     * @param AddressInterface $fullAddress
     *
     * @return self
     */
    public function setFullAddress(AddressInterface $fullAddress);

    /**
     * Set Currency.
     *
     * @param $currency
     *
     * @return self
     */
    public function setCurrency($currency);

    /**
     * Set Logo File.
     *
     * @param ImageInterface $logoFile
     *
     * @return self
     */
    public function setLogo(ImageInterface $logoFile);

    /**
     * Set Ticket Logo File.
     *
     * @param ImageInterface $ticketLogo
     *
     * @return self
     */
    public function setTicketLogo(ImageInterface $ticketLogo);
}
