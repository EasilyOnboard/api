<?php

namespace EO\CompanyBundle\Manager\ORM;

use EO\CompanyBundle\Manager\DBAL\CompanyManagerInterface;
use EO\CoreBundle\Manager\ORM\Manager;

class CompanyManager extends Manager implements CompanyManagerInterface
{
}