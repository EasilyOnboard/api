<?php

namespace EO\CompanyBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;

interface CompanyManagerInterface extends ManagerInterface
{
}