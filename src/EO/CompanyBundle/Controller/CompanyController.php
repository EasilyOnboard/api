<?php

namespace EO\CompanyBundle\Controller;

use EO\CompanyBundle\Manager\DBAL\CompanyManagerInterface;
use EO\CoreBundle\Controller\EOController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All Companies.",
     *  output={
     *      "class"="array<EO\CompanyBundle\Model\Company>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     *
     * @return View
     */
    public function allAction()
    {
        $companies = $this->getCompanyManager()->findAll();
        return $this->createView(array('companies' => $companies), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="New company.",
     *  input="eo_company",
     *  output={
     *      "class"="EO\CompanyBundle\Model\Company",
     *      "groups"={"basic", "full_company"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        $company = $this->getCompanyManager()->create();
        $form = $this->createRestForm('eo_company', $company, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getCompanyManager()->update($company);
            return $this->createView($company, Codes::HTTP_CREATED, array('basic', 'full_company'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get company.",
     *  output={
     *      "class"="EO\CompanyBundle\Model\Company",
     *      "groups"={"basic", "full_company"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @param string $companyId
     *
     * @Security("has_role('ROLE_STAFF')")
     *
     * @return View
     */
    public function getAction($companyId)
    {
        $company = $this->getCompanyManager()->find($companyId);
        if (!$company) {
            throw $this->createNotFoundException();
        }
        return $this->createView($company, Codes::HTTP_OK, array('basic', 'full_company'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update company.",
     *  input="eo_company",
     *  output={
     *      "class"="EO\CompanyBundle\Model\Company",
     *      "groups"={"basic", "full_company"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put
     *
     * @Security("has_role('ROLE_COMPANY_UPDATE')")
     *
     * @param Request $request
     * @param string  $companyId
     *
     * @return View
     */
    public function putAction(Request $request, $companyId)
    {
        $company = $this->getCompanyManager()->find($companyId);
        if (!$company) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_company', $company);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getCompanyManager()->update($company);
            return $this->createView($company, Codes::HTTP_ACCEPTED, array('basic', 'full_company'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete company.",
     *  statusCodes={
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid",
     *      Codes::HTTP_ACCEPTED="Returned when the company is successfully deleted"
     *  }
     * )
     *
     * @Rest\Delete
     *
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     *
     * @param string $companyId
     *
     * @return View
     */
    public function deleteAction($companyId)
    {
        $company = $this->getCompanyManager()->find($companyId);
        if (!$company) {
            throw $this->createNotFoundException();
        }
        $this->getCompanyManager()->delete($company);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @return CompanyManagerInterface
     */
    private function getCompanyManager()
    {
        return $this->get('eo_company.manager.company');
    }
}