<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\Company;

class TestLoadCompanyData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $companies = $this->getModelFixtures();

        /** @var ManagerInterface $companyManager */
        $companyManager = $this->container->get('eo_core.company_manager');

        foreach ($companies['Companies'] as $reference => $data) {
            /** @var Company $company */
            $company = $companyManager->create();
            $company->setName($data['name']);
            $company->setEmail($data['email']);
            $company->setAddress($data['address']);
            $company->setCity($data['city']);
            $company->setState($data['state']);
            $company->setPostcode($data['postcode']);
            $company->setCountry($data['country']);
            $company->setPhone($data['phone']);

            $this->addReference('Company_'.$reference, $company);

            $manager->persist($company);
            $manager->flush();
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'companies';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}
