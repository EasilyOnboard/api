<?php

namespace EO\CompanyBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CompanyBundle\Model\Company;
use EO\CoreBundle\DataFixtures\AbstractLoadData;

class TestLoadCompanyData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var Company $company */
        $company = $this->getManager()->create();
        $company->setName($data['name']);
        $company->setEmail($data['email']);
        $company->setAddress($data['address']);
        $company->setCity($data['city']);
        $company->setState($data['state']);
        $company->setPostcode($data['postcode']);
        $company->setCountry($data['country']);
        $company->setPhone($data['phone']);
        $company->setCurrency($data['currency']);
        $this->addReference('Company_'.$reference, $company);
        $this->getManager()->update($company);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    /**
     * @return \EO\CompanyBundle\Manager\DBAL\CompanyManagerInterface
     */
    protected function getManager()
    {
        return $this->container->get('eo_company.manager.company');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'companies';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}
