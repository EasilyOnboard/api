<?php

namespace EO\CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use EO\CoreBundle\Model\CreditCard as BaseCreditCard;

/**
 * Class CreditCard
 *
 * @MongoDB\EmbeddedDocument
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class CreditCard extends BaseCreditCard
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $firstname;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $lastname;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $cardNumber;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $cryptogramme;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     * @Assert\NotBlank()
     */
    protected $expireAt;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @var Address
     *
     * @MongoDB\EmbedOne(targetDocument="Address")
     */
    protected $billingAddress;

    public function __construct()
    {
        $this->billingAddress = new Address();
    }
}
