<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\ComplaintMessage as BaseComplaintMessage;

/**
 * Class ComplaintMessage
 *
 * @MongoDB\EmbeddedDocument()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class ComplaintMessage extends BaseComplaintMessage
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $message;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    protected $date;

    /**
     * @var User
     *
     * @MongoDB\ReferenceOne(targetDocument="User")
     */
    protected $user;

    /**
     * @var boolean
     *
     * @MongoDB\Boolean
     * @Assert\NotBlank()
     */
    protected $saw;
}
