<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation as JMS;
use EO\CoreBundle\Doctrine\MongoDB\Annotations as EOMongoDB;
use EO\CoreBundle\Model\TicketType as BaseTicketType;

/**
 * Class TicketType.
 *
 * @MongoDB\Document
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class TicketType extends BaseTicketType
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var float
     *
     * @MongoDB\Float
     * @Assert\NotBlank()
     */
    protected $price;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $currency;

    /**
     * @var \DateInterval
     *
     * @EOMongoDB\DateInterval
     * @Assert\NotBlank()
     */
    protected $term;

    /**
     * @var Zone
     *
     * @MongoDB\ReferenceOne(targetDocument="Zone")
     */
    protected $zoneA;

    /**
     * @var Zone
     *
     * @MongoDB\ReferenceOne(targetDocument="Zone")
     */
    protected $zoneB;

    /**
     * @var Station
     *
     * @MongoDB\ReferenceOne(targetDocument="Station")
     */
    protected $stationA;

    /**
     * @var Station
     *
     * @MongoDB\ReferenceOne(targetDocument="Station")
     */
    protected $stationB;

    /**
     * @var LineType
     *
     * @MongoDB\ReferenceOne(targetDocument="LineType")
     */
    protected $lineType;

    /**
     * @var Network
     *
     * @MongoDB\ReferenceOne(targetDocument="Network")
     * @Assert\NotBlank()
     */
    protected $network;
}
