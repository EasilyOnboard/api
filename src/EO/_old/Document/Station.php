<?php

namespace EO\CoreBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\Station as BaseStation;

/**
 * Class Station.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Station extends BaseStation
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var float
     *
     * @MongoDB\Float
     * @Assert\NotBlank()
     */
    protected $longitude;

    /**
     * @var float
     *
     * @MongoDB\Float
     * @Assert\NotBlank()
     */
    protected $latitude;

    /**
     * @var Zone
     *
     * @MongoDB\ReferenceOne(targetDocument="Zone", inversedBy="stations")
     */
    protected $zone;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="Line")
     */
    protected $lines;

    /**
     * @var Network
     *
     * @MongoDB\ReferenceOne(targetDocument="Network", inversedBy="stations")
     * @Assert\NotBlank()
     */
    protected $network;

    /**
     * {@inheritDoc.
     */
    public function __toString()
    {
        return $this->name;
    }
}
