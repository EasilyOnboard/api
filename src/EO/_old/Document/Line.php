<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\Line as BaseLine;

/**
 * Class Line.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Line extends BaseLine
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $color;

    /**
     * @var LineType
     *
     * @MongoDB\ReferenceOne(targetDocument="LineType")
     * @Assert\NotBlank()
     */
    protected $lineType;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="Station")
     */
    protected $stations;

    /**
     * @var array
     *
     * @MongoDB\Collection
     */
    protected $orders;

    /**
     * @var Network
     *
     * @MongoDB\ReferenceOne(targetDocument="Network", inversedBy="lines")
     * @Assert\NotBlank()
     */
    protected $network;
}
