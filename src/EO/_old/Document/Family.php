<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections\ArrayCollection;
use EO\CoreBundle\Model\Family as BaseFamily;

/**
 * Class Family.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Family extends BaseFamily
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="Customer")
     */
    protected $members;
}
