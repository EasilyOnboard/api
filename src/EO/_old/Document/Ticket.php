<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation as JMS;
use EO\CoreBundle\Doctrine\MongoDB\Annotations as EOMongoDB;
use EO\CoreBundle\Model\Ticket as BaseTicket;

/**
 * Class Ticket.
 *
 * @MongoDB\Document
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Ticket extends BaseTicket
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $data;

    /**
     * @var \DateTime
     *
     * @EOMongoDB\DateInterval
     * @Assert\NotBlank()
     */
    protected $term;

    /**
     * @var Zone
     *
     * @MongoDB\ReferenceOne(targetDocument="Zone")
     */
    protected $zoneA;

    /**
     * @var Zone
     *
     * @MongoDB\ReferenceOne(targetDocument="Zone")
     */
    protected $zoneB;

    /**
     * @var Station
     *
     * @MongoDB\ReferenceOne(targetDocument="Station")
     */
    protected $stationA;

    /**
     * @var Station
     *
     * @MongoDB\ReferenceOne(targetDocument="Station")
     */
    protected $stationB;

    /**
     * @var LineType
     *
     * @MongoDB\ReferenceOne(targetDocument="LineType")
     */
    protected $lineType;

    /**
     * @var Network
     *
     * @MongoDB\ReferenceOne(targetDocument="Network")
     * @Assert\NotBlank()
     */
    protected $network;

    /**
     * @var float
     *
     * @MongoDB\Float
     * @Assert\NotBlank()
     */
    protected $price;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $currency;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $status;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     * @Assert\NotBlank()
     */
    protected $buyAt;

    /**
     * @var Customer
     *
     * @MongoDB\ReferenceOne(targetDocument="Customer")
     * @Assert\NotBlank()
     */
    protected $buyBy;

    /**
     * @var Customer
     *
     * @MongoDB\ReferenceOne(targetDocument="Customer")
     * @Assert\NotBlank()
     */
    protected $ownBy;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    protected $transferredAt;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    protected $compostedAt;

    /**
     * @var Station
     *
     * @MongoDB\ReferenceOne(targetDocument="Station")
     */
    protected $compostedIn;
}
