<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\Manager\TicketTypeManagerInterface;
use EO\CoreBundle\Model\NetworkInterface;

class TicketTypeManager extends MongoDBManager implements TicketTypeManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByNetwork(NetworkInterface $network)
    {
        return $this->repository->createQueryBuilder('tt')
            ->field('network')->references($network)
            ->getQuery()
            ->execute();
    }
}