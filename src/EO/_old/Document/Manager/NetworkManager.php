<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CompanyInterface;
use EO\CoreBundle\Model\Manager\NetworkManagerInterface;

class NetworkManager extends MongoDBManager implements NetworkManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder('n')
            ->field('company')->references($company)
            ->getQuery()
            ->execute();
    }
}