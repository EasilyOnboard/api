<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CompanyInterface;
use EO\CoreBundle\Model\CustomerInterface;
use EO\CoreBundle\Model\EmployeeInterface;
use EO\CoreBundle\Model\Manager\FineManagerInterface;

class FineManager extends MongoDBManager implements FineManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByCustomer(CustomerInterface $customer)
    {
        return $this->repository->createQueryBuilder('f')
            ->field('customer')->references($customer)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByInspector(EmployeeInterface $inspector)
    {
        return $this->repository->createQueryBuilder('f')
            ->field('inspector')->references($inspector)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder('f')
            ->field('company')->references($company)
            ->getQuery()
            ->execute();
    }
}