<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\ComplaintInterface;
use EO\CoreBundle\Model\Manager\ComplaintMessageManagerInterface;

class ComplaintMessageManager extends MongoDBManager implements ComplaintMessageManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByComplaint(ComplaintInterface $complaint)
    {
        $this->reload($complaint);
        return $complaint->getMessages();
    }
}