<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CompanyInterface;
use EO\CoreBundle\Model\CustomerInterface;
use EO\CoreBundle\Model\Manager\TicketManagerInterface;
use EO\CoreBundle\Model\NetworkInterface;

class TicketManager extends MongoDBManager implements TicketManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByCompany(CompanyInterface $company)
    {
        $networkManager = new NetworkManager($this->dm, 'EO\CoreBundle\Document\Network');
        $networks = $networkManager->findAllByCompany($company);

        return $this->repository->createQueryBuilder('t')
            ->field('network')->in($networks)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByNetwork(NetworkInterface $network)
    {
        return $this->repository->createQueryBuilder('t')
            ->field('network')->references($network)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByCustomer(CustomerInterface $customer)
    {
        return $this->repository->createQueryBuilder('t')
            ->field('ownBy')->references($customer)
            ->getQuery()
            ->execute();
    }

    /**
     * @param CustomerInterface $customerInterface
     *
     * @return array
     */
    public function findAllValidAndCompostedAndLastExpiredByCustomer(CustomerInterface $customerInterface)
    {
    }
}