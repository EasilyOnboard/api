<?php

namespace EO\CoreBundle\Document\Manager;

use FOS\UserBundle\Model\GroupInterface;
use FOS\UserBundle\Model\UserInterface;
use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\Manager\EmployeeManagerInterface;
use EO\CoreBundle\Model\CompanyInterface;

class EmployeeManager extends MongoDBManager implements EmployeeManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneByUser(UserInterface $user)
    {
        return $this->repository->createQueryBuilder()
            ->field('user')->references($user)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder()
            ->field('company')->references($company)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByGroup(GroupInterface $group)
    {
        $users = $this->dm->createQueryBuilder('EOCoreBundle:User')
            ->select('id')
            ->field('groups')->includesReferenceTo($group)
            ->getQuery()
            ->execute();

        $userIds = array();
        /** @var User $user */
        foreach ($users as $user) {
            $userIds[] = $user->getId();
        }

        return $this->repository->createQueryBuilder()
            ->field('user')->in($userIds)
            ->getQuery()
            ->execute();
    }
}
