<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CustomerInterface;
use EO\CoreBundle\Model\Manager\PaypalManagerInterface;

class PaypalManager extends MongoDBManager implements PaypalManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByCustomer(CustomerInterface $customer)
    {
        $this->reload($customer);
        return $customer->getPaypalAccounts();
    }
}