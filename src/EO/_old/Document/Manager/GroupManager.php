<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CompanyInterface;
use EO\CoreBundle\Model\Manager\GroupManagerInterface;

class GroupManager extends MongoDBManager implements GroupManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder('g')
            ->field('company')->references($company)
            ->getQuery()
            ->execute();
    }
}