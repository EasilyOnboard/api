<?php

namespace EO\CoreBundle\Document\Manager;

use FOS\UserBundle\Model\UserInterface;
use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\Manager\CustomerManagerInterface;

class CustomerManager extends MongoDBManager implements CustomerManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findOneByUser(UserInterface $user)
    {
        return $this->repository->createQueryBuilder()
            ->field('user')->references($user)
            ->getQuery()
            ->getSingleResult();
    }
}
