<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CompanyInterface;
use EO\CoreBundle\Document\Employee;

class CompanyManager extends MongoDBManager
{
    /**
     * @param CompanyInterface $company
     */
    public function delete($company)
    {
        $employeeManager = new EmployeeManager($this->dm, 'EO\CoreBundle\Document\Employee');
        $employees = $employeeManager->findAllByCompany($company);

        if ($employees != null) {
            /** @var Employee $employee */
            foreach ($employees as $employee) {
                $employeeManager->delete($employee);
            }
        }

        $this->dm->remove($company);
        $this->dm->flush();
    }
}
