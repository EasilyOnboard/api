<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CustomerInterface;
use EO\CoreBundle\Model\Manager\CreditCardManagerInterface;

class CreditCardManager extends MongoDBManager implements CreditCardManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByCustomer(CustomerInterface $customer)
    {
        $this->reload($customer);
        return $customer->getCreditCards()->toArray();
    }
}