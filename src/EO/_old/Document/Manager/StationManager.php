<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\Manager\StationManagerInterface;
use EO\CoreBundle\Model\NetworkInterface;

class StationManager extends MongoDBManager implements StationManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByNetwork(NetworkInterface $network)
    {
        $this->reload($network);
        return $network->getStations();
    }
}