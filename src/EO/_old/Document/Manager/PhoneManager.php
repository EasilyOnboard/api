<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CustomerInterface;
use EO\CoreBundle\Model\Manager\PhoneManagerInterface;

class PhoneManager extends MongoDBManager implements PhoneManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByCustomer(CustomerInterface $customer)
    {
        $this->reload($customer);
        return $customer->getPaypalAccounts();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByOs($os)
    {
        return $this->repository->createQueryBuilder('p')
            ->field('os')->equals($os)
            ->getQuery()
            ->execute();
    }
}