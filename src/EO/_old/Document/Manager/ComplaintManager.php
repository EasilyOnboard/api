<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CompanyInterface;
use EO\CoreBundle\Model\CustomerInterface;
use EO\CoreBundle\Model\EmployeeInterface;
use EO\CoreBundle\Model\Manager\ComplaintManagerInterface;

class ComplaintManager extends MongoDBManager implements ComplaintManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder('c')
            ->field('company')->references($company)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByCompanyUnassigned(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder('c')
            ->field('company')->references($company)
            ->field('employee')->references(null)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByEmployee(EmployeeInterface $employee)
    {
        return $this->repository->createQueryBuilder('f')
            ->field('employee')->references($employee)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByCustomer(CustomerInterface $customer)
    {
        return $this->repository->createQueryBuilder('f')
            ->field('customer')->references($customer)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllUnassigned()
    {
        return $this->repository->createQueryBuilder('f')
            ->field('employee')->references(null)
            ->getQuery()
            ->execute();
    }
}