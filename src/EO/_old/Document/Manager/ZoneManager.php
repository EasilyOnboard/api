<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\Manager\ZoneManagerInterface;
use EO\CoreBundle\Model\NetworkInterface;

class ZoneManager extends MongoDBManager implements ZoneManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByNetwork(NetworkInterface $network)
    {
        $this->reload($network);
        return $network->getZones();
    }
}