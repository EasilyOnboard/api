<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\CompanyInterface;
use EO\CoreBundle\Model\CustomerInterface;
use EO\CoreBundle\Model\Manager\OrderManagerInterface;

class OrderManager extends MongoDBManager implements OrderManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function createOrder($amount, $currency, $orderNumber)
    {
        return new $this->class($amount, $currency, $orderNumber);
    }

    /**
     * @param string $orderNumber
     * @return null|object
     */
    public function find($orderNumber)
    {
        return $this->findOneBy(array('orderNumber' => $orderNumber));
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByCompany(CompanyInterface $company)
    {
        return $this->repository->createQueryBuilder('o')
            ->field('company')->references($company)
            ->getQuery()
            ->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByCustomer(CustomerInterface $customer)
    {
        return $this->repository->createQueryBuilder('o')
            ->field('customer')->references($customer)
            ->getQuery()
            ->execute();
    }
}