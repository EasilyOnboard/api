<?php

namespace EO\CoreBundle\Document\Manager;

use EO\CoreBundle\Doctrine\MongoDBManager;
use EO\CoreBundle\Model\Manager\LineManagerInterface;
use EO\CoreBundle\Model\NetworkInterface;

class LineManager extends MongoDBManager implements LineManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByNetwork(NetworkInterface $network)
    {
        $this->reload($network);
        return $network->getLines();
    }
}