<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use EO\CoreBundle\Model\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class User.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class User extends BaseUser
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Group")
     */
    protected $groups;

    /**
     * @MongoDB\String
     * @Assert\NotBlank
     * @Assert\Length(max=45)
     */
    protected $firstName;

    /**
     * @MongoDB\String
     * @Assert\NotBlank
     * @Assert\Length(max=45)
     */
    protected $lastName;

    /**
     * @MongoDB\Date
     */
    protected $createdAt;
}