<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\Fine as BaseFine;

/**
 * Class Fine.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Fine extends BaseFine
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $message;

    /**
     * @var float
     *
     * @MongoDB\Float
     * @Assert\NotBlank()
     */
    protected $amount;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $currency;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $status;

    /**
     * @var Customer
     *
     * @MongoDB\ReferenceOne(targetDocument="Customer")
     */
    protected $customer;

    /**
     * @var Employee
     *
     * @MongoDB\ReferenceOne(targetDocument="Employee")
     */
    protected $inspector;

    /**
     * @var Company
     *
     * @MongoDB\ReferenceOne(targetDocument="Company")
     */
    protected $company;
}
