<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\Company as BaseCompany;

/**
 * Class Company.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Company extends BaseCompany
{
    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $email;

    /**
     * @var Address
     *
     * @MongoDB\EmbedOne(targetDocument="Address")
     */
    protected $fullAddress;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $currency;

    /**
     * @var Image
     *
     * @MongoDB\EmbedOne(targetDocument="Image")
     */
    protected $logo;

    /**
     * @var Image
     *
     * @MongoDB\EmbedOne(targetDocument="Image")
     */
    protected $ticketLogo;

    public function __construct()
    {
        $this->fullAddress = new Address();
        $this->logo = new Image();
        $this->ticketLogo = new Image();
    }
}