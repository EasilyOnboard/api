<?php

namespace EO\CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\Employee as BaseEmployee;

/**
 * Class Employee.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Employee extends BaseEmployee
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $firstname;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $lastname;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $email;

    /**
     * @var User
     *
     * @MongoDB\ReferenceOne(targetDocument="User")
     */
    protected $user;

    /**
     * @var Company
     *
     * @MongoDB\ReferenceOne(targetDocument="Company")
     */
    protected $company;
}