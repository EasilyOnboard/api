<?php

namespace EO\CoreBundle\Document;

use EO\CoreBundle\Model\Group as BaseGroup;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Group.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Group extends BaseGroup
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var Company
     *
     * @MongoDB\ReferenceOne(targetDocument="Company")
     */
    protected $company;
}