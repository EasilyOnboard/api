<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections\ArrayCollection;
use EO\CoreBundle\Model\Complaint as BaseComplaint;

/**
 * Class Complaint
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Complaint extends BaseComplaint
{
    /**
     * @MongoDB\Id
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String()
     * @Assert\NotBlank()
     */
    protected $body;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $comment;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    protected $updated;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    protected $closed;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $status;

    /**
     * @var ComplaintType
     *
     * @MongoDB\EmbedOne(targetDocument="ComplaintType")
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @var Customer
     *
     * @MongoDB\ReferenceOne(targetDocument="Customer")
     * @Assert\NotBlank()
     */
    protected $customer;

    /**
     * @var Employee
     *
     * @MongoDB\ReferenceOne(targetDocument="Employee")
     */
    protected $employee;

    /**
     * @var Company
     *
     * @MongoDB\ReferenceOne(targetDocument="Company")
     */
    protected $company;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\EmbedMany(targetDocument="ComplaintMessage")
     */
    protected $messages;
}