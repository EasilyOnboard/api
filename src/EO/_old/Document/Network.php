<?php

namespace EO\CoreBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\Network as BaseNetwork;

/**
 * Class Network.
 *
 * @MongoDB\Document
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Network extends BaseNetwork
{
    /**
     * @var Integer
     *
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $city;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $postcode;

    /**
     * @var string
     *
     * @MongoDB\Boolean
     * @Assert\NotBlank()
     */
    protected $status;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="Zone", mappedBy="network")
     */
    protected $zones;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="Line", mappedBy="network")
     */
    protected $lines;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="Station", mappedBy="network")
     */
    protected $stations;

    /**
     * @var Company
     *
     * @MongoDB\ReferenceOne(targetDocument="Company")
     * @Assert\NotBlank()
     */
    protected $company;
}
