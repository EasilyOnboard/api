<?php

namespace EO\CoreBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use JMS\Serializer\Annotation as JMS;
use EO\CoreBundle\Model\Customer as BaseCustomer;

/**
 * Class Customer
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Customer extends BaseCustomer
{
    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $firstname;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $lastname;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $email;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank
     */
    protected $sex;

    /**
     * @var \Datetime
     *
     * @MongoDB\Date
     * @Assert\NotBlank
     * @Assert\Date
     * @JMS\Type("DateTime<'Y-m-d'>")
     */
    protected $birthday;

    /**
     * @var User
     *
     * @MongoDB\ReferenceOne(targetDocument="User")
     * @Assert\Type("EO\CoreBundle\Document\User")
     * @Assert\Valid
     */
    protected $user;

    /**
     * @var Address
     *
     * @MongoDB\EmbedOne(targetDocument="Address")
     * @Assert\Type("EO\CoreBundle\Document\Address")
     * @Assert\Valid
     */
    protected $fullAddress;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\EmbedMany(targetDocument="Paypal")
     */
    protected $paypalAccounts;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\EmbedMany(targetDocument="CreditCard")
     */
    protected $creditCards;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\EmbedMany(targetDocument="Phone")
     */
    protected $mobiles;

    /**
     * @var Family
     *
     * @MongoDB\ReferenceOne(targetDocument="Family")
     */
    protected $family;

    public function __construct()
    {
        parent::__construct();
        $this->fullAddress = new Address();
    }
}