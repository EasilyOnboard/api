<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\Address as BaseAddress;

/**
 * Class Address
 *
 * @MongoDB\EmbeddedDocument
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Address extends BaseAddress
{
    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    protected $address;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    protected $city;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank
     * @Assert\Length(min=3, max=10)
     */
    protected $postcode;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    protected $state;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank
     * @Assert\Length(min=2, max=2)
     */
    protected $country;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank
     * @Assert\Length(min=7, max=16)
     */
    protected $phone;
}