<?php

namespace EO\CoreBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\Payment as BaseOrder;

class Order extends BaseOrder
{
    /**
     * @var string
     *
     * @MongoDB\String
     * @MongoDB\Index(options={"unique"=true, "safe"=true, "sparse"=true})
     */
    protected $orderNumber;

    /**
     * @var float
     *
     * @ODM\Float
     */
    protected $amount;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $currency;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="TicketType")
     */
    protected $ticketTypes;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="Ticket")
     */
    protected $tickets;

    /**
     * @var Customer
     *
     * @MongoDB\ReferenceOne(targetDocument="Customer")
     */
    protected $customer;

    /**
     * @var Company
     *
     * @MongoDB\ReferenceOne(targetDocument="Company")
     */
    protected $company;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    protected $date;
}