<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use EO\CoreBundle\Model\Device as BasePhone;

/**
 * Class Phone
 *
 * @MongoDB\EmbeddedDocument
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Phone extends BasePhone
{
    /**
     * @MongoDB\Id(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $mac;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $os;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $model;

    /**
     * @var boolean
     *
     * @MongoDB\Boolean
     * @Assert\NotBlank()
     */
    protected $blocked;

    /**
     * @var \Datetime
     *
     * @MongoDB\Date
     * @Assert\NotBlank()
     */
    protected $date;

    /**
     * @var \Datetime
     *
     * @MongoDB\Date
     * @Assert\NotBlank()
     */
    protected $lastLogin;
}
