<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections\ArrayCollection;
use EO\CoreBundle\Model\Zone as BaseZone;

/**
 * Class Zone.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Zone extends BaseZone
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var float
     *
     * @MongoDB\Float
     * @Assert\NotBlank()
     */
    protected $price;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $color;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\ReferenceMany(targetDocument="Station", mappedBy="zone")
     */
    protected $stations;

    /**
     * @var Network
     *
     * @MongoDB\ReferenceOne(targetDocument="Network", inversedBy="zones")
     * @Assert\NotBlank()
     */
    protected $network;
}
