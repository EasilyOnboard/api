<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use EO\CoreBundle\Model\Image as BaseImage;

/**
 * Class Image
 *
 * @MongoDB\EmbeddedDocument()
 * @Vich\Uploadable()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Image extends BaseImage
{
    /**
     * @var File
     *
     * @Vich\UploadableField(mapping="logo", fileNameProperty="name")
     * @Assert\File(
     *  maxSize="5M",
     *  mimeTypes={"image/png", "image/jpeg", "image/pjpeg"}
     * )
     */
    protected $file;

    /**
     * @var string
     *
     * @MongoDB\String
     */
    protected $name;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    protected $createdAt;

    /**
     * @var \DateTime
     *
     * @MongoDB\Date
     */
    protected $updatedAt;
}