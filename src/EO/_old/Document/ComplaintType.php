<?php

namespace EO\CoreBundle\Document;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Doctrine\Common\Collections\ArrayCollection;
use EO\CoreBundle\Model\ComplaintType as BaseComplaintType;

/**
 * Class ComplaintType.
 *
 * @MongoDB\Document()
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class ComplaintType extends BaseComplaintType
{
    /**
     * @var Integer
     *
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $label;

    /**
     * @var ArrayCollection
     *
     * @MongoDB\EmbedMany(targetDocument="ComplaintType")
     */
    protected $subtypes;
}
