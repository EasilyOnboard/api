<?php

namespace EO\CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;
use EO\CoreBundle\Model\Paypal as BasePaypal;

/**
 * Class Paypal
 *
 * @MongoDB\EmbeddedDocument
 *
 * @author Maxime Perrimond <max.perrimond@gmail.com>
 */
class Paypal extends BasePaypal
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @var string
     *
     * @MongoDB\String
     * @Assert\NotBlank()
     */
    protected $account;
}
