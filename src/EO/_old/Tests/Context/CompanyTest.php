<?php

namespace EO\ApiBundle\Tests\Context;

use Doctrine\Common\Collections\ArrayCollection;
use EO\ApiBundle\Tests\BaseTest;
use EO\CoreBundle\Doctrine\Manager\DBAL\ManagerInterface;
use EO\CoreBundle\Model\Company;
use EO\CoreBundle\Model\UserInterface;
use FOS\RestBundle\Util\Codes;

class CompanyTest extends BaseTest
{
    /**
     * @var Company
     */
    private $companyTest;

    /**
     * @var ArrayCollection
     */
    private $entities;

    public function setUp()
    {
        parent::setUp();
        $this->entities = new ArrayCollection();
    }

    public function test()
    {
        // Admin user creation
        /** @var UserInterface $admin */
        $admin = $this->getUserManager()->create();
        $admin->setEnabled(true);
        $admin->addRole('ROLE_SUPER_ADMIN');
        $admin->setEmail('admin@test.com');
        $admin->setPlainPassword('password');
        $this->getUserManager()->getFOSUserManager()->updateUser($admin);
        $this->entities->add($admin);

        // Login with admin
        $login = $this->login("admin@test.com", "password");
        $this->assertTrue($login);

        // Company creation
        $form = array(
            'name' => 'Company Test',
            'email' => 'contact@company.test.com',
            'full_address' => array(
                'address' => '42 road of test',
                'city' => 'City of test',
                'postcode' => '424242',
                'state' => 'test state',
                'country' => 'UA',
                'phone' => '0983494835'),
            'currency' => 'UAH'
        );
        $response = $this->executeRequest($this->createRequest('eo_back_new_company', array(), $form, 'POST'));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_CREATED);
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());
        $id = json_decode($response->getContent())->id;
        $this->companyTest = $this->getCompanyManager()->find($id);
        $this->assertNotNull($this->companyTest);

        // Groups creation
        $form = array(
            'name' => 'Test manager',
            'roles' => array('ROLE_ADMIN')
        );
        $groupManager = $this->createEntity('eo_back_new_company_group', $form, array(
            'cid' => $this->companyTest->getId()),
            $this->getGroupManager()
        );

        $form = array(
            'name' => 'Test team',
            'roles' => array('ROLE_CUSTOMER_LIST', 'ROLE_TICKET_ADMIN')
        );
        $groupTeam = $this->createEntity('eo_back_new_company_group', $form, array(
            'cid' => $this->companyTest->getId()
        ), $this->getGroupManager());

        /*
         * Staff creation
         */
        $form = array(
            'email' => 'staff.1@test.com',
            'first_name' => 'staff',
            'last_name' => '1',
            'language' => 'uk',
            'groups' => array($groupManager->getId()),
            'plain_password' => array(
                'first' => 'password',
                'second' => 'password'
            )
        );
        $staff1 = $this->createEntity('eo_back_new_company_staff', $form, array(
            'cid' => $this->companyTest->getId()
        ), $this->getUserManager());

        $form = array(
            'email' => 'staff.2@test.com',
            'first_name' => 'staff',
            'last_name' => '2',
            'language' => 'uk',
            'groups' => array($groupManager->getId(), $groupTeam->getId()),
            'plain_password' => array(
                'first' => 'password',
                'second' => 'password'
            )
        );
        $staff2 = $this->createEntity('eo_back_new_company_staff', $form, array(
            'cid' => $this->companyTest->getId()
        ), $this->getUserManager());


        $form = array(
            'email' => 'staff.3@test.com',
            'first_name' => 'staff',
            'last_name' => '3',
            'language' => 'uk',
            'groups' => array($groupTeam->getId()),
            'plain_password' => array(
                'first' => 'password',
                'second' => 'password'
            )
        );
        $staff3 = $this->createEntity('eo_back_new_company_staff', $form, array(
            'cid' => $this->companyTest->getId()
        ), $this->getUserManager());


        // Second Group creation
        $form = array(
            'name' => 'Test group',
            'roles' => array('ROLE_INSPECTOR'),
            'members' => array($staff1->getId(), $staff2->getId(), $staff3->getId())
        );
        $groupTest = $this->createEntity('eo_back_new_company_group', $form, array(
            'cid' => $this->companyTest->getId()),
            $this->getGroupManager()
        );

        $form = array(
            'name' => 'Test group',
            'roles' => array()
        );
        $groupTest2 = $this->createEntity('eo_back_new_company_group', $form, array(
            'cid' => $this->companyTest->getId()),
            $this->getGroupManager()
        );

        $response = $this->executeRequest($this->createRequest('eo_back_get_company_group', array(
            'cid' => $this->companyTest->getId(),
            'id' => $groupTest->getId()
        )));
        $this->printData($response->getContent());

        /*
         * Delete staff
         */
        $this->deleteEntity('eo_back_delete_company_staff', array(
            'cid' => $this->companyTest->getId(),
            'id' => $staff1->getId()
        ));
        $this->deleteEntity('eo_back_delete_company_staff', array(
            'cid' => $this->companyTest->getId(),
            'id' => $staff2->getId()
        ));
        $this->deleteEntity('eo_back_delete_company_staff', array(
            'cid' => $this->companyTest->getId(),
            'id' => $staff3->getId()
        ));

        /*
         * Delete groups
         */
        $this->deleteEntity('eo_back_delete_company_group', array(
            'cid' => $this->companyTest->getId(),
            'id' => $groupTeam->getId()
        ));

        $this->deleteEntity('eo_back_delete_company_group', array(
            'cid' => $this->companyTest->getId(),
            'id' => $groupManager->getId()
        ));
        $this->deleteEntity('eo_back_delete_company_group', array(
            'cid' => $this->companyTest->getId(),
            'id' => $groupTest->getId()
        ));
        $this->deleteEntity('eo_back_delete_company_group', array(
            'cid' => $this->companyTest->getId(),
            'id' => $groupTest2->getId()
        ));
    }

    /**
     * @param string $route
     * @param array $form
     * @param array $param
     * @param ManagerInterface $manager
     * @return mixed
     */
    public function createEntity($route, array $form, array $param = array(), $manager) {
        $response = $this->executeRequest($this->createRequest($route, $param, $form, 'POST'));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_CREATED, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());
        $id = json_decode($response->getContent())->id;
        $entity = $manager->find($id);
        $this->assertNotNull($entity);
        $this->entities->add($entity);
        return $entity;
    }

    /**
     * @param string $route
     * @param array $param
     */
    public function deleteEntity($route, array $param)
    {
        $response = $this->executeRequest($this->createRequest($route, $param, array(), 'DELETE'));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_ACCEPTED, $response->getContent());
    }

    public function tearDown()
    {
        $this->logout();
        $admin = $this->getUserByEmail("admin@test.com");
        if ($admin) {
            $this->getUserManager()->delete($admin);
        }

        $this->getCompanyManager()->delete($this->companyTest);
    }
}