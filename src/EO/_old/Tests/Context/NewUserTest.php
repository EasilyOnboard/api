<?php

namespace EO\ApiBundle\Tests\Context;

use EO\ApiBundle\Tests\BaseTest;

class NewUserTest extends BaseTest
{
    public function test()
    {
        $form = array(
            'user' => array(
                'email' => 'max.perrimond@gmail.com',
                //'first_name' => 'max',
                'last_name' => 'mad',
                'language' => 'fr',
                'plain_password' => array(
                    'first' => 'password',
                    'second' => 'password'
                )
            ),
            'device' => array(
                'appId' => '987897',
                'name' => 'iphone de max',
                'os' => 'iOS',
                'model' => 'iPhone 5s'
            )
        );
        $request = $this->generateRequest($this->generateUrl('eo_security_register'), $form, 'POST');
        $response = $this->doRequest($request);

        $this->printData($response->getStatusCode());
        $this->printData($response->getContent());
    }
}