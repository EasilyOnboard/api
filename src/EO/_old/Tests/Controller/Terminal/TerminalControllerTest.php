<?php

namespace EO\ApiBundle\Tests\Controller\Terminal;

use EO\ApiBundle\Tests\BaseTest;
use EO\CoreBundle\Model\UserInterface;
use FOS\RestBundle\Util\Codes;

class TerminalControllerTest extends BaseTest
{
    const USER_USERNAME = "user-X-1@terminal.eo";
    const USER_PASSWORD = "password";

    public function testWhoiam()
    {
        $this->assertTrue($this->login(self::USER_USERNAME, self::USER_PASSWORD));
        $response = $this->executeRequest($this->createRequest('eo_terminal_me'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK);
        $this->printData($response->getContent());
        $this->logout();
    }

    public function testCompost()
    {
        /** @var UserInterface $user */
        $user = $this->getUserManager()->create();
        $user->setFirstName("test");
        $user->setLastName("test");
        $user->setEmail("test@test.eo");
        $user->setPlainPassword("password");
        $this->getUserManager()->getFOSUserManager()->updateUser($user);
        $terminal = $this->getUserByEmail(self::USER_USERNAME)->getTerminal();
        $ticketTypes = $this->getTicketTypeManager()->findAllByNetwork($terminal->getStation()->getNetwork());
        $ticket = $this->getTicketFactory()->create($user, $ticketTypes[0]);
        $this->getTicketManager()->update($ticket);
        $this->assertTrue($this->login(self::USER_USERNAME, self::USER_PASSWORD));
        $response = $this->executeRequest($this->createRequest('eo_terminal_compost_ticket_customer', array(
            'tid' => $ticket->getId(),
            'cid' => $user->getId())));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getStatusCode());
        $this->logout();
    }

    public function setUp()
    {
        parent::setUp();
        $user = $this->getUserByEmail("test@test.eo");
        if ($user) {
            $tickets = $this->getTicketManager()->findAllByCustomer($user);
            foreach ($tickets as $ticket) {
                $this->getTicketManager()->delete($ticket);
            }
            $this->getUserManager()->delete($user);
        }
    }
}