<?php

namespace EO\ApiBundle\Tests\Controller\Inspector;

use EO\ApiBundle\Tests\BaseTest;

class InspectorControllerTest extends BaseTest
{
    public function testInspector()
    {
        $this->assertTrue($this->login("inspecteur.gadget@scep.eg", "password"));
        $response = $this->executeRequest($this->createRequest('eo_inspector_get_networks'));
        $this->printData($response->getContent());
        $networks = json_decode($response->getContent());
        foreach ($networks->networks as $network) {
            $response = $this->executeRequest($this->createRequest('eo_inspector_get_network_lines', array(
                'networkId' => $network->id
            )));
            $this->printData($response->getContent());
            $lines = json_decode($response->getContent());
            foreach ($lines->lines as $line) {
                $response = $this->executeRequest($this->createRequest('eo_inspector_get_network_line_stations', array(
                    'networkId' => $network->id,
                    'lineId' => $line->id
                )));
                $this->printData($response->getContent());
            }
        }
        $response = $this->executeRequest($this->createRequest('eo_inspector_get_fine_types'));
        $this->printData($response->getContent());
        $this->logout();
    }
}