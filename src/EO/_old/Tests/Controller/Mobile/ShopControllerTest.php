<?php

namespace EO\ApiBundle\Tests\Controller\Mobile;

use EO\ApiBundle\Tests\BaseTest;
use FOS\RestBundle\Util\Codes;

class ShopControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "heisenberg@gmail.com";
    const USER_TEST_PASSWORD = "password";

    public function testGetters()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);


        $response = $this->executeRequest($this->createRequest('eo_mobile_shop_list_validity'));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
        $this->printData($response->getContent());

        $response = $this->executeRequest($this->createRequest('eo_mobile_shop_list_networks'));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());

        $networks = json_decode($response->getContent(), true);
        $this->assertNotEmpty($networks['networks']);
        foreach ($networks['networks'] as $network) {
            $id = $network['id'];
            $response = $this->executeRequest($this->createRequest('eo_mobile_shop_list_network_zones', array('nid' => $id)));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK);
            $response = $this->executeRequest($this->createRequest('eo_mobile_shop_list_network_lines', array('nid' => $id)));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK);
            $response = $this->executeRequest($this->createRequest('eo_mobile_shop_list_network_stations', array('nid' => $id)));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK);
            $response = $this->executeRequest($this->createRequest('eo_mobile_shop_list_network_ticket_types', array('nid' => $id)));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK);
            $response = $this->executeRequest($this->createRequest('eo_mobile_shop_list_network_line_types', array('nid' => $id)));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK);
        }
        $this->logout();
    }

    public function testSearchNetwork()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        $response = $this->executeRequest($this->createRequest('eo_mobile_shop_search_networks', array('arg' => 'ca')));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());
        $this->logout();
    }

    public function testSearchStation()
    {
        /** @var NetworkInterface $network */
        $network = $this->getNetworkManager()->findOneBy(array('name' => 'Cairo'));

        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);

        $response = $this->executeRequest($this->createRequest('eo_mobile_shop_search_network_stations', array(
            'nid' => $network->getId(),
            'arg' => 'Orabi'
        )));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());
        $this->logout();
    }

    public function testNewTicketType()
    {
        /** @var NetworkInterface $network */
        $network = $this->getNetworkManager()->findOneBy(array('name' => 'Cairo'));

        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);

        $form = array(
            'station_a' => $this->getStationManager()->findOneBy(array('name' => 'Orabi'))->getId(),
            'station_b' => $this->getStationManager()->findOneBy(array('name' => 'Kit Kat'))->getId(),
            'validity' => $this->getValidityManager()->findAll()[1]->getId()
        );

        $response = $this->executeRequest($this->createRequest('eo_mobile_shop_new_network_ticket_types', array(
            'nid' => $network->getId()
        ), $form, 'POST'));

        $this->assertTrue($response->getStatusCode() === Codes::HTTP_ACCEPTED, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $ticketType = $this->getTicketTypeManager()->find(json_decode($response->getContent())->id);
        $form = array(
            'ticket_types' => array($ticketType->getId())
        );
        $response = $this->executeRequest($this->createRequest('eo_mobile_shop_buy_me_that_shit', array(), $form, 'POST'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getContent());
        $this->printData($response->getContent());
        $this->getTicketTypeManager()->delete($ticketType);
        $ticket = $this->getTicketManager()->find(json_decode($response->getContent())->tickets[0]->id);
        $this->getTicketManager()->delete($ticket);
        $this->logout();
    }
}