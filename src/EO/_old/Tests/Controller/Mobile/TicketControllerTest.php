<?php

namespace EO\ApiBundle\Tests\Controller\Mobile;

use EO\ApiBundle\Tests\BaseTest;
use EO\TicketBundle\Enum\TicketStatus;
use FOS\RestBundle\Util\Codes;

class TicketControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "heisenberg@gmail.com";
    const USER_TEST_PASSWORD = "password";
    const USER_TEST_2_EMAIL = "w.w.jr@gmail.com";
    const NETWORK_NAME = "Cairo";

    public function testTickets()
    {
        $user = $this->getUserByEmail(self::USER_TEST_EMAIL);
        $response = $this->doRequest($this->eosRequest->generateNewRequest(
            $user, self::USER_TEST_PASSWORD, $this->generateUrl('eo_mobile_list_tickets')));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
    }

    public function testShareTicket()
    {
        $user = $this->getUserByEmail(self::USER_TEST_EMAIL);
        $user2 = $this->getUserByEmail(self::USER_TEST_2_EMAIL);

        /** @var NetworkInterface $network */
        $network = $this->getNetworkManager()->findOneBy(array("name" => self::NETWORK_NAME));
        $this->assertNotNull($network, "Test network missing in the DB");
        /** @var TicketTypeInterface $ticketType */
        $ticketType = $this->getTicketTypeManager()->create();
        $ticketType->setPrice(1.);
        $ticketType->setCurrency($network->getCompany()->getCurrency());
        $ticketType->setNetwork($network);
        $ticketType->setTerm(new \DateInterval('PT2H0M0S'));
        /** @var TicketFactoryInterface $ticketFactory */
        $ticketFactory = $this->container->get('eo_core.factory.ticket');
        $ticket = $ticketFactory->create($user, $ticketType);
        $this->getTicketManager()->update($ticket);

        $form = array(
            'own_by' => $user2->getId()
        );

        $request = $this->eosRequest->generateNewRequest(
            $user, self::USER_TEST_PASSWORD,
            $this->generateUrl('eo_mobile_patch_ticket_share', array('id' => $ticket->getId())),
            'PATCH', $form
        );

        $ticket->setStatus(EOCore::TICKET_STATUS_COMPOSTED);
        $this->getTicketManager()->update($ticket);

        $response = $this->doRequest($request);
        $this->assertTrue($response->getStatusCode() !== Codes::HTTP_ACCEPTED);

        $ticket->setStatus(TicketStatus::VALID);
        $this->getTicketManager()->update($ticket);

        $response = $this->doRequest($request);
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_ACCEPTED, $response->getContent());

        $response = $this->doRequest($request);
        $this->assertTrue($response->getStatusCode() !== Codes::HTTP_ACCEPTED);

        $this->getTicketManager()->delete($ticket);
    }
}
