<?php

namespace EO\ApiBundle\Tests\Controller\Back;

use EO\ApiBundle\Tests\BaseTest;
use FOS\RestBundle\Util\Codes;

class NetworkControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "lucien.bramard@scep.eg";
    const USER_TEST_PASSWORD = "password";

    public function testListNetwork()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);

        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL);
        $cid = $user->getCompany()->getId();

        $response = $this->executeRequest($this->createRequest('eo_back_list_company_networks', array('cid' => $cid)));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());

        $this->printData($response->getContent());
        $networks = json_decode($response->getContent());
        foreach ($networks->networks as $network) {
            $response = $this->executeRequest($this->createRequest('eo_back_get_company_network', array(
                'cid' => $cid,
                'nid' => $network->id
            )));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
            $this->printData($response->getContent());

            $response = $this->executeRequest($this->createRequest('eo_back_list_company_network_zones', array(
                'cid' => $cid,
                'nid' => $network->id
            )));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
            $this->printData($response->getContent());

            $response = $this->executeRequest($this->createRequest('eo_back_list_company_network_lines', array(
                'cid' => $cid,
                'nid' => $network->id
            )));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
            $this->printData($response->getContent());

            $lines = json_decode($response->getContent());
            foreach ($lines->lines as $line) {
                $response = $this->executeRequest($this->createRequest('eo_back_get_company_network_line', array(
                    'cid' => $cid,
                    'nid' => $network->id,
                    'id' => $line->id
                )));
                $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
                $this->printData($response->getContent());
            }

            $response = $this->executeRequest($this->createRequest('eo_back_list_company_network_stations', array(
                'cid' => $cid,
                'nid' => $network->id
            )));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK, $response->getContent());
            $this->printData($response->getContent());
        }

        $this->logout();
    }

    public function testGetNetwork()
    {

    }

    public function testNewNetwork()
    {

    }

    public function testPutNetwork()
    {

    }

    public function testPatchNetwork()
    {

    }

    public function testDeleteNetwork()
    {

    }
}