<?php

use EO\ApiBundle\Tests\BaseTest;

class StaffControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "lucien.bramard@scep.eg";
    const USER_TEST_PASSWORD = "password";

    public function testListStaff()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        /** @var \EO\CoreBundle\Model\UserInterface $user */
        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL);

        $response = $this->executeRequest($this->createRequest('eo_back_list_company_staff',
            array("cid" => $user->getCompany()->getId())
        ));
        $this->assertTrue($response->getStatusCode() == \FOS\RestBundle\Util\Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());
        $this->logout();
    }

    public function testGetStaff()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        /** @var \EO\CoreBundle\Model\UserInterface $user */
        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL);

        $response = $this->executeRequest($this->createRequest('eo_back_get_company_staff', array(
                "cid" => $user->getCompany()->getId(),
                "id" => $user->getId()
            )));
        $this->assertTrue($response->getStatusCode() == \FOS\RestBundle\Util\Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());

        $response = $this->executeRequest($this->createRequest('eo_back_get_company_staff', array(
            "cid" => $user->getCompany()->getId(),
            "id" => 0
        )));
        $this->assertTrue($response->getStatusCode() == \FOS\RestBundle\Util\Codes::HTTP_NOT_FOUND, $response->getContent());
        $this->assertNotEmpty($response->getContent());

        $this->logout();
    }
}