<?php

namespace EO\ApiBundle\Tests\Controller\Back;

use EO\ApiBundle\Tests\BaseTest;
use EO\CoreBundle\Model\UserInterface;
use FOS\RestBundle\Util\Codes;

class CustomerControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "lucien.bramard@scep.eg";
    const USER_TEST_PASSWORD = "password";

    public function testListCustomer()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);

        $response = $this->executeRequest($this->createRequest('eo_back_customer_list_customers'));

        $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());

        $customers = json_decode($response->getContent());

        /** @var UserInterface $customer */
        foreach ($customers->customers as $customer) {
            $response = $this->executeRequest($this->createRequest('eo_back_customer_get_customer', array(
                'id' => $customer->id
            )));
            $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK, $response->getContent());
            $this->assertNotEmpty($response->getContent());

            $this->printData($response->getContent());
        }
        $this->logout();
    }
/*
    public function testGetFamily()
    {
        $customer = $this->getUserByEmail('heisenberg@gmail.com');

        $user = $this->getUserByEmail(self::USER_TEST_EMAIL);
        $response = $this->doRequest($this->eosRequest->generateNewRequest(
            $user, self::USER_TEST_PASSWORD, $this->generateUrl('eo_back_customer_get_customer_family', array(
            'id' => $customer->getFamily()->getId()
        ))));

        $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());

        $this->printData($response->getContent());
    }*/
}
