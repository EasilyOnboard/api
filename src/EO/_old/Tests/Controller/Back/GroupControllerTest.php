<?php

namespace EO\ApiBundle\Tests\Controller\Back;

use EO\ApiBundle\Tests\BaseTest;
use EO\CoreBundle\Model\Group;
use FOS\RestBundle\Util\Codes;

class GroupControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "lucien.bramard@scep.eg";
    const USER_TEST_PASSWORD = "password";

    public function testGroup()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        /** @var \EO\CoreBundle\Model\UserInterface $user */
        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL);

        $response = $this->executeRequest($this->createRequest('eo_back_list_company_groups',
            array("cid" => $user->getCompany()->getId())
        ));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());

        /** @var Group $group */
        foreach ($user->getGroups() as $group) {
            $response = $this->executeRequest($this->createRequest('eo_back_get_company_group',
                array("cid" => $group->getCompany()->getId(), "id" => $group->getId())
            ));
            $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK, $response->getContent());
            $this->assertNotEmpty($response->getContent());
            $this->printData($response->getContent());
        }

        $response = $this->executeRequest($this->createRequest('eo_back_get_company_group_roles'));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK);
        $this->printData($response->getContent());

        $this->logout();
    }
}