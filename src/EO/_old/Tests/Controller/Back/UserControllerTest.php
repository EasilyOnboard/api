<?php

namespace EO\ApiBundle\Tests\Controller\Back;

use EO\ApiBundle\Tests\BaseTest;
use FOS\RestBundle\Util\Codes;

class UserControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "lucien.bramard@scep.eg";
    const USER_TEST_PASSWORD = "password";

    public function testProfile()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        $response = $this->executeRequest($this->createRequest('eo_back_user_get_profile'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());
        $this->logout();
    }


    public function testPutProfileAction()
    {
        $user = $this->getUserByEmail(self::USER_TEST_EMAIL);
        $currentFirstName = $user->getFirstname();
        $currentLastName = $user->getLastname();
        $newFirstName = 'sylvestre';
        $newLastName = 'michel';
        $form = array(
            'email' => self::USER_TEST_EMAIL,
            'first_name' => $newFirstName,
            'last_name' => $newLastName,
            'language' => 'en',
        );
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        $response = $this->executeRequest($this->createRequest('eo_back_user_get_profile', array(), $form, 'PUT'));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_ACCEPTED, "Update not accepted, code: ".$response->getStatusCode()."\n".$response->getContent());
        $this->getUserManager()->reload($user);
        $this->assertTrue($newFirstName === $user->getFirstName(), 'Update not effective for the first name');
        $this->assertTrue($newLastName === $user->getLastName(), 'Update not effective for the last name');
        $user->setFirstName($currentFirstName);
        $user->setLastName($currentLastName);
        $this->getUserManager()->getFOSUserManager()->updateUser($user);
        $this->logout();
    }
}