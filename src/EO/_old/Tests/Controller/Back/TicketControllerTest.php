<?php

namespace EO\ApiBundle\Tests\Controller\Back;

use EO\ApiBundle\Tests\BaseTest;
use FOS\RestBundle\Util\Codes;

class TicketControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "lucien.bramard@scep.eg";
    const USER_TEST_PASSWORD = "password";

    public function testGettersTicket()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL);
        $company = $user->getCompany();
        $response = $this->executeRequest($this->createRequest('eo_back_list_tickets'));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK);
        $this->printData($response->getContent());
        $tickets = json_decode($response->getContent());
        foreach ($tickets as $ticket) {
            $response = $this->executeRequest($this->createRequest('eo_back_get_ticket', array('id' => $ticket->id)));
            $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK);
            $this->printData($response->getContent());
        }
        $response = $this->executeRequest($this->createRequest('eo_back_list_company_tickets', array(
            'cid' => $company->getId()
        )));
        $this->assertTrue($response->getStatusCode() === Codes::HTTP_OK);
        $this->printData($response->getContent());
        $this->logout();
    }
}