<?php

namespace EO\ApiBundle\Tests\Controller\User;

use EO\ApiBundle\Tests\BaseTest;
use EO\UserBundle\Enum\Sex;
use FOS\RestBundle\Util\Codes;

class ProfileControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "heisenberg@gmail.com";
    const USER_TEST_PASSWORD = "password";

    public function testProfile()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        $response = $this->executeRequest($this->createRequest('eo_user_get_profile'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->printData($response->getContent());
        $this->assertTrue($this->refresh());
        $this->logout();
    }

    public function testPutProfileAction()
    {
        $user = $this->getUserByEmail(self::USER_TEST_EMAIL);

        $currentFirstName = $user->getFirstname();
        $currentLastName = $user->getLastname();
        $currentSex = $user->getSex();
        $currentBirthday = $user->getBirthday();
        $currentPhone = $user->getPhone();

        $newFirstName = 'sylvestre';
        $newLastName = 'michel';
        $newSex = ($currentSex === Sex::MALE) ? Sex::FEMALE : Sex::MALE;
        $newBirthday = new \DateTime("now");
        $newPhone = "075677550";

        $form = array(
            'email' => self::USER_TEST_EMAIL,
            'first_name' => $newFirstName,
            'last_name' => $newLastName,
            'sex' => $newSex,
            'birthday' => $newBirthday->format('Y-m-d'),
            'language' => 'en',
            'full_address' => array(
                'address' => $user->getAddress(),
                'city' => $user->getCity(),
                'postcode' => $user->getPostcode(),
                'state' => $user->getState(),
                'country' => $user->getCountry(),
                'phone' => $newPhone,
            ),
        );

        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        $response = $this->executeRequest($this->createRequest('eo_user_put_profile', array(), $form, 'PUT'));

        $this->assertTrue($response->getStatusCode() === Codes::HTTP_ACCEPTED, "Update not accepted, code: ".$response->getStatusCode()."\n".$response->getContent());

        $this->getUserManager()->reload($user);

        $this->assertTrue($newFirstName === $user->getFirstName(), 'Update not effective for the first name');
        $this->assertTrue($newLastName === $user->getLastName(), 'Update not effective for the last name');
        $this->assertTrue($newSex === $user->getSex(), 'Update not effective for the sex');
        $this->assertTrue($newBirthday->format("Y-m-d") == $user->getBirthday()->format("Y-m-d"), 'Update not effective for the birthday');
        $this->assertTrue($newPhone === $user->getPhone(), 'Update not effective for the phone');

        $user->setFirstName($currentFirstName);
        $user->setLastName($currentLastName);
        $user->setSex($currentSex);
        $user->setBirthday($currentBirthday);
        $user->setPhone($currentPhone);

        $this->getUserManager()->getFOSUserManager()->updateUser($user);
        $this->logout();
    }

    public function testPatchProfileAction()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        $form = array(
            'current_password' => self::USER_TEST_PASSWORD,
            'new_password' => array(
                'first' => "pas5w0rd",
                'second' => "pas5w0rd",
            ),
        );
        $response = $this->executeRequest($this->createRequest('eo_user_patch_profile', array(), $form, 'PATCH'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getContent());
        $form = array(
            'current_password' => "pas5w0rd",
            'new_password' => array(
                'first' => "test",
                'second' => "password",
            ),
        );
        $response = $this->executeRequest($this->createRequest('eo_user_patch_profile', array(), $form, 'PATCH'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_BAD_REQUEST, $response->getContent());
        $form = array(
            'current_password' => "test",
            'new_password' => array(
                'first' => "pas5w0rd",
                'second' => "pas5w0rd",
            ),
        );
        $response = $this->executeRequest($this->createRequest('eo_user_patch_profile', array(), $form, 'PATCH'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_BAD_REQUEST, $response->getContent());
        $form = array(
            'current_password' => "pas5w0rd",
            'new_password' => array(
                'first' => "password",
                'second' => "password",
            ),
        );
        $response = $this->executeRequest($this->createRequest('eo_user_patch_profile', array(), $form, 'PATCH'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getContent());
        $this->logout();
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        $this->logout();
    }

    public function testGetFamilyMembersAction()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USER_TEST_PASSWORD);
        $this->assertTrue($login);
        $response = $this->executeRequest($this->createRequest('eo_user_get_family'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK, $response->getContent());
    }
}