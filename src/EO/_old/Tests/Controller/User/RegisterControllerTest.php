<?php

namespace EO\ApiBundle\Tests\Controller\User;

use EO\ApiBundle\Tests\BaseTest;
use FOS\RestBundle\Util\Codes;

class RegisterControllerTest extends BaseTest
{
    public function testRegisterAction()
    {
        $email = "test.unit@gmail.com";
        $firstName = "test";
        $lastName = "unit";
        $password = "password";
        $form = array(
            'user' => array(
                'email' => $email,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'language' => 'fr',
                'plain_password' => array(
                    'first' => $password,
                    'second' => $password
                )));
        $response = $this->executeRequest($this->createRequest('eo_security_register', array(), $form, 'POST'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_CREATED, "Register fail, code: ".$response->getStatusCode()."\n".$response->getContent());
        $user = $this->getUserByEmail($email);
        $this->getUserManager()->getFOSUserManager()->deleteUser($user);
        $this->assertNull($this->getUserManager()->getFOSUserManager()->findUserByEmail($email), "Fail to delete the test user.");
    }
}