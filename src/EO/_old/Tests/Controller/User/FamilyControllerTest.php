<?php

namespace EO\ApiBundle\Tests\Controller\User;

use EO\ApiBundle\Tests\BaseTest;
use EO\CoreBundle\Model\FamilyInterface;
use FOS\RestBundle\Util\Codes;

class FamilyControllerTest extends BaseTest
{
    const USER_TEST_EMAIL = "heisenberg@gmail.com";
    const USER_2_TEST_EMAIL = "skyler.white@gmail.com";
    const USER_3_TEST_EMAIL = "customer@test.com";
    const USERS_TEST_PASSWORD = "password";

    public function testGetFamily()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USERS_TEST_PASSWORD);
        $this->assertTrue($login);
        $response = $this->executeRequest($this->createRequest('eo_user_get_family'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_OK, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $this->logout();
    }

    public function testNewFamily()
    {
        $login = $this->login(self::USER_3_TEST_EMAIL, self::USERS_TEST_PASSWORD);
        $this->assertTrue($login);
        $form = array(
            "name" => "test"
        );
        $response = $this->executeRequest($this->createRequest('eo_user_new_family', array(), $form, 'POST'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_CREATED, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $family = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_3_TEST_EMAIL)->getFamily();
        $this->assertTrue($family->getName() === "test");
        $this->getFamilyManager()->delete($family);
        $this->logout();
    }

    public function testPutFamily()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USERS_TEST_PASSWORD);
        $this->assertTrue($login);
        $form = array(
            'name' => 'test'
        );
        $response = $this->executeRequest($this->createRequest('eo_user_put_family', array(), $form, 'PUT'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        /** @var FamilyInterface $family */
        $family = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL)->getFamily();
        $this->assertTrue($family->getName() === "test");
        $family->setName('White');
        $this->getFamilyManager()->update($family);
        $this->logout();
    }

    public function testPatchFamily()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USERS_TEST_PASSWORD);
        $this->assertTrue($login);
        $currentMaster = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL);
        $newMaster = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_2_TEST_EMAIL);
        $form = array(
            'master' => $newMaster->getId()
        );
        $response = $this->executeRequest($this->createRequest('eo_user_patch_family', array(), $form, 'PATCH'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        /** @var FamilyInterface $family */
        $family = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL)->getFamily();
        $this->assertTrue($family->getMaster() === $newMaster->getId());
        $family->setMaster($currentMaster->getId());
        $this->getFamilyManager()->update($family);
        $this->logout();
    }

    public function testLeaveFamily()
    {
        $login = $this->login(self::USER_2_TEST_EMAIL, self::USERS_TEST_PASSWORD);
        $this->assertTrue($login);
        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_2_TEST_EMAIL);
        /** @var FamilyInterface $family */
        $family = $user->getFamily();
        $response = $this->executeRequest($this->createRequest('eo_user_leave_family'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getContent());
        $family->addMember($user);
        $this->getFamilyManager()->update($family);
        $this->logout();
    }

    public function testDeleteFamily()
    {

    }

    public function testInviteFamily()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USERS_TEST_PASSWORD);
        $this->assertTrue($login);

        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_2_TEST_EMAIL);
        /** @var FamilyInterface $family */
        $family = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL)->getFamily();
        $family->removeMember($user);
        $this->getFamilyManager()->update($family);
        $response = $this->executeRequest($this->createRequest('eo_user_invite_family_member', array(
            'email' => $user->getEmail()
        ), array(), 'POST'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        $invite = json_decode($response->getContent());
        $this->logout();

        $login = $this->login(self::USER_2_TEST_EMAIL, self::USERS_TEST_PASSWORD);
        $this->assertTrue($login);
        $response = $this->executeRequest($this->createRequest('eo_user_accept_family_invite', array(
            'id' => $invite->id
        )));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getContent());
        $this->logout();
    }

    public function testExcludeFamily()
    {
        $login = $this->login(self::USER_TEST_EMAIL, self::USERS_TEST_PASSWORD);
        $this->assertTrue($login);
        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_2_TEST_EMAIL);
        $response = $this->executeRequest($this->createRequest('eo_user_exclude_family_member', array(
            'id' => $user->getId()
        ), array(), 'POST'));
        $this->assertTrue($response->getStatusCode() == Codes::HTTP_ACCEPTED, $response->getContent());
        $this->assertNotEmpty($response->getContent());
        /** @var FamilyInterface $family */
        $family = $this->getUserManager()->getFOSUserManager()->findUserByEmail(self::USER_TEST_EMAIL)->getFamily();
        $family->addMember($user);
        $this->getFamilyManager()->update($family);
        $this->logout();
    }
}