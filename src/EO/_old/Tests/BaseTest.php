<?php

namespace EO\ApiBundle\Tests;

use EO\CoreBundle\Doctrine\Manager\DBAL\ComplaintManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\ComplaintMessageManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\FineManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\GroupManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\LineManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\ManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\NetworkManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\StationManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\TerminalManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\TicketManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\TicketTypeManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\TokenManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\UserManagerInterface;
use EO\CoreBundle\Doctrine\Manager\DBAL\ZoneManagerInterface;
use EO\CoreBundle\Factory\TicketFactoryInterface;
use EO\CoreBundle\Model\UserInterface;

abstract class BaseTest extends EOSessionTestCase
{
    /**
     * @param string    $email
     * @return UserInterface
     */
    protected function getUserByEmail($email)
    {
        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail($email);
        return $user;
    }

    protected function printError($var)
    {
        fwrite(STDERR, "\n\033[1;31mTest Error:\n");
        fwrite(STDERR, print_r($var, TRUE));
        fwrite(STDERR, "\033[0m\n");
    }

    protected function printData($var)
    {
        fwrite(STDOUT, "\n\033[0;36mTest Print:\n");
        fwrite(STDOUT, print_r($var, TRUE));
        fwrite(STDOUT, "\033[0m\n");
    }

    /**
     * @return UserManagerInterface
     */
    protected function getUserManager()
    {
        return $this->getService('eo_core.user_manager');
    }

    /**
     * @return GroupManagerInterface
     */
    protected function getGroupManager()
    {
        return $this->getService('eo_core.group_manager');
    }

    /**
     * @return ManagerInterface
     */
    protected function getCompanyManager()
    {
        return $this->getService('eo_core.company_manager');
    }

    /**
     * @return ManagerInterface
     */
    protected function getFamilyManager()
    {
        return $this->getService('eo_core.family_manager');
    }

    /**
     * @return NetworkManagerInterface
     */
    protected function getNetworkManager()
    {
        return $this->getService('eo_core.network_manager');
    }

    /**
     * @return ZoneManagerInterface
     */
    protected function getZoneManager()
    {
        return $this->getService('eo_core.zone_manager');
    }

    /**
     * @return LineManagerInterface
     */
    protected function getLineManager()
    {
        return $this->getService('eo_core.line_manager');
    }

    /**
     * @return StationManagerInterface
     */
    protected function getStationManager()
    {
        return $this->getService('eo_core.station_manager');
    }

    /**
     * @return ManagerInterface
     */
    protected function getLineTypeManager()
    {
        return $this->getService('eo_core.line_type_manager');
    }

    /**
     * @return TicketManagerInterface
     */
    protected function getTicketManager()
    {
        return $this->getService('eo_core.ticket_manager');
    }

    /**
     * @return TicketTypeManagerInterface
     */
    protected function getTicketTypeManager()
    {
        return $this->getService('eo_core.ticket_type_manager');
    }

    /**
     * @return FineManagerInterface
     */
    protected function getFineManager()
    {
        return $this->getService('eo_core.fine_manager');
    }

    /**
     * @return ComplaintManagerInterface
     */
    protected function getComplaintManager()
    {
        return $this->getService('eo_core.complaint_manager');
    }

    /**
     * @return ComplaintMessageManagerInterface
     */
    protected function getComplaintMessageManager()
    {
        return $this->getService('eo_core.complaint_message_manager');
    }

    /**
     * @return TokenManagerInterface
     */
    protected function getTokenManager()
    {
        return $this->getService('eo_core.token_manager');
    }

    /**
     * @return ManagerInterface
     */
    protected function getValidityManager()
    {
        return $this->getService('eo_core.validity_manager');
    }

    /**
     * @return TerminalManagerInterface
     */
    protected function getTerminalManager()
    {
        return $this->getService('eo_core.terminal_manager');
    }

    /**
     * @return TicketFactoryInterface
     */
    protected function getTicketFactory()
    {
        return $this->getService('eo_core.factory.ticket');
    }
}
