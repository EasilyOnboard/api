<?php

namespace EO\ApiBundle\Tests;

use EO\CoreBundle\Model\UserInterface;
use EO\SecureBundle\Util\EOS;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class EosRequest
{
    /**
     * @var MessageDigestPasswordEncoder
     */
    private $encoder;

    public function __construct()
    {
        $this->encoder = new MessageDigestPasswordEncoder('sha256', true, 1);
    }

    /**
     * @param UserInterface $user
     * @param string        $password
     * @param string        $url
     * @param string        $method
     * @param string|null   $content
     *
     * @return Request
     */
    public function generateNewRequest(UserInterface $user, $password, $url, $method = 'GET', $content = null)
    {
        $digest = $this->digestPassword($password);
        // Header
        $header = array(
            'Content-Type' => 'application/json',
            'Authorization' => 'EOS profile="UsernameToken"',
            'X-EOS' => sprintf('UsernameToken Username="%s", PasswordDigest="%s"',
                $user->getUsername(),
                $digest
            ),
        );
        // Request
        $request = Request::create($url, $method, array(), array(), array(), array(), json_encode($content));
        $request->headers->add($header);

        return $request;
    }

    /**
     * @param string $password
     *
     * @return string
     */
    public function digestPassword($password)
    {
        return $this->encoder->encodePassword($password, EOS::DEFAULT_APP_SALT);
    }
}
