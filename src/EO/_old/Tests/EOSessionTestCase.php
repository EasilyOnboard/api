<?php

namespace EO\ApiBundle\Tests;

use EO\CoreBundle\Model\DeviceInterface;
use FOS\RestBundle\Util\Codes;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\TerminableInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EOSessionTestCase extends WebTestCase
{
    /**
     * @var string
     */
    protected $accessToken;

    /**
     * @var string
     */
    protected $refreshToken;

    /**
     * @var \DateTime
     */
    protected $accessExpireAt;

    /**
     * @var \DateTime
     */
    protected $refreshExpireAt;

    /**
     * @var array
     */
    protected $roles;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Container
     */
    protected $container;

    protected function setUp()
    {
        parent::setUp();
        $this->client = $this->createClient();
        $this->container = $this->client->getContainer();
        $this->router = $this->getService('router');
    }

    public function __construct()
    {
        parent::__construct();
        $this->roles = array();
    }

    /**
     * @param string $service
     * @return object
     */
    public function getService($service)
    {
        return $this->container->get($service);
    }

    public function login($email, $password, DeviceInterface $device = null)
    {
        $form = array(
            'email' => $email,
            'password' => $password
        );
        if ($device) {
            $form = array_merge($form, array(
                'device' => array(
                    'app_id' => $device->getAppId(),
                    'name' => $device->getName(),
                    'model' => $device->getModel(),
                    'os' => $device->getOS()
                )
            ));
        }

        $request = $this->createRequest('eo_security_login', array(), $form, 'POST');
        $response = $this->executeRequest($request);

        if ($response->getStatusCode() == Codes::HTTP_ACCEPTED) {
            $content = json_decode($response->getContent());
            $this->accessToken = $content->access_token;
            $this->refreshToken = $content->refresh_token;
            $this->accessExpireAt = $content->access_expires_at;
            $this->refreshExpireAt = $content->refresh_expires_at;
            $this->roles = $content->roles;

            return true;
        }
        return false;
    }

    public function refresh()
    {
        $response = $this->executeRequest($this->createRequest('eo_security_refresh'));
        if ($response->getStatusCode() == Codes::HTTP_ACCEPTED) {
            $content = json_decode($response->getContent());
            $this->accessToken = $content->access_token;
            $this->refreshToken = $content->refresh_token;
            $this->accessExpireAt = $content->access_expires_at;
            $this->refreshExpireAt = $content->refresh_expires_at;
            return true;
        }
        return false;
    }

    public function logout()
    {
        $this->executeRequest($this->createRequest('eo_security_logout'));
        $this->accessToken = null;
        $this->refreshToken = null;
        $this->accessExpireAt = null;
        $this->refreshExpireAt = null;
        $this->roles = array();
    }

    /**
     * @param string $route
     * @param array $params
     * @param array $content
     * @param string $method
     * @param string $format
     * @return Request
     */
    public function createRequest($route, $params = array(), $content = array(), $method = 'GET', $format = 'json')
    {
        $parameters = array_merge($params, array('_format' => $format));
        $url = $this->router->generate($route, $parameters, UrlGeneratorInterface::ABSOLUTE_PATH);

        $request = Request::create($url, $method, array(), array(), array(), array(), json_encode($content));
        $request->headers->add(array(
            'Content-Type' => 'application/'.$format,
            'X-EO-PLATFORM' => 'web',
            'Authorization' => 'EOAPI'
        ));
        if ($this->accessToken !== null) {
            $request->headers->add(array(
                'X-EO-TOKEN' => $this->accessToken,
            ));
        }

        return $request;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function executeRequest(Request $request)
    {
        $response = static::$kernel->handle($request);

        if (static::$kernel instanceof TerminableInterface) {
            static::$kernel->terminate($request, $response);
        }

        return $response;
    }

    /**
     * @param string $data
     * @param string $type
     * @param string $format
     * @return array|\JMS\Serializer\scalar|mixed|object
     */
    public function deserialization($data, $type, $format = "json")
    {
        $serializer = $this->container->get('serializer');
        return $serializer->deserialize($data, $type, $format);
    }
}