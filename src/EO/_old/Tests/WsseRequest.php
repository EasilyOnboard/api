<?php

namespace EO\ApiBundle\Tests;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use EO\CoreBundle\Model\UserInterface;

class WsseRequest
{
    /**
     * @var MessageDigestPasswordEncoder
     */
    private $encoder;

    /**
     * @param string $algorithm
     * @param bool   $base64
     * @param int    $iteration
     */
    public function __construct($algorithm = 'sha512', $base64 = true, $iteration = 5000)
    {
        $this->encoder = new MessageDigestPasswordEncoder($algorithm, $base64, $iteration);
    }

    /**
     * @param UserInterface $user
     * @param string        $password
     * @param string        $url
     * @param string        $method
     * @param string|null   $content
     *
     * @return Request
     */
    public function generateNewRequest(UserInterface $user, $password, $url, $method = 'GET', $content = null)
    {
        // Secret
        $secret = $this->encoder->encodePassword($password, $user->getSalt());
        // Nonce
        $nonce = uniqid();
        // Timestamp
        $created = gmdate(DATE_ISO8601);
        // PasswordDigest
        $digest = $this->encoder->encodePassword(
            sprintf(
                '%s%s%s',
                base64_decode($nonce),
                $created,
                $secret
            ),
            $user->getSalt()
        );
        // Header
        $header = array(
            'Authorization' => 'WSSE profile="UsernameToken"',
            'X-WSSE' => sprintf('UsernameToken Username="%s", PasswordDigest="%s", Nonce="%s", Created="%s"',
                $user->getUsername(),
                $digest,
                $nonce,
                $created
            ),
        );
        // Request
        $request = Request::create($url, $method, array(), array(), array(), array(), $content);
        $request->headers->add($header);

        return $request;
    }
}
