<?php

namespace EO\CoreBundle\Tests\Handler;

use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;

class TicketHandlerTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Container
     */
    private $container;

    public function setUp()
    {
        parent::setUp();
        $this->client = $this->createClient();
        $this->container = $this->client->getContainer();
    }
/*
    public function testPrice()
    {
        $stationManager = $this->container->get('eo_core.station_manager');

        $stationA = $stationManager->findOneBy(array('name' => 'ghamra'));
        $stationB = $stationManager->findOneBy(array('name' => 'dokki'));

        $ticketType = new TicketType();
        $ticketType
            ->setNetwork($stationA->getNetwork())
            ->setStationA($stationA)
            ->setStationB($stationB)
            ->setTerm(new \DateInterval(EOCore::$TICKET_TERMS[EOCore::TICKET_TERM_DAY][EOCore::TERM_VALUE]))
            ->setReducedPrice(true)
        ;

        $ticketHandler = $this->container->get('eo_core.handler.ticket');

        $ticketHandler->setPrice($ticketType);

        $this->printData($ticketType->getPath());
        $this->printData($ticketType->getPrice());
    }*/

    public function testCheckStatus()
    {
        $ticketFactory = $this->container->get('eo_core.factory.ticket');
        $ticketType = $this->container->get('eo_core.ticket_type_manager')->findOneBy(array('name' => 'Standard'));
        $user = $this->container->get('eo_core.user_manager')->getFOSUserManager()->findUserByEmail('heisenberg@gmail.com');

        $ticket = $ticketFactory->create($user, $ticketType);
        $ticket->setStatus('c');
        $ticket->setCompostedAt(new \DateTime('now'));
        $this->container->get('eo_core.ticket_manager')->update($ticket);
        $check = $this->container->get('eo_core.handler.ticket')->checkTicketStatus($ticket);
        $this->assertTrue($check, $ticket->getStatus());
        $this->container->get('eo_core.ticket_manager')->delete($ticket);
    }

    protected function printData($var)
    {
        fwrite(STDOUT, "\n\033[0;36mTest Print:\n");
        fwrite(STDOUT, print_r($var, TRUE));
        fwrite(STDOUT, "\033[0m\n");
    }
}