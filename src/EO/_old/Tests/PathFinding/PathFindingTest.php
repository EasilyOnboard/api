<?php

namespace EO\CoreBundle\Tests\PathFinding;

use EO\CoreBundle\Model\StationInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;

class PathFindingTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var Container
     */
    private $container;

    public function setUp()
    {
        parent::setUp();
        $this->client = $this->createClient();
        $this->container = $this->client->getContainer();
    }

    public function testPath()
    {
        $stationManager = $this->container->get('eo_core.station_manager');

        $stationA = $stationManager->findOneBy(array('name' => 'ghamra'));
        $stationB = $stationManager->findOneBy(array('name' => 'dokki'));

        $pathFinding = $this->container->get('eo_core.path_finding');

        $path = $pathFinding->find($stationA, $stationB);

        $stations = array();
        /** @var StationInterface $station */
        foreach ($path as $station) {
            $stations[] = $station->getName();
        }

        $this->printData($stations);
    }

    protected function printData($var)
    {
        fwrite(STDOUT, "\n\033[0;36mTest Print:\n");
        fwrite(STDOUT, print_r($var, TRUE));
        fwrite(STDOUT, "\033[0m\n");
    }
}