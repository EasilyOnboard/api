<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Model\Manager\CustomerManagerInterface;
use EO\CoreBundle\Document\Customer;
use EO\CoreBundle\Document\User;

class TestLoadCustomerData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $clients = $this->getModelFixtures();

        /** @var CustomerManagerInterface $customerManager */
        $customerManager = $this->container->get('eo_core.customer_manager');

        foreach ($clients['Customers'] as $reference => $data) {
            /** @var User $user */
            $user = $this->getReference('User_'.$data['user']);
            if (!$user) {
                continue;
            }
            /** @var Customer $customer */
            $customer = $customerManager->create();
            $customer->setEmail($user->getEmail());
            $customer->setUser($user);
            $customer->setFirstname($user->getFirstname());
            $customer->setLastname($user->getLastname());
            $customer->setSex($data['sex']);
            $customer->setBirthday(new \DateTime($data['birthday']));
            $customer->setAddress($data['address']);
            $customer->setCity($data['city']);
            $customer->setState($data['state']);
            $customer->setPostcode($data['postcode']);
            $customer->setCountry($data['country']);
            $customer->setPhone($data['phone']);

            $customerManager->update($customer);

            $this->addReference('Customer_'.$reference, $customer);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'customers';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}
