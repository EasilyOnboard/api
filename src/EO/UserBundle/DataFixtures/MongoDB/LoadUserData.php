<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Doctrine\UserManager;
use EO\SecureBundle\Util\EOS;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Document\User;

class LoadUserData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var UserManager $userManager */
        $userManager = $this->container->get('fos_user.user_manager');

        $users = $this->getModelFixtures();
        foreach ($users['Users'] as $reference => $data) {
            $encoder = new MessageDigestPasswordEncoder('sha256', true, 1);
            $password = $encoder->encodePassword($data['password'], EOS::DEFAULT_APP_SALT);

            /** @var User $user */
            $user = $userManager->createUser();
            $user->setEmail($data['email']);
            $user->setPlainPassword($password);
            $user->setFirstname($data['firstname']);
            $user->setLastname($data['lastname']);
            $user->setEnabled(true);
            if (!empty($data['roles'])) {
                foreach ($data['roles'] as $role) {
                    $user->addRole($role);
                }
            }

            $userManager->updateUser($user);

            $this->addReference('User_'.$reference, $user);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'users';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'prod';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}
