<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Model\UserManagerInterface;
use EO\CoreBundle\Model\Manager\EmployeeManagerInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Document\Employee;
use EO\CoreBundle\Document\Company;
use EO\CoreBundle\Document\User;
use EO\CoreBundle\Document\Group;

class TestLoadEmployeeData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $employees = $this->getModelFixtures();

        /** @var EmployeeManagerInterface $employeeManager */
        $employeeManager = $this->container->get('eo_core.employee_manager');
        /** @var UserManagerInterface $userManager */
        $userManager = $this->container->get('fos_user.user_manager');

        foreach ($employees['Employees'] as $reference => $data) {
            /** @var User $user */
            $user = $this->getReference('User_'.$data['user']);
            if (!$user) {
                continue;
            }
            /** @var Company $company */
            $company = $this->getReference('Company_'.$data['company']);
            if (!$company) {
                continue;
            }

            /** @var Employee $employee */
            $employee = $employeeManager->create();
            $employee->setFirstname($user->getFirstname());
            $employee->setLastname($user->getLastname());
            $employee->setEmail($user->getEmail());
            $employee->setUser($user);
            $employee->setCompany($company);
            if (!empty($data['groups'])) {
                foreach ($data['groups'] as $g) {
                    /** @var Group $group */
                    $group = $this->getReference('Group_'.$g);
                    $user->addGroup($group);
                }
            }

            $userManager->updateUser($user);
            $employeeManager->update($employee);

            $this->addReference('Employee_'.$reference, $employee);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'employees';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 6;
    }
}
