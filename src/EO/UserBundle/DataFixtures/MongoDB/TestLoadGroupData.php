<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Doctrine\GroupManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Document\Company;
use EO\CoreBundle\Document\Group;

class TestLoadGroupData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var GroupManager $groupManager */
        $groupManager = $this->container->get('fos_user.group_manager');

        $groups = $this->getModelFixtures();
        foreach ($groups['Groups'] as $reference => $data) {
            /** @var Group $group */
            $group = $groupManager->createGroup($data['name']);
            /** @var Company $company */
            $company = $this->getReference('Company_'.$data['company']);
            $group->setCompany($company);
            if (!empty($data['roles'])) {
                foreach ($data['roles'] as $role) {
                    $group->addRole($role);
                }
            }

            $groupManager->updateGroup($group);

            $this->setReference('Group_'.$reference, $group);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'groups';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 5;
    }
}
