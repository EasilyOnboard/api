<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\CoreBundle\Doctrine\ManagerInterface;
use EO\CoreBundle\Document\Family;
use EO\CoreBundle\Model\CustomerInterface;
use EO\CoreBundle\Model\Manager\CustomerManagerInterface;

class TestLoadFamilyData extends AbstractLoadData implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed DocumentManager.
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $families = $this->getModelFixtures();

        /** @var ManagerInterface $familyManager */
        $familyManager = $this->container->get('eo_core.family_manager');
        /** @var CustomerManagerInterface $customerManager */
        $customerManager = $this->container->get('eo_core.customer_manager');

        foreach ($families['Families'] as $reference => $data) {
            /** @var Family $family */
            $family = $familyManager->create();
            $family->setName($data['name']);

            $familyManager->update($family);

            foreach ($data['members'] as $ref => $values) {
                /** @var CustomerInterface $customer */
                $customer = $this->getReference('Customer_'.$ref);

                $family->addMember($customer);
                $customer->setFamily($family);

                if (!empty($values['roles'])) {
                    foreach ($values['roles'] as $role) {
                        $customer->addRole($role);
                        $family->addRoleToMember($customer, $role);
                    }
                }
                $customerManager->update($customer);
            }
            $familyManager->update($family);
        }
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'families';
    }

    /**
     * Return the env folder for fixtures.
     */
    public function getEnvFolder()
    {
        return 'dev';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}
