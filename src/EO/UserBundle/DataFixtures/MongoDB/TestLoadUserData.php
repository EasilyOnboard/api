<?php

namespace EO\CoreBundle\DataFixtures\MongoDB;

class TestLoadUserData extends LoadUserData
{
    public function getEnvFolder()
    {
        return 'dev';
    }
}
