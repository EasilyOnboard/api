<?php

namespace EO\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\UserBundle\Model\FamilyInterface;
use EO\UserBundle\Model\UserInterface;

class TestLoadFamilyData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var FamilyInterface $family */
        $family = $this->getManager()->create();
        $family->setName($data['name']);
        /** @var UserInterface $user */
        $user = $this->getReference('User_'.$data['master']);
        $family->setMaster($user->getId());
        $this->getManager()->update($family);
        foreach ($data['members'] as $values) {
            /** @var UserInterface $user */
            $user = $this->getReference('User_'.$values);
            $family->addMember($user);
        }
        $this->getManager()->update($family);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    protected function getManager()
    {
        return $this->container->get('eo_user.manager.family');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'families';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 4;
    }
}
