<?php

namespace EO\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;
use EO\MiscBundle\Model\Address;
use EO\UserBundle\Model\GroupInterface;
use EO\UserBundle\Model\UserInterface;

class LoadUserData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var UserInterface $user */
        $user = $this->getManager()->getFOSUserManager()->createUser();
        $user->setEnabled(true);
        $user->setEmail($data['email']);
        $user->setPlainPassword($data['password']);
        $user->setLanguage('en');
        if (!empty($data['firstname'])) {
            $user->setFirstName($data['firstname']);
        }
        if (!empty($data['lastname'])) {
            $user->setLastName($data['lastname']);
        }
        // Customer Info
        if (!empty($data['sex'])) {
            $user->setSex($data['sex']);
        }
        if (!empty($data['birthday'])) {
            $user->setBirthday(new \DateTime($data['birthday']));
        }
        if (!empty($data['full_address'])) {
            $address = new Address();
            $user->setFullAddress($address);
            $user->setAddress($data['full_address']['address']);
            $user->setCity($data['full_address']['city']);
            $user->setState($data['full_address']['state']);
            $user->setPostcode($data['full_address']['postcode']);
            $user->setCountry($data['full_address']['country']);
            $user->setPhone($data['full_address']['phone']);
            $this->getManager()->update($user->getFullAddress());
        }
        // Employee Info
        /** @var CompanyInterface $company */
        if (!empty($data['company'])) {
            $company = $this->getReference('Company_'.$data['company']);
            if ($company) {
                $user->setCompany($company);
            }
        }
        if (!empty($data['groups'])) {
            foreach ($data['groups'] as $g) {
                /** @var GroupInterface $group */
                $group = $this->getReference('Group_'.$g);
                $user->addGroup($group);
            }
        }
        if (!empty($data['roles'])) {
            foreach ($data['roles'] as $role) {
                $user->addRole($role);
            }
        }
        $this->getManager()->getFOSUserManager()->updateUser($user);
        $this->addReference('User_'.$reference, $user);
    }

    protected function getEnvironments()
    {
        return array('prod', 'dev', 'test');
    }

    /**
     * @return \EO\UserBundle\Manager\DBAL\UserManagerInterface
     */
    protected function getManager()
    {
        return $this->container->get('eo_user.manager.user');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'users';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}
