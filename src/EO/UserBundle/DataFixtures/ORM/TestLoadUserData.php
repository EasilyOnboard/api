<?php

namespace EO\UserBundle\DataFixtures\ORM;

class TestLoadUserData extends LoadUserData
{
    protected function getEnvironments()
    {
        return array('dev', 'test');
    }
}
