<?php

namespace EO\UserBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use EO\CompanyBundle\Model\CompanyInterface;
use EO\UserBundle\Model\GroupInterface;
use EO\CoreBundle\DataFixtures\AbstractLoadData;

class TestLoadGroupData extends AbstractLoadData implements OrderedFixtureInterface
{
    protected function loadData($reference, array $data)
    {
        /** @var GroupInterface $group */
        $group = $this->getManager()->createGroup($data['name']);
        /** @var CompanyInterface $company */
        $company = $this->getReference('Company_'.$data['company']);
        $group->setCompany($company);
        if (!empty($data['roles'])) {
            foreach ($data['roles'] as $role) {
                $group->addRole($role);
            }
        }
        $this->getManager()->updateGroup($group);
        $this->setReference('Group_'.$reference, $group);
    }

    protected function getEnvironments()
    {
        return array('dev', 'test');
    }

    /**
     * @return \FOS\UserBundle\Doctrine\GroupManager
     */
    protected function getManager()
    {
        return $this->container->get('fos_user.group_manager');
    }

    /**
     * Return the file for the current model.
     */
    public function getModelFile()
    {
        return 'groups';
    }

    /**
     * Get the order of this fixture.
     *
     * @return integer
     */
    public function getOrder()
    {
        return 2;
    }
}
