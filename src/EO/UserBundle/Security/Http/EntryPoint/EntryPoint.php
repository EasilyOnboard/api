<?php

namespace EO\UserBundle\Security\Http\EntryPoint;

use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;

class EntryPoint implements AuthenticationEntryPointInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $realm;

    /**
     * @var string
     */
    private $profile;

    public function __construct(LoggerInterface $logger, $realm, $profile)
    {
        $this->logger = $logger;
        $this->realm = $realm;
        $this->profile = $profile;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        if ($this->logger !== null) {
            if ($authException instanceof AuthenticationException) {
                $this->logger->warn($authException->getMessage());
            }
        }
        $response = new Response();
        $response->headers->set(
            'WWW-Authenticate',
            sprintf(
                'EO-API realm="%s", profile="%s"',
                $this->realm,
                $this->profile
            )
        );
        $response->setStatusCode(Codes::HTTP_UNAUTHORIZED);
        return $response;
    }

    public function getRealm()
    {
        return $this->realm;
    }

    public function getProfile()
    {
        return $this->profile;
    }
}