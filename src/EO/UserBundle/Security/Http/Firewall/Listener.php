<?php

namespace EO\UserBundle\Security\Http\Firewall;

use EO\UserBundle\Enum\HttpHeader;
use EO\UserBundle\Security\Token\SessionToken;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;

class Listener implements ListenerInterface
{
    /**
     * @var SecurityContextInterface
     */
    private $securityContext;

    /**
     * @var AuthenticationManagerInterface
     */
    private $authenticationProvider;

    /**
     * @var string
     */
    private $providerKey;

    /**
     * @var AuthenticationEntryPointInterface
     */
    private $authenticationEntryPoint;

    public function __construct(SecurityContextInterface $securityContext,
                                AuthenticationProviderInterface $authenticationProvider,
                                $providerKey,
                                AuthenticationEntryPointInterface $authenticationEntryPoint)
    {
        $this->securityContext = $securityContext;
        $this->authenticationProvider = $authenticationProvider;
        $this->providerKey = $providerKey;
        $this->authenticationEntryPoint = $authenticationEntryPoint;
    }

    public function handle(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (HttpHeader::AUTHORIZATION_VALUE === $request->headers->get(strtolower(HttpHeader::AUTHORIZATION))) {
            if (null !== $platform = $request->headers->get(strtolower(HttpHeader::PLATFORM))) {
                if ($platform == HttpHeader::PLATFORM_WEB || $platform == HttpHeader::PLATFORM_MOBILE ||
                    $platform == HttpHeader::PLATFORM_TERMINAL) {
                    if (null !== $accessToken = $request->headers->get(strtolower(HttpHeader::TOKEN))) {
                        $sessionToken = new SessionToken($accessToken, $platform, $this->providerKey);
                        if ($this->authenticationProvider->supports($sessionToken)) {
                            try {
                                $result = $this->authenticationProvider->authenticate($sessionToken);
                                $this->securityContext->setToken($result);
                            } catch (AuthenticationException $e) {
                                $event->setResponse($this->authenticationEntryPoint->start($request, $e));
                            }
                        }
                    }
                } else {
                    $event->setResponse($this->authenticationEntryPoint->start($request));
                }
            }
        } else {
            $event->setResponse($this->authenticationEntryPoint->start($request));
        }
    }
}