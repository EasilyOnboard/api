<?php

namespace EO\UserBundle\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

class Factory implements SecurityFactoryInterface
{
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $container->setParameter('eo_user.authentication.provider_key', $id);
        $container->setParameter('eo_user.authentication.access_lifetime', $config['access_lifetime']);
        $container->setParameter('eo_user.authentication.refresh_lifetime', $config['refresh_lifetime']);
        $container->setParameter('eo_user.authentication.realm', $config['realm']);
        $container->setParameter('eo_user.authentication.profile', $config['profile']);

        $providerTokenId = 'eo_user.authentication.provider.token.'.$id;
        $entryPointId = 'eo_user.authentication.entry_point.'.$id;
        $listenerId = 'eo_user.authentication.listener.'.$id;

        $container->setDefinition($providerTokenId, new DefinitionDecorator('eo_user.authentication.provider.token'));
        $container->setDefinition($entryPointId, new DefinitionDecorator('eo_user.authentication.entry_point'));
        $container
            ->setDefinition($listenerId, new DefinitionDecorator('eo_user.authentication.listener'))
            ->replaceArgument(3, new Reference($entryPointId));

        return array($providerTokenId, $listenerId, $entryPointId);
    }

    /**
     * Defines the position at which the provider is called.
     * Possible values: pre_auth, form, http, and remember_me.
     *
     * @return string
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    public function getKey()
    {
        return 'eo_api';
    }

    public function addConfiguration(NodeDefinition $builder)
    {
        $builder
            ->children()
                ->scalarNode('provider')->end()
                ->scalarNode('realm')->defaultValue(null)->end()
                ->scalarNode('profile')->defaultValue('EOAPI')->end()
                ->scalarNode('access_lifetime')->defaultValue(3600)->end()
                ->scalarNode('refresh_lifetime')->defaultValue(7200)->end()
            ->end();
    }
}