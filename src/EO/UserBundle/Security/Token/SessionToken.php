<?php

namespace EO\UserBundle\Security\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

class SessionToken extends AbstractToken
{
    /**
     * @var string
     */
    private $credentials;

    /**
     * @var string
     */
    private $platform;

    /**
     * @var string
     */
    private $providerKey;

    /**
     * @param string $credentials
     * @param string $platform
     * @param string $providerKey
     * @param array $roles
     */
    public function __construct($credentials, $platform, $providerKey, array $roles = array())
    {
        parent::__construct($roles);
        if (empty($providerKey)) {
            throw new \InvalidArgumentException('$providerKey must not be empty.');
        }
        $this->credentials = $credentials;
        $this->platform = $platform;
        $this->providerKey = $providerKey;
    }

    public function getProviderKey()
    {
        return $this->providerKey;
    }

    public function getCredentials()
    {
        return $this->credentials;
    }

    public function getPlatform()
    {
        return $this->platform;
    }
}