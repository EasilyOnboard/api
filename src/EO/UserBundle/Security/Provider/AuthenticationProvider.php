<?php

namespace EO\UserBundle\Security\Provider;

use EO\UserBundle\Enum\HttpHeader;
use EO\UserBundle\Manager\DBAL\TokenManagerInterface;
use EO\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;

class AuthenticationProvider implements AuthenticationProviderInterface
{
    /**
     * @var PasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var string
     */
    private $providerKey;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var int
     */
    private $accessLifetime;

    /**
     * @var int
     */
    private $refreshLifetime;

    /**
     * @param ContainerInterface    $container
     * @param string                $providerKey
     * @param int                   $accessLifetime
     * @param int                   $refreshLifetime
     */
    public function __construct(ContainerInterface $container, $providerKey, $accessLifetime, $refreshLifetime)
    {
        $this->providerKey = $providerKey;
        $this->container = $container;
        $this->accessLifetime = $accessLifetime;
        $this->refreshLifetime = $refreshLifetime;
    }

    public function authenticate(TokenInterface $token)
    {
        $platform = $this->getRequest()->headers->get(HttpHeader::PLATFORM);
        if (null === $platform) {
            throw new AuthenticationException();
        }
        /** @var UserInterface $user */
        $user = $token->getUser();
        $this->encoder = $this->getEncoderFactory()->getEncoder($user);
        if (null === $user || !$this->validateDigest($user, $token->getCredentials())) {
            throw new AuthenticationException();
        }
        /** @var \EO\UserBundle\Model\TokenInterface $accessToken */
        $accessToken = $this->getTokenManager()->findOneByUserAndPlatform($user, $platform);
        if (null !== $accessToken) {
            $now = new \DateTime('now');
            if ($accessToken->getAccessExpiresAt() < $now) {
                if ($accessToken->getRefreshExpiresAt() < $now) {
                    $this->getTokenManager()->delete($accessToken);
                    $accessToken = null;
                } else {
                    $accessToken->resetRefreshToken();
                    $this->setTokenLifetime($accessToken);
                    $this->getTokenManager()->update($accessToken);
                }
            }
        }
        if (null === $accessToken) {
            $accessToken = $this->getTokenManager()->create();
            $accessToken->setUser($user);
            $accessToken->setPlatform($platform);
            $accessToken->setRoles($user->getRoles());
            $this->setTokenLifetime($accessToken);
            $this->getTokenManager()->update($accessToken);
        }
        return $accessToken;
    }

    private function setTokenLifetime(\EO\UserBundle\Model\TokenInterface $token)
    {
        $accessExpiresAt = new \DateTime('now');
        $accessExpiresAt->add(new \DateInterval('PT'.$this->accessLifetime.'S'));
        $token->setAccessExpiresAt($accessExpiresAt);
        $refreshExpiresAt = new \DateTime('now');
        $refreshExpiresAt->add(new \DateInterval('PT'.$this->refreshLifetime.'S'));
        $token->setRefreshExpiresAt($refreshExpiresAt);
    }

    /**
     * @param UserInterface $user
     * @param string $digest
     *
     * @return bool
     */
    private function validateDigest(UserInterface $user, $digest)
    {
        if ($user->isAccountNonLocked() === false) {
            return false;
        }
        $secret = $this->encoder->encodePassword($digest, $user->getSalt());
        return $user->getPassword() === $secret;
    }

    /**
     * @return TokenManagerInterface
     */
    private function getTokenManager()
    {
        return $this->container->get('eo_user.manager.token');
    }

    /**
     * @return Request
     */
    private function getRequest()
    {
        return $this->container->get('request');
    }

    /**
     * @return EncoderFactory
     */
    private function getEncoderFactory()
    {
        return $this->container->get('security.encoder_factory');
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof UsernamePasswordToken;
    }
}