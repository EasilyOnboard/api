<?php

namespace EO\UserBundle\Security\Provider;

use EO\UserBundle\Manager\DBAL\TokenManagerInterface;
use EO\UserBundle\Security\Token\SessionToken;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;

class TokenAuthenticationProvider implements AuthenticationProviderInterface
{
    /**
     * @var TokenManagerInterface
     */
    private $tokenManager;

    /**
     * @var string
     */
    private $providerKey;

    /**
     * @param TokenManagerInterface $tokenManager
     * @param string $providerKey
     */
    public function __construct(TokenManagerInterface $tokenManager, $providerKey)
    {
        $this->tokenManager = $tokenManager;
        $this->providerKey = $providerKey;
    }

    public function authenticate(TokenInterface $token)
    {
        if (null !== $accessToken = $this->validateToken($token)) {
            $user = $accessToken->getUser();
            if ($user->isAccountNonLocked() === false) {
                throw new AuthenticationException();
            }
            return new UsernamePasswordToken($user, $accessToken->getAccessToken(), $this->providerKey, $accessToken->getRoles());
        }
        throw new AuthenticationException();
    }

    /**
     * @param SessionToken $accessToken
     *
     * @return \EO\UserBundle\Model\TokenInterface|null
     */
    private function validateToken(SessionToken $accessToken)
    {
        /** @var \EO\UserBundle\Model\TokenInterface $token */
        $token = $this->tokenManager->find($accessToken->getCredentials());
        if (null === $token || $token->getPlatform() !== $accessToken->getPlatform() || !$token->getUser()->isEnabled()) {
            return null;
        }
        $now = new \DateTime("now");
        if ($token->getAccessExpiresAt() >= $now) {
            return $token;
        }
        if ($token->getRefreshExpiresAt() < $now) {
            $this->tokenManager->delete($token);
        }
        throw new CredentialsExpiredException();
    }

    public function supports(TokenInterface $token)
    {
        return $token instanceof SessionToken;
    }
}