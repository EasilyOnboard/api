<?php

namespace EO\UserBundle\Form\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class UserPassword extends Constraint
{
    public $message = 'This value should be the user\'s current password.';

    public function validatedBy()
    {
        return 'eo_user_password';
    }
}