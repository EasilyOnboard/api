<?php

namespace EO\UserBundle\Form\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class BirthdayValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($value === null) {
            return;
        }
        if ($value instanceof \DateTime) {
            $now = new \DateTime('now');
            $now->sub(new \DateInterval('P12Y'));
            if ($value < $now) {
                return;
            }
        }
        $this->context->addViolation($constraint->message);
    }
}