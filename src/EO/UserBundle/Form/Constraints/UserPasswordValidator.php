<?php

namespace EO\UserBundle\Form\Constraints;

use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserPasswordValidator extends ConstraintValidator
{
    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @var EncoderFactory
     */
    private $encoderFactory;

    public function __construct(TokenStorage $tokenStorage, EncoderFactory $encoderFactory)
    {
        $user = null;
        if (null !== $token = $tokenStorage->getToken()) {
            $user = $token->getUser();
            if (!is_object($user)) {
                $user = null;
            }
        }
        $this->user = $user;
        $this->encoderFactory = $encoderFactory;
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     *
     * @api
     */
    public function validate($value, Constraint $constraint)
    {
        if ($this->user !== null) {
            $encoder = $this->encoderFactory->getEncoder($this->user);
            if (!$encoder->isPasswordValid($this->user->getPassword(), $value, $this->user->getSalt())) {
                $this->context->addViolation($constraint->message);
            }
        }
    }
}