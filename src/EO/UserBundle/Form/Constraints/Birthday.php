<?php

namespace EO\UserBundle\Form\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Birthday extends Constraint
{
    public $message = "The datetime is not a valid birthday.";

    public function validatedBy()
    {
        return 'eo_birthday';
    }
}