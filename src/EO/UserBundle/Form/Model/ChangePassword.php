<?php

namespace EO\UserBundle\Form\Model;

class ChangePassword
{
    /**
     * @var string
     */
    protected $currentPassword;

    /**
     * @var string
     */
    protected $newPassword;

    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    public function getNewPassword()
    {
        return $this->newPassword;
    }

    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;

        return $this;
    }

    public function setNewPassword($newPassword)
    {
        $this->newPassword = $newPassword;

        return $this;
    }
}