<?php

namespace EO\UserBundle\Form\Model;

use EO\UserBundle\Model\Device;

class Login
{
    protected $email;
    protected $password;
    protected $device;

    public function __construct()
    {
        $this->device = new Device();
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getDevice()
    {
        return $this->device;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function setDevice($device)
    {
        $this->device = $device;

        return $this;
    }
}