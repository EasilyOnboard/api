<?php

namespace EO\UserBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ChangePasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('current_password', 'password')
            ->add('new_password', 'password')
        ;
    }

    public function getName()
    {
        return 'eo_change_password';
    }
}