<?php

namespace EO\UserBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RefreshType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('access_token', 'text')
            ->add('refresh_token', 'text')
        ;
    }

    public function getName()
    {
        return 'eo_refresh';
    }
}