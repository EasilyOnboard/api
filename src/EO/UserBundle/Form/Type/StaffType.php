<?php

namespace EO\UserBundle\Form\Type;

use EO\UserBundle\Model\UserInterface;
use Symfony\Component\Form\FormBuilderInterface;

class StaffType extends UserType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $groups = array();
        /** @var UserInterface $user */
        $user = $builder->getData();
        if ($user && $user->getCompany()) {
            $groups = $this->container->get('eo_user.manager.group')->findAllByCompany($user->getCompany()->getId());
        }
        $builder
            ->add('language', 'language')
            ->add('groups', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_user.model.group.class'),
                'choices' => $groups,
                'multiple' => true,
                'expanded' => true
            ));
    }

    public function getName()
    {
        return 'eo_staff';
    }
}