<?php

namespace EO\UserBundle\Form\Type;

use EO\UserBundle\Enum\Sex;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerType extends UserType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('sex', 'choice', array(
                'choices' => array(
                    Sex::MALE => "male",
                    Sex::FEMALE => "female",
                    Sex::OTHER => "other"
                )
            ))
            ->add('birthday', 'date', array(
                'widget' => 'single_text',
                'input' => 'datetime',
                'format' => 'yyyy-MM-dd',
            ))
            ->add('full_address', 'eo_address')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        parent::setDefaultOptions($resolver);
        $resolver->setDefaults(array('validation_groups' => array('profile', 'customer')));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_customer';
    }
}
