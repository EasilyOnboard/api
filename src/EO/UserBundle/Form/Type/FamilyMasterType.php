<?php

namespace EO\UserBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\UserBundle\Model\FamilyInterface;
use EO\UserBundle\Model\UserInterface;
use Symfony\Component\Form\FormBuilderInterface;

class FamilyMasterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $ids = array();
        /** @var FamilyInterface $family */
        $family = $builder->getData();
        if ($family) {
            /** @var UserInterface $member */
            foreach ($family->getMembers() as $member) {
                $ids[$member->getId()] = $member->getId();
            }
        }
        $builder->add('master', 'choice', array('choices' => $ids));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_family_master';
    }
}