<?php

namespace EO\UserBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use EO\UserBundle\Model\GroupInterface;
use Symfony\Component\Form\FormBuilderInterface;

class GroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $users = array();
        /** @var GroupInterface $group */
        $group = $builder->getData();
        if ($group) {
            $users = $this->container->get('eo_user.manager.user')->findAllStaffByCompany($group->getCompany());
        }
        $builder
            ->add('name', 'text')
            ->add('roles', 'choice', array(
                'choices' => $this->getRolesList(),
                'multiple' => true,
                'expanded' => true
            ))
            ->add('members', $this->getModelType(), array(
                'class' => $this->container->getParameter('eo_user.model.user.class'),
                'choices' => $users,
                'multiple' => true,
                'expanded' => true
            ))
        ;
    }

    private function getRolesList()
    {
        $roles = array();
        $hierarchy = $this->container->getParameter('security.role_hierarchy.roles');
        array_walk_recursive(
            $hierarchy,
            function($a) use (&$roles) {
                if ($a !== 'ROLE_SUPER_ADMIN') {
                    $roles[] = $a;
                }
            }
        );
        $roles = array_values(array_unique($roles, SORT_STRING));
        return array_combine($roles, $roles);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'eo_group';
    }
}
