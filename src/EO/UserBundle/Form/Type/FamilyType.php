<?php

namespace EO\UserBundle\Form\Type;

use EO\CoreBundle\Form\Type\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class FamilyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', 'text');
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'eo_family';
    }
}