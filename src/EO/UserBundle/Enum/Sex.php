<?php

namespace EO\UserBundle\Enum;

final class Sex
{
    const MALE = 'm';
    const FEMALE = 'f';
    const OTHER = 'o';
}