<?php

namespace EO\UserBundle\Enum;

final class HttpHeader
{
    const TOKEN = "X-EO-TOKEN";

    const PLATFORM = "X-EO-PLATFORM";
    const PLATFORM_WEB = "web";
    const PLATFORM_MOBILE = "mobile";
    const PLATFORM_TERMINAL = "terminal";

    const AUTHORIZATION = "Authorization";
    const AUTHORIZATION_VALUE = "EOAPI";
}