<?php

namespace EO\UserBundle\Enum;

final class DeviceOS
{
    const IOS = "iOS";
    const ANDROID = "Android";
    const WP = "WP";
}