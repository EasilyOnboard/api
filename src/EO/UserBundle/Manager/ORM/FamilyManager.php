<?php

namespace EO\UserBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\UserBundle\Manager\DBAL\FamilyManagerInterface;
use EO\UserBundle\Model\FamilyInterface;
use EO\UserBundle\Model\UserInterface;

class FamilyManager extends Manager implements FamilyManagerInterface
{
    public function delete($family)
    {
        if (!$family instanceof FamilyInterface) {
            return;
        }
        /** @var UserInterface $member */
        foreach ($family->getMembers() as $member) {
            $member->setFamily(null);
            $this->em->persist($member);
        }
        $invites = $this->container->get('eo_user.manager.invite')->findAllByFamily($family);
        foreach ($invites as $invite) {
            $this->em->remove($invite);
        }
        parent::delete($family);
    }
}