<?php

namespace EO\UserBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\UserBundle\Manager\DBAL\InviteManagerInterface;
use EO\UserBundle\Model\FamilyInterface;
use EO\UserBundle\Model\UserInterface;

class InviteManager extends Manager implements InviteManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByWho(UserInterface $user)
    {
        return $this->repository->findByWho($user);
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByBy(UserInterface $user)
    {
        return $this->repository->findByBy($user);
    }

    /**
     * {@inheritdoc}
     */
    public function findAllByFamily(FamilyInterface $family)
    {
        return $this->repository->findByFamily($family);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByByAndWho(UserInterface $by, UserInterface $who)
    {
        return $this->repository->findOneBy(array(
            'by' => $by,
            'who' => $who
        ));
    }
}