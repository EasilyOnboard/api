<?php

namespace EO\UserBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\UserBundle\Manager\DBAL\TokenManagerInterface;
use EO\UserBundle\Model\UserInterface;

class TokenManager extends Manager implements TokenManagerInterface
{
    public function findOneByUserAndPlatform(UserInterface $user, $platform)
    {
        return $this->repository->findOneBy(array(
            'user' => $user,
            'platform' => $platform
        ));
    }

    public function findAllByUser(UserInterface $user)
    {
        return $this->repository->findByUser($user);
    }

    public function findCurrentOneByUser(UserInterface $user, $lifetime)
    {
        $now = new \DateTime('now');
        $later = \DateTime::createFromFormat("YmdHis", ($now->getTimestamp() - $lifetime));

        return $this->repository->createQueryBuilder('t')
            ->join('t.user', 'u')
            ->where('u.id = :id')
            ->andWhere('t.date > :later')
            ->setParameter('id', $user->getId())
            ->setParameter('later', $later)
            ->getQuery()
            ->getOneOrNullResult();
    }
}