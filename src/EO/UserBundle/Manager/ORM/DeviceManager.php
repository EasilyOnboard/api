<?php

namespace EO\UserBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\UserBundle\Manager\DBAL\DeviceManagerInterface;
use EO\UserBundle\Model\UserInterface;

class DeviceManager extends Manager implements DeviceManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function findAllByCustomer(UserInterface $customer)
    {
        return $this->repository->findByCustomer($customer);
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByToken($token)
    {
        return $this->repository->findOneBy(array('token' => $token));
    }

    /**
     * {@inheritdoc}
     */
    public function findOneByAppId($device)
    {
        return $this->repository->findOneBy(array('appId' => $device));
    }
}