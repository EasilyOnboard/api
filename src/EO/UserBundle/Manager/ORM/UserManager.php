<?php

namespace EO\UserBundle\Manager\ORM;

use Doctrine\ORM\EntityManager;
use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\ORM\Manager;
use EO\UserBundle\Manager\DBAL\UserManagerInterface;
use EO\UserBundle\Model\FamilyInterface;
use EO\UserBundle\Model\GroupInterface;
use EO\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserManager extends Manager implements UserManagerInterface
{
    /**
     * @var \FOS\UserBundle\Model\UserManagerInterface
     */
    private $fosUserManager;

    public function __construct(ContainerInterface $container,  EntityManager $em, $class)
    {
        parent::__construct($container, $em, $class);
        $this->fosUserManager = $container->get('fos_user.user_manager');
    }

    /**
     * @return \FOS\UserBundle\Model\UserManagerInterface
     */
    public function getFOSUserManager()
    {
        return $this->fosUserManager;
    }

    public function findCustomer($id)
    {
        $qb = $this->repository->createQueryBuilder('u');
        $query = $qb
            ->where('u.id = :id')
            ->andWhere('u.roles LIKE :roles')
            ->orderBy('u.firstName', 'ASC')
            ->setParameters(array(
                'id' => $id,
                'roles' => '%"ROLE_CUSTOMER"%'))
            ->getQuery();
        return $query->getOneOrNullResult();
    }

    public function findStaffByCompany($id, $cid)
    {
        $qb = $this->repository->createQueryBuilder('u');
        $query = $qb
            ->where('u.id = :id')
            ->andWhere('u.roles LIKE :roles')
            ->andWhere('u.company = :cid')
            ->setParameters(array(
                "id" => $id,
                "roles" => '%"ROLE_STAFF"%',
                "cid" => $cid
            ))
            ->getQuery();
        return $query->getOneOrNullResult();
    }

    public function findAllCustomers()
    {
        $qb = $this->repository->createQueryBuilder('u');
        $query = $qb
            ->where('u.roles LIKE :roles')
            ->orderBy('u.firstName', 'ASC')
            ->setParameter('roles', '%"ROLE_CUSTOMER"%')->getQuery();

        return $query->getResult();
    }

    public function findAllOtherFamilyMembersByCustomer(UserInterface $customer)
    {
        return $this->repository->createQueryBuilder('u')
            ->where('u.family = :fid')
            ->andWhere('u.id != :id')
            ->setParameters(array(
                'fid' => $customer->getFamily()->getId(),
                'id' => $customer->getId()))
            ->getQuery()
            ->getResult();
    }

    public function findAllStaffByCompany(CompanyInterface $company)
    {
        $qb = $this->repository->createQueryBuilder('u');
        $query = $qb
            ->where('u.roles LIKE :roles')
            ->andWhere('u.company = :cid')
            ->setParameters(array(
                'roles' => '%"ROLE_STAFF"%',
                'cid' => $company->getId()))
            ->getQuery();

        return $query->getResult();
    }

    public function findAllStaffByGroup(GroupInterface $group)
    {
        return $this->repository->createQueryBuilder('u')
            ->leftJoin('u.groups', 'g')
            ->where('g.id = :id')
            ->setParameter('id', $group->getId())
            ->getQuery()
            ->getResult();
    }

    public function findOneByIdInFamily($id, FamilyInterface $family)
    {
        return $this->repository->findOneBy(array('id' => $id, 'family' => $family));
    }

    public function findAllCustomerLike($like)
    {
        $qb = $this->repository->createQueryBuilder('u');
        return $qb
            ->where('u.roles LIKE :roles')
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like('u.email', ':like'),
                $qb->expr()->like('u.firstName', ':like'),
                $qb->expr()->like('u.lastName', ':like')
            ))
            ->setParameters(array(
                'roles' => '%"ROLE_CUSTOMER"%',
                'like' => '%'.$like.'%'
            ))
            ->getQuery()
            ->getResult();
    }
}