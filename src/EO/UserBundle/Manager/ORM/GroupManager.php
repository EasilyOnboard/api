<?php

namespace EO\UserBundle\Manager\ORM;

use EO\CoreBundle\Manager\ORM\Manager;
use EO\UserBundle\Manager\DBAL\GroupManagerInterface;
use EO\UserBundle\Model\GroupInterface;
use EO\UserBundle\Model\UserInterface;

class GroupManager extends Manager implements GroupManagerInterface
{
    public function findAllByCompany($cid)
    {
        return $this->repository->createQueryBuilder('g')
            ->where('g.company = :cid')
            ->setParameter('cid', $cid)
            ->getQuery()
            ->getResult();
    }

    public function findOneByCompany($id, $cid)
    {
        return $this->repository->createQueryBuilder('g')
            ->where('g.id = :id')
            ->andWhere('g.company = :cid')
            ->setParameters(array(
                'id' => $id,
                'cid' => $cid
            ))
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @inheritDoc
     */
    public function update($object)
    {
        parent::update($object);
        if ($object instanceof GroupInterface) {
            /** @var UserInterface $member */
            foreach ($object->getMembers() as $member) {
                $member->addGroup($object);
                $this->update($member);
            }
        }
    }
}
