<?php

namespace EO\UserBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\UserBundle\Model\GroupInterface;

interface GroupManagerInterface extends ManagerInterface
{
    /**
     * @param string $cid
     *
     * @return array
     */
    public function findAllByCompany($cid);

    /**
     * @param string $id
     * @param string $cid
     *
     * @return GroupInterface
     */
    public function findOneByCompany($id, $cid);
}