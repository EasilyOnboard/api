<?php

namespace EO\UserBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\UserBundle\Model\FamilyInterface;
use EO\UserBundle\Model\InviteInterface;
use EO\UserBundle\Model\UserInterface;

interface InviteManagerInterface extends ManagerInterface
{
    /**
     * @param UserInterface $user
     *
     * @return InviteInterface
     */
    public function findAllByWho(UserInterface $user);

    /**
     * @param UserInterface $user
     *
     * @return array
     */
    public function findAllByBy(UserInterface $user);

    /**
     * @param FamilyInterface $family
     *
     * @return array
     */
    public function findAllByFamily(FamilyInterface $family);

    /**
     * @param UserInterface $by
     * @param UserInterface $who
     *
     * @return InviteInterface
     */
    public function findOneByByAndWho(UserInterface $by, UserInterface $who);
}