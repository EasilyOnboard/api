<?php

namespace EO\UserBundle\Manager\DBAL;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\UserBundle\Model\FamilyInterface;
use EO\UserBundle\Model\GroupInterface;
use EO\UserBundle\Model\UserInterface;

interface UserManagerInterface extends ManagerInterface
{
    /**
     * @return \FOS\UserBundle\Model\UserManagerInterface
     */
    public function getFOSUserManager();

    /**
     * @param string $id
     * @return UserInterface
     */
    public function findCustomer($id);

    /**
     * @param string $id
     * @param string $cid
     * @return UserInterface
     */
    public function findStaffByCompany($id, $cid);

    /**
     * @return array
     */
    public function findAllCustomers();

    /**
     * @param UserInterface $customer
     *
     * @return array
     */
    public function findAllOtherFamilyMembersByCustomer(UserInterface $customer);

    /**
     * @param CompanyInterface $company
     *
     * @return array
     */
    public function findAllStaffByCompany(CompanyInterface $company);

    /**
     * @param GroupInterface $group
     *
     * @return array
     */
    public function findAllStaffByGroup(GroupInterface $group);

    /**
     * @param string $id
     * @param FamilyInterface $family
     *
     * @return UserInterface
     */
    public function findOneByIdInFamily($id, FamilyInterface $family);

    /**
     * @param $like
     *
     * @return array
     */
    public function findAllCustomerLike($like);
}