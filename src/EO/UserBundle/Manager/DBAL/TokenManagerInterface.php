<?php

namespace EO\UserBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\UserBundle\Model\TokenInterface;
use EO\UserBundle\Model\UserInterface;

interface TokenManagerInterface extends ManagerInterface
{
    /**
     * @param UserInterface $user
     * @param string $platform
     *
     * @return TokenInterface|null
     */
    public function findOneByUserAndPlatform(UserInterface $user, $platform);

    /**
     * @param UserInterface $user
     *
     * @return TokenInterface[]
     */
    public function findAllByUser(UserInterface $user);

    /**
     * @param UserInterface $user
     * @param integer $lifetime
     *
     * @return TokenInterface
     */
    public function findCurrentOneByUser(UserInterface $user, $lifetime);
}