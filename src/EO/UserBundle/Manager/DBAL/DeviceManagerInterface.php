<?php

namespace EO\UserBundle\Manager\DBAL;

use EO\CoreBundle\Manager\DBAL\ManagerInterface;
use EO\UserBundle\Model\DeviceInterface;
use EO\UserBundle\Model\UserInterface;

interface DeviceManagerInterface extends ManagerInterface
{
    /**
     * @param UserInterface $customer
     *
     * @return array
     */
    public function findAllByCustomer(UserInterface $customer);

    /**
     * @param string $token
     *
     * @return DeviceInterface
     */
    public function findOneByToken($token);

    /**
     * @param string $appId
     *
     * @return DeviceInterface
     */
    public function findOneByAppId($appId);
}