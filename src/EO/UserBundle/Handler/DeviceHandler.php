<?php

namespace EO\UserBundle\Handler;

use EO\UserBundle\Manager\DBAL\DeviceManagerInterface;
use EO\UserBundle\Model\DeviceInterface;
use EO\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Translation\TranslatorInterface;

class DeviceHandler implements DeviceHandlerInterface
{
    /**
     * @var DeviceManagerInterface
     */
    private $deviceManager;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var string
     */
    private $email;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var TwigEngine
     */
    private $renderer;

    /**
     * @var integer
     */
    private $lifetime;

    /**
     * @param DeviceManagerInterface $deviceManager
     * @param \Swift_Mailer $mailer
     * @param TranslatorInterface $translator
     * @param TwigEngine $renderer
     * @param Router $router
     * @param string $email
     * @param integer $lifetime
     */
    public function __construct(
        DeviceManagerInterface $deviceManager,
        \Swift_Mailer $mailer,
        TranslatorInterface $translator,
        TwigEngine $renderer,
        Router $router,
        $email,
        $lifetime
    ) {
        $this->deviceManager = $deviceManager;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->email = $email;
        $this->translator = $translator;
        $this->renderer = $renderer;
        $this->lifetime = $lifetime;
    }

    public function newDeviceToUser(DeviceInterface $device, UserInterface $user, $blocked = true)
    {
        if ($device->getAppId() === null) {
            return null;
        }
        $device->setCustomer($user);
        $device->setBlocked($blocked);
        $device->setLastLogin(new \DateTime("now"));
        $this->deviceManager->update($device);

        if ($blocked) {
            if (!$this->sendTokenValidationEmail($device, $user)) {
                $this->deviceManager->delete($device);
                return null;
            }
        }

        return $device;
    }

    public function loginWithDevice(DeviceInterface $device)
    {
        $device->setLastLogin(new \DateTime("now"));
        $this->deviceManager->update($device);

        return $device;
    }

    public function checkDeviceAppId(DeviceInterface $device, UserInterface $user)
    {
        $realDevice = $this->deviceManager->findOneByAppId($device->getAppId());

        return $realDevice && $realDevice->getBlocked() === false && $realDevice->getCustomer()->getId() === $user->getId();
    }

    public function blockDevice(DeviceInterface $device)
    {
        $device->setBlocked(true);
        $this->deviceManager->update($device);

        return $device;
    }

    public function unblockDevice(DeviceInterface $device)
    {
        $this->sendTokenValidationEmail($device, $device->getCustomer());

        return $device;
    }

    public function validateToken($token)
    {
        $device = $this->deviceManager->findOneByToken($token);
        if (!$device) {
            return null;
        }
        $device->setToken(null);
        $device->setTokenDate(null);
        $device->setShelved(false);

        $now = new \DateTime("now");
        if ($device->getTokenDate()->getTimestamp() - $now->getTimestamp() < $this->lifetime) {
            $device->setBlocked(false);
        }
        $this->deviceManager->update($device);
        return $device;
    }

    private function sendTokenValidationEmail(DeviceInterface $device)
    {
        $device->setBlocked(true);
        $device->setShelved(true);
        $device->setToken(bin2hex(openssl_random_pseudo_bytes(64)));
        $device->setTokenDate(new \DateTime("now"));
        $this->deviceManager->update($device);

        $url = $this->router->generate('eo_security_validate', array(
            'token' => $device->getToken(),
            '_format' => 'json'
        ));

        $mail = \Swift_Message::newInstance();
        $mail
            ->setSubject('Device token')
            ->setFrom($this->email)
            ->setTo($device->getCustomer()->getEmail())
            ->setBody($this->renderer->render('EOCoreBundle:email:device.html.twig', array('url' => $url)))
            ->setContentType('text/html')
        ;
        if (!$this->mailer->send($mail)) {
            $device->setShelved(false);
            $device->setToken(null);
            $device->setTokenDate(null);
            $this->deviceManager->update($device);

            return false;
        }
        return true;
    }
}