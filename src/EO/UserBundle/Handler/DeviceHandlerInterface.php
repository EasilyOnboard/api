<?php

namespace EO\UserBundle\Handler;

use EO\UserBundle\Model\DeviceInterface;
use EO\UserBundle\Model\UserInterface;

interface DeviceHandlerInterface
{
    /**
     * @param DeviceInterface $device
     * @param UserInterface $user
     *
     * @return DeviceInterface
     */
    public function newDeviceToUser(DeviceInterface $device, UserInterface $user);

    /**
     * @param DeviceInterface $device
     *
     * @return DeviceInterface
     */
    public function loginWithDevice(DeviceInterface $device);

    /**
     * @param DeviceInterface $device
     * @param UserInterface $user
     *
     * @return boolean
     */
    public function checkDeviceAppId(DeviceInterface $device, UserInterface $user);

    /**
     * @param DeviceInterface $device
     *
     * @return DeviceInterface
     */
    public function blockDevice(DeviceInterface $device);

    /**
     * @param DeviceInterface $device
     *
     * @return DeviceInterface
     */
    public function unblockDevice(DeviceInterface $device);

    /**
     * @param string $token
     *
     * @return DeviceInterface
     */
    public function validateToken($token);
}