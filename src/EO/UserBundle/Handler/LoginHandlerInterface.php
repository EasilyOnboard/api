<?php

namespace EO\UserBundle\Handler;

use EO\UserBundle\Form\Model\Login;
use EO\UserBundle\Form\Model\Refresh;
use EO\UserBundle\Model\TokenInterface;
use EO\UserBundle\Model\UserInterface;

interface LoginHandlerInterface
{
    /**
     * @param Login $login
     *
     * @return TokenInterface|null
     */
    public function handleLogin(Login $login);

    /**
     * @param Refresh $refresh
     *
     * @return TokenInterface|null
     */
    public function handleRefresh(Refresh $refresh);

    /**
     * @param UserInterface $user
     */
    public function handleLogout(UserInterface $user);
}