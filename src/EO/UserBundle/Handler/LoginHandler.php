<?php

namespace EO\UserBundle\Handler;

use EO\UserBundle\Enum\HttpHeader;
use EO\UserBundle\Form\Model\Login;
use EO\UserBundle\Form\Model\Refresh;
use EO\UserBundle\Manager\DBAL\TokenManagerInterface;
use EO\UserBundle\Manager\DBAL\UserManagerInterface;
use EO\UserBundle\Model\TokenInterface;
use EO\UserBundle\Model\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CredentialsExpiredException;
use Symfony\Component\Security\Core\SecurityContextInterface;

class LoginHandler implements LoginHandlerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function handleLogin(Login $login)
    {
        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail($login->getEmail());
        if (!$user) {
            throw new AuthenticationCredentialsNotFoundException();
        }
        $authenticationToken = new UsernamePasswordToken($user, $login->getPassword(), $this->getProviderKey(), $user->getRoles());
        /** @var TokenInterface $accessToken */
        $accessToken = $this->getAuthenticationProvider()->authenticate($authenticationToken);
        $token = new UsernamePasswordToken($accessToken->getUser(), $accessToken->getAccessToken(), $this->getProviderKey(), $accessToken->getRoles());
        $this->getSecurityContext()->setToken($token);
        return $accessToken;
    }

    public function handleRefresh(Refresh $refresh)
    {
        /** @var TokenInterface $token */
        $token = $this->getTokenManager()->find($refresh->getAccessToken());
        if ($token->getRefreshToken() === $refresh->getRefreshToken() &&
            $token->getPlatform() === $this->getRequest()->headers->get(strtolower(HttpHeader::PLATFORM))) {
            $now = new \DateTime('now');
            if ($token->getRefreshExpiresAt() >= $now) {
                $token->resetRefreshToken();
                $accessLifetime = $this->container->getParameter('eo_core.authentication.access_lifetime');
                $refreshLifetime = $this->container->getParameter('eo_core.authentication.refresh_lifetime');
                $accessExpiresAt = new \DateTime('now');
                $refreshExpiresAt = new \DateTime('now');
                $accessExpiresAt->add(new \DateInterval('PT'.$accessLifetime.'S'));
                $refreshExpiresAt->add(new \DateInterval('PT'.$refreshLifetime.'S'));
                $token->setAccessExpiresAt($accessExpiresAt);
                $token->setRefreshExpiresAt($refreshExpiresAt);
                return $token;
            } else {
                $this->getTokenManager()->delete($token);
                throw new CredentialsExpiredException();
            }
        }
        throw new AuthenticationException();
    }

    public function handleLogout(UserInterface $user)
    {
        if (null !== $user) {
            /** @var \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $sessionToken */
            $sessionToken = $this->getSecurityContext()->getToken();
            $token = $this->getTokenManager()->find($sessionToken->getCredentials());
            $this->getTokenManager()->delete($token);
            $token = new AnonymousToken(null, "");
            $this->getSecurityContext()->setToken($token);
            $this->getSession()->invalidate();
        }
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Request
     */
    private function getRequest()
    {
        return $this->container->get('request');
    }

    /**
     * @return string
     */
    private function getProviderKey()
    {
        return $this->container->getParameter('eo_user.authentication.provider_key');
    }

    /**
     * @return AuthenticationProviderInterface
     */
    private function getAuthenticationProvider()
    {
        return $this->container->get('eo_user.authentication.provider');
    }

    /**
     * @return SecurityContextInterface
     */
    private function getSecurityContext()
    {
        return $this->container->get('security.context');
    }

    /**
     * @return SessionInterface
     */
    private function getSession()
    {
        return $this->container->get('session');
    }

    /**
     * @return UserManagerInterface
     */
    private function getUserManager()
    {
        return $this->container->get('eo_user.manager.user');
    }

    /**
     * @return TokenManagerInterface
     */
    public function getTokenManager()
    {
        return $this->container->get('eo_user.manager.token');
    }
}