<?php

namespace EO\UserBundle\Model;

interface InviteInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Family.
     *
     * @return FamilyInterface
     */
    public function getFamily();

    /**
     * Get By.
     *
     * @return UserInterface
     */
    public function getBy();

    /**
     * Get Who.
     *
     * @return UserInterface
     */
    public function getWho();

    /**
     * Get At.
     *
     * @return \DateTime
     */
    public function getAt();

    /**
     * Set Family.
     *
     * @param FamilyInterface $family
     *
     * @return self
     */
    public function setFamily(FamilyInterface $family);

    /**
     * Set By.
     *
     * @param UserInterface $user
     *
     * @return self
     */
    public function setBy(UserInterface $user);

    /**
     * Set Who.
     *
     * @param UserInterface $user
     *
     * @return self
     */
    public function setWho(UserInterface $user);

    /**
     * Set At.
     *
     * @param \DateTime $at
     *
     * @return self
     */
    public function setAt(\DateTime $at);
}