<?php

namespace EO\UserBundle\Model;

class Invite implements InviteInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var FamilyInterface
     */
    protected $family;

    /**
     * @var UserInterface
     */
    protected $by;

    /**
     * @var UserInterface
     */
    protected $who;

    /**
     * @var \DateTime
     */
    protected $at;

    public function __construct()
    {
        $this->at = new \DateTime("now");
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFamily()
    {
        return $this->family;
    }

    public function getBy()
    {
        return $this->by;
    }

    public function getWho()
    {
        return $this->who;
    }

    public function getAt()
    {
        return $this->at;
    }

    public function setFamily(FamilyInterface $family)
    {
        $this->family = $family;

        return $this;
    }

    public function setBy(UserInterface $by)
    {
        $this->by = $by;

        return $this;
    }

    public function setWho(UserInterface $who)
    {
        $this->who = $who;

        return $this;
    }

    public function setAt(\DateTime $at)
    {
        $this->at = $at;

        return $this;
    }
}