<?php

namespace EO\UserBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

interface FamilyInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get Master.
     *
     * @return integer
     */
    public function getMaster();

    /**
     * Get Members.
     *
     * @return ArrayCollection
     */
    public function getMembers();

    /**
     * Set Name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * Set Master.
     *
     * @param integer $master
     *
     * @return self
     */
    public function setMaster($master);

    /**
     * Add a Member.
     *
     * @param UserInterface $member
     *
     * @return self
     */
    public function addMember(UserInterface $member);

    /**
     * Remove a Member.
     *
     * @param UserInterface $member
     *
     * @return self
     */
    public function removeMember(UserInterface $member);
}
