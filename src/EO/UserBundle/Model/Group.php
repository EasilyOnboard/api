<?php

namespace EO\UserBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\CompanyBundle\Model\CompanyInterface;

class Group extends \FOS\UserBundle\Model\Group implements GroupInterface
{
    /**
     * @var CompanyInterface
     */
    protected $company;

    /**
     * @var ArrayCollection
     */
    protected $members;

    public function __construct($name = "", $roles = array())
    {
        parent::__construct($name, $roles);
        $this->members = new ArrayCollection();
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getMembers()
    {
        return $this->members;
    }

    public function setCompany(CompanyInterface $company)
    {
        $this->company = $company;

        return $this;
    }

    public function addMember(UserInterface $user)
    {
        if (!$this->members->contains($user)) {
            $this->members->add($user);
        }
        return $this;
    }

    public function removeMember(UserInterface $user)
    {
        if ($this->members->contains($user)) {
            $this->members->removeElement($user);
        }
        return $this;
    }

    /**
     * Group to string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }
}
