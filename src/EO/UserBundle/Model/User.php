<?php

namespace EO\UserBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\CompanyBundle\Model\CompanyInterface;
use EO\MiscBundle\Model\AddressInterface;
use EO\MiscBundle\Model\NotificationInterface;
use EO\TerminalBundle\Model\TerminalInterface;

class User extends \FOS\UserBundle\Model\User implements UserInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var string
     */
    protected $sex;

    /**
     * @var \Datetime
     */
    protected $birthday;

    /**
     * @var AddressInterface
     */
    protected $fullAddress;

    /**
     * @var ArrayCollection
     */
    protected $devices;

    /**
     * @var FamilyInterface
     */
    protected $family;

    /**
     * @var CompanyInterface
     */
    protected $company;

    /**
     * @var TerminalInterface
     */
    protected $terminal;

    /**
     * @var ArrayCollection
     */
    protected $notifications;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    public function __construct()
    {
        parent::__construct();
        $this->devices = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->createdAt = new \DateTime("now");
        $this->groups = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getLanguage()
    {
        return $this->language;
    }

    public function getSex()
    {
        return $this->sex;
    }

    public function getBirthday()
    {
        return $this->birthday;
    }

    public function getFullAddress()
    {
        return $this->fullAddress;
    }

    public function getAddress()
    {
        return $this->fullAddress->getAddress();
    }

    public function getCity()
    {
        return $this->fullAddress->getCity();
    }

    public function getPostcode()
    {
        return $this->fullAddress->getPostcode();
    }

    public function getState()
    {
        return $this->fullAddress->getState();
    }

    public function getCountry()
    {
        return $this->fullAddress->getCountry();
    }

    public function getPhone()
    {
        return $this->fullAddress->getPhone();
    }

    public function getDevices()
    {
        return $this->devices;
    }

    public function getFamily()
    {
        return $this->family;
    }

    public function getCompany()
    {
        return $this->company;
    }

    public function getTerminal()
    {
        return $this->terminal;
    }

    public function getNotifications()
    {
        return $this->terminal;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    public function setEmailCanonical($emailCanonical)
    {
        $this->setUsernameCanonical($emailCanonical);

        return parent::setEmailCanonical($emailCanonical);
    }

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function setFullAddress(AddressInterface $address)
    {
        $this->fullAddress = $address;

        return $this;
    }

    public function setAddress($address)
    {
        $this->fullAddress->setAddress($address);

        return $this;
    }

    public function setCity($city)
    {
        $this->fullAddress->setCity($city);

        return $this;
    }

    public function setPostcode($postcode)
    {
        $this->fullAddress->setPostcode($postcode);

        return $this;
    }

    public function setState($state)
    {
        $this->fullAddress->setState($state);

        return $this;
    }

    public function setCountry($country)
    {
        $this->fullAddress->setCountry($country);

        return $this;
    }

    public function setPhone($phone)
    {
        $this->fullAddress->setPhone($phone);

        return $this;
    }

    public function addDevice(DeviceInterface $mobile)
    {
        if (!$this->devices->contains($mobile)) {
            $this->devices->add($mobile);
        }

        return $this;
    }

    public function removeDevice(DeviceInterface $mobile)
    {
        if ($this->devices->contains($mobile)) {
            $this->devices->removeElement($mobile);
        }

        return $this;
    }

    public function setFamily(FamilyInterface $family = null)
    {
        $this->family = $family;

        return $this;
    }

    public function setCompany(CompanyInterface $company = null)
    {
        $this->company = $company;

        return $this;
    }

    public function setTerminal(TerminalInterface $terminal)
    {
        $this->terminal = $terminal;

        return $this;
    }

    public function addNotification(NotificationInterface $notification)
    {
        if (!$this->notifications->contains($notification)) {
            $this->notifications->add($notification);
        }

        return $this;
    }

    public function removeNotification(NotificationInterface $notification)
    {
        if ($this->notifications->contains($notification)) {
            $this->notifications->removeElement($notification);
        }

        return $this;
    }

    public function hasGroup($name = "")
    {
        if (empty($name)) {
            return !$this->groups->isEmpty();
        }
        return parent::hasGroup($name);
    }

    public function __toString()
    {
        return $this->getFirstname().' '.$this->getLastname();
    }
}
