<?php

namespace EO\UserBundle\Model;

interface TokenInterface
{
    /**
     * Get token.
     *
     * @return string
     */
    public function getAccessToken();

    /**
     * Get refresh token.
     *
     * @return string
     */
    public function getRefreshToken();

    /**
     * Get platform.
     *
     * @return string
     */
    public function getPlatform();

    /**
     * Get Roles.
     *
     * @return array
     */
    public function getRoles();

    /**
     * Get User.
     *
     * @return UserInterface
     */
    public function getUser();

    /**
     * Get access expires at.
     *
     * @return \DateTime
     */
    public function getAccessExpiresAt();

    /**
     * Get refresh expires at.
     *
     * @return \DateTime
     */
    public function getRefreshExpiresAt();

    /**
     * Get created at.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Reset refresh token.
     *
     * @return self
     */
    public function resetRefreshToken();

    /**
     * Set platform.
     *
     * @param string $platform
     *
     * @return TokenInterface
     */
    public function setPlatform($platform);

    /**
     * Set User.
     *
     * @param UserInterface $user
     *
     * @return self
     */
    public function setUser(UserInterface $user);

    /**
     * Set access expires at.
     *
     * @param \DateTime $accessExpiresAt
     *
     * @return TokenInterface
     */
    public function setAccessExpiresAt(\DateTime $accessExpiresAt);

    /**
     * Set refresh expires at.
     *
     * @param \DateTime $refreshExpiresAt
     *
     * @return TokenInterface
     */
    public function setRefreshExpiresAt(\DateTime $refreshExpiresAt);

    /**
     * Has role.
     *
     * @param string $role
     *
     * @return boolean
     */
    public function hasRole($role);

    /**
     * Add Role.
     *
     * @param string $role
     *
     * @return self
     */
    public function addRole($role);

    /**
     * Remove Role.
     *
     * @param string $role
     *
     * @return self
     */
    public function removeRole($role);

    /**
     * Set Roles.
     *
     * @param array $roles
     *
     * @return self
     */
    public function setRoles(array $roles);
}