<?php

namespace EO\UserBundle\Model;

class Device implements DeviceInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $appId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $os;

    /**
     * @var string
     */
    protected $model;

    /**
     * @var boolean
     */
    protected $blocked;

    /**
     * @var boolean
     */
    protected $shelved;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var \DateTime
     */
    protected $tokenDate;

    /**
     * @var \DateTime
     */
    protected $date;

    /**
     * @var \DateTime
     */
    protected $lastLogin;

    /**
     * @var UserInterface
     */
    protected $customer;

    public function __construct()
    {
        $this->date = new \DateTime("now");
        $this->blocked = false;
        $this->shelved = false;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getOS()
    {
        return $this->os;
    }

    /**
     * {@inheritdoc}
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlocked()
    {
        return $this->blocked;
    }

    /**
     * {@inheritdoc}
     */
    public function getShelved()
    {
        return $this->shelved;
    }

    /**
     * {@inheritdoc}
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public function getTokenDate()
    {
        return $this->tokenDate;
    }

    /**
     * {@inheritdoc}
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * {@inheritdoc}
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * {@inheritdoc}
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setOS($os)
    {
        $this->os = $os;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setBlocked($blocked)
    {
        $this->blocked = $blocked;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setShelved($shelved)
    {
        $this->shelved = $shelved;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setTokenDate(\DateTime $tokenDate)
    {
        $this->tokenDate = $tokenDate;

        return $this;
    }


    /**
     * {@inheritdoc}
     */
    public function setLastLogin(\DateTime $lastLogin)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCustomer(UserInterface $customer)
    {
        $this->customer = $customer;

        return $this;
    }
}
