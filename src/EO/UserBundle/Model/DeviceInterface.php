<?php

namespace EO\UserBundle\Model;

interface DeviceInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get AppId.
     *
     * @return string
     */
    public function getAppId();

    /**
     * Get Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get OS.
     *
     * @return string
     */
    public function getOS();

    /**
     * Get Model.
     *
     * @return string
     */
    public function getModel();

    /**
     * Get Blocked.
     *
     * @return boolean
     */
    public function getBlocked();

    /**
     * Get Shelved.
     *
     * @return boolean
     */
    public function getShelved();

    /**
     * Get Token.
     *
     * @return string
     */
    public function getToken();

    /**
     * Get Token Date.
     *
     * @return \DateTime
     */
    public function getTokenDate();

    /**
     * Get Date.
     *
     * @return \Datetime $date
     */
    public function getDate();

    /**
     * Get Last Login.
     *
     * @return \DateTime $lastLogin
     */
    public function getLastLogin();

    /**
     * Get Customer.
     *
     * @return UserInterface
     */
    public function getCustomer();

    /**
     * Set AppId.
     *
     * @param string $appId
     *
     * @return self
     */
    public function setAppId($appId);

    /**
     * Set Name.
     *
     * @param string $name
     *
     * @return self
     */
    public function setName($name);

    /**
     * Set OS.
     *
     * @param string $os
     *
     * @return self
     */
    public function setOS($os);

    /**
     * Set Model.
     *
     * @param string $model
     *
     * @return self
     */
    public function setModel($model);

    /**
     * Set Blocked.
     *
     * @param boolean $blocked
     *
     * @return self
     */
    public function setBlocked($blocked);

    /**
     * Set Shelved.
     *
     * @param boolean $shelved
     *
     * @return self
     */
    public function setShelved($shelved);

    /**
     * Set Token.
     *
     * @param string $token
     *
     * @return self
     */
    public function setToken($token);

    /**
     * Set Token Date.
     *
     * @param \DateTime $tokenDate
     * @return self
     */
    public function setTokenDate(\DateTime $tokenDate);

    /**
     * Set Last Login.
     *
     * @param \DateTime $lastLogin
     *
     * @return self
     */
    public function setLastLogin(\DateTime $lastLogin);

    /**
     * Set Customer.
     *
     * @param UserInterface $customer
     *
     * @return self
     */
    public function setCustomer(UserInterface $customer);
}
