<?php

namespace EO\UserBundle\Model;

use EO\CoreBundle\Util\TokenGenerator;

class Token implements TokenInterface
{
    /**
     * @var string
     */
    private $accessToken;

    /**
     * @var string
     */
    private $refreshToken;

    /**
     * @var string
     */
    private $platform;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @var \DateTime
     */
    private $accessExpiresAt;

    /**
     * @var \DateTime
     */
    private $refreshExpiresAt;

    /**
     * @var \DateTime
     */
    private $createdAt;

    public function __construct()
    {
        $this->accessToken = TokenGenerator::newUniqueToken();
        $this->refreshToken = TokenGenerator::newUniqueToken();
        $this->roles = array();
        $this->createdAt = new \DateTime("now");
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    public function getPlatform()
    {
        return $this->platform;
    }

    public function getAccessExpiresAt()
    {
        return $this->accessExpiresAt;
    }

    public function getRefreshExpiresAt()
    {
        return $this->refreshExpiresAt;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getRoles()
    {
        return array_unique($this->roles);
    }

    public function getUser()
    {
        return $this->user;
    }

    public function resetRefreshToken()
    {
        $this->refreshToken = TokenGenerator::newUniqueToken();
        return $this;
    }

    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    public function setUser(UserInterface $user)
    {
        $this->user = $user;
        return $this;
    }

    public function setAccessExpiresAt(\DateTime $accessExpiresAt)
    {
        $this->accessExpiresAt = $accessExpiresAt;
        return $this;
    }

    public function setRefreshExpiresAt(\DateTime $refreshExpiresAt)
    {
        $this->refreshExpiresAt = $refreshExpiresAt;
        return $this;
    }

    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function addRole($role)
    {
        $role = strtoupper($role);
        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }
        return $this;
    }

    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }
        return $this;
    }

    public function setRoles(array $roles)
    {
        $this->roles = array();

        foreach ($roles as $role) {
            $this->addRole($role);
        }
        return $this;
    }
}