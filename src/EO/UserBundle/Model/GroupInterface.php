<?php

namespace EO\UserBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\CompanyBundle\Model\CompanyInterface;

interface GroupInterface extends \FOS\UserBundle\Model\GroupInterface
{
    /**
     * Get company.
     *
     * @return CompanyInterface $company
     */
    public function getCompany();

    /**
     * Get members.
     *
     * @return ArrayCollection
     */
    public function getMembers();

    /**
     * Set company.
     *
     * @param CompanyInterface $company
     *
     * @return self
     */
    public function setCompany(CompanyInterface $company);

    /**
     * Add member.
     *
     * @param UserInterface $user
     *
     * @return self
     */
    public function addMember(UserInterface $user);

    /**
     * Remove member.
     *
     * @param UserInterface $user
     *
     * @return self
     */
    public function removeMember(UserInterface $user);
}
