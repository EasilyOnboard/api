<?php

namespace EO\UserBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class Family implements FamilyInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var integer
     */
    protected $master;

    /**
     * @var ArrayCollection
     */
    protected $members;

    public function __construct()
    {
        $this->members = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getMaster()
    {
        return $this->master;
    }

    public function getMembers()
    {
        return $this->members;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setMaster($master)
    {
        $this->master = $master;

        return $this;
    }

    public function addMember(UserInterface $member)
    {
        if (!$this->members->contains($member)) {
            $member->setFamily($this);
            $this->members->add($member);
        }

        return $this;
    }

    public function removeMember(UserInterface $member)
    {
        if ($this->members->contains($member)) {
            $member->setFamily(null);
            $this->members->removeElement($member);
        }

        return $this;
    }
}
