<?php

namespace EO\UserBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use EO\CompanyBundle\Model\CompanyInterface;
use EO\MiscBundle\Model\AddressInterface;
use EO\MiscBundle\Model\NotificationInterface;
use EO\TerminalBundle\Model\TerminalInterface;
use FOS\UserBundle\Model\GroupableInterface;

interface UserInterface extends \FOS\UserBundle\Model\UserInterface, GroupableInterface, AddressInterface
{
    /**
     * Get Id.
     *
     * @return string
     */
    public function getId();

    /**
     * Get First name.
     *
     * @return string
     */
    public function getFirstName();

    /**
     * Get Last name.
     *
     * @return string
     */
    public function getLastName();

    /**
     * Get Language.
     *
     * @return string
     */
    public function getLanguage();

    /**
     * Get Sex.
     *
     * @return string
     */
    public function getSex();

    /**
     * Get Birthday.
     *
     * @return \Datetime
     */
    public function getBirthday();

    /**
     * Get Full Address.
     *
     * @return AddressInterface
     */
    public function getFullAddress();

    /**
     * Get Mobiles.
     *
     * @return ArrayCollection
     */
    public function getDevices();

    /**
     * Get family.
     *
     * @return FamilyInterface
     */
    public function getFamily();

    /**
     * Get company.
     *
     * @return CompanyInterface
     */
    public function getCompany();

    /**
     * Get Terminal.
     *
     * @return TerminalInterface
     */
    public function getTerminal();

    /**
     * Get Notifications.
     *
     * @return ArrayCollection
     */
    public function getNotifications();

    /**
     * Get Created At.
     *
     * @return \DateTime
     */
    public function getCreatedAt();

    /**
     * Set First name.
     *
     * @param string $firstName
     *
     * @return self
     */
    public function setFirstName($firstName);

    /**
     * Set Last name.
     *
     * @param string $lastName
     *
     * @return self
     */
    public function setLastName($lastName);

    /**
     * Set Language.
     *
     * @param string $language
     * @return self
     */
    public function setLanguage($language);

    /**
     * Set Sex.
     *
     * @param string $sex
     *
     * @return self
     */
    public function setSex($sex);

    /**
     * Set Birthday.
     *
     * @param \Datetime $birthday
     *
     * @return self
     */
    public function setBirthday($birthday);

    /**
     * Set Full Address.
     *
     * @param AddressInterface $address
     *
     * @return self
     */
    public function setFullAddress(AddressInterface $address);

    /**
     * Add Mobile.
     *
     * @param DeviceInterface $mobile
     *
     * @return self
     */
    public function addDevice(DeviceInterface $mobile);

    /**
     * Remove Mobile.
     *
     * @param DeviceInterface $mobile
     *
     * @return self
     */
    public function removeDevice(DeviceInterface $mobile);

    /**
     * Set Family.
     *
     * @param FamilyInterface $family
     *
     * @return self
     */
    public function setFamily(FamilyInterface $family);

    /**
     * Set company.
     *
     * @param CompanyInterface $company
     *
     * @return self
     */
    public function setCompany(CompanyInterface $company);

    /**
     * Set Terminal.
     *
     * @param TerminalInterface $terminal
     *
     * @return self
     */
    public function setTerminal(TerminalInterface $terminal);

    /**
     * Add Notification.
     *
     * @param NotificationInterface $notification
     *
     * @return self
     */
    public function addNotification(NotificationInterface $notification);

    /**
     * Remove Notification.
     *
     * @param NotificationInterface $notification
     *
     * @return self
     */
    public function removeNotification(NotificationInterface $notification);

    public function __toString();
}
