<?php

namespace EO\UserBundle\Controller;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Controller\EOController;
use EO\UserBundle\Model\GroupInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class GroupController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All Groups.",
     *  output={
     *      "class"="array<EO\UserBundle\Model\Group>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_GROUP_LIST')")
     *
     * @param string $companyId
     *
     * @return View
     */
    public function allAction($companyId)
    {
        $groups = $this->getGroupManager()->findAllByCompany($companyId);
        return $this->createView($groups, Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create group.",
     *  input="eo_group",
     *  output={
     *      "class"="EO\UserBundle\Model\Group",
     *      "groups"={"basic", "full_group"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_GROUP_CREATE')")
     *
     * @param Request $request
     * @param string  $companyId
     *
     * @return View
     */
    public function postAction(Request $request, $companyId)
    {
        /** @var GroupInterface $group */
        $group = $this->getGroupManager()->create();
        /** @var CompanyInterface $company */
        $company = $this->getCompanyManager()->find($companyId);
        if (!$company) {
            throw $this->createNotFoundException();
        }
        $group->setCompany($company);
        $form = $this->createRestForm('eo_group', $group);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getGroupManager()->update($group);
            return $this->createView($group, Codes::HTTP_CREATED, array('basic', 'full_group'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get group.",
     *  output={
     *      "class"="EO\UserBundle\Model\Group",
     *      "groups"={"basic", "full_group"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get("/{companyId}/{groupId}")
     *
     * @Security("has_role('ROLE_GROUP_GET')")
     *
     * @param string $companyId
     * @param string $groupId
     *
     * @return View
     */
    public function getAction($companyId, $groupId)
    {
        $group = $this->getGroupManager()->findOneByCompany($groupId, $companyId);
        if (!$group) {
            $this->createNotFoundException();
        }
        return $this->createView($group, Codes::HTTP_OK, array('basic', 'full_group'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update group.",
     *  input="eo_group",
     *  output={
     *      "class"="EO\UserBundle\Model\Group",
     *      "groups"={"basic", "full_group"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put("/{companyId}/{groupId}")
     *
     * @Security("has_role('ROLE_GROUP_UPDATE')")
     *
     * @param Request $request
     * @param string  $companyId
     * @param string  $groupId
     *
     * @return View
     */
    public function putAction(Request $request, $companyId, $groupId)
    {
        $group = $this->getGroupManager()->findOneByCompany($groupId, $companyId);
        if (!$group) {
            $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_group', $group);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getGroupManager()->update($group);
            return $this->createView($group, Codes::HTTP_ACCEPTED, array('basic', 'full_group'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete group.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when the Group is successfully deleted"
     *  }
     * )
     *
     * @Rest\Delete("/{companyId}/{groupId}")
     *
     * @Security("has_role('ROLE_GROUP_DELETE')")
     *
     * @param string $companyId
     * @param string $groupId
     *
     * @return View
     */
    public function deleteAction($companyId, $groupId)
    {
        $group = $this->getGroupManager()->findOneByCompany($groupId, $companyId);
        if (!$group) {
            $this->createNotFoundException();
        }
        $this->getGroupManager()->delete($group);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @ApiDoc(
     *  description="All Group roles.",
     *  output="array",
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_STAFF')")
     *
     * @return View
     */
    public function rolesAction()
    {
        $roles = $this->container->getParameter('security.role_hierarchy.roles');
        return $this->createView(array('roles' => $roles), Codes::HTTP_OK);
    }

    /**
     * @return \EO\UserBundle\Manager\DBAL\GroupManagerInterface
     */
    private function getGroupManager()
    {
        return $this->get('eo_user.manager.group');
    }

    /**
     * @return \EO\CompanyBundle\Manager\DBAL\CompanyManagerInterface
     */
    private function getCompanyManager()
    {
        return $this->get('eo_company.manager.company');
    }
}