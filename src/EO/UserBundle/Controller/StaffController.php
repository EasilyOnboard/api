<?php

namespace EO\UserBundle\Controller;

use EO\CompanyBundle\Model\CompanyInterface;
use EO\CoreBundle\Controller\EOController;
use EO\UserBundle\Model\UserInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Util\Codes;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class StaffController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All staff.",
     *  output={
     *      "class"="array<EO\UserBundle\Model\User>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_STAFF_LIST')")
     *
     * @param string $companyId
     *
     * @return View
     */
    public function allAction($companyId)
    {
        $staff = $this->getUserManager()->findAllStaffByCompany($this->getCompany($companyId));
        return $this->createView($staff, Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create staff.",
     *  input="eo_staff_password",
     *  output={
     *      "class"="EO\UserBundle\Model\User",
     *      "groups"={"basic", "user", "staff"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_STAFF_CREATE')")
     *
     * @param Request $request
     * @param string $companyId
     *
     * @return View
     */
    public function postAction(Request $request, $companyId)
    {
        $company = $this->getCompany($companyId);
        /** @var UserInterface $user */
        $user = $this->getUserManager()->getFOSUserManager()->createUser();
        $user->setCompany($company);
        $form = $this->createRestForm('eo_staff_password', $user, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $user->setEnabled(true);
            $user->addRole('ROLE_STAFF');
            $this->getUserManager()->getFOSUserManager()->updateUser($user);
            return $this->createView($user, Codes::HTTP_CREATED, array('basic', 'user', 'staff'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get staff.",
     *  output={
     *      "class"="EO\UserBundle\Model\User",
     *      "groups"={"basic", "user", "staff"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get("/{companyId}/{staffId}")
     *
     * @Security("has_role('ROLE_STAFF_GET')")
     *
     * @param string $companyId
     * @param string $staffId
     *
     * @return View
     */
    public function getAction($companyId, $staffId)
    {
        $staff = $this->getUserManager()->findStaffByCompany($staffId, $companyId);
        if (!$staff) {
            throw $this->createNotFoundException();
        }
        return $this->createView($staff, Codes::HTTP_OK, array('basic', 'user', 'staff'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update staff.",
     *  input="eo_staff",
     *  output={
     *      "class"="EO\UserBundle\Model\User",
     *      "groups"={"basic", "user", "staff"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put("/{companyId}/{staffId}")
     *
     * @Security("has_role('ROLE_STAFF_UPDATE')")
     *
     * @param Request $request
     * @param string  $companyId
     * @param string  $staffId
     *
     * @return View
     */
    public function putAction(Request $request, $companyId, $staffId)
    {
        $staff = $this->getUserManager()->findStaffByCompany($staffId, $companyId);
        if (!$staff) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_staff', $staff);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getUserManager()->update($staff);
            return $this->createView($staff, Codes::HTTP_ACCEPTED, array('basic', 'user', 'staff'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Change staff password.",
     *  input="eo_password",
     *  output={
     *      "class"="EO\UserBundle\Model\User",
     *      "groups"={"basic", "user", "staff"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Patch("/{companyId}/{staffId}")
     *
     * @Security("has_role('ROLE_STAFF_UPDATE')")
     *
     * @param Request $request
     * @param string  $companyId
     * @param string  $staffId
     *
     * @return View
     */
    public function patchAction(Request $request, $companyId, $staffId)
    {
        $staff = $this->getUserManager()->findStaffByCompany($staffId, $companyId);
        if (!$staff) {
            throw $this->createNotFoundException();
        }
        $form = $this->createRestForm('eo_password', $staff, 'PATCH');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getUserManager()->getFOSUserManager()->updatePassword($staff);
            return $this->createView($staff, Codes::HTTP_ACCEPTED, array('basic', 'user', 'staff'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete staff.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when the employee is successfully deleted"
     *  }
     * )
     *
     * @Rest\Delete("/{companyId}/{staffId}")
     *
     * @Security("has_role('ROLE_STAFF_DELETE')")
     *
     * @param string $companyId
     * @param string $staffId
     *
     * @return View
     */
    public function deleteAction($companyId, $staffId)
    {
        $staff = $this->getUserManager()->findStaffByCompany($staffId, $companyId);
        if (!$staff) {
            throw $this->createNotFoundException();
        }
        $this->getUserManager()->getFOSUserManager()->deleteUser($staff);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @param string $companyId
     *
     * @return CompanyInterface
     */
    private function getCompany($companyId)
    {
        /** @var CompanyInterface $company */
        $company = $this->getCompanyManager()->find($companyId);
        if (!$company) {
            throw $this->createNotFoundException();
        }
        return $company;
    }

    /**
     * @return \EO\UserBundle\Manager\DBAL\UserManagerInterface
     */
    private function getUserManager()
    {
        return $this->get('eo_user.manager.user');
    }

    /**
     * @return \EO\CompanyBundle\Manager\DBAL\CompanyManagerInterface
     */
    private function getCompanyManager()
    {
        return $this->get('eo_company.manager.company');
    }
}
