<?php

namespace EO\UserBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\UserBundle\Form\Model\ChangePassword;
use EO\UserBundle\Manager\DBAL\UserManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class UserController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="User profile.",
     *  output={
     *      "class"="EO\UserBundle\Model\User",
     *      "groups"={"basic", "user", "customer", "staff"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return View
     */
    public function getAction()
    {
        $groups = array('basic', 'user');
        if ($this->isGranted('ROLE_CUSTOMER')) {
            $groups[] = 'customer';
        } else if ($this->isGranted('ROLE_STAFF')) {
            $groups[] = 'staff';
        }
        return $this->createView($this->getUser(), Codes::HTTP_OK, $groups);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update user profile.",
     *  input="eo_customer",
     *  output={
     *      "class"="EO\UserBundle\Model\User",
     *      "groups"={"basic", "user", "customer", "staff"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid",
     *  }
     * )
     *
     * @Rest\Put
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function putAction(Request $request)
    {
        $user = $this->getUser();
        if ($this->isGranted('ROLE_CUSTOMER')) {
            $form = $this->createRestForm('eo_customer', $user, 'PUT');
        } else {
            $form = $this->createRestForm('eo_user', $user, 'PUT');
        }
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getUserManager()->getFOSUserManager()->updateUser($user);
            $groups = array('basic', 'user');
            if ($this->isGranted('ROLE_CUSTOMER')) {
                $groups[] = 'customer';
            } else if ($this->isGranted('ROLE_STAFF')) {
                $groups[] = 'staff';
            }
            return $this->createView($user, Codes::HTTP_ACCEPTED, $groups);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Change user password.",
     *  input="eo_change_password",
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful",
     *         Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *     }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function patchAction(Request $request)
    {
        $changePassword = new ChangePassword();
        $form = $this->createRestForm('eo_change_password', $changePassword, 'PATCH');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $user = $this->getUser();
            $user->setPlainPassword($form->getData()->getNewPassword());
            $this->getUserManager()->getFOSUserManager()->updateUser($user);
            return $this->createView(null, Codes::HTTP_ACCEPTED);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @return UserManagerInterface
     */
    private function getUserManager()
    {
        return $this->get('eo_user.manager.user');
    }
}