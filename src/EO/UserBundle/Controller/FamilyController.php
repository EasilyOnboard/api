<?php

namespace EO\UserBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\MiscBundle\Model\NotificationInterface;
use EO\UserBundle\Model\FamilyInterface;
use EO\UserBundle\Model\InviteInterface;
use EO\UserBundle\Model\UserInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class FamilyController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All Families",
     *  output={
     *      "class"="array<EO\UserBundle\Model\Family>",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful",
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER_LIST')")
     *
     * @return View
     */
    public function allAction()
    {
        $families = $this->getFamilyManager()->findAll();
        return $this->createView(array('families' => $families), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get Family.",
     *  output={
     *      "class"="EO\UserBundle\Model\Family",
     *      "groups"={"basic", "full_family"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful",
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return View
     */
    public function getAction()
    {
        $user = $this->getUser();
        if ($user->getFamily() == null) {
            throw $this->createNotFoundException();
        }
        return $this->createView($user->getFamily(), Codes::HTTP_OK, array('basic', 'full_family'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get invites",
     *  output={
     *      "class"="EO\UserBundle\Model\Invite",
     *      "groups"={"basic", "full_invite"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful",
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return View
     */
    public function getInvitesAction()
    {
        $invites = $this->getInviteManager()->findAllByWho($this->getUser());
        return $this->createView(array('invites' => $invites), Codes::HTTP_OK, array('basic', 'full_invite'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get Family.",
     *  output={
     *      "class"="EO\UserBundle\Model\Family",
     *      "groups"={"basic", "full_family"}
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful",
     *     }
     * )
     *
     * @Rest\Get("/{familyId}")
     *
     * @Security("has_role('ROLE_CUSTOMER_GET')")
     *
     * @param string $familyId
     *
     * @return View
     */
    public function getByIdAction($familyId)
    {
        $family = $this->getFamilyManager()->find($familyId);
        if (!$family) {
            throw $this->createNotFoundException();
        }
        return $this->createView($family, Codes::HTTP_OK, array('basic', 'full_family'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Create Family.",
     *  input="eo_family",
     *  output={
     *      "class"="EO\UserBundle\Model\Family",
     *      "groups"={"basic", "full_family"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_NOT_ACCEPTABLE="Returned when user has already a family",
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        $user = $this->getUser();
        if ($user->getFamily() !== null) {
            return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
        }
        /** @var FamilyInterface $family */
        $family = $this->getFamilyManager()->create();
        $family->setMaster($user->getId());
        $family->addMember($user);
        $form = $this->createRestForm('eo_family', $family, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getFamilyManager()->update($family);
            return $this->createView($family, Codes::HTTP_CREATED, array('basic', 'full_family'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update family.",
     *  input="eo_family",
     *  output={
     *      "class"="EO\UserBundle\Model\Family",
     *      "groups"={"basic", "full_family"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function putAction(Request $request)
    {
        $family = $this->getUser()->getFamily();
        if (!$family) {
            throw $this->createNotFoundException();
        }
        if ($this->getUser()->getId() != $family->getMaster()) {
            throw $this->createAccessDeniedException();
        }
        $form = $this->createRestForm('eo_family', $family, 'PUT');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getFamilyManager()->update($family);
            /** @var UserInterface $member */
            foreach ($family->getMembers() as $member) {
                /** @var NotificationInterface $notification */
                $notification = $this->getNotificationManager()->create();
                $notification->setUser($member);
                $notification->setType('user_family_new_name');
                $message = $this->getTranslator()->trans('user.family.name', array(
                    '%name%' => $this->getUser()->__toString()), 'notification');
                $notification->setMessage($message);
                $this->getNotificationManager()->update($notification);
                $this->getNotificationHandler()->handlePushNewNotification($notification);
            }
            return $this->createView($family, Codes::HTTP_ACCEPTED, array('basic', 'full_family'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Change family master.",
     *  input="eo_family_master",
     *  output={
     *      "class"="EO\UserBundle\Model\Family",
     *      "groups"={"basic", "full_family"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function patchAction(Request $request)
    {
        $family = $this->getUser()->getFamily();
        if (!$family) {
            throw $this->createNotFoundException();
        }
        if ($this->getUser()->getId() != $family->getMaster()) {
            throw $this->createAccessDeniedException();
        }
        $form = $this->createRestForm('eo_family_master', $family, 'PUT');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $this->getFamilyManager()->update($family);
            /** @var UserInterface $user */
            $user = $this->getUserManager()->find($family->getMaster());
            /** @var NotificationInterface $notification */
            $notification = $this->getNotificationManager()->create();
            $notification->setUser($user);
            $notification->setType('user_family_master');
            $message = $this->getTranslator()->trans('user.family.master', array(), 'notification');
            $notification->setMessage($message);
            $this->getNotificationManager()->update($notification);
            $this->getNotificationHandler()->handlePushNewNotification($notification);
            /** @var UserInterface $member */
            foreach ($family->getMembers() as $member) {
                /** @var NotificationInterface $notification */
                $notification = $this->getNotificationManager()->create();
                $notification->setUser($member);
                $notification->setType('user_family_new_master');
                $message = $this->getTranslator()->trans('user.family.new_master', array(
                    '%name%' => $user->__toString()), 'notification');
                $notification->setMessage($message);
                $this->getNotificationManager()->update($notification);
                $this->getNotificationHandler()->handlePushNewNotification($notification);
            }
            return $this->createView($family, Codes::HTTP_ACCEPTED, array('basic', 'full_family'));
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Leave the Family",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_NOT_ACCEPTABLE="Returned when the user is master"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return View
     */
    public function leaveAction()
    {
        $family = $this->getUser()->getFamily();
        if (!$family) {
            throw $this->createNotFoundException();
        }
        if ($family->getMaster() == $this->getUser()->getId()) {
            return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
        }
        $family->removeMember($this->getUser());
        $this->getFamilyManager()->update($family);
        /** @var UserInterface $member */
        foreach ($family->getMembers() as $member) {
            /** @var NotificationInterface $notification */
            $notification = $this->getNotificationManager()->create();
            $notification->setUser($member);
            $notification->setType('user_family_leave');
            $message = $this->getTranslator()->trans('user.family.leave', array(
                '%name%' => $this->getUser()->__toString()), 'notification');
            $notification->setMessage($message);
            $this->getNotificationManager()->update($notification);
            $this->getNotificationHandler()->handlePushNewNotification($notification);
        }
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete family.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful"
     *  }
     * )
     *
     * @Rest\Delete
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return View
     */
    public function deleteAction()
    {
        $family = $this->getUser()->getFamily();
        if (!$family) {
            throw $this->createNotFoundException();
        }
        if ($this->getUser()->getId() != $family->getMaster()) {
            throw $this->createAccessDeniedException();
        }
        /** @var UserInterface $member */
        foreach ($family->getMembers() as $member) {
            /** @var NotificationInterface $notification */
            $notification = $this->getNotificationManager()->create();
            $notification->setUser($member);
            $notification->setType('user_family_delete');
            $message = $this->getTranslator()->trans('user.family.delete', array(
                '%name%' => $this->getUser()->__toString()), 'notification');
            $notification->setMessage($message);
            $this->getNotificationManager()->update($notification);
            $this->getNotificationHandler()->handlePushNewNotification($notification);
        }
        $this->getFamilyManager()->delete($family);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Invite user.",
     *  output={
     *      "class"="EO\UserBundle\Model\Invite",
     *      "groups"={"basic", "full_invite"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_NOT_ACCEPTABLE="Returned when no user was found by the given email",
     *      Codes::HTTP_ACCEPTED="Returned when successful"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $email
     *
     * @return View
     */
    public function inviteAction($email)
    {
        $family = $this->getUser()->getFamily();
        if (!$family) {
            throw $this->createNotFoundException();
        }
        if ($this->getUser()->getId() != $family->getMaster()) {
            throw $this->createAccessDeniedException();
        }
        /** @var UserInterface $user */
        $user = $this->getUserManager()->getFOSUserManager()->findUserByEmail($email);
        if ($user !== null) {
            /** @var InviteInterface $invite */
            $invite = $this->getInviteManager()->findOneByByAndWho($this->getUser(), $user);
            if (null !== $invite) {
                return $this->createView($invite, Codes::HTTP_ALREADY_REPORTED, array('basic', 'full_invite'));
            }
            $invite = $this->getInviteManager()->create();
            $invite->setWho($user);
            $invite->setBy($this->getUser());
            $invite->setFamily($family);
            $invite->setAt(new \DateTime("now"));
            $this->getInviteManager()->update($invite);
            /** @var NotificationInterface $notification */
            $notification = $this->getNotificationManager()->create();
            $notification->setUser($user);
            $notification->setType('user_family_invite');
            $message = $this->getTranslator()->trans('user.family.invite', array(
                '%name%' => $this->getUser()->__toString()), 'notification');
            $notification->setMessage($message);
            $this->getNotificationManager()->update($notification);
            $this->getNotificationHandler()->handlePushNewNotification($notification);
            return $this->createView($invite, Codes::HTTP_ACCEPTED, array('basic', 'full_invite'));
        }
        return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Exclude user.",
     *  statusCodes={
     *      Codes::HTTP_NOT_ACCEPTABLE="Returned when no user was found in the family by the given id",
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the user wants to exclude him self"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $userId
     *
     * @return View
     */
    public function excludeAction($userId)
    {
        $family = $this->getUser()->getFamily();
        if (!$family) {
            throw $this->createNotFoundException();
        }
        if ($this->getUser()->getId() != $family->getMaster()) {
            throw $this->createAccessDeniedException();
        }
        if ($this->getUser()->getId() == $userId) {
            return $this->createView(null, Codes::HTTP_BAD_REQUEST);
        }
        $user = $this->getUserManager()->findOneByIdInFamily($userId, $family);
        if ($user !== null) {
            $family->removeMember($user);
            $this->getFamilyManager()->update($family);
            /** @var NotificationInterface $notification */
            $notification = $this->getNotificationManager()->create();
            $notification->setUser($user);
            $notification->setType('user_family_exclude');
            $message = $this->getTranslator()->trans('user.family.exclude', array(), 'notification');
            $notification->setMessage($message);
            $this->getNotificationManager()->update($notification);
            $this->getNotificationHandler()->handlePushNewNotification($notification);
            return $this->createView(null, Codes::HTTP_ACCEPTED);
        }
        return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Accept invite.",
     *  statusCodes={
     *      Codes::HTTP_NOT_ACCEPTABLE="Returned when the user has already a family",
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $inviteId
     *
     * @return View
     */
    public function acceptAction($inviteId)
    {
        /** @var InviteInterface $invite */
        $invite = $this->getInviteManager()->find($inviteId);
        if (!$invite) {
            throw $this->createNotFoundException();
        }
        if ($invite->getWho()->getId() != $this->getUser()->getId()) {
            throw $this->createAccessDeniedException();
        }
        if ($this->getUser()->getFamily() === null) {
            $family = $invite->getFamily();
            $family->addMember($this->getUser());
            $this->getFamilyManager()->update($family);
            $this->getInviteManager()->delete($invite);
            foreach ($family->getMembers() as $member) {
                /** @var NotificationInterface $notification */
                $notification = $this->getNotificationManager()->create();
                $notification->setUser($member);
                $notification->setType('user_family_accept');
                $message = $this->getTranslator()->trans('user.family.new_master', array(
                    '%name%' => $this->getUser()->__toString()), 'notification');
                $notification->setMessage($message);
                $this->getNotificationManager()->update($notification);
                $this->getNotificationHandler()->handlePushNewNotification($notification);
            }
            return $this->createView(null, Codes::HTTP_ACCEPTED);
        }
        return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Decline invite.",
     *  statusCodes={
     *      Codes::HTTP_UNAUTHORIZED="Returned when the invite is not for the user",
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $inviteId
     *
     * @return View
     */
    public function declineAction($inviteId)
    {
        /** @var InviteInterface $invite */
        $invite = $this->getInviteManager()->find($inviteId);
        if (!$invite) {
            throw $this->createNotFoundException();
        }
        if ($invite->getWho()->getId() != $this->getUser()->getId()) {
            throw $this->createAccessDeniedException();
        }
        $this->getInviteManager()->delete($invite);
        return $this->createView(null, Codes::HTTP_ACCEPTED);
    }

    /**
     * @return \EO\UserBundle\Manager\DBAL\FamilyManagerInterface
     */
    private function getFamilyManager()
    {
        return $this->get('eo_user.manager.family');
    }

    /**
     * @return \EO\UserBundle\Manager\DBAL\UserManagerInterface
     */
    private function getUserManager()
    {
        return $this->get('eo_user.manager.user');
    }

    /**
     * @return \EO\UserBundle\Manager\DBAL\InviteManagerInterface
     */
    private function getInviteManager()
    {
        return $this->get('eo_user.manager.invite');
    }

    /**
     * @return \EO\MiscBundle\Manager\DBAL\NotificationManagerInterface
     */
    private function getNotificationManager()
    {
        return $this->get('eo_misc.manager.notification');
    }

    /**
     * @return \EO\MiscBundle\Handler\NotificationHandlerInterface
     */
    private function getNotificationHandler()
    {
        return $this->get('eo_misc.handler.notification');
    }
}