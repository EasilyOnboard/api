<?php

namespace EO\UserBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\PaymentBundle\Model\Card;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class CardController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All cards",
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful",
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return View
     */
    public function listAction()
    {
        if (null === $this->getStripeHandler()->customer()->get($this->getUser())) {
            if (null === $this->getStripeHandler()->customer()->create($this->getUser())) {
                return $this->createView(array(
                    'error' => "Customer couldn't be registered"
                ), Codes::HTTP_NOT_ACCEPTABLE);
            }
        }
        if (($cards = $this->getStripeHandler()->customer()->getCards($this->getUser())) !== null) {
            return $this->createView(array('cards' => $cards->jsonSerialize()['data']), Codes::HTTP_OK);
        }
        return $this->createView(array('error' => "Couldn't retrieve cards"), Codes::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Register card.",
     *  input="eo_payment_card",
     *  statusCodes={
     *      Codes::HTTP_CREATED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        $card = new Card();
        $form = $this->createRestForm('eo_payment_card', $card);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            if (null === $this->getStripeHandler()->customer()->get($this->getUser())) {
                if (null === $this->getStripeHandler()->customer()->create($this->getUser())) {
                    return $this->createView(array(
                        'error' => "Customer couldn't be registered"
                    ), Codes::HTTP_NOT_ACCEPTABLE);
                }
            }
            if (($card = $this->getStripeHandler()->customer()->addCard($this->getUser(), $card)) !== null) {
                return $this->createView($card->jsonSerialize(), Codes::HTTP_CREATED);
            }
            return $this->createView(array(
                'error' => "Card couldn't be registered"
            ), Codes::HTTP_NOT_ACCEPTABLE);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Update card.",
     *  input="eo_payment_card_info",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Put
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param Request $request
     * @param string $cardId
     *
     * @return View
     */
    public function putAction(Request $request, $cardId)
    {
        $card = new Card();
        $form = $this->createRestForm('eo_payment_card_info', $card);
        $form->submit($request->request->all());
        if ($form->isValid()) {
            if (($card = $this->getStripeHandler()->customer()->updateCard($this->getUser(), $card, $cardId)) !== null) {
                return $this->createView($card->jsonSerialize(), Codes::HTTP_CREATED);
            }
            return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Set default card.",
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $cardId
     *
     * @return View
     */
    public function patchAction($cardId)
    {
        if ($this->getStripeHandler()->customer()->setDefaultCard($this->getUser(), $cardId)) {
            return $this->createView(null, Codes::HTTP_ACCEPTED);
        }
        return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Delete card.",
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when is successfully deleted",
     *      Codes::HTTP_NOT_ACCEPTABLE="Returned when an error has occurred"
     *  }
     * )
     *
     * @Rest\Delete
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $cardId
     *
     * @return View
     */
    public function deleteAction($cardId)
    {
        if ($this->getStripeHandler()->customer()->removeCard($this->getUser(), $cardId)) {
            return $this->createView(null, Codes::HTTP_ACCEPTED);
        }
        return $this->createView(null, Codes::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @return \EO\PaymentBundle\Handler\PaymentHandler
     */
    private function getStripeHandler()
    {
        return $this->get('eo_payment.handler');
    }
}