<?php

namespace EO\UserBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\MiscBundle\Model\Address;
use EO\UserBundle\Form\Model\Login;
use EO\UserBundle\Form\Model\Refresh;
use EO\UserBundle\Handler\DeviceHandlerInterface;
use EO\UserBundle\Handler\LoginHandlerInterface;
use EO\UserBundle\Manager\DBAL\UserManagerInterface;
use EO\UserBundle\Model\UserInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends EOController
{
    /**
     * @ApiDoc(
     *  description="Login.",
     *  input="eo_login",
     *  output={
     *      "class"="EO\UserBundle\Model\Token"
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when login success",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @param Request $request
     *
     * @return View
     */
    public function loginAction(Request $request)
    {
        $login = new Login();
        $form = $this->createRestForm('eo_login', $login, 'POST');
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            return $this->createView($form, Codes::HTTP_BAD_REQUEST);
        }
        return $this->createView($this->getLoginHandler()->handleLogin($login), Codes::HTTP_ACCEPTED);
    }

    /**
     * @ApiDoc(
     *  description="Logout.",
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when logout success",
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @return View
     */
    public function logoutAction()
    {
        $this->getLoginHandler()->handleLogout($this->getUser());
        return $this->createView(null, Codes::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  description="Refresh.",
     *  input="eo_refresh",
     *  output={
     *      "class"="EO\UserBundle\Model\Token"
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when refresh success",
     *  }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return View
     */
    public function refreshAction(Request $request)
    {
        $refresh = new Refresh();
        $form = $this->createRestForm('eo_refresh', $refresh, 'POST');
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            return $this->createView($form, Codes::HTTP_BAD_REQUEST);
        }
        return $this->createView($this->getLoginHandler()->handleRefresh($refresh), Codes::HTTP_ACCEPTED);
    }

    /**
     * @ApiDoc(
     *  description="Register.",
     *  input="eo_user_password",
     *  statusCodes={
     *      200="Returned when successful",
     *      400="Returned when the form is invalid"
     *  }
     * )
     *
     * @Rest\Post
     *
     * @param Request $request
     *
     * @return View
     */
    public function registerAction(Request $request)
    {
        /** @var UserInterface $user */
        $user = $this->getUserManager()->create();
        $form = $this->createRestForm('eo_user_password', $user, 'POST');
        $form->submit($request->request->all());
        if ($form->isValid()) {
            $user->setFullAddress(new Address());
            $user->setEnabled(true);
            $user->addRole('ROLE_CUSTOMER');
            $this->getUserManager()->getFOSUserManager()->updateUser($user);
            return $this->createView(null, Codes::HTTP_CREATED);
        }
        return $this->createView($form, Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @ApiDoc(
     *  description="Validate to unblock a device",
     *  statusCodes={
     *         Codes::HTTP_ACCEPTED="Returned when successful",
     *         Codes::HTTP_BAD_REQUEST="Returned when time out",
     *     }
     * )
     *
     * @Rest\Get
     *
     * @param string $token
     *
     * @return View
     */
    public function validateAction($token)
    {
        if ($this->getDeviceHandler()->validateToken($token) !== null) {
            return $this->createView(array("success" => true), Codes::HTTP_ACCEPTED);
        }
        return $this->createView(array("success" => false), Codes::HTTP_BAD_REQUEST);
    }

    /**
     * @return DeviceHandlerInterface
     */
    public function getDeviceHandler()
    {
        return $this->get('eo_user.handler.device');
    }

    /**
     * @return LoginHandlerInterface
     */
    private function getLoginHandler()
    {
        return $this->get('eo_user.handler.login');
    }

    /**
     * @return UserManagerInterface
     */
    private function getUserManager()
    {
        return $this->get('eo_user.manager.user');
    }
}