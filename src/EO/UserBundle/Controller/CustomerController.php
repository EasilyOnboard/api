<?php

namespace EO\UserBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\UserBundle\Manager\DBAL\UserManagerInterface;
use EO\UserBundle\Model\UserInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class CustomerController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All Customers.",
     *  output={
     *      "class"="array<EO\UserBundle\Model\User>",
     *      "groups"={"basic", "user", "back"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER_LIST')")
     *
     * @return View
     */
    public function allAction()
    {
        $customers = $this->getUserManager()->findAllCustomers();
        return $this->createView(array('customers' => $customers), Codes::HTTP_OK, array('basic', 'user', 'back'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Get customer.",
     *  output={
     *      "class"="EO\UserBundle\Model\User",
     *      "groups"={"basic", "user", "back", "customer"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER_GET')")
     *
     * @param string $customerId
     *
     * @return View
     */
    public function getAction($customerId)
    {
        /** @var UserInterface $customer */
        $customer = $this->getUserManager()->findCustomer($customerId);
        if (!$customer) {
            throw $this->createNotFoundException();
        }
        return $this->createView($customer, Codes::HTTP_OK, array('basic', 'user', 'back', 'customer'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="Search customer.",
     *  output={
     *      "class"="EO\UserBundle\Model\User",
     *      "groups"={"basic"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_OK="Returned when successful"
     *  }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_INSPECTOR')")
     *
     * @param string $arg
     *
     * @return View
     */
    public function searchAction($arg)
    {
        $customers = $this->getUserManager()->findAllCustomerLike($arg);
        return $this->createView(array('customers' => $customers), Codes::HTTP_OK, array('basic'));
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="(Un)Block a customer.",
     *  output={
     *      "class"="EO\UserBundle\Model\User",
     *      "groups"={"basic", "user", "back", "customer"}
     *  },
     *  statusCodes={
     *      Codes::HTTP_ACCEPTED="Returned when successful",
     *      Codes::HTTP_BAD_REQUEST="Returned when the form is invalid",
     *  }
     * )
     *
     * @Rest\Patch
     *
     * @Security("has_role('ROLE_CUSTOMER_ACTION')")
     *
     * @param string $customerId
     *
     * @return View
     */
    public function patchAction($customerId)
    {
        /** @var UserInterface $customer */
        $customer = $this->getUserManager()->findCustomer($customerId);
        if (!$customer) {
            throw $this->createNotFoundException();
        }
        $customer->setLocked($customer->isAccountNonLocked());
        $this->getUserManager()->update($customer);
        return $this->createView($customer, Codes::HTTP_ACCEPTED, array('basic', 'user', 'back', 'customer'));
    }

    /**
     * @return UserManagerInterface
     */
    private function getUserManager()
    {
        return $this->get('eo_user.manager.user');
    }
}
