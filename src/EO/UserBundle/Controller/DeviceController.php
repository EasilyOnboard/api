<?php

namespace EO\UserBundle\Controller;

use EO\CoreBundle\Controller\EOController;
use EO\UserBundle\Model\DeviceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DeviceController extends EOController
{
    /**
     * @ApiDoc(
     *  resource=true,
     *  description="All devices",
     *  output={
     *      "class"="array<EO\UserBundle\Model\Device>"
     *  },
     *  statusCodes={
     *         Codes::HTTP_OK="Returned when successful",
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @return View
     */
    public function allAction()
    {
        $devices = $this->getUser()->getDevices();
        return $this->createView(array('devices' => $devices), Codes::HTTP_OK);
    }

    /**
     * @ApiDoc(
     *  resource=true,
     *  description="(Un)Block device",
     *  output={
     *      "class"="EO\UserBundle\Model\Device"
     *  },
     *  statusCodes={
     *       Codes::HTTP_OK="Returned when successful"
     *     }
     * )
     *
     * @Rest\Get
     *
     * @Security("has_role('ROLE_CUSTOMER')")
     *
     * @param string $deviceId
     *
     * @return View
     */
    public function patchAction($deviceId)
    {
        /** @var DeviceInterface $device */
        $device = $this->getDeviceManager()->find($deviceId);
        if (!$device) {
            throw $this->createNotFoundException();
        }
        if ($device->getCustomer() !== $this->getUser()) {
            throw $this->createAccessDeniedException();
        }
        if ($device->getBlocked()) {
            $this->getDeviceHandler()->unblockDevice($device);
        } else {
            $this->getDeviceHandler()->blockDevice($device);
        }
        return $this->createView($device, Codes::HTTP_OK);
    }

    /**
     * @return \EO\UserBundle\Manager\DBAL\DeviceManagerInterface
     */
    private function getDeviceManager()
    {
        return $this->get('eo_user.manager.device');
    }

    /**
     * @return \EO\UserBundle\Handler\DeviceHandler
     */
    private function getDeviceHandler()
    {
        return $this->get('eo_user.handler.device');
    }
}